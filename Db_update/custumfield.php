/**
 *  Install Add-ons
 *  
 *  The following code will include all 4 premium Add-Ons in your theme.
 *  Please do not attempt to include a file which does not exist. This will produce an error.
 *  
 *  All fields must be included during the 'acf/register_fields' action.
 *  Other types of Add-ons (like the options page) can be included outside of this action.
 *  
 *  The following code assumes you have a folder 'add-ons' inside your theme.
 *
 *  IMPORTANT
 *  Add-ons may be included in a premium theme as outlined in the terms and conditions.
 *  However, they are NOT to be included in a premium / free plugin.
 *  For more information, please read http://www.advancedcustomfields.com/terms-conditions/
 */ 

// Fields 
add_action('acf/register_fields', 'my_register_fields');

function my_register_fields()
{
	//include_once('add-ons/acf-repeater/repeater.php');
	//include_once('add-ons/acf-gallery/gallery.php');
	//include_once('add-ons/acf-flexible-content/flexible-content.php');
}

// Options Page 
//include_once( 'add-ons/acf-options-page/acf-options-page.php' );


/**
 *  Register Field Groups
 *
 *  The register_field_group function accepts 1 array which holds the relevant data to register a field group
 *  You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_home-page-learn-more',
		'title' => 'Home Page Learn More',
		'fields' => array (
			array (
				'key' => 'field_51e95ebf0a8d4',
				'label' => 'Learn More',
				'name' => 'learn_more',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'enjoy_23_105',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'splsavings_23_105',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'dontmissout_23_105',
					'order_no' => 0,
					'group_no' => 2,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_video-news',
		'title' => 'Video News',
		'fields' => array (
			array (
				'key' => 'field_51dabb0129741',
				'label' => 'Video Upload',
				'name' => 'video_upload',
				'type' => 'file',
				'save_format' => 'url',
			),
			array (
				'key' => 'field_51dabb321c4aa',
				'label' => 'Embeded Video Url',
				'name' => 'embeded_video_url',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_51e104aed768d',
				'label' => 'News Title',
				'name' => 'news_title',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'basic',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_51efb727ee617',
				'label' => 'PDF file path',
				'name' => 'pdf_file_path',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_51efb73dee618',
				'label' => 'External url',
				'name' => 'external_url',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'news',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'inthenews',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_cottage-photo',
		'title' => 'Cottage Photo',
		'fields' => array (
			array (
				'key' => 'field_51d7ddd336c0e',
				'label' => 'Cottage Photo Image',
				'name' => 'cottage_photo_image',
				'type' => 'image',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'menutab',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_menus',
		'title' => 'Menus',
		'fields' => array (
			array (
				'key' => 'field_51d678b44547b',
				'label' => 'Rate',
				'name' => 'rate',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'breakfastlunch_17_68',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'dinner_17_68',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'desserts_17_68',
					'order_no' => 0,
					'group_no' => 2,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'libations_17_68',
					'order_no' => 0,
					'group_no' => 3,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'breakfastlunch_18_53',
					'order_no' => 0,
					'group_no' => 4,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'dinner_18_53',
					'order_no' => 0,
					'group_no' => 5,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'desserts_18_53',
					'order_no' => 0,
					'group_no' => 6,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'libations_18_53',
					'order_no' => 0,
					'group_no' => 7,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'breakfastlunch_19_58',
					'order_no' => 0,
					'group_no' => 8,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'dinner_19_58',
					'order_no' => 0,
					'group_no' => 9,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'desserts_19_58',
					'order_no' => 0,
					'group_no' => 10,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'libations_19_58',
					'order_no' => 0,
					'group_no' => 11,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'breakfastlunch_16_78',
					'order_no' => 0,
					'group_no' => 12,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'dinner_16_78',
					'order_no' => 0,
					'group_no' => 13,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'desserts_16_78',
					'order_no' => 0,
					'group_no' => 14,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'libations_16_78',
					'order_no' => 0,
					'group_no' => 15,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 16,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 17,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 18,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 19,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'congress-hall_9_104',
					'order_no' => 0,
					'group_no' => 20,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'virginia-hotel_9_104',
					'order_no' => 0,
					'group_no' => 21,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'beach-shack_9_104',
					'order_no' => 0,
					'group_no' => 22,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_wedding-events',
		'title' => 'Wedding events',
		'fields' => array (
			array (
				'key' => 'field_51c7ec0411500',
				'label' => 'Wedding Specialist',
				'name' => 'wedding_specialist',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'packages_1_8',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'specials_1_8',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'packages_2_26',
					'order_no' => 0,
					'group_no' => 2,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'packages_4_18',
					'order_no' => 0,
					'group_no' => 3,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_capacity-chart',
		'title' => 'Capacity chart',
		'fields' => array (
			array (
				'key' => 'field_51c31d5401cd8',
				'label' => 'View capacity chart label',
				'name' => 'capacitychartlabel',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_51c31d8301cd9',
				'label' => 'capacity chart image',
				'name' => 'capacity_chart_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
			),
			array (
				'key' => 'field_51c31db001cda',
				'label' => 'Meeting specialist',
				'name' => 'meeting_specialist',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'facilities_1_9',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'facilities_2_27',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'packages_2_27',
					'order_no' => 0,
					'group_no' => 2,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'packages_1_9',
					'order_no' => 0,
					'group_no' => 3,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_menu-tabs',
		'title' => 'Menu Tabs',
		'fields' => array (
			array (
				'key' => 'field_51baceb364f7e',
				'label' => 'Sub Header',
				'name' => 'sub_header',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_51dbace1b85d7',
				'label' => 'Go to Website',
				'name' => 'go_to_website',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'menutab',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_page-property',
		'title' => 'Page property',
		'fields' => array (
			array (
				'key' => 'field_51b872f41d36b',
				'label' => 'Property',
				'name' => 'pageproperties',
				'type' => 'select',
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					'' => 'SELECT PROPERTY',
					1 => 'CONGRESS HALL',
					2 => 'THE VIRGINIA',
					3 => 'THE COTTAGES',
					4 => 'BEACH SHACK',
					5 => 'THE STAR',
					6 => 'SANDPIPER BEACH CLUB',
					7 => 'BARONS COVE SAG HARBOR SPRING 2014',
					8 => 'SEA SPA',
					9 => 'BEACH SERVICE',
					10 => 'TOMMY’S FOLLY',
					11 => 'BEACH PLUM FARM',
					12 => 'WEST END GARAGE',
					13 => 'SEASONAL EXPERIENCES',
					14 => 'SHOPPING',
					15 => 'THE BLUE PIG TAVERN',
					16 => 'THE EBBITT ROOM',
					17 => 'THE RUSTY NAIL',
					18 => 'THE BOILER ROOM',
					19 => 'THE BROWN ROOM',
					20 => 'VERANDA BAR',
					21 => 'TOMMY’S FOLLY COFFEE SHOP',
					22 => 'THE STAR COFFEE SHOP',
					23 => 'Home Page',
					24 => 'Cape May Overview',
				),
				'default_value' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_parent',
					'operator' => '==',
					'value' => '17',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'page_parent',
					'operator' => '==',
					'value' => '449',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
			array (
				array (
					'param' => 'page_parent',
					'operator' => '==',
					'value' => '569',
					'order_no' => 0,
					'group_no' => 2,
				),
			),
			array (
				array (
					'param' => 'page_parent',
					'operator' => '==',
					'value' => '689',
					'order_no' => 0,
					'group_no' => 3,
				),
			),
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => '1691',
					'order_no' => 0,
					'group_no' => 4,
				),
			),
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => '2104',
					'order_no' => 0,
					'group_no' => 5,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_properties',
		'title' => 'Properties',
		'fields' => array (
			array (
				'key' => 'field_51a3132101117',
				'label' => 'Property',
				'name' => 'property',
				'type' => 'select',
				'multiple' => 1,
				'allow_null' => 0,
				'choices' => array (
					'' => 'SELECT PROPERTY',
					1 => 'CONGRESS HALL',
					2 => 'THE VIRGINIA',
					3 => 'THE COTTAGES',
					4 => 'BEACH SHACK',
					5 => 'THE STAR',
					6 => 'SANDPIPER BEACH CLUB',
					7 => 'BARONS COVE SAG HARBOR SPRING 2014',
					8 => 'SEA SPA',
					9 => 'BEACH SERVICE',
					10 => 'TOMMY’S FOLLY',
					11 => 'BEACH PLUM FARM',
					12 => 'WEST END GARAGE',
					13 => 'SEASONAL EXPERIENCES',
					14 => 'SHOPPING',
					15 => 'THE BLUE PIG TAVERN',
					16 => 'THE EBBITT ROOM',
					17 => 'THE RUSTY NAIL',
					18 => 'THE BOILER ROOM',
					19 => 'THE BROWN ROOM',
					20 => 'VERANDA BAR',
					21 => 'TOMMY’S FOLLY COFFEE SHOP',
					22 => 'THE STAR COFFEE SHOP',
					23 => 'Home Page',
					24 => 'Cape May Overview',
				),
				'default_value' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'activities',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'seasonalevents',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'specialoffers',
					'order_no' => 0,
					'group_no' => 2,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'entertainment',
					'order_no' => 0,
					'group_no' => 3,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'ps_promotion',
					'order_no' => 0,
					'group_no' => 4,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_concierge',
		'title' => 'Concierge',
		'fields' => array (
			array (
				'key' => 'field_5193935328d63',
				'label' => 'Subtitle1',
				'name' => 'subtitle1',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_5193937328d64',
				'label' => 'Subtitle 2',
				'name' => 'subtitle_2',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_5193938b28d65',
				'label' => 'Subtitle 3',
				'name' => 'subtitle_3',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'menutab',
					'order_no' => 1,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'ps_promotion',
					'order_no' => 2,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'news',
					'order_no' => 3,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'our-hotels_23_105',
					'order_no' => 4,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'enjoy_23_105',
					'order_no' => 5,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'splsavings_23_105',
					'order_no' => 6,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'dontmissout_23_105',
					'order_no' => 7,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'inthenews',
					'order_no' => 8,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_buttons',
		'title' => 'Buttons',
		'fields' => array (
			array (
				'key' => 'field_51bc1ac4a928f',
				'label' => 'Button 1',
				'name' => 'button_1',
				'type' => 'text',
				'instructions' => 'Instruction to create buttons : Button Text,website url . for eg: Book Now,http://caperesorts.com/staging',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_51bc1ad0a9290',
				'label' => 'Button 2',
				'name' => 'button_2',
				'type' => 'text',
				'instructions' => 'Instruction to create buttons : Button Text,website url . for eg: Book Now,http://caperesorts.com/staging',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_51bc1ad7a9291',
				'label' => 'Button 3',
				'name' => 'button_3',
				'type' => 'text',
				'instructions' => 'Instruction to create buttons : Button Text,website url . for eg: Book Now,http://caperesorts.com/staging',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_51bc1ae0a9292',
				'label' => 'Button 4',
				'name' => 'button_4',
				'type' => 'text',
				'instructions' => 'Instruction to create buttons : Button Text,website url . for eg: Book Now,http://caperesorts.com/staging',
				'default_value' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'page',
					'order_no' => 1,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'menutab',
					'order_no' => 2,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'ps_promotion',
					'order_no' => 3,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'facilities_1_9',
					'order_no' => 4,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'post',
					'order_no' => 5,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'news',
					'order_no' => 6,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'our-hotels_23_105',
					'order_no' => 7,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'enjoy_23_105',
					'order_no' => 8,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'splsavings_23_105',
					'order_no' => 9,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'dontmissout_23_105',
					'order_no' => 10,
					'group_no' => 0,
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'inthenews',
					'order_no' => 11,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 4,
	));
}

