<?php
	/*
	Template Name: Concierge-calender
	*/
?>
<?php get_header(); 
?>
<!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pub=at_com"></script>

<div class="addthis_toolbox addthis_default_style "
        addthis:url="http://example.com"
        addthis:title="An Example Title"
        addthis:description="An Example Description"> 
<a href="http://www.addthis.com/bookmark.php?v=250&pubid=ra-51a85d1612218d0e" class="addthis_button_compact">Share</a> 
 
  </div> 
<div id='shareThisShareEgg' class='shareEgg'></div>
<script type="text/javascript">stLight.options({publisher: "c058d568-dc16-43ad-937b-402f7cdf9598", doNotHash: false, doNotCopy: false, hashAddressBar: false, onhover:false});</script>
<link media="screen" type="text/css" rel="stylesheet" href="http://w.sharethis.com/gallery/shareegg/shareegg.css"></link>
<script type='text/javascript'>stlib.shareEgg.createEgg('shareThisShareEgg', ['facebook','twitter','linkedin','googleplus','pinterest','email'], {title:'ShareThis Rocks!!!',url:'http://www.sharethis.com',theme:'shareegg'});</script> -->


<?php

$dayname =$_GET['dn'];
$month =$_GET['mo'];
$day =$_GET['d'];
$year =$_GET['y'];
if($month) {
$monthName = date("F", mktime(0, 0, 0, $month, 10));
}
?>

<section>
 <img src="<?php echo child_template_directory; ?>/img/con-images/concierge.png" class="con-logo" alt="Cape resorts Concierge"/>
<!-- concierge Left Side bar Starts -->
		<?php get_sidebar('concierge'); ?>

<div class="content calender">
<h2 class="heading"><span class="parent_cat">Calendar:&nbsp;</span><span class="child_cat">

<?php 
$event_ids=array();
if($month || $dayname || $day || $year ) {
$con = new Concierge();
	$event_ids = $con->get_event_byDate($day, $month, $year);

echo "$dayname, $monthName $day,  $year"; 
}
else {
	echo "ALL";


	$events = $wpdb->get_results("select post_id FROM cr_em_events where event_status=1 AND STR_TO_DATE( `event_end_date`, '%Y-%m-%d%' ) >= DATE( NOW( ) )");

foreach($events as $event) {

$event_ids[] = $event->post_id;
}
}
?>

</span></h2>

<?php 

foreach($event_ids as $event_id) {
	$current = get_post($event_id);
	if($current->post_status  == "publish"){
 $image_id = get_post_thumbnail_id($event_id);
 $image_url = wp_get_attachment_image_src($image_id);
?>

<div class="calen">
  <img src="<?php echo $image_url[0]; ?>" title="<?php echo get_the_title($event_id); ?>" alt="<?php echo get_the_title($event_id); ?>"/>
  <div class="act-content">
    <h3><a href="<?php get_permalink($event_id); ?>" class="title_tag" data-id="<?php echo $event_id; ?>"><?php echo get_the_title($event_id); ?></a></h3>
    <h4><?php the_field('subtitle1', $event_id); ?>,&nbsp;<i><?php the_field('subtitle_2', $event_id); ?></i></h4>
    <span><?php the_field('subtitle_3', $event_id); ?></span>
    <p><?php echo wp_trim_words( get_the_content($event_id), 18 ) ?><a href="<?php echo get_permalink($event_id); ?>" data-id="<?php echo $event_id; ?>" class="caln_learn">Learn More</a></p>
  </div>
</div>

<?php
}
}

?>

</div>
</section>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
<?php get_footer(); ?>
