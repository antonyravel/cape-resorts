<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Twelve child
 * @since Twenty Twelve child 1.0
 */
?>

 <div class="details-banner">

 <?php
 $image_id = get_post_thumbnail_id();
 	 $image_url = wp_get_attachment_image_src($image_id,'med_700_309', true);
?>

   <img src="<?php echo $image_url[0]; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?> " />

  </div>
  <div class="details-content act-content">
  <h3><?php echo the_title(); ?></h3>
  <h4><?php the_field('subtitle1'); ?></h4>
  <i><?php the_field('subtitle_2'); ?></i>
  <span><?php the_field('subtitle_3'); ?></span>
  <?php the_content(); ?>
  
  </div>

