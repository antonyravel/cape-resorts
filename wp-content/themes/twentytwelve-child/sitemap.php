<?php
	/*
	Template Name: XML Site Map
	*/
?>

<?php 


/* XML Coding */
global $wpdb;
$con = new Concierge();

 $sitemap = '<?xml version="1.0" encoding="UTF-8"?>';
 $sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

 $rormap = '<?xml version="1.0" encoding="UTF-8"?>';
 $rormap .= '<rss version="2.0" xmlns:ror="http://rorweb.com/0.1/" >';
 $rormap .= '<channel><title>ROR Sitemap for http://caperesorts.com/</title><link>http://caperesorts.com/</link>';


$rormap .= '<item>
     <link>'.site_url().'</link>
     <title>'.get_bloginfo('name').'</title>
     <description>'.get_bloginfo('description').'</description>
     <ror:updatePeriod>monthly</ror:updatePeriod>
     <ror:resourceOf>sitemap</ror:resourceOf>
	</item>';



$args['post_type']= 'page';
$args['posts_per_page']=-1;
$args['post_status']='publish';
$all_pages = get_posts($args);

$sitemap .= '<url><loc>http://caperesorts.com/</loc></url>';






  foreach($all_pages as $post) {
	if($post->ID != 2654) {
    $postdate = explode(" ", $post->post_modified);

    $sitemap .= '<url>'.
      '<loc>'. get_permalink($post->ID) .'</loc>'.
      '<lastmod>'. $postdate[0] .'</lastmod>'.
      '<changefreq>monthly</changefreq>'.
    '</url>';

$rormap .= '<item>
    <link>'.get_permalink($post->ID).'</link>';

	$title = $con->con_page_title($post->ID);
	$descr = $con->con_page_desc($post->ID);
    $rormap .='<title>'.$title.'</title>';
     $rormap .='<description>'.$descr.'</description>
     <ror:updatePeriod>monthly</ror:updatePeriod>
     <ror:resourceOf>sitemap</ror:resourceOf>
	</item>';

	}
  }
wp_reset_query();








$cat1 = get_categories('taxonomy=seasonalevents_category&type=seasonalevents'); 


  foreach($cat1 as $cat) {

    $sitemap .= '<url>'.
     '<loc>'. site_url().'/concierge/capemay/seasonalevents/?cat='.$cat->slug.'</loc>'.
      '<changefreq>monthly</changefreq>'.
    '</url>';


$rormap .= '<item>
    <link>'. site_url().'/concierge/capemay/seasonalevents/?cat='.$cat->slug.'</link>';

    $rormap .='<title>'.$cat->name.'</title>';
	if($cat->description) {
		$desc = $cat->description;
	}else {

		$desc .= 'Cape resorts Seasonal events - '.$cat->name; 
	}

    $rormap .='<description>'.$desc.'</description>
     <ror:updatePeriod>monthly</ror:updatePeriod>
     <ror:resourceOf>sitemap</ror:resourceOf>
	</item>';

  }


$cat2 = get_categories('taxonomy=activity_category&type=activities'); 


  foreach($cat2 as $cat) {

    $sitemap .= '<url>'.
     '<loc>'. site_url().'/concierge/capemay/activities/?cat='.$cat->slug.'</loc>'.
      '<changefreq>monthly</changefreq>'.
    '</url>';


$rormap .= '<item>
    <link>'. site_url().'/concierge/capemay/activities/?cat='.$cat->slug.'</link>';

    $rormap .='<title>'.$cat->slug.'</title>';
	if($cat->description) {
		$desc = $cat->description;
	}else {

		$desc .= 'Cape Resorts Activities - '.$cat->name; 
	}

    $rormap .='<description>'.$desc.'</description>
     <ror:updatePeriod>monthly</ror:updatePeriod>
     <ror:resourceOf>sitemap</ror:resourceOf>
	</item>';

  }

$cat3 = get_categories('taxonomy=entertainment_category&type=entertainment'); 


  foreach($cat3 as $cat) {

    $sitemap .= '<url>'.
     '<loc>'. site_url().'/concierge/capemay/entertainment/?cat='.$cat->slug.'</loc>'.
      '<changefreq>monthly</changefreq>'.
    '</url>';

$rormap .= '<item>
    <link>'. site_url().'/concierge/capemay/entertainment/?cat='.$cat->slug.'</link>';

    $rormap .='<title>'.$cat->name.'</title>';
	if($cat->description) {
		$desc = $cat->description;
	}else {

		$desc .= 'Cape Resorts Entertainment - '.$cat->name; 
	}

    $rormap .='<description>'.$desc.'</description>
     <ror:updatePeriod>monthly</ror:updatePeriod>
     <ror:resourceOf>sitemap</ror:resourceOf>
	</item>';

  }


$cat4 = get_categories('taxonomy=stories_category&type=stories'); 


  foreach($cat4 as $cat) {

    $sitemap .= '<url>'.
     '<loc>'. site_url().'/concierge/capemay/stories/?cat='.$cat->slug.'</loc>'.
      '<changefreq>monthly</changefreq>'.
    '</url>';

$rormap .= '<item>
    <link>'. site_url().'/concierge/capemay/stories/?cat='.$cat->slug.'</link>';

    $rormap .='<title>'.$cat->name.'</title>';
	if($cat->description) {
		$desc = $cat->description;
	}else {

		$desc .= 'Cape Resorts Stories - '.$cat->name; 
	}

    $rormap .='<description>'.$desc.'</description>
     <ror:updatePeriod>monthly</ror:updatePeriod>
     <ror:resourceOf>sitemap</ror:resourceOf>
	</item>';


  }



$cat5 = get_categories('taxonomy=offers_category&type=specialoffers'); 


  foreach($cat5 as $cat) {
    $sitemap .= '<url>'.
     '<loc>'. site_url().'/concierge/capemay/specialoffers/?cat='.$cat->slug.'</loc>'.
      '<changefreq>monthly</changefreq>'.
    '</url>';

$rormap .= '<item>
    <link>'. site_url().'/concierge/capemay/specialoffers/?cat='.$cat->slug.'</link>';



    $rormap .='<title>'.$cat->name.'</title>';
	if($cat->description) {
		$desc = $cat->description;
	}else {

		$desc .= 'Cape Resorts Special offers - '.$cat->name; 
	}

    $rormap .='<description>'.$desc.'</description>
     <ror:updatePeriod>monthly</ror:updatePeriod>
     <ror:resourceOf>sitemap</ror:resourceOf>
	</item>';


  }


$args['post_type']= array( 'seasonalevents','activities','entertainment', 'stories', 'specialoffers' );

$args['post_status']='publish'; 

$all_posts = get_posts($args);

  foreach($all_posts as $con) {
	
    $condate = explode(" ", $con->post_modified);

    $sitemap .= '<url>'.
      '<loc>'. site_url().'/concierge/capemay/'.$con->post_type.'/'.$con->post_name .'</loc>'.
      '<lastmod>'. $condate[0] .'</lastmod>'.
      '<changefreq>monthly</changefreq>'.
    '</url>';


$rormap .= '<item>
    <link>'. site_url().'/concierge/capemay/'.$con->post_type.'/'.$con->post_name .'</link>';

  	$title = con_page_title($con->ID);
	$desc = con_page_desc($con->ID); 

    $rormap .='<title>'.$title.'</title>';
    $rormap .='<description>'.$desc.'</description>
     <ror:updatePeriod>monthly</ror:updatePeriod>
     <ror:resourceOf>sitemap</ror:resourceOf>
	</item>';


  }

		
		$hotels=$wpdb->get_results("SELECT t1.base_url, t2.nav_slug FROM $wpdb->hotels AS t1 INNER JOIN $wpdb->hotelsnav AS t2 ON t1.id = t2.hotel_id order by t2.hotel_id");
		


  foreach($hotels as $hotel) {
	if($hotel->base_url) {

	if($hotel->nav_slug == 'home') {
		$navslug = '';

    $sitemap .= '<url>'.
      '<loc>'. site_url().'/'.$hotel->base_url.'#contactus</loc>'.
      '<changefreq>monthly</changefreq>'.
    '</url>';

    $sitemap .= '<url>'.
      '<loc>'. site_url().'/'.$hotel->base_url.'#directions</loc>'.
      '<changefreq>monthly</changefreq>'.
    '</url>';


	}else {
		$navslug = $hotel->nav_slug;

	}


    $sitemap .= '<url>'.
      '<loc>'. site_url().'/'.$hotel->base_url.'/'.$navslug.'</loc>'.
      '<changefreq>monthly</changefreq>'.
    '</url>';
	}
  }

	$hoteltabs=$wpdb->get_results("SELECT t1.base_url, t2.nav_slug, t3.slug FROM $wpdb->hotels AS t1 INNER JOIN $wpdb->hotelsnav AS t2 ON t1.id = t2.hotel_id INNER JOIN $wpdb->property_tab AS t3 ON t2.nid = t3.menu_id order by t2.hotel_id");

  foreach($hoteltabs as $tab) {
	if($tab->base_url) {

    $sitemap .= '<url>'.
      '<loc>'. site_url().'/'.$tab->base_url.'/'.$tab->nav_slug.'#'.$tab->slug.'</loc>'.
      '<changefreq>monthly</changefreq>'.
    '</url>';
	}
  }

  $sitemap .= '</urlset>';

  $fp = fopen(ABSPATH . "sitemap/sitemap.xml", 'w');
  fwrite($fp, $sitemap);
  fclose($fp);  

/* XML Coding End */

?>
