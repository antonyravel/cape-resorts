<?php
	/*
	Template Name: CapeResorts privacy Template
	*/
?>
<?php get_header(); ?>

<section>


<div class="other-template press-container">
<h2 class="heading">Development</h2>

  
  
 <p><strong>Welcome to the Cape Resorts Group  website (www.caperesortsgroup.com or the “Website”). </strong></p>
<p>This written policy (the “Privacy Policy”) is designed to tell you about our practices regarding collection, use, and disclosure of information that you may provide via this Website, affiliated websites including but not limited to www.congresshall.com, www.virginiahotel.com, www.thestarinn.net, www.beachshack.com, www.sandpipercapemay.com, and certain related services. Please be sure to read this entire Privacy Policy before using, or submitting information, to this Website.</p>

<p class="sub-heading">Our Policy </p>

<p>The policy of Cape Resorts Group is to respect and protect the privacy of our users. To fulfill this policy, Cape Resorts Group agrees to exercise the precautions set forth in this Privacy Policy to maintain the confidentiality of information provided by you in connection with accessing and using the Website.
Types of Information We Collect</p>
    
    
<p class="sub-heading">Types of Information We Collect</p>
<p>Like many websites, this Website actively collects information from its visitors both by asking you specific questions and by permitting you to communicate directly with us via e-mail, feedback forms, and/or chat rooms. Some of the information that you submit may be personally identifiable information (that is, information that can be uniquely identified with you, such as your full name, e-mail address, mailing address, phone number and sales delivery, billing and credit card information).</p>

<p>Some areas of this Website may require you to submit information in order for you to benefit from the specified features (such as newsletter subscriptions, tips/pointers, order processing) or to participate in a particular activity (such as sweepstakes or other promotions). You will be informed at each information collection point what information is required and what information is optional. </p>

<p class="sub-heading">Your Consent</p>
<p>This Privacy Policy sets forth Cape Resorts Group’s current policies and practices with respect to nonpublic personal information of the users of the Website. By using this Website, you agree with the terms of this Privacy Policy. Whenever you submit information via this Website, you consent to the collection, use, and disclosure of that information in accordance with this Privacy Policy.</p>

<p class="sub-heading">Our Commitment to Children’s Privacy</p>


<p>Protecting the privacy of the very young is especially important to Cape Resorts Group. This Website is not intended for children under the age of 13. We never collect or maintain information at the Website from persons we know to be under age 13, and no part of our Website is designed to attract anyone under age 13. We encourage parents to talk to their children about their use of the Internet and the information they disclose to websites. </p>
<p class="sub-heading">Disclosure of Information</p>
<p>Except as otherwise stated, we may use your information to improve the content of our Website, customize the Website to your preferences, communicate information to you (if you have requested it), for our marketing and research purposes, and for the purposes specified in this Privacy Policy. If you provide personally identifiable information to this Website, we may combine such information with other actively collected information unless we specify otherwise at the point of collection. We may combine your personally identifiable information with passively collected information. We may disclose your personally identifiable information to other Cape Resorts Group, LLC or Cape Advisors, Inc. affiliates worldwide that agree to treat it in accordance with this Privacy Policy. In addition, we may disclose your personally identifiable information to third parties, located in the United States and/or any other country, but only:</p>

<ul>
  <li>(I) to contractors we use to support our business (e.g., fulfillment services, technical support, delivery services, and financial institutions), in which case we will require such third parties to agree to treat it in accordance with this Privacy Policy; (ii) in connection with the sale, assignment, or other transfer of the business of this Website to which the information relates, in which case we will require any such buyer to agree to treat it in accordance with this Privacy Policy; or </li>
  <li>(II) where required by applicable laws, court orders, or government regulations. In addition, we will make full use of all information acquired through this Website that is not in personally identifiable form. </li>
  
</ul>

<p class="sub-heading">Security of Information<p>
<p>You can be assured that personal information collected through the Website is secure and is maintained in a manner consistent with current industry standards. The importance of security for all personal information associated with our subscribers is of utmost concern to us. Your personal information is protected in several ways. Your personal information resides on a secure server that only selected employees and contractors have access to via password. In order to most efficiently serve you, credit card transactions and order fulfillment are handled by established third-party banking, processing agents and distribution institutions. They receive the information needed to verify and authorize your credit card or other payment information and to process and ship your order. Unfortunately, no data transmission over the Internet or any wireless network can be guaranteed to be 100% secure. As a result, while we strive to protect your personal information, you acknowledge that: (a) there are security and privacy limitations of the internet which are beyond our control; (b) the security, integrity, and privacy of any and all information and data exchanged between you and us through this Website cannot be guaranteed; and (c) any such information and data may be viewed or tampered with in transit by a third party. Moreover, where you use passwords, ID numbers, or other special access features on this Website, it is your responsibility to safeguard them. </p>


<p class="sub-heading">On-Line Technologies:</p>
<p>As you navigate through a website, certain information can be passively collected (that is, gathered without your actively providing the information) using various technologies and means, such as Internet Protocol addresses, cookies, Internet tags, and navigational data collection. This Website may use Internet Protocol (IP) addresses. An IP Address is a number assigned to your computer by your Internet service provider so you can access the Internet and is generally considered to be non-personally identifiable information, because in most cases an IP address is dynamic (changing each time you connect to the Internet), rather than static (unique to a particular user's computer). We use your IP address to diagnose problems with our server, report aggregate information, determine the fastest route for your computer to use in connecting to our Website, and administer and improve services to our consumers. A “cookie” is a bit of information that a website sends to your web browser that helps the site remember information about you and your preferences. “Session cookies” are temporary bits of information that are erased once you exit your web browser window or otherwise turn your computer off. Session cookies are used to improve navigation on websites and to collect aggregate statistical information. This Website uses session cookies. “Persistent cookies” are more permanent bits of information that are placed on the hard drive of your computer and stay there unless you delete the cookie. Persistent cookies store information on your computer for a number of purposes, such as retrieving certain information you have previously provided (e.g., username), helping to determine what areas of the website visitors find most valuable, and customizing the website based on your preferences. This Website uses persistent cookies. “Internet tags”(also known as single-pixel GIFs, clear GIFs, invisible GIFs, and 1-by-1 GIFs) are smaller than cookies and tell the website server information such as the IP address and browser type related to the visitor's computer. This Website uses Internet tags. “Navigational data”(“log files,” “server logs,” and “clickstream” data) and “Internet Tags” are used for system management, to improve the content of the site, market research purposes, and to communicate information to visitors. This Website uses navigational data. </p>

<p class="sub-heading">Links</p>
<p>This Website may contain links to websites of third parties not affiliated with Cape Resorts Group. This Privacy Policy will not apply to these third-party websites and Cape Resorts Group is not responsible for the privacy practices or the content on any of these other websites, even where these third-party sites indicate a special relationship or “partnership” with Cape Resorts Group. Cape Resorts Group does not disclose personally identifiable information to those responsible for the linked sites. The linked sites, however, may collect personal information from you when you link to their site. This collecting of information is not subject to the control of Cape Resorts Group. To ensure protection of your privacy, always review the privacy policies of the sites you visit by linking from this Website.</p>
<p class="sub-heading">How to Contact Us</p>
<p>If you have any questions regarding our Privacy Policy, would like to update your information, modify your communication preferences, or if you do not want to receive marketing communications from Cape Resorts Group in the future, you can contact us: <p>
<p>By email: <a href="mailto:marketing@caperesortsgroup.com">marketing@caperesortsgroup.com</a></p>
<p>By writing us at: Cape Resorts Group, c/o Marketing Department –Privacy, 29 Perry Street, Cape May, NJ 08204</p>

<p class="sub-heading">Updates to the Privacy Policy:</p>
<p>Cape Resorts Group may amend this Privacy Policy from time to time in order to meet changes in the regulatory environment, properties, business needs, or to satisfy the needs of our guests, strategic marketing partners, and service providers. Updated versions will be made available on our website and date stamped so that you are always aware of when the Privacy Policy was last updated.</p>
</p>


</div>

</section>
</div></div>
</div><?php get_footer(); ?>

