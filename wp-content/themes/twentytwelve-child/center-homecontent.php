<?php
	global $p_id;
	global $subMenu;

?>

 <div class="sliderDiv">
<?php $phone = get_globalvalues($p_id, 'logo_phone');  ?>

    <p class="reserve">Reservations&nbsp;<?php echo $phone->reservation_number; ?></p>

	    <?php $slides = get_prop_slides($p_id, $subMenu); ?>

	<ul id="jqueryCycle">

		<?php foreach($slides as $slide): ?>

    		<li><a href="<?php echo $slide->description; ?>"><img src="<?php echo site_url().'/'.$slide->path.'/'.$slide->filename; ?>" alt="<?php echo $slide->alttext; ?>" class="slider-img" /></a></li>

		<?php endforeach; ?>
	</ul>
  </div>
