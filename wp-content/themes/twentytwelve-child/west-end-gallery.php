<?php
	/*
	Template Name: west-gallery
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(609)770-8261</p>
    <ul id="jqueryCycle">
<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>

	</ul>
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/west-end-garage/">home</a></li>
        <li><a href="<?php echo site_url(); ?>/west-end-vendor-list/">vendor listing</a></li>
        <li><a href="<?php echo site_url(); ?>/west-end-gallery/">gallery</a></li>
       </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>484 PERRY STREET</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/west-end-directions/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/west-end-contact/">contact us</a></li>
    </ul>
    <ul class="social-icons">     
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/venctor-icon.jpg" alt="corner-img" /></a></li>      
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/youtube.jpg" alt="corner-img" /></a></li> 
      <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/facebook.png" alt="corner-img" /></a></li>      
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/twitter.png" alt="corner-img" /></a></li>      
        <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/pinit.png" alt="corner-img" /></a></li>           
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="JOIN OUR MAILING LIST" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>    
  </div>
</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content gly">
  <h3>GALLERY</h3>
    <div class="gallery-container">
      
      <div class="gallery-images">
         <ul>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/guest-room.png" alt="corner-img" /></a>
          <div class="galler-text">
            <h3>guest rooms</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/dining.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>dining</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/pool.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>pool</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/easter.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>easter weekend</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/beach.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>beach</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/guest-room.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>mothers day weekend</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/dining.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>guest rooms</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/pool.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>guest rooms</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/easter.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>guest rooms</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
         
          
         </ul>
         
         <div class="pop-up">
				         
         </div>
      </div>
    </div>
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('west-end-gallery')) { ?>
<style>
body{background:#EBEBEB;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>
