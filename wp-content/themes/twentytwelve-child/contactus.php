<?php
	/*
	Template Name: CapeResorts contact-us Template
	*/
?>
<?php get_header(); ?>

<section>
<div class="other-template press-container" style="min-height:300px;">
<h2 class="heading">Contact Us</h2>
 


<?php /* Start the Loop */ ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php global $more; $more = 0; 
$ismore = @strpos( $post->post_content, '<!--more-->'); 

if ($ismore) :
?>
<div id="column1">
<?php the_content('');
$more = 1; ?>
</div>
<div id="column2">
<?php the_content('',true); ?>
</div>
<?php else: 

the_content();

endif;

endwhile; ?>
</div>



<!--<p>General information and inquiries: <a href="mailto:congress@congresshall.com">congress@congresshall.com</a> </p>
<p>To contact our General Manager: <a href="mailto:jdaily@congresshall.com">jdaily@congresshall.com</a></p>
<p>Group sales department: <a href="mailto:jplatt@congresshall.com">jplatt@congresshall.com</a></p>
<p>Wedding Inquiries: <a href="#">Click here to fill out an inquiry form or call Krista Ostrander</a></p>
<p>Reach us by phone at: (888) 944-1816</p> -->
  
</div>
</section>
</div></div>
</div><?php get_footer(); ?>



