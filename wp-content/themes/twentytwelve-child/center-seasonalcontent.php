<?php
	global $p_id;
	global $subMenu;

?>

 <div class="sliderDiv">
<?php $phone = get_globalvalues($p_id, 'logo_phone');  ?>

    <p class="reserve">Reserve a Cabana&nbsp;<?php echo $phone->reservation_number; ?></p>

	    <?php $slides = get_prop_slides($p_id, $subMenu); ?>

	<ul id="jqueryCycle">

		<?php foreach($slides as $slide): ?>

    		<li><img src="<?php echo site_url().'/'.$slide->path.'/'.$slide->filename; ?>" alt="<?php echo $slide->alttext; ?>" class="slider-img" /></li>

		<?php endforeach; ?>
	</ul>

    <div class="banner-navigation">
 <span class="nav-corner-img"></span>
      <?php echo get_hotel_menus($p_id, $subMenu); ?>
    </div>
<a href="#" class="banner-toggle-menu"><?php echo get_the_title(); ?> menu</a>
  </div>

  
  <div class="socialDiv">
    <ul class="directions">

<?php $address = get_globalvalues($p_id, 'address');  ?>

      <li><a href="javascript:void(0);" class="no-link"><?php echo $address->addressline2; ?></a></li>

<?php 
		preg_match_all('/(?<=\s|^)[a-z]/i', $address->state,$matches); 
		
		$result = strtoupper(implode('', $matches[0]));
?>

      <li><a href="javascript:void(0);" class="no-link"><?php echo $address->city; ?>, <?php echo $result; ?> <?php echo $address->zipcode; ?></a></li>


      <li><a href="javscript:void(0);" data-href="#directions" class="identify seasonal-dir" data-class="cnt" data-hide="#contactUs" >directions</a></li>
      <li><a href="javscript:void(0);" data-href="#contactUs" class="identify seasonal-con" data-class="drt" data-hide="#directions">contact us</a></li>


    </ul>

<!-- Get Social Links -->
      <?php get_property_social($p_id); ?>

<!-- Get newsletter Id -->

      <?php //$n_id = get_prop_newsletter($p_id); ?>
    


  <p class="mailing-list">

	<!--<input type="text" name="email" value="JOIN OUR MAILING LIST" disabled=true />-->
	<a class="mail-a" href="#">Join our Mailing List</a>
    <button class="mail-bt" type="submit"> </button>

    </p>



    
  </div>
