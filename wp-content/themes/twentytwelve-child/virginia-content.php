<?php
	/*
	Template Name: the-virgina-content
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(800)732-4236</p>
    <ul id="jqueryCycle">
<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>

	</ul>    
    <div class="banner-navigation">
      <!--<img src="<?php echo child_template_directory; ?>/img/congress-hall-images/corner-img.png" class="corner-img" alt="corner-img" />-->
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/capemay/virginia/">home</a></li>
        <li><a href="#">reservations</a></li>
        <li><a href="<?php echo site_url(); ?>/capemay/virginia/specials/">specials</a></li>
        <li><a href="#">accommodations</a></li>
        <li><a href="#">resort life</a></li>
        <li><a href="#">gallery</a></li>
        <li><a href="#">the ebbitt room</a></li>
        <li><a href="#">weddings &amp; events</a></li>
        <li><a href="#">meetings</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
    
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>25 Jackson Street</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/capemay/virginia/direction/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/capemay/virginia/contact/">contact us</a></li>
    </ul>
    <ul class="social-icons">
      <li><a href="http://www.tripadvisor.com/Hotel_Review-g46341-d92337-Reviews-Congress_Hall-Cape_May_New_Jersey.html" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/vector-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://www.youtube.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/tube-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://www.facebook.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/facebook-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://twitter.com/CongressHall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/twitter-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://pinterest.com/caperesorts/" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/pinit.jpg" alt="corner-img" /></a></li>
    </ul>
    

    <p class="mailing-list"><input type="text" name="mailing" value="JOIN OUR MAILING LIST" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content spl spls">
    <h3>content</h3>
    <div class="spec">
      <ul>
        <li class="dining-content">
          <div>
          <img src="http://sritsprojects.com/stage/caperesorts/wp-content/themes/twentytwelve-child/img/congress-hall-images/dining-samp1.jpg" alt="dining">
         <div>
         <h4>Content Item One</h4>
         <h5>Experience Cape May Clam Fest and Get a Great Deal</h5>
         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Lorem ipsum dolor sit amet, consectetur adipisicing elit,</p>
         <p class="view-links">
         <span class="v-gallery"><a href="#">learn more</a><a href="#">book now</a></span>
      </p>
     </div>
</div>
        </li>
        <li class="dining-content">
          <div>
          <img src="http://sritsprojects.com/stage/caperesorts/wp-content/themes/twentytwelve-child/img/congress-hall-images/dining-samp1.jpg" alt="dining">
         <div>
         <h4>Content Item Two</h4>
         <h5>experience capemay clam fest and get a great deal</h5>
         <h5>book early and save an additional 15%</h5>
         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Lorem ipsum dolor sit amet, consectetur adipisicing elit,</p>
         <p class="view-links">
         <span class="v-gallery"><a href="#">learn more</a><a href="#">book now</a></span>
      </p>
     </div>
     </div>
        </li>
        <li class="dining-content">
          <div>
          <img src="http://sritsprojects.com/stage/caperesorts/wp-content/themes/twentytwelve-child/img/congress-hall-images/dining-samp1.jpg" alt="dining">
         <div>
          <h4>Content Item Three</h4>
         <h5>experience capemay clam fest and get a great deal</h5>
         <h5>book early and save an additional 15%</h5>
         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Lorem ipsum dolor sit amet, consectetur adipisicing elit,</p>
         <p class="view-links">
         <span class="v-gallery"><a href="#">learn more</a><a href="#">book now</a></span>
      </p>
     </div>
     </div>
        </li>
        <li class="dining-content">
          <div>
          <img src="http://sritsprojects.com/stage/caperesorts/wp-content/themes/twentytwelve-child/img/congress-hall-images/dining-samp1.jpg" alt="dining">
         <div>
         <h4>Content Item Four</h4>
         <h5>experience capemay clam fest and get a great deal</h5>
         <h5>book early and save an additional 15%</h5>
         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Lorem ipsum dolor sit amet, consectetur adipisicing elit,</p>
         <p class="view-links">
         <span class="v-gallery"><a href="#">learn more</a><a href="#">book now</a></span>
      </p>
     </div></div>
        </li>
      </ul>
  </div>
    
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('content')) { ?>
<style>
body{background:#f2f2f2;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>

