<?php
	/*
	Template Name: Sea-spa
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reserve a Treatment&nbsp;(888)944-1816</p>
    
    
    <ul id="jqueryCycle">
<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider2.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider3.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider4.png" alt="slider" class="slider-img" /></li>


	</ul>
    
    
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/sea-spa/">home</a></li>
        <li><a href="<?php echo site_url(); ?>/sea-spa-menu/">spa menu</a></li>
        <li><a href="<?php echo site_url(); ?>/sea-spa-specials/">spa specials</a></li>
        <li><a href="<?php echo site_url(); ?>/sea-spa-gallery/">gallery</a></li>
        <li><a href="#">book a treatment</a></li>
        <li><a href="#">congress hall</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>200 congress place</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/sea-spa-directions/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/sea-spa-contactus/">contact us</a></li>
    </ul>
    <ul class="social-icons">
      <li><a href="http://www.tripadvisor.com/Hotel_Review-g46341-d92337-Reviews-Congress_Hall-Cape_May_New_Jersey.html" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/vector-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://www.youtube.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/tube-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://www.facebook.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/facebook-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://twitter.com/CongressHall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/twitter-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://pinterest.com/caperesorts/" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/pin-icon.png" alt="corner-img" /></a></li>
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="join our mailing list" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content">
    <div class="tabsDiv">
    <div class="sea-spa-cont">
    <h3>welcome to the sea spa</h3>
    <p>
    Sea Spa is a haven of healthy indulgence and personal rejuvenation. Our luxurious massage therapy in the surrounds of yellow and white striped cabana-style treatment rooms will make your spa experience like no other. 
    </p>
    <p>
      <span><b>Hours of Operation:</b></span>
      <span>Monday-Thursday: 10:30am-3pm (by reservation only; guests must book at least 24 hours in advance)</span>
      <span>Friday - Saturday: 9:30am – 6pm</span>
      <span>Sunday: 9:30am-3pm</span>
    </p>
    <p>
      <span>Easter Weekend - Extended Hours:</span>
      <span>Open Daily Thursday March 28 - Sunday April 7th</span>
      <span>CLOSED Easter Sunday, March 31</span>
    </p>
    
    <p>
      <span>CLOSED for Renovations:</span>
      <span>The Sea Spa will be closed for renovations from April 8-11 and April 15-17.</span>
    </p>
    <p>
      <span>Reservations are strongly recommended. A Spa Concierge will be available daily to schedule reservations!</span>
    </p>
    <p>
    Please call 609.884.6543 or ext. 6543 from your Congress Hall guest room to reserve your appointment now, or email our spa manager Kristy Speigel at kspeigel@congresshall.com. Please click here for our complete menu of services.
    </p>
    <p>
      Cancellation and Refund Policy All cancellations must be made 24 hours prior to your service. Cancellations within 24 hours are subject to the full amount of the services reserved. Refunds will not be given for a service that was rendered.
    </p>
   </div>
   </div>
   <div class="rt-bar">
    <div class="spring">
      <h6>try our</6>
      <h3>spa sampler package</h3>
      <p>available through May 1, 2013</p>
    </div>
    <span class="hr-w"></span>
    <div class="sea">
      <p>40% off</p>
      <h2>aveda</h2>
      <h6>products</h6>
    </div>
    <span class="hr-w"></span>
    <div class="retreat">
      <h6>easter weekend</h6>
      <h3>extended hours</h3>
      <p>Open Daily Thursday&nbsp;3/28 - Sunday 4/7</p>
      <p> Closed Easter Sunday, March 31</p>
    </div>
    <span class="hr-w"></span>
    <div class="resort-video">
      <a href="#">downlaod the <b>spa menu</b></a>
    </div>
   </div>
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('sea-spa')) { ?>
<style>
body{background:#EEEBE6;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>



