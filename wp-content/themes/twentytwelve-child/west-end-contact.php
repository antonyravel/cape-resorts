<?php
	/*
	Template Name: west-contact
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(609)770-8261</p>
    <ul id="jqueryCycle">
<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>

	</ul>
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/west-end-garage/">home</a></li>
        <li><a href="<?php echo site_url(); ?>/west-end-vendor-list/">vendor listing</a></li>
        <li><a href="<?php echo site_url(); ?>/west-end-gallery/">gallery</a></li>
       </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>484 PERRY STREET</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/west-end-directions/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/west-end-contact/">contact us</a></li>
    </ul>
    <ul class="social-icons">     
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/venctor-icon.jpg" alt="corner-img" /></a></li>      
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/youtube.jpg" alt="corner-img" /></a></li> 
      <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/facebook.png" alt="corner-img" /></a></li>      
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/twitter.png" alt="corner-img" /></a></li>      
        <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/pinit.png" alt="corner-img" /></a></li>           
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="JOIN OUR MAILING LIST" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>    
  </div>
</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content cnt">
    <h3>CONTACT US</h3>
    <div class="directionDiv">
		    <div class="direct-content">
		        <div class="gen-information">
		         	<h3>general information:</h3> 
						<h5>jim smith xxx xxx-xxxx</h5>
						<h5><a href="#">congress@congresshall.com</a></h5>
						
						<h3>general manager</h3> 
						<h5><a href="#">jdaily@congresshall.com</a></h5>
						
						<h3>general sales:</h3> 
						<h5><a href="#">jplatt@congresshall.com</a></h5>
						
						<h3>wedding inquiries:</h3> 
						<h4>krista ostrander(609)884-6553</h4>
						<h5><a href="#" style="color: #555555;" >click <span style="color: #f4776f" >here</span> to fill out an inquiry form</a></h5>
		          </div>
		         <div class="gen-information cntus">
		         	<h3>general information:</h3> 
						<h5>jim smith xxx xxx-xxxx</h5>
						<h5><a href="#">congress@congresshall.com</a></h5>
						
						<h3>general manager</h3> 
						<h5><a href="#">jdaily@congresshall.com</a></h5>
						
						<h3>general sales:</h3> 
						<h5><a href="#">jplatt@congresshall.com</a></h5>
						
						<h3>wedding inquiries:</h3> 
						<h4>krista ostrander(609)884-6553</h4>
						<h5><a href="#" style="color: #555555;" >click <span style="color: #f4776f">here</span> to fill out an inquiry form</a></h5>
		          </div>
						
		          </div>
		         <div class="gen-information reach">
		         <h3>REACH US BY PHONE:</h3>
		         <h5>( 888 ) 944 1816 OR (609) 884-8421</h5>
		         </div>      
		        
		       </div>
		        
		    </div>
    </div>
    
  </div>
<!--content end here-->  
<!--content end here-->


<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('west-end-contact')) { ?>
<style>
body{background:#EBEBEB;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>

