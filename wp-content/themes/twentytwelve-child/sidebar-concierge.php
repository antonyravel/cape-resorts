
<?php 
	global $parent;
	global $cat;
	global $taxnomy;
	global $post_type;
	global $termarray;
	global $filename;
if(count($termarray) > 0) {
	$cat =-99999999;
}
	$pageid = get_the_ID();

	if($pageid =="72") {

		$parent =27;
	}

	elseif($pageid =="80") {

		$parent ="28";
	}
	elseif($pageid =="83") {

			$parent ="29";
		}
	elseif($pageid =="85") {

			$parent ="30";
		}
	elseif($pageid =="87") {

			$parent ="34";
		}

//if($parent): 

 $args = array(
	'parent'                   => 27,
	'orderby'                  => 'menu_order',
	'taxonomy'                 => 'activity_category');

 $categories1 = get_categories( $args ); 

//endif;

$args = array(
	'parent'                   => 28,
	'orderby'                  => 'menu_order',
	'taxonomy'                 => 'seasonalevents_category');

 $categories2 = get_categories( $args );

$args = array(
	'parent'                   => 29,
	'orderby'                  => 'menu_order',
	'taxonomy'                 => 'entertainment_category');

 $categories3 = get_categories( $args );

$args = array(
	'parent'                   => 30,
	'orderby'                  => 'menu_order',
	'taxonomy'                 => 'stories_category');

 $categories4 = get_categories( $args );

$args = array(
	'parent'                   => 34,
	'orderby'                  => 'menu_order',
	'taxonomy'                 => 'offers_category');

 $categories5 = get_categories( $args );


?>

<script>
// <![CDATA[
jQuery('#concierge-nav').addClass('active');
// ]]>
</script>

<!-- <ul><li>-->

<aside>

<div class="aside-content">
<ul><?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(4) ) : else : ?></ul> 
No calender Available.


<?php endif; ?>

<ul id="side-accordian">

<li class="<? echo ($pageid == 17) ? 'active mainSidenav': 'mainSidenav'; ?>"><a href="<?php echo get_permalink(1873); ?>" data-type="con-home">Concierge Home</a></li>



<li class="<? echo ($pageid == 9) ? 'active mainSidenav': 'mainSidenav'; ?>"><a href="<?php echo  get_permalink(9); ?>" data-type="page">Pre-Arrival</a></li>


<li class="<? echo ($pageid == 72 || $post_type=='activities') ? 'active mainSidenav': 'mainSidenav'; ?>"><a href="<?php echo  get_permalink(72); ?>" data-type="all-posts"data-taxnomy="activity_category" data-post="activities">Activities</a>

<?php 



//if($parent==27): ?>

<ul><li>

 <?php 
  foreach ($categories1 as $category) { 

		$class = ($cat == $category->term_id || in_array($category->term_id, $termarray)) ? "active": "";

	?>
	 <span class="<? echo $class; ?>"><a data-type="specific-posts" href="<?php echo  get_permalink(72); ?>?cat=<?php echo $category->slug; ?>" data-taxnomy="activity_category" data-post="activities"><?php echo $category->name; ?></a></span>

 <?php }
 ?>
</li>
</ul>

<?php //endif; ?>

</li>

<li class="<? echo ($pageid == 80 || $post_type=='seasonalevents') ? 'active mainSidenav': 'mainSidenav'; ?>"><a href="<?php echo  get_permalink(80); ?>" data-type="all-posts" data-taxnomy="seasonalevents_category" data-post="seasonalevents">Seasonal Events</a>

<?php //if($parent==28): ?>

<ul><li>

 <?php 
  foreach ($categories2 as $category) { 

		$class = ($cat == $category->term_id || in_array($category->term_id, $termarray)) ? "active": "";

	?>
		<span class="<? echo $class; ?>"><a href="<?php echo  get_permalink(80); ?>?cat=<?php echo $category->slug; ?>" data-type="specific-posts" data-taxnomy="seasonalevents_category" data-post="seasonalevents"><?php echo $category->name; ?></a></span>

 <?php }
 ?>
</li>
</ul>

<?php //endif; ?>

</li>

<li class="<? echo ($pageid == 83 || $post_type=='entertainment') ? 'active mainSidenav': 'mainSidenav'; ?>"><a href="<?php echo  get_permalink(83); ?>" data-type="all-posts" data-taxnomy="entertainment_category" data-post="entertainment">Entertainment</a>

<?php //if($parent==29): ?>

<ul><li>

 <?php 
  foreach ($categories3 as $category) { 

		$class = ($cat == $category->term_id || in_array($category->term_id, $termarray)) ? "active": "";

	?>
		<span class="<? echo $class; ?>"><a href="<?php echo  get_permalink(83); ?>?cat=<?php echo $category->slug; ?>" data-type="specific-posts" data-taxnomy="entertainment_category" data-post="entertainment"><?php echo $category->name; ?></a></span>

 <?php }
 ?>

</li>
</ul>

<?php //endif; ?>

</li>

<li class="<? echo ($pageid == 85 || $post_type=='stories') ? 'active mainSidenav': 'mainSidenav'; ?>" ><a href="<?php echo  get_permalink(85); ?>" data-type="all-posts" data-taxnomy="stories_category" data-post="stories">Our Stories</a>
<?php //if($parent==30): ?>

<ul><li>

 <?php 
  foreach ($categories4 as $category) { 

		$class = ($cat == $category->term_id || in_array($category->term_id, $termarray)) ? "active": "";

	?>
		<span class="<? echo $class; ?>"><a href="<?php echo  get_permalink(85); ?>?cat=<?php echo $category->slug; ?>" data-type="specific-posts" data-taxnomy="stories_category" data-post="stories"><?php echo $category->name; ?></a></span>

 <?php }
 ?>
</li>
</ul>

<?php //endif; ?>

</li>

<li class="<? echo ($pageid == 87 || $post_type=='specialoffers') ? 'active mainSidenav': 'mainSidenav'; ?>"><a href="<?php echo get_permalink(87); ?>" data-type="all-posts" data-taxnomy="offers_category" data-post="specialoffers">Special Offers</a>

<?php //if($parent==6): ?>

<ul><li>

 <?php 
  foreach ($categories5 as $category) { 

		$class = ($cat == $category->term_id || in_array($category->term_id, $termarray)) ? "active": "";

	?>
		<span class="<? echo $class; ?>"><a href="<?php echo get_permalink(87); ?>?cat=<?php echo $category->slug; ?>" data-type="specific-posts" data-taxnomy="offers_category" data-post="specialoffers"><?php echo $category->name; ?></a></span>

 <?php }
 ?>
</li>
</ul>

<?php //endif; ?>

</li>

</ul>
<?php global $pdfpath; ?>
<!-- <span><a href="<?php echo $pdfpath; ?>" download="<?php echo $filename; ?>" target="_blank" class="download_link" data-type="download">Concierge Magazine</a></span> -->
<span><a href="http://issuu.com/congresshall.com/docs/concierge_2013?e=1173389/2612690" target="_blank" class="download_link" data-type="download">Concierge Magazine</a></span>
</div>

</aside>
