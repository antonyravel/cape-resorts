<?php
	/*
	Template Name: Home page Template
	*/
?>
<?php get_header();

global $p_id;
$p_id = get_field('pageproperties');
global $subMenu; 
global $menu; 

	$subMenu = urldecode($wp_query->query_vars['subMenu']);
	if(!$subMenu)
		$subMenu='homepage';
	$menu = getMenuBySlug($p_id, $subMenu);
?>
<div class="CH-home">

	<!-- Center_content bar -->
		<?php get_template_part( 'center', 'homecontent' ); ?>

</div> <!-- CH-home -->


</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->

	<?php if($subMenu=='gallery'): 
			$congclass= 'cong-content gly';
		  else:
			$congclass= 'cong-content';
		endif;
		?>
  <div class="<?php echo $congclass; ?>">


		<?php echo get_cong_template($menu, $p_id, $subMenu); ?>
  </div> 

<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>

