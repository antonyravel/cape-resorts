<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]> <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]> <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--[if lt IE 9]><script src="<?php echo child_template_directory; ?>/js/html5shiv.js" type="text/javascript"></script>
<![endif]-->
<html class='no-js' lang='en'>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>

<script src="<?php echo child_template_directory; ?>/js/html5shiv.js" type="text/javascript"></script>

<script type="text/javascript" src="http://fast.fonts.com/jsapi/4a6b6a4a-3ff6-4aa9-927d-65e10982b716.js"></script>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo child_template_directory; ?>/css/jquery-ui.css" /> -->
<link rel="stylesheet" type="text/css" href="<?php echo child_template_directory; ?>/js/chosen/chosen.css" />
<link rel="stylesheet" type="text/css" href="<?php echo child_template_directory; ?>/js/ResponsiveSlides/responsiveslides.css" />

<link rel="stylesheet" type="text/css" href="<?php echo child_template_directory; ?>/js/colorbox-master/example1/colorbox.css" />

 <link rel="stylesheet" type="text/css" href="<?php echo child_template_directory; ?>/js/jcarousel/skins/tango/skin.css" /> 

<!--<link rel="stylesheet" type="text/css" href="<?php echo child_template_directory; ?>/js/flexisel-master/css/style.css" /> -->

<link rel="stylesheet" type="text/css" href="<?php echo child_template_directory; ?>/js/jquery.bxslider/jquery.bxslider.css" />
<link rel="stylesheet" type="text/css" href="<?php echo child_template_directory; ?>/css/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="<?php echo child_template_directory; ?>/css/style1.css" />


<?php wp_head(); ?>

</head>

<?php if(is_page('virginia') OR is_page('direction') OR is_page('specials') OR is_page('contact') OR is_page('content') ){?>
<body class="virginia-body">

<?php }  elseif(is_page('seaspa')){?>
<body class="seaspa-body">

<?php }  elseif(is_page('west-end-garage') OR is_page('westendgarage') OR is_page('west-end-directions') OR is_page('west-end-gallery') OR is_page('west-end-contact') OR is_page('west-end-vendor-list') ){?>
<body class="west-end-body">

<?php }  elseif(is_page('sandpiper') OR is_page('sandpiper-direction') OR is_page('sandpiper-specials') OR is_page('sandpiper-contact')){?>
<body class="sandpiper-body">

<?php }  elseif(is_page('beachplumfarm') OR is_page('beachplumfarm-direction') OR is_page('beachplumfarm-specials') OR is_page('beachplumfarm-contact')){?>
<body class="beachplumfarm-body">

<?php }  elseif(is_page('rustynail') OR is_page('rustynail-direction') OR is_page('rustynail-specials') OR is_page('rustynail-contact')){?>
<body class="rustynail-body">

<?php } else {?>
<body>
<?php } ?>
<input type="hidden" value="<?php echo site_url(); ?>" name="siteurl" id="siteurl" />
<?php

$meg_url = get_option('magazine_url'); // some IP address
$iparr = split ("=", $meg_url); 
global $pdfpath, $filename;

$pdfpath = wp_get_attachment_url( $iparr[1] );
$filename = basename($pdfpath);      

?>

<!-- class adding for congress hall -->

<?php if (is_page('congresshall') OR is_page('gallery') OR is_page('dining') OR is_page('view-detail') OR is_page('directions') OR is_page('contact-us') OR is_page('view-details') ) { ?>
<div class="wrapper cong-wrapper">

<!-- class adding for beack shack -->
<?php }  elseif (is_page('beachshack') OR is_page('beach-shack-contact') OR is_page('beach-shack-directions') OR is_page('beach-shack-gallery') OR is_page('beach shack specials')) { ?>
<div class="wrapper beach-wrapper"> 

<!-- class adding for sea spa -->
<?php }  elseif (is_page('sea-spa') OR is_page('seaspa') OR is_page('sea-spa-menu') OR is_page('sea-spa-contactus') OR is_page('sea-spa-directions') OR is_page('sea-spa-gallery') OR is_page('sea-spa-specials') OR is_page('treatment-booking')) { ?>
<div class="wrapper spa-wrapper">

<!-- class adding for tommy -->
<?php }  elseif (is_page('tommys-folly') OR is_page('tommysfolly') OR is_page('tommy-folly-directions') OR is_page('tommy-folly-gallery') OR is_page('tommy-folly-contact')) { ?>
<div class="wrapper tommy-wrapper">

<!-- class adding for west end garage -->
<?php }  elseif (is_page('west-end-garage') OR is_page('westendgarage') OR is_page('west-end-directions') OR is_page('west-end-gallery') OR is_page('west-end-contact') OR is_page('west-end-vendor-list')) { ?>
<div class="wrapper west-end-wrapper">

<!-- class adding for virginia -->
<?php }  elseif(is_page('virginia') OR is_page('direction') OR is_page('specials') OR is_page('contact') OR is_page('content') ){?>
<div class="wrapper virginia-wrapper">

<!-- class adding for sandpiper -->
<?php }  elseif(is_page('sandpiper') OR is_page('sandpiper-direction') OR is_page('sandpiper-specials') OR is_page('sandpiper-contact')){?>
<div class="wrapper sandpiper-wrapper">

<!-- class adding for beachplumfarm -->
<?php }  elseif(is_page('beachplumfarm') OR is_page('beachplumfarm-direction') OR is_page('beachplumfarm-specials') OR is_page('beachplumfarm-contact')){?>
<div class="wrapper beachplumfarm-wrapper">


<!-- class adding for boiler room -->
<?php }  elseif(is_page('boilerroom')){?>
<div class="wrapper boiler-wrapper">

<!-- class adding for brownroom -->
<?php }  elseif(is_page('brownroom')){?>
<div class="wrapper brown-wrapper">

<!-- class adding for rusty nail -->
<?php }  elseif(is_page('rustynail')){?>
<div class="wrapper rustynail-wrapper">

<!-- class adding for beach service -->
<?php }  elseif(is_page('beachservice')){?>
<div class="wrapper service-wrapper">

<!-- class adding for ebbittroom -->
<?php }  elseif(is_page('ebbittroom')){?>
<div class="wrapper ebbit-wrapper">


<?php }  else { ?> 


<!-- normal wrapper class -->
<div class="wrapper">
<?php } ?>

<div class="wrapper-top">
<div class="wrapper-bottom">
<div class="wrapper-middle">
<div class="wr-bot">
<div class="wr-top">
<div class="main">

<!--header starts here-->
<header> 

<!-- logo change for congress hall -->

<?php if (is_page('congress-hall') OR is_page('congresshall') OR is_page('gallery') OR is_page('dining') OR is_page('view-detail') OR is_page('directions') OR is_page('contact-us') OR is_page('view-details') ) { ?>
<div class="logo c-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/con-images/c-logo.png" alt="Cape resorts Logo"/></a></h1>

<!-- logo change for beack shack -->

<?php }  elseif(is_page('beachshack') OR is_page('beach-shack-contact') OR is_page('beach-shack-directions') OR is_page('beach-shack-gallery') OR is_page('beach shack specials')){?>
<div class="logo bs-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/bs-logo.png" alt="Cape resorts Logo"/></a></h1>


<!-- logo change for sea spa -->

<?php }  elseif(is_page('sea-spa') OR is_page('seaspa') OR is_page('sea-spa-contactus') OR is_page('sea-spa-directions') OR is_page('sea-spa-menu') OR is_page('sea-spa-gallery') OR is_page('treatment-booking') OR is_page('sea-spa-specials')){?> 
<div class="logo spa-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/spa-logo.png" alt="Cape resorts Logo"/></a></h1>

<!-- logo change for tommy folly -->

<?php }  elseif(is_page('tommys-folly') OR is_page('tommysfolly') OR is_page('tommy-folly-directions') OR is_page('tommy-folly-gallery') OR is_page('tommy-folly-contact')){?>
<div class="logo tommy-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/tommy-logo.png" alt="Cape resorts Logo"/></a></h1>

<!-- logo change for west end garage -->

<?php }  elseif(is_page('west-end-garage') OR is_page('westendgarage') OR is_page('west-end-directions') OR is_page('west-end-gallery') OR is_page('west-end-contact') OR is_page('west-end-vendor-list') ){?>
<div class="logo west-end-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/west-end-images/west-logo.png" alt="Cape resorts Logo"/></a></h1>

<!-- logo change for virgina -->

<?php }  elseif(is_page('virginia') OR is_page('direction') OR is_page('specials') OR is_page('contact') OR is_page('content') ){?>
<div class="logo virginia-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/virginia-images/virginia-logo.png" alt="Cape resorts Logo"/></a></h1>

<!-- logo change for sandpiper -->

<?php }  elseif(is_page('sandpiper') OR is_page('sandpiper-direction') OR is_page('sandpiper-specials') OR is_page('sandpiper-contact')  ){?>
<div class="logo sandpiper-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/sandpiper-logo.png" alt="Cape resorts Logo"/></a></h1>

<!-- logo change for beachplumfarm -->

<?php }  elseif(is_page('beachplumfarm') OR is_page('beachplumfarm-direction') OR is_page('beachplumfarm-specials') OR is_page('beachplumfarm-contact')  ){?>
<div class="logo beachplumfarm-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/beachplumfarm-images/beachplumfarm-logo.png" alt="Cape-resorts-Logo"/></a></h1>


<!-- logo change for boiler room -->

<?php }  elseif(is_page('boilerroom')){?>
<div class="logo boiler-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/boiler-brown-img/boiler-logo.png" alt="boiler-logo"/></a></h1>

<!-- logo change for brown room -->

<?php }  elseif(is_page('brownroom')){?>
<div class="logo brown-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/boiler-brown-img/brown-logo.png" alt="brown-logo"/></a></h1>

<!-- logo change for ebbittroom -->

<?php }  elseif(is_page('ebbittroom')){?>
<div class="logo ebbit-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/ebbit/ebit-logo.png" alt="brown-logo"/></a></h1>

<!-- logo change for rusty nail -->

<?php }  elseif(is_page('rustynail')){?>
<div class="logo rustynail-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/rusty-images/logo.png" alt="rusty-logo"/></a></h1>



<!-- common logo -->
<?php }  else{?>
<div class="logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/con-images/logo.png" alt="Cape resorts Logo"/></a></h1>
<?php } ?>
</div>
<nav>
<div class="nav-top">
<form id="reservations">
<ul>
<li><a href="#">book a room</a></li>
<li class="ngt htl">

<?php  echo hotels_list('dropdown');  ?>

<!-- <a href="#">hotel<span><img src="<?php echo child_template_directory; ?>/img/con-images/down-arrow.png" alt="Cape resorts room booking nights"/></span></a> -->



</li>
<li class="arr">



<input type="text" name="arrivaldate" id="arrivaldate" value="arrival" onclick="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue; }" title="Sign up for our newsletter"/>

<!-- <a href="#" id="arrivaldate">arrival date<span><img src="<?php echo child_template_directory; ?>/img/con-images/date-bg.png" alt="Cape resorts room booking date"/></span></a>-->
</li>
<li class="ngt">
<select name="nights" data-placeholder="Nights" id="nights" class="chosen-select">
	<option value=''></option>";
	<option value='1'>1</option>";
	<option value='2'>2</option>";
	<option value='3'>3</option>";
	<option value='4'>4</option>";
	<option value='5'>5</option>";
	<option value='6'>6</option>";
	<option value='7'>7</option>";
	<option value='8'>8</option>";
	<option value='9'>9</option>";
	<option value='10'>10</option>";
</select>
<!-- <a href="#">nights<span><img src="<?php echo child_template_directory; ?>/img/con-images/down-arrow.png" alt="Cape resorts room booking nights"/></span></a> -->
</li>
<li class="adl">

<select name="adults" data-placeholder="adults" id="adults" class="chosen-select">
	<option value=''></option>";
	<option value='1'>1</option>";
	<option value='2'>2</option>";
	<option value='3'>3</option>";
	<option value='4'>4</option>";
	<option value='5'>5</option>";
	<option value='6'>6</option>";
	<option value='7'>7</option>";
	<option value='8'>8</option>";
	<option value='9'>9</option>";
	<option value='10'>10</option>";
</select>

<!--<a href="#">adults<span><img src="<?php echo child_template_directory; ?>/img/con-images/down-arrow.png" alt="Adults"/></span></a> -->
</li>
<li class="ngt">
<select name="children" data-placeholder="children" id="children" class="chosen-select">
	<option value=''></option>";
	<option value='1'>1</option>";
	<option value='2'>2</option>";
	<option value='3'>3</option>";
	<option value='4'>4</option>";
	<option value='5'>5</option>";
	<option value='6'>6</option>";
	<option value='7'>7</option>";
	<option value='8'>8</option>";
	<option value='9'>9</option>";
	<option value='10'>10</option>";
</select>
<!-- <a href="#">children<span><img src="<?php echo child_template_directory; ?>/img/con-images/down-arrow.png" alt="Cape resorts room booking nights"/></span></a> -->
</li>
<li class="res">
<a href="javascript:void(0);" class="reserve_button">Reservations<span><img src="<?php echo child_template_directory; ?>/img/con-images/right-arrow.png" alt="Cape resorts room reservations"/></span></a>
</li>
</ul>
</form> <!-- End Form Reservation -->
</div>
<div class="star">
<span class="hr-line"></span>
<img src="<?php echo child_template_directory; ?>/img/con-images/star.png" alt="Capre resorts star"/>
</div>

<div class="top-menu">
<a class="toggleMenu" href="#">Menu</a>
<ul id="menu-menu" class="dropdown">
<li><a href="#">hotels</a>
	<ul class="sub-menu">

		<li><a href="<?php echo site_url(); ?>/capemay/congresshall/">CONGRESS HALL</a></li>
		<li><a href="<?php echo site_url(); ?>/capemay/virginia/">THE VIRGINIA</a></li>
		<li><a href="http://www.caperesorts.com/hotels/capemay/cottages">THE VIRGINIA COTTAGES</a></li>
		<li><a href="<?php echo site_url(); ?>/capemay/beachshack/">BEACH SHACK</a></li>
    <li><a href="<?php echo site_url(); ?>/capemay/sandpiper">SANDPIPER BEACH CLUB</a></li>
		<li><a href="http://www.caperesorts.com/hotels/capemay/thestar">THE STAR</a></li>
		
		<li><a href="http://www.caperesorts.com/hotels/sagharbor/baronscove">BARONS COVE SAG HARBOR SPRING 2014</a></li>
		<li><a href="http://www.caperesorts.com/giftcards">GIFT CARDS</a></li>
	</ul>
</li>
<li><a href="#">restaurants</a>

	<ul class="sub-menu">
		<li><a href="http://www.caperesorts.com/restaurants/capemay/bluepigtavern">THE BLUE PIG TAVERN</a></li>
		<li><a href="http://www.caperesorts.com/restaurants/capemay/ebbittroom">THE EBBITT ROOM</a></li>
		<li><a href="<?php echo site_url(); ?>/restaurants/rustynail">THE RUSTY NAIL</a></li>
		<!--<li><a href="http://www.caperesorts.com/restaurants/capemay/boilerroom">THE BOILER ROOM</a></li>-->
		<li><a href="<?php echo site_url(); ?>/restaurants/boilerroom/">THE BOILER ROOM</a></li>
		<!--<li><a href="http://www.caperesorts.com/restaurants/capemay/brownroom">THE BROWN ROOM</a></li>	-->
		<li><a href="<?php echo site_url(); ?>/restaurants/brownroom/">THE BROWN ROOM</a></li>
		<li><a href="http://www.caperesorts.com/restaurants/capemay/boilerroom">VERANDA BAR</a></li>
		<li><a href="http://www.caperesorts.com/restaurants/capemay/tommysfolly/coffeeshop">TOMMY’S FOLLY COFFEE SHOP</a></li>
		<li><a href="http://www.caperesorts.com/restaurants/capemay/thestarcoffeeshop">THE STAR COFFEE SHOP</a></li>
		<!--<li><a href="#">THE BARON'S TABLE</a></li>-->
		<li><a href="http://www.caperesorts.com/giftcards">GIFT CARDS</a></li>
	</ul>

</li>
<li><a href="#">experiences</a>

	<ul class="sub-menu">
		<li><a href="<?php echo site_url(); ?>/experiences/seaspa/">SEA SPA</a></li>
		<li><a href="<?php echo site_url(); ?>/experiences/beachservice/">BEACH SERVICE</a></li>
		<li><a href="<?php echo site_url(); ?>/experiences/tommysfolly">TOMMY’S FOLLY</a></li>
		<li><a href="<?php echo site_url(); ?>/experiences/beachplumfarm">BEACH PLUM FARM</a></li>
		<li><a href="<?php echo site_url(); ?>/experiences/westendgarage">WEST END GARAGE</a></li>
		<li><a href="http://www.caperesorts.com/experiences/capemay/seasonal">SEASONAL EXPERIENCES</a></li>
		<li><a href="http://www.caperesorts.com/experiences/capemay/shopping">SHOPPING</a></li>
		<li><a href="http://www.caperesorts.com/giftcards">GIFT CARDS</a></li>
	</ul>


</li>
<li id="concierge-nav"><a href="<?php echo site_url(); ?>/concierge/" data-type="con-home">concierge</a>

	<ul class="sub-menu">
		<li><a href="<?php echo get_permalink(9); ?>" data-type="page" >PRE-ARRIVAL</a></li>
		<li><a href="<?php echo site_url(); ?>/concierge/concierge-calender/">DAILY CALENDAR</a></li>
		<li><a href="<?php echo get_permalink(72); ?>" data-type="all-posts" data-taxnomy="activity_category" data-post="activities">ACTIVITIES</a></li>
		
		
		<li><a href="<?php echo get_permalink(80); ?>" data-type="all-posts" data-taxnomy="seasonalevents_category" data-post="seasonalevents">SEASONAL EVENTS</a></li>
		<li><a href="<?php echo get_permalink(83); ?>" data-type="all-posts" data-taxnomy="entertainment_category" data-post="entertainment">ENTERTAINMENT</a></li>
		<li><a href="<?php echo get_permalink(85); ?>" data-type="all-posts" data-taxnomy="stories_category" data-post="stories">OUR STORIES</a></li>
		<!--<li><a href="<?php echo get_permalink(87); ?>" data-type="all-posts" data-taxnomy="offers_category" data-post="specialoffers">SPECIAL OFFERS</a></li>-->
		<li><a href="<?php echo $pdfpath; ?>" download="<?php echo $filename; ?>" target="_blank">CONCIERGE MAGAZINE</a></li>
	</ul>

</li>
<li><a href="#">weddings &amp; events</a>

	<ul class="sub-menu">
		<li><a href="http://www.caperesorts.com/weddingsandevents/capemay">CAPE MAY OVERVIEW</a></li>
		<li><a href="http://www.caperesorts.com/weddingsandevents/capemay/congresshall/meetings">CONGRESS HALL MEETING</a></li>
		<li><a href="http://www.caperesorts.com/weddingsandevents/capemay/virginiahotel/meetings">VIRGINIA MEETINGS</a></li>
		<li><a href="http://www.caperesorts.com/weddingsandevents/capemay/congresshall/weddings">CONGRESS HALL WEDDINGS</a></li>
		<li><a href="http://www.caperesorts.com/weddingsandevents/capemay/virginiahotel/weddings">VIRGINIA WEDDINGS</a></li>
		<li><a href="http://www.caperesorts.com/weddingsandevents/capemay/beachshack/weddings">BEACH SHACK WEDDINGS</a></li>
	</ul>


</li>
<li class="spl"><a href="#">specials</a>

	<ul class="sub-menu">
		<li><a href="http://www.caperesorts.com/specials/">ALL</a></li>
		<li><a href="<?php echo site_url(); ?>/capemay/congresshall/">CONGRESS HALL</a></li>
		<li><a href="<?php echo site_url(); ?>/virginia/">THE VIRGINIA</a></li>
		<li><a href="http://www.caperesorts.com/specials/capemay/cottages">THE VIRGINIA COTTAGES</a></li>
		<li><a href="<?php echo site_url(); ?>/beach-shack/">BEACH SHACK</a></li>
		<li><a href="http://www.caperesorts.com/specials/capemay/thestar">THE STAR</a></li>
		<li><a href="<?php echo site_url(); ?>/capemay/sandpiper">SANDPIPER BEACH CLUB</a></li>
		<!--<li><a href="#">GIFT CARDS</a></li>-->
	</ul>


</li>
</ul>
</div>
</nav>
</header>
