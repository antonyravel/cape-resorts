<?php
	/*
	Template Name: sandpiper-beach-specials
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(800)732-7816</p>
    <ul id="jqueryCycle">
<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
	</ul>
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/capemay/sandpiper/">home</a></li>
        <li><a href="<?php echo site_url(); ?>/capemay/sandpiper/sandpiper-specials/">SPECIALS</a></li>
        <li><a href="#">ACCOMMODATIONS</a></li>
        <li><a href="#">RESORT LIFE</a></li>
        <li><a href="#">GALLERY</a></li>
        <li><a href="#">OWNER’S SIGNIN</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>11 West Beach Ave</li>
      <li>Cape May, NJ 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/capemay/sandpiper/sandpiper-direction">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/capemay/sandpiper/sandpiper-contact">contact us</a></li>
    </ul>
    <ul class="social-icons">     
       <li class="vec-icon"><a href="#" target="_blank"></a></li>      
       <li class="you-icon"><a href="#" target="_blank"></a></li> 
      <li class="fac-icon"><a href="#" target="_blank"></a></li>      
       <li class="twi-icon"><a href="#" target="_blank"></a></li>      
        <li class="pin-icon"><a href="#" target="_blank"></a></li>           
    </ul>
    
    <p class="mailing-list"><a class="mail-a" href="#">JOIN OUR MAILING LIST</a>
    <button class="mail-bt"> </button>
    </p>    
  </div>
</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
<div class="cong-content spl spls">
    <h3>Specials</h3>
    <div class="spec">
      <ul>
        <li class="dining-content">
          <div>
          <img src="http://sritsprojects.com/stage/caperesorts/wp-content/themes/twentytwelve-child/img/congress-hall-images/dining-samp1.jpg" alt="dining">
         <div>
         <h4>CLAMORAMA !</h4>
         <h5>Experience Cape May Clam Fest and Get a Great Deal Book <br/>Early and Save An Additional 15%</h5>
         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Lorem ipsum dolor sit amet, consectetur adipisicing elit,</p>
         <p class="view-links"><a href="#" class="share"><span>share</span></a>
         <span class="v-gallery"><a href="#">learn more</a><a href="#">book now</a></span>
      </p>
     </div>
</div>
        </li>
        <li class="dining-content">
          <div>
          <img src="http://sritsprojects.com/stage/caperesorts/wp-content/themes/twentytwelve-child/img/congress-hall-images/dining-samp1.jpg" alt="dining">
         <div>
             <h4>CLAMORAMA !</h4>
         <h5>Experience Cape May Clam Fest and Get a Great Deal Book <br/>Early and Save An Additional 15%</h5>
         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Lorem ipsum dolor sit amet, consectetur adipisicing elit,</p>
         <p class="view-links"><a href="#" class="share"><span>share</span></a>
         <span class="v-gallery"><a href="#">learn more</a><a href="#">book now</a></span>
      </p>
     </div>
     </div>
        </li>
        <li class="dining-content">
          <div>
          <img src="http://sritsprojects.com/stage/caperesorts/wp-content/themes/twentytwelve-child/img/congress-hall-images/dining-samp1.jpg" alt="dining">
         <div>
             <h4>CLAMORAMA !</h4>
         <h5>Experience Cape May Clam Fest and Get a Great Deal Book <br/>Early and Save An Additional 15%</h5>
         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Lorem ipsum dolor sit amet, consectetur adipisicing elit,</p>
         <p class="view-links"><a href="#" class="share"><span>share</span></a>
         <span class="v-gallery"><a href="#">learn more</a><a href="#">book now</a></span>
      </p>
     </div>
     </div>
        </li>
        <li class="dining-content">
          <div>
          <img src="http://sritsprojects.com/stage/caperesorts/wp-content/themes/twentytwelve-child/img/congress-hall-images/dining-samp1.jpg" alt="dining">
         <div>
             <h4>CLAMORAMA !</h4>
         <h5>Experience Cape May Clam Fest and Get a Great Deal Book <br/>Early and Save An Additional 15%</h5>
         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Lorem ipsum dolor sit amet, consectetur adipisicing elit,</p>
         <p class="view-links"><a href="#" class="share"><span>share</span></a>
         <span class="v-gallery"><a href="#">learn more</a><a href="#">book now</a></span>
      </p>
     </div></div>
        </li>
      </ul>
  </div>
    
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('sandpiper-specials')) { ?>
<style>
body{background:#f2f2f2;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>

