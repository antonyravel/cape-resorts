<?php
	/*
	Template Name: west-vendor-list
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(609)770-8261</p>
    <ul id="jqueryCycle">
<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>

	</ul>
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/west-end-garage/">home</a></li>
        <li><a href="<?php echo site_url(); ?>/west-end-vendor-list/">vendor listing</a></li>
        <li><a href="<?php echo site_url(); ?>/west-end-gallery/">gallery</a></li>
       </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>484 PERRY STREET</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/west-end-directions/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/west-end-contact/">contact us</a></li>
    </ul>
    <ul class="social-icons">     
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/venctor-icon.jpg" alt="corner-img" /></a></li>      
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/youtube.jpg" alt="corner-img" /></a></li> 
      <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/facebook.png" alt="corner-img" /></a></li>      
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/twitter.png" alt="corner-img" /></a></li>      
        <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/pinit.png" alt="corner-img" /></a></li>           
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="JOIN OUR MAILING LIST" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>    
  </div>
</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content spl">
    <h3>VENDOR LIST</h3>
    <div class="spec">
      <ul>
        <li class="dining-content">
          <div>
          <img src="<?php echo child_template_directory; ?>/img/west-end-images/gallery.jpg" alt="dining" />         
         <div>
         <h4>Trove</h4>
         <h4>"Best of philly" winner 2012 </h4>         
         <p>If you crave home decor that captures the patina of the past transformed for the present, that will outlive the latest design trend, and that minimizes our impact on the planet... welcome to Trove.</p>
         <p class="view-links">
         <span class="v-gallery"><a href="#">VISIT WEBSITE</a></span>
      </p>
     </div>
</div>
        </li>
        <li class="dining-content">
          <div>
             <img src="<?php echo child_template_directory; ?>/img/west-end-images/gallery.jpg" alt="dining" />   
         <div>
         <h4>SJEA’s CLOSET</h4>  
         <p>"be real...be true...be you" Quality sweaters and knitwear designed in a way that allows your real beauty, essence, and style to shine through, and the jewelry and accessories to go along with them.<br/>
"be real...be true...be you" Quality sweaters and knitwear designed in a way that allows your real beauty, essence, and style to shine through, and the jewelry and accessories to go along. <em>Read More</em>
</p>
         <p class="view-links">
      </p>
     </div></div>
        </li>
        <li class="dining-content">
          <div>
             <img src="<?php echo child_template_directory; ?>/img/west-end-images/gallery.jpg" alt="dining" />   
         <div>
          <h4>Trove</h4>
         <h4>"Best of philly" winner 2012 </h4>          
         <p>If you crave home décor that captures the patina of the past transformed for the present, that will outlive the latest design trend, and that minimizes our impact on the planet... welcome to Trove. sign trend, and that minimizes our impact on the planet... welcome to Trove.. <em>Read More</em></p>
         <p class="view-links">        
      </p>
     </div>
     </div>
        </li>
        
      </ul>
  </div>
    
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('west-end-vendor-list')) { ?>
<style>
 body{background:#EBEBEB;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>

