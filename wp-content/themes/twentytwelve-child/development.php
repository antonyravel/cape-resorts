<?php
	/*
	Template Name: Development page Template
	*/
?>
<?php get_header(); ?>

<section>
<div class="other-template press-container">
<h2 class="heading">Development</h2>

<?php while (have_posts()) : the_post(); ?>   
 
<?php the_content();?>
  <?php endwhile; ?>
<address>  
<span>Cape Resorts,</span>
<span>483 Broadway, 5th Floor,</span> 
<span>New York, NY 10013,</span>
<span>212.343.1700.</span>
</address>
</div>
</section>
</div></div>
</div><?php get_footer(); ?>

