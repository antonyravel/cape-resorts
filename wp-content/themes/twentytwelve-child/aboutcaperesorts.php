<?php
	/*
	Template Name: About-Cape-Resort page Template
	*/
?>
<?php get_header(); ?>

<section>
<div class="other-template press-container">
<h2 class="heading">About Cape Resorts</h2>
<p class="sub-heading">The Cape Resort's Story</p>
<?php while (have_posts()) : the_post(); ?>   
 
<?php the_content();?>
  <?php endwhile; ?>
</div>
</section>

</div></div>
</div><?php get_footer(); ?>

