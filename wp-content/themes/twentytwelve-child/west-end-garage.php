<?php
	/*
	Template Name: west-end
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(609)770-8261</p>
    <ul id="jqueryCycle">
<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/west-end-images/slider-1.png" alt="slider" class="slider-img" /></li>

	</ul>
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/west-end-garage/">home</a></li>
        <li><a href="<?php echo site_url(); ?>/west-end-vendor-list/">vendor listing</a></li>
        <li><a href="<?php echo site_url(); ?>/west-end-gallery/">gallery</a></li>
       </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>484 PERRY STREET</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/west-end-directions/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/west-end-contact/">contact us</a></li>
    </ul>
    <ul class="social-icons">     
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/venctor-icon.jpg" alt="corner-img" /></a></li>      
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/youtube.jpg" alt="corner-img" /></a></li> 
      <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/facebook.png" alt="corner-img" /></a></li>      
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/twitter.png" alt="corner-img" /></a></li>      
        <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/west-end-images/pinit.png" alt="corner-img" /></a></li>           
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="JOIN OUR MAILING LIST" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>    
  </div>
</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content garage">
    <div class="tabsDiv">
    <div class="tommy-cont">
    <h2>WELCOME TO THE WEST END GARAGE</h2>
    <h3>HOURS OF OPERATION:<br>
    MON-SUN: 10AM-6PM			
    </h3>								
    <p>
   Cape May's favorite shopping destination and Winner of the "Best of Shore" award. The West End Garage has been transformed into a market featuring shops and stalls with local art exhibits, antiques, collectibles, crafts, and fresh, locally produced food specialties. The West End Garage is the primary shopping destination for all guests of Cape May's premier luxury hotels: Congress Hall, The Virginia Hotel and Cottages, The Star, and the Beach Shack.
    </p>
    <p>
     Etiam tremulus oratori lucide corrumperet adlaudabilis concubine. Agricolae circumgrediet apparatus bellis. Catelli imputat umbraculi, ut rures corrumperet tremulus umbraculi.
    </p>
    </div>
   </div>
   <div class="rt-bar">
    <div class="spring">
      <h2>Spring Break</h2>
      <h3>Enjoy the our fun getaways & activities</h3>
      <p>
      <a href="http://www.congresshall.com/content/winter-weddings.php" target="_blank" style="color:#fff" >learn more</a>
      </p>
    </div>
    <div class="spring">
      <h2>Spring Break</h2>
      <h3>Relax, unwind, and treat yourself to a spa day.</h3>
      <p>
      <a href="http://www.congresshall.com/content/winter-weddings.php" target="_blank">learn more about this package</a>
      </p>
    </div>
    <div class="spring">
      <h2>Spring Break</h2>
      <p>
      <a href="http://www.congresshall.com/content/winter-weddings.php" target="_blank">Getaway....with dinner, drinks and a relaxing spa treatment.starting at $125.</a>
      </p>
    </div>
    <div class="resort-video"><a href="http://caperesortsgroup.com/video.html" target="">Watch the Cape Resorts Video</a></div>
   </div>
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('west-end-garage')) { ?>
<style>
body{background:#EBEBEB;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>



