<?php
/*
Template Name: text-template
*/
?>
<?php get_header(); ?>
<section>

 <img src="<?php echo child_template_directory; ?>/img/con-images/concierge.png" class="con-logo"/>

<!-- concierge Left Side bar Starts -->
		<?php get_sidebar('concierge'); ?>

<div class="content details">
<h2 class="heading" style="visibility:hidden;"><span class="parent_cat">Pre Arrival</span></h2>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content','page' ); ?>

			<?php endwhile; // end of the loop. ?>
</div>
</section>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->

<?php get_footer(); ?>
