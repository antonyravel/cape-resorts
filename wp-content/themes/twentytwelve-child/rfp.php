<?php
	/*
	Template Name: RFP
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(888)944-1816</p>
    <img src="<?php echo child_template_directory; ?>/img/beach-shack-images/slider-image.png" alt="slider" class="slider-img" />
    <div class="banner-navigation">
      <!--<img src="<?php echo child_template_directory; ?>/img/congress-hall-images/corner-img.png" class="corner-img" alt="corner-img" />-->
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="#">home</a></li>
        <li><a href="#">reservations</a></li>
        <li><a href="<?php echo site_url(); ?>/beach-shack-specials">specials</a></li>
        <li><a href="<?php echo site_url(); ?>/guest-room">guest rooms</a></li>
        <li><a href="#">resort life</a></li>
        <li><a href="<?php echo site_url(); ?>/beach-shack-gallery">gallery</a></li>
        <li><a href="#">rusty nail</a></li>
        <li><a href="#">weddings &amp; events</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
    
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>205 beach avenue</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/beach-shack-directions/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/beach-shack-contact/">contact us</a></li>
    </ul>
    <ul class="social-icons">
      <li><a href="http://www.tripadvisor.com/Hotel_Review-g46341-d92337-Reviews-Congress_Hall-Cape_May_New_Jersey.html" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/vector-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://www.youtube.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/tube-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://www.facebook.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/facebook-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://twitter.com/CongressHall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/twitter-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://pinterest.com/caperesorts/" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/pin-icon.png" alt="corner-img" /></a></li>
    </ul>
    

    <p class="mailing-list"><input type="text" name="mailing" value="join our mailing list" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content">
    <div class="tabsDiv">
      <div id="tabs">
    <ul>
      <li><a href="#tabs-1">welcome</a></li>
      <li><a href="#tabs-2">events</a></li>
    </ul>
    <div class="all-tabs">
    <div id="tabs-1">
      
      <h3>INTRODUCTORY HEADER ABOUT BEACH SHACK<br> SECONDARY SUB-HEADER</h3>
      
      <p> Octavius libere insectat lascivius oratori, ut tremulus catelli imputat Medusa, quod fragilis zothecas senesceret quadrupei, et Octavius conubium santet parsimonia matrimonii, etiam tremulus oratori lucide corrumperet adlaudabilis concubine. Agricolae circumgrediet apparatus bellis. Catelli imputat umbraculi, ut rures corrumperet tremulus umbraculi.</p>     
      <p> quamquam apparatus bellis satis spinosus agnascor Aquae Sulis. Zothecas insectat fiducia suis. Catelli iocari saburre, semper apparatus bellis adquireret incredibiliter perspicax zothecas, et umbraculi lucide suffragarit. </p>
      <p>Octavius libere insectat lascivius oratori, ut tremulus catelli imputat Medusa, quod fragilis zothecas senesceret quadrupei, et Octavius conubium santet parsimonia matrimonii, etiam tremulus oratori lucide corrumperet adlaudabilis concubine. Agricolae circumgrediet apparatus bellis. Catelli imputat umbraculi, ut rures corrumperet tremulus umbraculi</p>
      <p>quamquam apparatus bellis satis spinosus agnascor Aquae Sulis. Zothecas insectat fiducia suis. Catelli iocari saburre, semper apparatus bellis adquireret incredibiliter perspicax zothecas, et umbraculi lucide suffragarit.</p>
    
    </div>
    
    <div id="tabs-2">
    <h3>EVENTS</h3>
      <p>For almost two centuries the hotel has offered hospitality to locals and visitors alike. It began life in 1816 as a simple boarding house for summer visitors to one of America's earliest seaside resorts. Its owner, Thomas H. Hughes, called his new boarding house "The Big House". The local people had other ideas, though. Convinced the building was far too large to ever be a success they nicknamed it "Tommy's Folly".</p>
      <p>
      In this first incarnation it was a quite different affair. Downstairs was a single room that served as the dining room for all the guests, who stayed in simply partitioned quarters on the two upper floors. The walls and woodwork were bare and supplies of provisions were at times unreliable.
      </p>
      <p>Guests were undeterred by the Spartan conditions and summer after summer the new hotel was packed to bursting. In 1828 Hughes had been elected to Congress and in honor of his new status his hotel was renamed Congress Hall. </p>
      <p>As Congress Hall's reputation grew, so did Cape May's. By the middle of the 19th Century Cape May had become a booming holiday destination, rivaling Saratoga and Newport for popularity. Congress Hall had doubled in size and was welcoming guests from around the region, but in 1878 the building was destroyed when a huge fire swept through 38 acres of Cape May's seafront.</p>
      <p>Within a year, the owners rebuilt the hotel, this time in brick rather than wood, and business blossomed once again. The hotel and Cape May proved so popular that they gained renown as a summer retreat for the nation's presidents. Ulysses S. Grant, Franklin Pierce, and James Buchanan all chose to vacation here. President Benjamin Harrison made Congress Hall his "summer White House" and conducted the affairs of state from the hotel.</p>
      <a href="#" class="learn">Read more</a>
    </div>
    
    
    </div>
</div>      
    </div>
    <div class="rt-bar">
      <div class="spring">
        <h2>clam diggers</h2>
        <h3>Kick off the summer with a good old fashioned clam toss and a stay at the Beach Shack.</h3>
        <h4></h4>
        <p><a href="#">learn more</a></p>
      </div>
      <div class="sea">
        <h2>one day sale</h2>
        <h3>Don't miss out on our One Day Sale on April 9th!</h3>
        <h4></h4>
        <p><a href="#">learn more</a></p>
      </div>
      <div class="retreat">
        <h2>clam-A-rama <sup>2013</sup> </h2>
        <h3></h3>
        <h4>Take part in our 2nd annual Clam-A-Rama on Sat, 5/11. The Clam Pitching Tournament will take place on the beach in front of the Beach Shack. Compete against your friends, family or some clam pitching pros! </h4>
        <p><a href="#">learn more</a></p>
      </div>
      <div class="resort-video">
        <a href="#">Watch the Cape Resorts Video.</a>
      </div>
    </div>
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('beach-shack')) { ?>
<style>
body{background:#E7E6E4;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>

