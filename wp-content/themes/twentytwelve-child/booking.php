<?php
	/*
	Template Name: Booking
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="bookingDiv">
    <ul class="dateTbas">
      <li><a href="#">select dates</a></li>
      <li><a href="#">select rooms &amp; rates</a></li>
      <li><a href="#">enhance yuor stay</a></li>
      <li><a href="#">enter guest details</a></li>
      <li><a href="#">confirm booking</a></li>
    </ul>
    <div class="dateDiv">
      <div class="dateLeft">
         <div class="dateLeftInner">
         <h3>hotel selection:</h3>
         <select class="se-Hotel chosen-select">
          <option value="">Select A Hotel</option>
          <option value="hotel1">HOTEL 1</option>
          <option value="hotel2">HOTEL 2</option>
          <option value="hotel3">HOTEL 3</option>
          <option value="hotel4">HOTEL 4</option>
         <select>
         <span class="hrline"></span>
        <h3>select your stay dates</h3>
        <h5>arrival</h5>
        <input type="text" name="arrival" class="arr-textbox">
       
          <h5>departure</h5>
          <input type="text" name="arrival" class="arr-textbox">
       
          <span class="hrline"></span>
          <h3>select rooms and guests</h3>
          <div><label>rooms</label>
          <select class= "chosen-select">
            <option>1</option>
            <option>2</option>
            <option>3</option>
          </select></div>
          <div class="adlt"><label>adults</label><select class= "chosen-select">
            <option>1</option>
            <option>2</option>
            <option>3</option>
          </select></div>
          <div class="chld"><label>children</label><select class= "chosen-select">
            <option>1</option>
            <option>2</option>
            <option>3</option>
          </select></div>  
          </div>
          <ul>
            <li><a href ="#">special codes</a></li>
            <li><a href ="#">refine your search</a></li>
          </ul>
          <div class="left-links">
            <a href="#">View, modify or cancel my reservation</a>
            <a href="#">Speak with an agent</a>
            <a href="#">Best Rate Promise</a>
          </div>
      </div>
      <div class="dateRight">
         
      </div>
    </div>
    
  </div>
  
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->

<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('booking')) { ?>
<style>

/*body{background: url("<?php echo site_url();?>/wp-content/themes/twentytwelve-child/img/con-images/CH-body-bg.png") no-repeat center top;}*/
.nav-top{visibility:hidden;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>

