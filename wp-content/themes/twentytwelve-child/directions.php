<?php
	/*
	Template Name: Directions
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(888)944-1816</p>
    <img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider9.png" alt="slider" class="slider-img" />
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul>
        <li><a href="#">home</a></li>
        <li><a href="#">reservations</a></li>
        <li><a href="#">specials</a></li>
        <li><a href="#">guest rooms</a></li>
        <li><a href="#">gallery</a></li>
        <li><a href="#">resort life</a></li>
        <li><a href="#">dining</a></li>
        <li><a href="#">sea spa</a></li>
        <li><a href="#">weddings &amp; events</a></li>
        <li><a href="#">meetings</a></li>
      </ul>
    </div>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li><a href="#">200 congress place</a></li>
      <li><a href="#">cape may, nj 08204</a></li>
      <li><a href="#">directions</a></li>
      <li><a href="#">contact us</a></li>
    </ul>
    <ul class="social-icons">
      <li><a href="http://www.tripadvisor.com/Hotel_Review-g46341-d92337-Reviews-Congress_Hall-Cape_May_New_Jersey.html" target="_blank"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/vector-smart.png" alt="corner-img" /></a></li>
      <li><a href="http://www.youtube.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/tube.png" alt="corner-img" /></a></li>
      <li><a href="https://www.facebook.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/facebook.png" alt="corner-img" /></a></li>
      <li><a href="https://twitter.com/CongressHall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/twitter.png" alt="corner-img" /></a></li>
      <li><a href="http://pinterest.com/caperesorts/" target="_blank"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/pinrest.png" alt="corner-img" /></a></li>
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="join our mailing list" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content drt">
    <h3>Directions</h3>
    <div class="directionDiv">
		    
		    <div class="direct-content">
		        <div class="dir-left">
		          <h2>congress hall</h2>
		          <h2>200 congress place</h2>
		          <h2>cape may, new jersy 08204</h2>
		          <h2>(888) 944-1816</h2>
		          <div class="find-us">
		            <h2>its easy for find us!</h2>
		            <p>Cape May is easily accessible from the Garden State Parkway and the Cape May-Lewes Ferry. The hotel is located right in the heart of the Historic District, right across the street from the beach and the Washington Street Mall</p>
		            <p><b>Valet Parking</b> is provided. Please pull up in front of the hotel and an attendant will assist you with the luggage and car.</p>
		          </div>
		        </div>
		        <div class="google-map">
		          <h2>from google maps:</h2>
		          <span>View Google Map &amp; Enter Your Starting Point</span>
		          <div class="specific">
		            <h2>from specific points</h2>
		            <a href="#">from the garden state parkway</a>
		            <span>approx.&nbsp;10 minutes</span>
		            <p>Take the Parkway to the end, Exit "0", to 109 South, Follow Route 109 over a large then a small bridge. This becomes Lafayette Street. Follow Lafayette Street through two traffic lights to the end. Lafayette will end at Jackson Street where there is a green liquor store called "Collier's". Turn left onto Jackson Street and immediately turn right onto Mansion Street. Continue to stop sign and turn left onto Perry Street. Make first right onto Congress Place and turn left into hotel reception area. </p>
		            <a href="#">from the cape ,ay lewes ferry</a>
		            <a href="#">from new york</a>
		            <a href="#">from washington/baltimore</a>
		            <a href="#">from philadelphia</a>
		            
		          </div>
		        </div>
		    </div>
    </div>
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('directions')) { ?>
<style>

.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>

