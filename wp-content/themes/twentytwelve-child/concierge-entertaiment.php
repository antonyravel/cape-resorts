<?php
/*
Template Name: concierge-entertainment
*/
?>
<?php get_header();
 
$args=array();
$slugname ='All';
$taxnomy = 'entertainment_category';
global $taxnomy;
$post_slug = urldecode($wp_query->query_vars['post_slug']);

if($post_slug) { 

	global $post;


global $post_type;
$post_type = 'entertainment';
global $taxnomy;
global $termarray;
global $parent;
$parent=29;


	$post_id = get_ID_by_slug($post_slug);
	
	$post = get_post( $post_id);
setup_postdata( $post );
	//print_r($post);


$terms = wp_get_post_terms( $post->ID, $taxnomy); 


foreach($terms as $term) {
	array_push($termarray, $term->term_id);
}

$args = array(
	'parent'                   => 29,
	'orderby'                  => 'menu_order',
	'taxonomy'                 => $taxnomy);

 $categories = get_categories( $args );

$catArray = array();
foreach($categories  as $category) {
	array_push($catArray, $category->term_id);
}


?>

<section>

 <img src="<?php echo child_template_directory; ?>/img/con-images/concierge.png" class="con-logo" alt="logo" />

<!-- concierge Left Side bar Starts -->
		<?php get_sidebar('concierge'); ?>

<div class="content details">

<?php $postterms = get_the_terms( $post->ID, $taxnomy ); 
	//print_r($postterms);
?>

	<h2 class="heading"><span class="parent_cat"><?php echo 'Entertainment'; ?>&nbsp;:&nbsp;</span>
<span class="child_cat">

	<?php
		$i=0; 
		if(is_array($postterms)):
		foreach ($postterms as $term) {
	if(in_array($term->term_id, $catArray)) {
		if($i==0) {
			echo ucwords($term->name); 
		}elseif($i > 0) {
			echo ' / '.ucwords($term->name); 
		}
		$i++;
		}
	}
	endif;

	$my_query = new WP_Query(array('p'=>$post->ID, 'post_type' => 'entertainment'));
	
	?>
	</span>
</h2>

			<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>

				<?php get_template_part( 'contentTemp','details' ); ?>

			<?php endwhile; // end of the loop. ?>
</div>
</section>



















<?php } else {
if(isset($_GET['cat']) && $_GET['cat'] !="") {

$slug = $_GET['cat'];

$cat_details = get_term_by( 'slug', $slug, $taxnomy, OBJECT );
$cat = $cat_details->term_id;
$slugname = $cat_details->name;
global $cat;
//print_r($cat_details);

$args['tax_query'] = array(
		array(
			'taxonomy' => $taxnomy,
			'field' => 'slug',
			'terms' => $slug
		));
}

?>
<section>
 <img src="<?php echo child_template_directory; ?>/img/con-images/concierge.png" class="con-logo" alt="Cape resorts Concierge"/>
<!-- concierge Left Side bar Starts -->
		<?php get_sidebar('concierge'); ?>

<div class="content activites">


<h2 class="heading"><span class="parent_cat">Entertainment&nbsp;:&nbsp;</span>

<span  class="child_cat"><?php echo $slugname; ?></span></h2>

<div>

<?php 
$args['post_type']='entertainment';
$args['posts_per_page']=-1;
$args['orderby'] = 'menu_order';

$activities = new WP_Query( $args );


while ( $activities->have_posts() ) : $activities->the_post();
 $image_id = get_post_thumbnail_id();
 	 $image_url = wp_get_attachment_image_src($image_id,'thumb_345_154', true);
?>
	<div>
	<img class="acti-img" src="<?php echo $image_url[0]; ?>" alt="act"/>
	<div class="act-content">
	<h3><a href="<?php the_permalink(); ?>" class="title_tag" data-id="<?php the_ID(); ?>"><?php the_title(); ?></a></h3>



	<?php $length = 80;
	 if(get_field('subtitle1')) {	?>
		

	<h4><?php the_field('subtitle1'); ?></h4>
	<?php }else {
		$length = $length+112;

	} ?>
	<?php if(get_field('subtitle_2')) {	?>

	<i><?php the_field('subtitle_2'); ?></i>
	<?php }else {
		$length = $length+56;

	} ?>

	<?php if(get_field('subtitle_3')) {	?>
	<span><?php the_field('subtitle_3'); ?></span>
	<?php }else {
		$length = $length+56;

	} ?>

		<p> <?php echo cr_trim_chars( get_the_content(), $length, 'false'); ?>...</p>




	<a href="<?php echo get_permalink(); ?>" class="learn" data-id="<?php the_ID(); ?>">Learn More</a>
	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="Share" st_title="<?php the_title(); ?>" st_url="<?php echo get_permalink(); ?>"></span></a>
	</div>
	</div>

<?php
	endwhile;
?>
</div>

</div>
</section>

<?php } ?>


</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
<?php get_footer(); ?>
