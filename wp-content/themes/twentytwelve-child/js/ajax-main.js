// New, awesome code!  :D
var $ = jQuery;
var page = {
   current: false,
   next: false,
   content_open: false,
   tempType:null,
   child: null,
   gChild: null,
   taxnomy: null,
   postType: null,
   slug: null,
   loader:null,
   calenderNav: false,
   loaded: false,	
   calender: {},
   details: {},
   aContainer:null,
	loop: 0
}

var ajax = {
    ajaxCall: function(params, content_area) {

				 ajax_id = params.slug;

				 if(page.calender.clicked) {
					if(params.month != 'ALL')
						ajax_id +='_'+params.month+'_'+params.day+'_'+params.year;
					

				}else if(params.cat) {

					ajax_id +='_'+params.cat;
				}
			else if(params.post_type) {

					ajax_id +='_'+params.post_type;
				} 
		   if($('#'+ajax_id).length ==0 ) { 

			page.loader.css({'opacity':1,'display':'block'});
			content_area.fadeTo('fast', 0);
			var pClass, ajax_id, rClass; 
			 $.getJSON(wpCalendarObj.ajaxurl, params).done(function (response) { 

				if(response.pageClass==='home') {
				rClass='activites details calender';
				content_area.removeClass(rClass);
					pClass=response.pageClass;
				}
				else if(response.pageClass==='details') {
				rClass='activites home calender';
				content_area.removeClass(rClass);
					pClass=response.pageClass;
				}
				else if(response.pageClass=="calender") {	
				rClass='activites home details';
				content_area.removeClass(rClass);
					pClass=response.pageClass;
										
				}
				else{
					rClass='home details calender';
					content_area.removeClass(rClass);
						pClass='activites';
				}
				 document.title = response.title;
				 content_area.addClass(pClass).html(response.data); 
				 ajax_id = params.slug;
				
				 if(page.calender.clicked) { 

					ajax_id +='_'+params.month+'_'+params.day+'_'+params.year;

				}else if(params.cat) {

					ajax_id +='_'+params.cat;
				} 
				else if(params.post_type) {
					ajax_id +='_'+params.post_type;
				} 
				page.calender.clicked= false;  page.details.clicked = false;
					var pIndex = $('.mainSidenav.active').index();
						cIndex = $('.mainSidenav.active').find('.active').index();
						
				 page.aContainer.append('<div id="'+ajax_id+'" data-class="'+pClass+'" data-rClass="'+rClass+'" data-pIndex="'+pIndex+'" data-cIndex="'+cIndex+'" data-title="'+response.title+'">'+response.data+'</div>')

				page.loader.fadeTo('fast', 0, function() {
				ReinitializeAddThis();
				 page.loader.hide(); content_area.fadeTo('fast', 1); });

				var res=  content_area.find(".jqueryCycle").responsiveSlides({
					pager: true,
					speed: 500,
					maxwidth: 943
				  }); 
             });

		} else { 
			
			content_area.fadeTo('fast', 0, function() {
				 var newEl = $('#'+ajax_id),
					addClass = newEl.attr('data-class'),
					removeClass = newEl.attr('data-rClass'),
					newTitle = newEl.attr('data-title');
					pIndex = newEl.attr('data-pIndex');
					cIndex = newEl.attr('data-cIndex');
					
				document.title = newTitle;  

			 	content_area.removeClass(removeClass).addClass(addClass).html(newEl.html());
	var res=  content_area.find(".jqueryCycle").responsiveSlides({
					pager: true,
					speed: 500,
					maxwidth: 943
				  }); 
				content_area.fadeTo('fast', 1);  page.calender.clicked= false; page.details.clicked = false;


			});

		}

			}
    }


var debugging = true;

function debug(msg) {
   if (debugging) {
      console.log(msg);
   }
}


 function ReinitializeAddThis(){ 
      if (stButtons){stButtons.locateElements(); }
   }



$(document).ready(function () {

	      var content_area=$('.content'),
    		con_menu = $(".aside-content a:not(.ui-state-default, .download_link)"),
		  content_width= content_area.width();
		  page.loader=$('.ajax-loader');
		  page.aContainer=$('#ajax_content');

       	con_menu.bind('click', function () {
			var el=$(this); 
			page.tempType=el.attr('data-type');
			page.taxnomy=el.attr('data-taxnomy');
			page.postType=el.attr('data-post');

			if(!el.hasClass('active')) {
				con_menu.parent().removeClass('active');
				if(el.parents('.mainSidenav').length > 0 && el.parent('.mainSidenav').length == 0) { 
						el.parents('.mainSidenav').addClass('active')
				}else {
					//el.parent().find('span').last().addClass('active');
				} 
				el.parent().addClass('active');
			} 
				page.calendernav=false;
   			page.loaded = true;

         }); 
		$('#menu-menu .active li, #menu-menu .active').each(function() { 
			$(this).bind('click', function() { 
				var el=$(this), link = $(this).find('a');  
			page.tempType=link.attr('data-type');
			page.taxnomy=link.attr('data-taxnomy');
			page.postType=link.attr('data-post');
				
		if(!el.hasClass('active')) { 			
			
				$('#menu-menu .active li').removeClass('active');
			if(page.tempType=='all-posts')
				$('.aside-content').find('.mainSidenav > a[data-post="'+page.postType+'"]').trigger('click');
			else if(page.tempType=="page")
				$('.aside-content').find('.mainSidenav > a[data-type="page"]').trigger('click');
			else if(page.tempType=="con-home")
				$('.aside-content').find('.mainSidenav > a[data-type="con-home"]').trigger('click');
			else{
page.loaded = true;
page.calender={
				action: "concierge_page",						
				tempType: 'calender',
				slug: 'concierge-calender',
				month:'ALL',
				day:'ALL',
				year:'ALL',
				dn:'ALL',
				clicked: true
		}
		
	$.address.value('dailycalender');

				//$('.WP-Cal-popup').first().trigger('click');
			}
				el.addClass('active');
			} 
				return false;

			}); });



		content_area.on('click', '.learn:not(.extlink), .caln_learn, .nav-previous a, .nav-next a, .title_tag', function(e) { 
			e.preventDefault();
			var urlParameter = this.href.split('/'),
				post_id = $(this).attr('data-id'),
				emptyVal = urlParameter.pop(),
				slug= urlParameter.pop();
				post_type= urlParameter.pop(),
				parent_cat = content_area.find('.parent_cat:visible').text(),
				child_cat = content_area.find('.child_cat:visible').text()

			 page.details={
				action: "concierge_page",						
				tempType: 'post-details',
				slug: slug,
				post_id:post_id,
				post_type: post_type,
				clicked: true,
				parent_cat:parent_cat,
				child_cat: child_cat
		}
		page.loaded=true;
		$.address.value('/'+post_type+'/'+slug+'/');

		});

      $.address.state('/concierge/capemay/').init(function () {

            // Initializes the plugin
            con_menu.address();

         }).internalChange(function (event) { 
			var params, ajax_id;


            if (page.loaded === true) { 

               var uri = event.value;
			
			if(page.calender.clicked === true) {
				params = page.calender; 

			}else if(page.details.clicked===true) { 
				params = page.details; 
				
			}
			else {

				page.slug = $.address.path().split('/');
				page.child=$.address.parameter('cat');
	
				if(page.slug.length == 3) { 
				page.slug = page.slug[1] 
				} else { 
				page.slug = page.slug[1] 
				} 
			
				params = {
					action: "concierge_page",						
					tempType: page.tempType,
					cat: page.child,
					gChild: page.gChild,
					postType: page.postType,
					taxnomy: page.taxnomy,
					slug: page.slug
				} 
			}  	 if(!params.slug) {
						params.slug='concierge_home';

					} 
				ajax.ajaxCall(params, content_area)

            } 
         }).externalChange(function(e) {  


  if(window.location.href.indexOf("concierge") > -1) {

     if (page.loaded === true) {	
			var ajax_id;

			page.slug = $.address.path().split('/');
			
			if(page.slug.length == 3) {
				page.slug = page.slug[1] 
			} else {
				page.slug = page.slug[2]+'_'+page.slug[1];
			} 
			ajax_id = page.slug; 
			page.child=$.address.parameter('cat');
			page.calender.day = $.address.parameter('dn');
			if(page.child) { 
					ajax_id +='_'+page.child;

			}
			else if(page.calender.day) {
					ajax_id +='_'+$.address.parameter('mo')+'_'+$.address.parameter('d')+'_'+$.address.parameter('y');

			} 
			if(ajax_id=='' || !ajax_id) {
				ajax_id='concierge_home';
				}
				/*if($.address.path().indexOf("concierge") == '-1') {

					var a = $.address.path().split('/');
					a.pop();
					a.pop();
					var d = a.pop();
					ajax_id+='_'+d;
				} */ 
			 
		if($('#'+ajax_id).length ==0) { 
				window.location.href = window.location.href; 
				} else {
			content_area.fadeTo('fast', 0, function() {
				var newEl = $('#'+ajax_id),
					addClass = newEl.attr('data-class'),
					removeClass = newEl.attr('data-rClass'),
					newTitle = newEl.attr('data-title');
					pIndex = newEl.attr('data-pIndex');
					cIndex = newEl.attr('data-cIndex');
					$('#side-accordian').find('.active').removeClass('active'); 
					$('.mainSidenav').eq(pIndex).addClass('active').find('span').eq(parseInt(cIndex)+1).addClass('active');
				document.title = newTitle;  
				
$('#side-accordian').accordion('destroy'); $('#side-accordian').accordion({ heightStyle: "content", active: parseInt(pIndex) });
			 	content_area.removeClass(removeClass).addClass(addClass).html(newEl.html());
				content_area.fadeTo('fast', 1); 

			});
			}
			page.loader.show();


	} else { 
		var addrPath = $.address.path();  
		page.loop++;
		if(page.loop > 1) {  

			var ajax_id;

			page.slug = $.address.path().split('/'); 

			var lastChar = page.slug[page.slug.length-1];
			if(lastChar =='')
				var len =3;
			else
				var len =2;

			if($.address.path() == '/concierge/capemay' || $.address.path() =='/') { 
				page.slug = 'concierge_home';
			}

			else if(page.slug.length == len) {
				page.slug = page.slug[1] 
			} 
	

		else {
				page.slug = page.slug[2]+'_'+page.slug[1] 
			} 
			ajax_id = page.slug; 
			if(ajax_id=='' || !ajax_id) {
				ajax_id='concierge_home';
				}
			page.child=$.address.parameter('cat');
			page.calender.day = $.address.parameter('dn');
			if(page.child) {
					ajax_id +='_'+page.child;

			}
			else if(page.calender.day) {
					ajax_id +='_'+$.address.parameter('mo')+'_'+$.address.parameter('d')+'_'+$.address.parameter('y');

			}
//			alert(ajax_id);
			if(content_area.hasClass()) {
				rClass='activites details calender';
				pClass='';
			}
			else if(content_area.hasClass('home')) {
				rClass='activites details calender';
				pClass='home';
			}
			else if(content_area.hasClass('details')) {
				rClass='activites home calender';
				pClass='details';
			}
			else if(content_area.hasClass('calender')) { 
				rClass='activites home details';
				pClass='calender';

			} else {
				rClass='home details calender';
				pClass='activites';
			}
			var pIndex = $('.mainSidenav.active').index();
			cIndex = $('.mainSidenav.active').find('.active').index();
 				page.aContainer.append('<div id="'+ajax_id+'" data-class="'+pClass+'" data-rClass="'+rClass+'" data-title="'+document.title+'" data-pIndex="'+pIndex+'" data-cIndex="'+cIndex+'">'+content_area.html()+'</div>')
		}

	} 
}

		});

    var res= $(".jqueryCycle").responsiveSlides({
        pager: true,
        speed: 500,
        maxwidth: 943
      }); 
   });
