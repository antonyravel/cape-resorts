var EM = {"firstDay":"1","locale":"en","dateFormat":"mm-dd-yy","ui_css":"http://sritsprojects.com/stage/caperesorts/wp-content/plugins/events-manager/includes/css/ui-lightness.css","show24hours":"","is_ssl":""};



jQuery(document).ready( function($){

    /*var form = $("form[name='post']"); var field1 =  $(".em-date-start ");
var field2 =  $(".em-date-end");
    $(form).find("input[type='submit']").click(function(e){
		if(form.length>0 && field1.length > 0) {  
			if(field1.val() == "" || field2.val() == "") { 
				alert('Please select start and end dates')
				 setTimeout(function(){$('.spinner').hide(); $(this).removeClass('button-primary-disabled'); },500)
 			e.preventDefault(); 
			} 
	
		}
    }); */








	var load_ui_css = false; //load jquery ui css?
	/* Time Entry */
	$('#start-time').each(function(i, el){
		$(el).addClass('em-time-input em-time-start').next('#end-time').addClass('em-time-input em-time-end').parent().addClass('em-time-range');
	});
	//if( $(".em-time-input").length > 0 ){
		em_setup_timepicker('body');
	//}
	/* Calendar AJAX */
	$('.em-calendar-wrapper a').unbind("click");
	$('.em-calendar-wrapper a').undelegate("click");
	$('.em-calendar-wrapper').delegate('a.em-calnav, a.em-calnav', 'click', function(e){
		e.preventDefault();
		$(this).closest('.em-calendar-wrapper').prepend('<div class="loading" id="em-loading"></div>');
		var url = em_ajaxify($(this).attr('href'));
		$(this).closest('.em-calendar-wrapper').load(url, function(){$(this).trigger('em_calendar_load');});
	} );


		

$('.date-pick')
					.datePicker(
						{
							createButton:false,
							displayClose:true,
							closeOnSelect:false,
							selectMultiple:true
							
						}
					)
					.bind(
						'click',
						function()
						{
							$(this).dpDisplay();
							this.blur();
							return false;
						}
					)
					.bind(
						'dateSelected',
						function(e, selectedDate, $td, state)
						{
							
						}
					)
					.bind(
						'dpClosed',
						function(e, selectedDates)
						{
						var hiddenField = $('#em-date-multiple'), val, con;
						hiddenField.val('');
						$.each( selectedDates, function( key, value ) {

							val=hiddenField.val();
							if(val=="") {
								con = '';
							}else { con = ','; }
							
							hiddenField.val(hiddenField.val()+con+$.datepicker.formatDate('yy-mm-dd', value));

						});

							
						}
					);



Date.format = 'yyyy-mm-dd';
var mul_dates =$('#em-date-multiple').val();
if(mul_dates) { 
var date_array = mul_dates.split(',');

$.each( date_array, function( key, value ) { 
							if(value)
							$('.date-pick').dpSetSelected(value);

						});

}


  //$('.date-pick').dpSetSelected('02/01/2014');

	//Datepicker
	if( $('.em-date-single, .em-date-range, #em-date-start').length > 0 ){
		if( EM.locale != 'en' && EM.locale_data ){
			$.datepicker.setDefaults(EM.locale_data);
		}
		load_ui_css = true;
		
		//initialize legacy start/end dates, makes the following code compatible
		if( $('#em-date-start').length > 0 ){
			$('#em-date-start').addClass('em-date-input').parent().addClass('em-date-range');
			$("#em-date-start-loc").addClass('em-date-input-loc em-date-start');
			$('#em-date-end').addClass('em-date-input');
			$("#em-date-end-loc").addClass('em-date-input-loc em-date-end');
		}
		if( $(".em-ticket-form, #em-tickets-form").length > 0 ){
			$(".em-ticket-form, #em-tickets-form .start").addClass('em-date-input').parent().addClass('em-date-single');
			$(".em-ticket-form, #em-tickets-form .start-loc").addClass('em-date-input-loc').each(
				function(i,el){ $(el).prev('.start').insertAfter(el); 
			}); //reverse elements for comapatability
			$(".em-ticket-form, #em-tickets-form .end").addClass('em-date-input').parent().addClass('em-date-single');
			$(".em-ticket-form, #em-tickets-form .end-loc").addClass('em-date-input-loc').each(
				function(i,el){ $(el).prev('.end').insertAfter(el);
			}); //reverse elements for comapatability
		}
		em_setup_datepicker('body');
	}
	if( load_ui_css && EM.ui_css ){
		var script = document.createElement("link");
		script.id = 'jquery-ui-css';
		script.rel = "stylesheet";
		script.href = EM.ui_css;
		document.body.appendChild(script);
	}
	
	//previously in em-admin.php
	function updateIntervalDescriptor () { 
		$(".interval-desc").hide();
		var number = "-plural";
		//if ($('input#recurrence-interval').val() == 1 || $('input#recurrence-interval').val() == "")
		//number = "-singular";

		var descriptor = "span#interval-"+$("select#recurrence-frequency").val()+number;
		
		$(descriptor).show(); 
	}
	function updateIntervalSelectors () {
		$('p.alternate-selector').hide();   
		$('p#'+ $('select#recurrence-frequency').val() + "-selector").show();
	}
	function updateShowHideRecurrence () {
		if( $('input#event-recurrence').attr("checked")) {
			$("#event_recurrence_pattern").fadeIn();
			$("#event-date-explanation").hide();
			$("#recurrence-dates-explanation").show();
			$("h3#recurrence-dates-title").show();
			$("h3#event-date-title").hide();     
		} else {
			$("#event_recurrence_pattern").hide();
			$("#recurrence-dates-explanation").hide();
			$("#event-date-explanation").show();
			$("h3#recurrence-dates-title").hide();
			$("h3#event-date-title").show();   
		}
	}		 
	$("#recurrence-dates-explanation").hide();
	$("#date-to-submit").hide();
	$("#end-date-to-submit").hide();
	
	$("#localised-date").show();
	$("#localised-end-date").show();
	
	$('#em-wrapper input.select-all').change(function(){
	 	if($(this).is(':checked')){
			$('input.row-selector').attr('checked', true);
			$('input.select-all').attr('checked', true);
	 	}else{
			$('input.row-selector').attr('checked', false);
			$('input.select-all').attr('checked', false);
		}
	}); 
	
	updateIntervalDescriptor(); 
	updateIntervalSelectors();
	updateShowHideRecurrence();
	$('input#event-recurrence').change(updateShowHideRecurrence);
	   
	// recurrency elements   
	$('input#recurrence-interval').keyup(updateIntervalDescriptor);
	$('select#recurrence-frequency').change(updateIntervalDescriptor);
	$('select#recurrence-frequency').change(updateIntervalSelectors);

	
});

function em_setup_datepicker(wrap){	
	wrap = jQuery(wrap);
	//default picker vals
	var datepicker_vals = { altFormat: "yy-mm-dd", changeMonth: true, changeYear: true, firstDay : EM.firstDay };
	if( EM.dateFormat != ''){ 
		datepicker_vals.dateFormat = EM.dateFormat;
	}
	jQuery(document).triggerHandler('em_datepicker', datepicker_vals);
	
	//apply datepickers
	dateDivs = wrap.find('.em-date-single, .em-date-range');
	if( dateDivs.length > 0 ){
		//apply datepickers to elements
		dateDivs.find('input.em-date-input-loc').each(function(i,dateInput){
			//init the datepicker
			var dateInput = jQuery(dateInput);
			var dateValue = dateInput.nextAll('input.em-date-input').first();
			var dateValue_value = dateValue.val();
			dateInput.datepicker(datepicker_vals);
			dateInput.datepicker('option', 'altField', dateValue);
			//now set the value 

			if( dateValue_value ){ if(dateValue_value && dateValue_value != '0000-00-00') {
				var this_date_formatted = jQuery.datepicker.formatDate( EM.dateFormat, jQuery.datepicker.parseDate('yy-mm-dd', dateValue_value) );
				//alert(this_date_formatted);
//var parts = this_date_formatted.split('/');
//var us_date = parts[2]+'/'+parts[1]+'/'+parts[0]; //alert(us_date);
//alert(Date.parseExact(dateValue_value, 'yy-mm-dd').toString('yyyy-MM-dd'));
				dateInput.val(this_date_formatted); //alert(us_date);
				dateValue.val(dateValue_value); }
			}
			//add logic for texts
			dateInput.change(function(){
				if( jQuery(this).val() == '' ){
					jQuery(this).nextAll('.em-date-input').first().val('');
				}
			});
		});
		//deal with date ranges
		dateDivs.filter('.em-date-range').find('input.em-date-input-loc').each(function(i,dateInput){
			//finally, apply start/end logic to this field
			dateInput = jQuery(dateInput);
			if( dateInput.hasClass('em-date-start') ){
				dateInput.datepicker('option','onSelect', function( selectedDate ) { 
					//get corresponding end date input, we expect ranges to be contained in .em-date-range with a start/end input element
					var startDate = jQuery(this); 
					var endDate = startDate.parents('.em-date-range').find('.em-date-end').first();
					if( startDate.val() > endDate.val() && endDate.val() != '' ){ 
						endDate.datepicker( "setDate" , selectedDate );
					} 
					endDate.datepicker( "option", 'minDate', selectedDate );
				});
			}else if( dateInput.hasClass('em-date-end') ){
				var startInput = dateInput.parents('.em-date-range').find('.em-date-start').first();
				if( startInput.val() != '' ){
					dateInput.datepicker('option', 'minDate', startInput.val());
				}
			}
		});
	}
}

function em_setup_timepicker(wrap){
	wrap = jQuery(wrap);

	wrap.find("#event_dates").change(function() {

	var multiple = jQuery(this);
		if( multiple.is(':checked') ){
			jQuery('#multiple_dates').show();
			jQuery('#recurring-event').hide();

			
					
		}else{
			
		jQuery('#multiple_dates').hide();
			jQuery('#recurring-event').show();
		}

	}).trigger('change');


/*
	wrap.find(".em-time-input").timePicker({
		show24Hours: EM.show24hours == 1,
		step:15
	});
	
	// Keep the duration between the two inputs.
	wrap.find(".em-time-range input.em-time-start").each( function(i, el){
		jQuery(el).data('oldTime', jQuery.timePicker(el).getTime());
	}).change( function() {
		var start = jQuery(this);
		var end = start.nextAll('.em-time-end');
		if (end.val()) { // Only update when second input has a value.
		    // Calculate duration.
			var oldTime = start.data('oldTime');
		    var duration = (jQuery.timePicker(end).getTime() - oldTime);
		    var time = jQuery.timePicker(start).getTime();
		    if( jQuery.timePicker(end).getTime() >= oldTime ){
			    // Calculate and update the time in the second input.
			    jQuery.timePicker(end).setTime(new Date(new Date(time.getTime() + duration)));
			}
		    start.data('oldTime', time); 
		}
	});
	// Validate.
	wrap.find(".em-time-range input.em-time-end").change(function() {
		var end = jQuery(this);
		var start = end.prevAll('.em-time-start');
		if( start.val() ){
			if( jQuery.timePicker(start).getTime() > jQuery.timePicker(this).getTime() && ( jQuery('.em-date-end').val().length == 0 || jQuery('.em-date-start').val() == jQuery('.em-date-end').val() ) ) { end.addClass("error"); }
			else { end.removeClass("error"); }
		}
	});
	//Sort out all day checkbox
	wrap.find('.em-time-range input.em-time-all-day').change(function(){
		var allday = jQuery(this);
		if( allday.is(':checked') ){
			allday.siblings('.em-time-input').css('background-color','#ccc');
		}else{
			allday.siblings('.em-time-input').css('background-color','#fff');
		}
	}).trigger('change'); */
}



/* jQuery timePicker - http://labs.perifer.se/timedatepicker/ @ http://github.com/perifer/timePicker commit 100644 */
(function(e){function t(t,n,r,i){t.value=e(n).text();e(t).change();if(!navigator.userAgent.match(/msie/i)){t.focus()}r.hide()}function n(e,t){var n=e.getHours();var i=t.show24Hours?n:(n+11)%12+1;var s=e.getMinutes();return r(i)+t.separator+r(s)+(t.show24Hours?"":n<12?" AM":" PM")}function r(e){return(e<10?"0":"")+e}function i(e,t){return typeof e=="object"?o(e):s(e,t)}function s(e,t){if(e){var n=e.split(t.separator);var r=parseFloat(n[0]);var i=parseFloat(n[1]);if(!t.show24Hours){if(r===12&&e.indexOf("AM")!==-1){r=0}else if(r!==12&&e.indexOf("PM")!==-1){r+=12}}var s=new Date(0,0,0,r,i,0);return o(s)}return null}function o(e){e.setFullYear(2001);e.setMonth(0);e.setDate(0);return e}e.fn.timePicker=function(t){var n=e.extend({},e.fn.timePicker.defaults,t);return this.each(function(){e.timePicker(this,n)})};e.timePicker=function(t,n){var r=e(t)[0];return r.timePicker||(r.timePicker=new jQuery._timePicker(r,n))};e.timePicker.version="0.3";e._timePicker=function(r,u){var a=false;var f=false;var l=i(u.startTime,u);var c=i(u.endTime,u);var h="selected";var p="li."+h;e(r).attr("autocomplete","OFF");var d=[];var v=new Date(l);while(v<=c){d[d.length]=n(v,u);v=new Date(v.setMinutes(v.getMinutes()+u.step))}var m=e('<div class="time-picker'+(u.show24Hours?"":" time-picker-12hours")+'"></div>');var g=e("<ul></ul>");for(var y=0;y<d.length;y++){g.append("<li>"+d[y]+"</li>")}m.append(g);m.appendTo("body").hide();m.mouseover(function(){a=true}).mouseout(function(){a=false});e("li",g).mouseover(function(){if(!f){e(p,m).removeClass(h);e(this).addClass(h)}}).mousedown(function(){a=true}).click(function(){t(r,this,m,u);a=false});var b=function(){if(m.is(":visible")){return false}e("li",m).removeClass(h);var t=e(r).offset();m.css({top:t.top+r.offsetHeight,left:t.left});m.show();var i=r.value?s(r.value,u):l;var a=l.getHours()*60+l.getMinutes();var f=i.getHours()*60+i.getMinutes()-a;var p=Math.round(f/u.step);var d=o(new Date(0,0,0,0,p*u.step+a,0));d=l<d&&d<=c?d:l;var v=e("li:contains("+n(d,u)+")",m);if(v.length){v.addClass(h);m[0].scrollTop=v[0].offsetTop}return true};e(r).focus(b).click(b);e(r).blur(function(){if(!a){m.hide()}});e(r)["keydown"](function(n){var i;f=true;var s=m[0].scrollTop;switch(n.keyCode){case 38:if(b()){return false}i=e(p,g);var o=i.prev().addClass(h)[0];if(o){i.removeClass(h);if(o.offsetTop<s){m[0].scrollTop=s-o.offsetHeight}}else{i.removeClass(h);o=e("li:last",g).addClass(h)[0];m[0].scrollTop=o.offsetTop-o.offsetHeight}return false;break;case 40:if(b()){return false}i=e(p,g);var a=i.next().addClass(h)[0];if(a){i.removeClass(h);if(a.offsetTop+a.offsetHeight>s+m[0].offsetHeight){m[0].scrollTop=s+a.offsetHeight}}else{i.removeClass(h);a=e("li:first",g).addClass(h)[0];m[0].scrollTop=0}return false;break;case 13:if(m.is(":visible")){var l=e(p,g)[0];t(r,l,m,u)}return false;break;case 27:m.hide();return false;break}return true});e(r).keyup(function(e){f=false});this.getTime=function(){return s(r.value,u)};this.setTime=function(t){r.value=n(i(t,u),u);e(r).change()}};e.fn.timePicker.defaults={step:30,startTime:new Date(0,0,0,0,0,0),endTime:new Date(0,0,0,23,30,0),separator:":",show24Hours:true}})(jQuery)
