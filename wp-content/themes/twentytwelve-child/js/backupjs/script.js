var $=jQuery;
var row =0;
var emp_row=0;
var ww=window.innerWidth
|| document.documentElement.clientWidth
|| document.body.clientWidth;
	var content_div;

$(document).ready(function() {
	
	content_div=$('.content');
	resizing();
	var site_url = $('#siteurl').val();

	$("#menu-menu li a").each(function() {
		if ($(this).next().length > 0) {
			$(this).addClass("parent");
		};
	})
	
	$(".toggleMenu").click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$("#menu-menu").toggle();
	});
	adjustMenu();

	 var collapsed = $("#side-accordian li.mainSidenav.active").index();
	
		if(collapsed < 0)  
			collapsed=1; 

	$("#side-accordian").accordion({heightStyle: "content", active: collapsed});

/* Header reservation DatePicker */



	$( "#arrivaldate" ).datepicker({
		showOn: "button",
		buttonImage: site_url+"/wp-content/themes/twentytwelve-child/img/con-images/date-bg.png",
		buttonImageOnly: true,
		dateFormat: 'mm/dd/yy',
		showAnim:'slideDown',
		minDate: 0
	});


$( "#dp1373363262549" ).datepicker({
		dateFormat: 'mm/dd/yy',
		showAnim:'slideDown',
		minDate: 0
	});



	$('.pressnews').click(function(e) {
		e.preventDefault();
		$('#newstitle').text($(this).text());
		$('.heading').hide();
		$('.imgtitle').hide();
		$(this).closest("li").addClass('odd Innerodd');
		$('#presstitle').show();
		$(this).closest(".imgtitle").show();
		$(this).closest("li").find('.contarea').show();

	});

	$('#backlink').click(function(e) {
		e.preventDefault();
		$('#presstitle').hide();
		$('.contarea').hide();
		$('.press-container li').removeClass('odd');
		$('.press-container li').removeClass('Innerodd');
		$('.press-container li:even').addClass('odd');
		$('.heading').show();
		$('.imgtitle').show();
	});
/* Reservation Form */

var reserve={
		a: $('.reserve_button')
}

reserve.a.bind('click', function() {

	var form = $('#reservations'),
			hotel=$("#head_hotel", form).val(),
			arrivaldate=$("#arrivaldate", form).val(),
			nights=$("#nights", form).val(),
			adults=$("#adults", form).val(),
			children=$("#children", form).val();
	var params='';
	if(!hotel || hotel=="") {
		alert("Please select hotel"); 
		return false; }
	else {
		params += 'Hotel='+ hotel
	} 
	if(arrivaldate) {
		params+='&Arrive='+arrivaldate
	}
	if(nights) {
			params+='&Nights='+nights
		}
	if(adults) {
			params+='&Adult='+adults
		}
	if(children) {
			params+='&Child='+children
		}
	window.location= 'https://reservations.synxis.com/lbe/rez.aspx?'+params;

});

/* reservation Form Ends */

    var res= $("#jqueryCycle").responsiveSlides({
        pager: true,
        speed: 500,
        maxwidth: 943
      });
}); /* Document ready Function Ends Here */

/* edit menu start here */
$(document).ready(function() {
	$('.view_gallery').click(function(){
$('.cboxElement').click();
	
	});
	content_div=$('.content');
	resizing();
	var site_url = $('#siteurl').val();

	$(".banner-nav-li li a").each(function() {
		if ($(this).next().length > 0) {
			$(this).addClass("parent");
		};
	})
	
	$(".banner-toggle-menu").click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$(".banner-nav-li").toggle();
	});


	adjustMenu1();

	var collapsed = $("#side-accordian li.mainSidenav.active").index();

	if(collapsed < 0)  
		collapsed=1; 

	$("#side-accordian").accordion({heightStyle: "content", active: collapsed});


	/* Chosen */

	$(".chosen-select").chosen();



/* Careers page Javascript */

$('.job-table .show').unbind('click').bind('click', function(e){ 
	e.preventDefault(); 

	var hiddenEl = $(this).next();

	if(hiddenEl.is(':visible')) 
		hiddenEl.hide();
	else 
		hiddenEl.show();

});

$('.upload input').bind('change', function(){ 
  	 $(this).parents('.upload').find('.imagename').text(this.value);
});


 $('.file_submit').on('submit', function(e)
    {   e.preventDefault();
		this.action = ajaxLoad.Url;
        $el =$(this);

		var resume = $el.parent().parent().find('.upload-resume input').val();

		var cover = $el.parent().parent().find('.upload-cover input').val();

      var exts = ['doc','docx','rtf','odt'];
      // first check if file field has any value
      if ( resume ) {
        // split file name at dot
        var get_ext = resume.split('.');
        // reverse name to check extension
        get_ext = get_ext.reverse();
        // check file type is valid as given in 'exts' array
        if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
          //alert( 'Allowed extension!' ); return false;
        } else {
          alert( 'Invalid file!' ); return false;
        }
      } 

 if ( cover ) {
        // split file name at dot
        var get_ext = cover.split('.');
        // reverse name to check extension
        get_ext = get_ext.reverse();
        // check file type is valid as given in 'exts' array
        if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
          //alert( 'Allowed extension!' ); return false;
        } else {
          alert( 'Invalid file!' ); return false;
        }
      } 

		if(resume=="") {
			alert('Please upload Your resume');
			return false;
		}
        $el.find('file').attr('disabled', 'disabled'); 
        $el.find('.output').html('<span style="padding:10px">Uploading...</span>');
        $el.ajaxSubmit({
        target: '.output',
        success:  function() {
			$el.resetForm();  // reset form 
			$('.upload').find('.imagename').eq(0).text('Browse to upload Resume');
			$('.upload').find('.imagename').eq(1).text('Browse to upload Cover letter');
			$el.find('file').removeAttr('disabled'); //enable submit button
		}

        });
    });





$('#add_school').bind('click', function(e) {
e.preventDefault();
row++;

var newField = '<tr id="row_'+row+'"><td><input type="text" name="jobman-school[name][]" value=""></td><td><input type="text" name="jobman-school[location][] value=""></td><td><input type="text" name="jobman-school[graduate][] value=""></td><td><input type="text" name="jobman-school[type][]" value=""></td><td><button title="Remove Education" data-id="row_'+row+'" onclick="remove_field(this);return false;"></button></td></tr>';

	$("#schoolTable tr").last().after(newField);
	$('#total_school').val(row);		
	});


$('#add_employee').bind('click', function(e) {
e.preventDefault();
	emp_row++;

var field ='<table class="job-apply-table table5 employers" id="emp_'+emp_row+'"><tbody><tr><th scope="row">Name of Employer</th><td><input type="text" name="jobman-emp[name][]" value=""></td></tr><tr class="row5 totalrow45 field50 odd"><th scope="row">Phone</th><td><input type="text" name="jobman-emp[phone][]" value=""></td></tr><tr class="row6 totalrow46 field51 even"><th scope="row">Number</th><td><input type="text" name="jobman-emp[number][]" value=""></td></tr><tr class="row7 totalrow47 field52 odd"><th scope="row">Employment Dates: Start</th><td> <input type="text" class="datepicker" name="jobman-emp[start][]" value="" id="dp1373363262552"></td></tr><tr class="row8 totalrow48 field53 even"><th scope="row">End</th><td> <input type="text" class="datepicker" name="jobman-emp[end][]" value="" id="dp1373363262553"></td></tr><tr class="row9 totalrow49 field54 odd"><th scope="row">Rate of Pay: Start</th><td> <input type="text" class="datepicker" name="jobman-emp[paystart][]" value="" id="dp1373363262554"></td></tr><tr class="row10 totalrow50 field55 even"><th scope="row">End</th><td> <input type="text" class="datepicker" name="jobman-emp[payend][]" value="" id="dp1373363262555"></td></tr><tr class="row11 totalrow51 field56 odd"><th scope="row">Supervisor’s Name &amp; Title</th><td><input type="text" name="jobman-emp[supervisor][]" value=""></td></tr><tr><th colspan="2">Address</th></td></tr><tr class="row1 totalrow52 field58 odd"><th scope="row">street</th><td><input type="text" name="jobman-emp[address][street][]" value=""></td></tr><tr class="row2 totalrow53 field59 even"><th scope="row">city</th><td><input type="text" name="jobman-emp[address][city][]" value=""></td></tr><tr class="row3 totalrow54 field60 odd"><th scope="row">State</th><td><input type="text" name="jobman-emp[address][state][]" value=""></td></tr><tr class="row4 totalrow55 field61 even"><th scope="row">zip</th><td><input type="text" name="jobman-emp[address][zip][]" value=""></td></tr><tr class="row5 totalrow56 field62 odd"><th scope="row">Reasons for Leaving</th><td> <textarea name="jobman-emp[reason][]"></textarea></td></tr><tr class="row6 totalrow57 field63 even"><th scope="row">Your Titles &amp; Duties</th><td> <textarea name="jobman-emp[duty][]"></textarea></td></tr><tr class="row7 totalrow58 field64 odd"><th scope="row">Lapse of time?</th><td><input type="text" name="jobman-emp[lapse][]" value=""></td></tr><tr><td colspan="2"><button title="Remove Education" data-id="emp_'+emp_row+'" onclick="remove_employee(this);return false;"></button></td></tr></tbody></table>';


	$(".employers").last().after(field);
	$('#total_employee').val(emp_row);	





});

}); // Document ready End



function remove_field(el) { 
	
	$('#'+$(el).attr('data-id')).remove();
		//$('#total_school').val(row);	

}
function remove_employee(el) {

	$('#'+$(el).attr('data-id')).remove();
		//$('#total_school').val(emp_row);	

}

/* edit menu end here */

$(window).bind('load', function() {

	$('#ajax-pageLoader').hide();

	$('.wrapper').fadeTo('fast', 1);

});

$(window).bind('resize orientationchange', function() {
	ww = document.body.clientWidth;
	adjustMenu();
	adjustMenu1();
	resizing();
});

var adjustMenu = function() {
	if (ww <= 768) {
		$(".toggleMenu").css("display", "inline-block");
		if (!$(".toggleMenu").hasClass("active")) {
			$("#menu-menu").hide();
		} else {
			$(".nav").show();
		}
		$("#menu-menu li").unbind('mouseenter mouseleave');
		$("#menu-menu li a.parent").unbind('click').bind('click', function(e) {
			// must be attached to anchor element to prevent bubbling
			e.preventDefault();
			$(this).parent("li").toggleClass("hover");
		});
	} 
	else if (ww >= 768) {
		$(".toggleMenu").css("display", "none");
		$("#menu-menu").show();
		$("#menu-menu li").removeClass("hover");
		$("#menu-menu li a").unbind('click');
		$("#menu-menu li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
		 	// must be attached to li so that mouseleave is not triggered when hover over submenu
		 	$(this).toggleClass('hover');
		});
	}
}

/* for navigation menu */

var adjustMenu1 = function() {
	if (ww <= 720) {
		$(".banner-toggle-menu").css("display", "block");
		if (!$(".banner-toggle-menu").hasClass("active")) {
			$(".banner-nav-li").hide();
		} else {
			$(".nav").show();
		}
		$(".banner-nav-li li").unbind('mouseenter mouseleave');
		$(".banner-nav-li li a.parent").unbind('click').bind('click', function(e) {
			// must be attached to anchor element to prevent bubbling
			e.preventDefault();
			$(this).parent("li").toggleClass("hover");
		});
	} 
	else if (ww >= 720) {
		$(".banner-toggle-menu").css("display", "none");
		$(".banner-nav-li").show();
		$(".banner-nav-li li").removeClass("hover");
		//$(".banner-nav-li li a").unbind('click');
		$(".banner-nav-li li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
		 	// must be attached to li so that mouseleave is not triggered when hover over submenu
		 	$(this).toggleClass('hover');
		});
	}
}

/* for navigation end here */
function resizing() {
	if(content_div.length > 0) {
		con_left = content_div.offset().left,
		con_width = content_div.width();
	 page.loader.css('left', con_left+(con_width/2));
	}
}

/* Hash Change Events */

var tab = {
   a: {
	z:null,
	y:null,
	x: null,
	w: null,
	identify:null
    },
   init: function() {

    this.bindEvents(); 
	
    },

	bindEvents: function() {
	tab.a.z=$('.all-tabs > div'),
	tab.a.y=$('#tabs ul:eq(0) li'),
	tab.a.x= $('#tabs ul:eq(0) li a'),
	tab.a.w= $('.all-tabs > div').first(),
	tab.a.identify=$('.identify');
	tab.a.directions=$('#directions').clone();
	tab.a.contactus=$('#contactUs').clone();
	if($('#mailinglist').length == 0) {

$.ajax({
    url: "http://caperesorts.com/staging/mailinglist.html",
    success: function (data) { $('.cong-content').append(data); },
    dataType: 'html'
});

} else if(tab.a.mailing) { $('.cong-content').append(tab.a.mailing);}
	tab.a.mailing = $('#mailinglist').clone();


	tab.a.x.unbind('click').bind('click', function(e) { 
		$el = $(this); 
		var currentTab = this.hash;
		if(!$el.parent().hasClass('active')) {

		
		if((currentTab =='#contact' || currentTab =='#rfp') && ($(currentTab).find('form').length == 0)  ) { 
			

		} else {

		tab.a.y.removeClass('active');
		$el.parent().addClass('active');
		
		tab.a.z.hide();
		$(currentTab).show();
		$('.tabsDiv').removeClass('diningDiv gly form');
		var d_class = $el.attr('data-class');  
		if(d_class) {
			$('.tabsDiv').addClass(d_class);
		}
		if($el.attr('data-promo') =='1') { 
			$('.rt-bar').html($(currentTab).find('.hided_promos').html()).show();
		}
		else {
			$('.rt-bar').hide();
		} }
		if(history.pushState) {
			history.pushState(null, null, this.hash);
		 if((currentTab =='#contact' || currentTab =='#rfp') && ($(currentTab).find('form').length == 0) ) { location.reload(); }
			return false;
		} } else { 	return false; }
	});

	$(".mailing-list .mail-a, .mailing-list button").bind('click', function(e) { 
		e.preventDefault();
			if(!$('#mailinglist').is(':visible')) {
			prop.El.tabsEl.fadeTo('fast', 0, function() {
			prop.El.tabsEl.hide(); $('#directions').hide();
			$('#contactUs').hide();
			$('#mailinglist').show();
		})
	} 

	});

	tab.a.identify.unbind('click').bind('click',  function(e) {  
		e.preventDefault();
		$el = $(this); 
$('#mailinglist').hide();
		if(!$el.hasClass('selected')) { 
		
		tab.a.identify.removeClass('selected'); $el.addClass('selected');
		var currentTab = $el.attr('data-href'); 
		var cl = $el.attr('data-class');
		    $($el.attr('data-hide')).hide();


		

		prop.El.tabsEl.fadeTo('fast', 0, function() {
		prop.El.tabsEl.hide();$(currentTab).css('opacity', 0);
		$('.currentContact').hide().removeClass('currentContact');
		$(currentTab).addClass('currentContact').fadeTo('fast', 1);
		}) 
		} 
		
		return false;
	});

	$('.points').unbind('click').bind('click', function(e) {

		$El = $(this);
		if($El.next().is(':visible')) {

			$El.next().hide();

		} else {
			$El.next().show();

		}
	});


$('.faq-ques a').unbind('click').bind('click', function(e){
e.preventDefault();
if($(this).parent().find('.faq-ans').css('display')=='none'){
$('.faq-ans').css("display","none");
$(this).parent().find('.faq-ans').css("display","block");
}else{
$(this).parent().find('.faq-ans').css("display","none");
}
});


$('.final-content p:last-child').append('... <a href="Javascript:void(0)" class="readless" data-id="137">READ LESS</a>');
$('.initial-content a').unbind('click').bind('click', function(e){
e.preventDefault(); $el = $(this);
	$el.parent().parent().parent().find('.initial-content').hide();
	$el.parent().parent().parent().find('.final-content').show();
});

$('.final-content a').unbind('click').bind('click', function(e){ 
e.preventDefault(); $el = $(this);
	$el.parent().parent().parent().find('.final-content').hide();
	$el.parent().parent().parent().find('.initial-content').show();
	
});
$('.weddingspecialist').unbind('click').bind('click', function(e){ 
e.preventDefault(); $el = $(this);
$('#weddingcontact').trigger('click');
});

		$(window).bind('hashchange', function() {  
		
		var hash= window.location.hash;
		if(hash) { 
			var $tab = $('li a[href="'+hash+'"]');
			$tab.parent().addClass('active');
			tab.a.z.hide();
			var d_class = $tab.attr('data-class');  
			if(d_class) {
			$('.tabsDiv').addClass(d_class);
		}
		if($tab.attr('data-promo') =='1') { 
			$('.rt-bar').html($(hash).find('.hided_promos').html()).show();
		}

		else {
			$('.rt-bar').hide();
		}
			$(hash).show();
		}else { 
			tab.a.z.hide();
			var d_class = tab.a.y.first().find('a').attr('data-class');
			$('.tabsDiv').removeClass('diningDiv gly form');
			$('.tabsDiv').addClass(d_class);
			tab.a.w.show();
			if(tab.a.y.first().find('a').attr('data-promo') =='1') {
				$('.rt-bar').html($(tab.a.w.find('.hided_promos').html())).show();
			}
			else {
				$('.rt-bar').hide();
			}
			tab.a.y.first().addClass('active');
		}
	});


	$(window).trigger('hashchange');

	var slider;
	$(".pretty_photo").colorbox({
	
	inline:true,
	height:"710px",
	width:"1080px",
	onComplete: function() {

		slider = $('#cboxContent').find('.bxslider').bxSlider({
			mode: 'fade',
			captions: true,
			pagerCustom: '.bx-pager-images'
		}); 

		jQuery('#cboxContent').find('.bx-pager-images').jcarousel({
		vertical: true,
		scroll: 2
		}); 
	},
	onClosed: function(el) { 
		slider.destroySlider();
	}

	});

	$(".capacity-chart").colorbox();

	}
}



 
