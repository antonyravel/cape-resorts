var prop = {
   El: {
		BannerNav: null,
		AjaxParams: {},
		Clicked: false,
		contentArea: null,
		tabC: null,
		tabs: null,
		promBar: null,
		develop: null,
    },
   init: function() { 
        this.bindEvents(); 
	
    },

	
	bindEvents: function() { 

		prop.El.BannerNav = $('.sliderDiv').find('.banner-nav-li li');
		prop.El.Slider = $('#jqueryCycle');
		prop.El.tabs=$('#tabs');
		prop.El.contentArea=$('.cong-content');
		prop.El.tabC=$('.tabsDiv');
		prop.El.promBar=$('.rt-bar');

		if($('.tabsDiv').length>0 ) { 
			prop.El.tabsEl=$('.rt-bar, .tabsDiv');

		}else {
			prop.El.tabsEl=$('.gallery-container');
		}	
		
		prop.El.BannerNav.find('a').not('.bookatreatment').unbind('click').bind('click', function() { 

			$el =$(this);
			if($el.hasClass('selected')) {
				$('.identify').removeClass('selected'); $('#contactUs,#directions, #mailinglist').hide();
				if($('.tabsDiv').length>0 ) { 
					$('.tabsDiv').show().css('opacity', 1);

					if($('.rt-bar').children().length > 0) {
	
						$('.rt-bar').show().css('opacity', 1);
					} 
				}
				else {
					if(!$('.gallery-container').is(':visible')) {
						$('.gallery-container').show().css('opacity', 1);
					} 
				}	

			}
			else {
			prop.El.BannerNav.find('a').removeClass('selected');
			$el.addClass('selected');
			tab.a.identify.removeClass('selected');
			prop.El.AjaxParams={
				menu:$el.attr('data-menu'),
				prop: $el.attr('data-prop'),
				menu_id: $el.attr('data-menu_id'),
				action: 'navContent',
			}
			prop.El.Clicked=true;
			$.address.value($el.attr('data-url'));
			}

			return false;
	});
    },


	AjaxCall: function(params, callback) {

		prop.El.Slider.height(prop.El.Slider.height());
 	
		$.getJSON(ajaxLoad.Url, params).done(function (response) {

			$('.rslides_nav, .rslides_tabs').remove();
			prop.El.contentArea.html(response.tabs).append(tab.a.directions).append(tab.a.contactus);
			
			prop.El.Slider.fadeTo('fast', 0, function() {  
			prop.El.Slider.html('');
			prop.El.Slider.fadeTo('fast', 1, function() {

			prop.El.Slider.html(response.slides).responsiveSlides({
		    pager: true,
		    speed: 500,
		    maxwidth: 943
		  });
			

			});

			});

		prop.bindEvents();
		tab.bindEvents();
	
		prop.ReinitializeAddThis()

		});

	},

	ReinitializeAddThis: function(){ 
      if (stButtons)	
		stButtons.locateElements(); 
   }
};

/* Address Change Location */
    if(window.location.href.indexOf("congresshall") > -1) {
      $.address.state('/caperesorts/capemay/congresshall/').init(function () {

			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("beachshack") > -1) {
      $.address.state('/staging/capemay/beachshack/').init(function () {

			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("virginia") > -1) {
      $.address.state('/staging/capemay/virginia/').init(function () {

			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("seaspa") > -1) {
      $.address.state('/staging/experiences/seaspa/').init(function () {

			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("tommysfolly") > -1) {
      $.address.state('/staging/experiences/tommysfolly/').init(function () {

			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("westendgarage") > -1) {
      $.address.state('/staging/experiences/westendgarage/').init(function () {

			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("beachplumfarm") > -1) {
      $.address.state('/staging/experiences/beachplumfarm/').init(function () {

			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("sandpiper") > -1) {
      $.address.state('/staging/capemay/sandpiper/').init(function () {
			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("cottages") > -1) {
      $.address.state('/staging/capemay/cottages/').init(function () {
			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("boilerroom") > -1) {
      $.address.state('/staging/restaurants/boilerroom/').init(function () {
			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("rustynail") > -1) {
      $.address.state('/staging/restaurants/rustynail/').init(function () {
			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("brownroom") > -1) {
      $.address.state('/staging/restaurants/brownroom/').init(function () {
			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("beachservice") > -1) {
      $.address.state('/staging/experiences/beachservice/').init(function () {

			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("ebbittroom") > -1) {
      $.address.state('/staging/restaurants/ebbittroom/').init(function () {
			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("bluepigtavern") > -1) {
      $.address.state('/staging/restaurants/bluepigtavern/').init(function () {
			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("baronscove") > -1) {
      $.address.state('/staging/capemay/baronscove/').init(function () {
			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
    if(window.location.href.indexOf("seasonal") > -1) {
      $.address.state('/staging/experiences/seasonal/').init(function () {

			//prop.El.BannerNav.find('a').address();

         }).internalChange(function (event) { 
				
				if(prop.El.Clicked) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');

				}

         }).externalChange(function(e) {

		}); 
}
(function($) {

prop.init();

/* Object Trigger for Tabs */
tab.init();


})(jQuery);

