var prop = {
   El: {
		BannerNav: null,
		AjaxParams: {},
		Clicked: false,
		contentArea: null,
		tabC: null,
		tabs: null,
		promBar: null,
		develop: null,
		loaded:false,
		ajaxContainer: null
    },
   init: function() { 
        this.bindEvents(); 
	
    },

	
	bindEvents: function() { 

		prop.El.BannerNav = $('.sliderDiv').find('.banner-nav-li li');
		prop.El.Slider = $('#jqueryCycle');
		prop.El.tabs=$('#tabs');
		prop.El.contentArea=$('.cong-content');
		prop.El.tabC=$('.tabsDiv');
		prop.El.promBar=$('.rt-bar');
prop.El.ajaxContainer=$('#ajax_content');
		if($('.tabsDiv').length>0 ) { 
			prop.El.tabsEl=$('.rt-bar, .tabsDiv');

		}else {
			prop.El.tabsEl=$('.gallery-container');
		}	
		
		prop.El.BannerNav.find('a').not('.bookatreatment').unbind('click').bind('click', function() { 

			$el =$(this); 
			if($el.hasClass('currentMenu')) {  $el.removeClass('currentMenu').addClass('selected');
				$('.identify').removeClass('selected'); $('#contactUs,#directions, #mailinglist').hide();
				if($('.tabsDiv').length>0 ) { 
					$('.tabsDiv').show().css('opacity', 1);

					if($('.rt-bar').children().length > 0) {
	
						$('.rt-bar').show().css('opacity', 1);
					} 
			var hash = $('#tabs').find('li.active a').attr('href'); 
if(history.pushState) { 
			history.pushState(hash, document.title, window.location.pathname);
			return false;
		} else
		return false;
				}
				else {
					if(!$('.gallery-container').is(':visible')) {
						$('.gallery-container').show().css('opacity', 1);
					} 
				}	

			}
			else { 
			prop.El.BannerNav.find('a').removeClass('selected');
			$el.addClass('selected');
			tab.a.identify.removeClass('selected');
			prop.El.AjaxParams={
				menu:$el.attr('data-menu'),
				prop: $el.attr('data-prop'),
				menu_id: $el.attr('data-menu_id'),
				action: 'navContent',
			}
			prop.El.Clicked=true; 
			$.address.value($el.attr('data-url'));
			}

			return false;
	});
    },


	AjaxCall: function(params, callback) {

		prop.El.Slider.height(prop.El.Slider.height());
 	
		$.getJSON(ajaxLoad.Url, params).done(function (response) {
			document.title = response.title;
			$('.rslides_nav, .rslides_tabs').remove();
			prop.El.contentArea.html(response.tabs).append(tab.a.directions).append(tab.a.contactus);
			
			prop.El.Slider.fadeTo('fast', 0, function() {  
			prop.El.Slider.html(''); prop.El.Slider.html(); 
			prop.El.Slider.fadeTo('fast', 1, function() {

prop.El.Slider.html(response.slides);
		prop.appendItem(params.ajax_id);
			prop.El.Slider.html(response.slides).responsiveSlides({
		    pager: true,
		    speed: 500,
		    maxwidth: 943
		  });
			

			});

			});

		prop.bindEvents();
		tab.bindEvents();
		prop.ReinitializeAddThis()

		});

	},

	ReinitializeAddThis: function(){ 
      if (stButtons)	
		stButtons.locateElements(); 
   },

	get_address_path: function() {
	var loc = window.location.href, start; 

    if(loc.indexOf("/staging") > -1) {
	 start =  '/staging';
	}
    else if(loc.indexOf("/caperesorts/") > -1) {
	 start =  '/caperesorts';
	}
    else {
	start='';

	}


    if(loc.indexOf("/beachshack") > -1) {
	 addInit =  '/hotels/capemay/beachshack/';
	}
    else if(loc.indexOf("/virginiahotel") > -1) {
	 addInit =  '/hotels/capemay/virginiahotel/';
	}
    else if(loc.indexOf("/seaspa") > -1) {

		if(loc.indexOf("congresshall") > -1)  {
			var lastChar = loc.charAt(loc.length - 1);
			if(lastChar=='/') {
				loc = loc.substring(0, loc.length - 1);
			}
			urls = loc.split('/');
			if(urls.pop() == 'seaspa') { 
				addInit = '/hotels/capemay/congresshall/'; 

			}else {
				addInit =  '/experiences/capemay/seaspa/';
			}
		}else {
		addInit =  '/experiences/capemay/seaspa/';
		}
	}
    else if(loc.indexOf("/tommysfolly") > -1) {
	 addInit =  '/experiences/capemay/tommysfolly/';
	}
    else if(loc.indexOf("/westendgarage") > -1) {
	 addInit =  '/experiences/capemay/westendgarage/';
	}
   else if(loc.indexOf("/beachplumfarm") > -1) {
	 addInit =  '/experiences/capemay/beachplumfarm/';
	}
   else if(loc.indexOf("/sandpiper") > -1) {
	 addInit =  '/hotels/capemay/sandpiper/';
	}
    else if(loc.indexOf("/cottages") > -1) {
	 addInit =  '/hotels/capemay/cottages/';
	}
    else if(loc.indexOf("/boilerroom") > -1) {
	 addInit =  '/restaurants/capemay/boilerroom/';
	}
    else if(loc.indexOf("/rustynail") > -1) {
	 addInit =  '/restaurants/capemay/rustynail/';
	}
    else if(loc.indexOf("/brownroom") > -1) {
	 addInit =  '/restaurants/capemay/brownroom/';
	}
    else if(loc.indexOf("/beachservice") > -1) {
	 addInit =  '/experiences/capemay/beachservice/';
	}
    else if(loc.indexOf("/ebbittroom") > -1) {
	 addInit =  '/restaurants/capemay/ebbittroom/';
	}
    else if(loc.indexOf("/bluepigtavern") > -1) {

	 addInit =  '/restaurants/capemay/bluepigtavern/';
	}
    else if(loc.indexOf("/verandabar") > -1) {

	 addInit =  '/restaurants/capemay/verandabar/';
	}
    else if(loc.indexOf("/baronscove") > -1) {
	 addInit =  '/hotels/capemay/baronscove/';
	}
	else if(loc.indexOf("/seasonal") > -1) {
	 addInit =  '/experiences/capemay/seasonal/';
	}
	else if(loc.indexOf("/thestar") > -1) {
	 addInit =  '/hotels/capemay/thestar/';
	}
 else if(loc.indexOf("/congresshall") > -1) {
	 addInit = '/hotels/capemay/congresshall/'; 
	}
 else if(loc.indexOf("/weddingsevents") > -1) {
	 addInit = '/capemay/weddingsevents/'; 
	}
addInit= start+addInit; 
	return addInit;
	},
	get_page_id: function(path) {
			var url_path = path;
			var url = window.location.href,
			path = path.split('/'),
			pathlength = path.length,
			urlvars = url.split('/'),
			ajax_id;  

			if(url_path=='/') { 
				urlvars.pop();
				ajax_id = urlvars[urlvars.length-1]+'_home';
			} else {

				if(pathlength>=4) { 
					ajax_id = urlvars[urlvars.length-1]+'_home';

				}else if(pathlength==3) { 
					path.pop();urlvars.pop(); urlvars.pop()
					ajax_id = urlvars[urlvars.length-1]+'_'+path[path.length-1];
					

				} else if(pathlength==2) { 

					urlvars.pop();
					ajax_id = urlvars[urlvars.length-1]+'_'+path[path.length-1];
					
				}

			}

		return ajax_id;

	},
	appendItem: function(ajax_id) {

		var html1 =$('#contactUs').clone(true, true).get(0);
		$(html1).find("*").each(function(index, element) {   // And all inner elements.
			if(element.id)
			{	
			$(element).attr('data-newid', element.id).attr('id', '');
			}
			if($(element).attr('class'))
			{	
			$(element).attr('data-newclass', $(element).attr('class')).attr('class', '');
			}
		});
		$(html1).attr('data-newid', $(html1).attr('id')).attr('id', '').attr('data-newclass',  $(html1).attr('class') ).attr('class', '');

		var html2 = $('#directions').clone();
		$(html2).find("*").each(function(index, element) {   // And all inner elements.
			if(element.id)
			{	
			$(element).attr('data-newid', element.id).attr('id', '');
			}
			if($(element).attr('class'))
			{	
			$(element).attr('data-newclass', $(element).attr('class')).attr('class', '');
			}
		});
		$(html2).attr('data-newid', $(html2).attr('id')).attr('id', '').attr('data-newclass',  $(html2).attr('class') ).attr('class', '');


		var html3 = $('#mailinglist').clone();
		$(html3).attr('data-newid', html3.id).attr('id', '').attr('data-newclass',  $(html3).attr('class') ).attr('class', '');


		if($('.tabsDiv').length>0 ) { 
			var htm = $('.tabsDiv').clone(true, true);
			
		}
		else {
			var htm =  $('.gallery-container').clone();
		}	

		$(htm).find("*").each(function(index, element) {   // And all inner elements.
			if(element.id)
			{	
			$(element).attr('data-newid', element.id).attr('id', '');
			}
			if($(element).attr('class'))
			{	
			$(element).attr('data-newclass', $(element).attr('class')).attr('class', '');
			}
		});
		$(htm).attr('data-newclass',  $(htm).attr('class') ).attr('class', '');



		
		var slider = $('#jqueryCycle').clone(false);

		//console.log(slider.html());
		$(slider).find("*").each(function(index, element) {   // And all inner elements.
			if(element.id)
			{		
			$(element).attr('data-newid', element.id).attr('id', '');
			}
			if($(element).attr('class'))
			{	
			$(element).attr('data-newclass', $(element).attr('class')).attr('class', '');
			}
		});

		$(slider).attr('data-newid', $(slider).attr('id')).attr('id', '').attr('data-newclass',  $(slider).attr('class') ).attr('class', '');

		if($.address.hash()) {
		var data_hash = $.address.hash();
		} else {
		var data_hash = '';
		}
		prop.El.ajaxContainer.find('div.current.appendeditems').removeClass('current');
		var selectindex = $('.banner-nav-li').find('a.selected').parent().index('.banner-nav-li li');
		prop.El.ajaxContainer.append('<div id="'+ajax_id+'" class="current appendeditems" data-hash ="'+data_hash+'" data-active ="'+selectindex+'" data-title="'+document.title+'"><div class="contactdub"></div><div class="directionDub"></div><div class="mailingDub"></div><div class="tabsDub"></div><div class="sliderDub"></div></div>');

		$('#'+ajax_id).find('.contactdub').append(html1); $('#'+ajax_id).find('.directionDub').append(html2);
		$('#'+ajax_id).find('.mailingDub').append(html3);$('#'+ajax_id).find('.sliderDub').append(slider);
		$('#'+ajax_id).find('.tabsDub').append(htm).append('<div class="rt-bar" style=""></div>'); 

	},
	get_cachitem: function(id) {
		prop.El.ajaxContainer.find('div.current.appendeditems').removeClass('current'); 
		prop.El.ajaxContainer.find('#'+id).addClass('current');
		var catchItem = prop.resetItem(id, 'data-newid', 'id', 'data-newclass', 'class' );
		var active = $('#'+id).attr('data-active');
		var the_title = $('#'+id).attr('data-title');
		document.title = the_title;
		$('.banner-nav-li').find('a').removeClass('selected');
		$('.banner-nav-li').find('li:eq('+active+') a').addClass('selected');

		 var contactdub = catchItem.find('.contactdub').html(),
		 mailingDub = catchItem.find('.mailingDub').html(),
		 sliderDub = catchItem.find('.sliderDub').html(),
		 tabsDub = catchItem.find('.tabsDub').html(),
		 directionDub = catchItem.find('.directionDub').html();

		$('.cong-content').html('').append(contactdub, mailingDub, tabsDub, directionDub);
		var slideDiv = $('.sliderDiv:eq(0)');
		slideDiv.find('#jqueryCycle, .rslides_tabs, .rslides_nav').remove();


		slideDiv.append(sliderDub);

		slideDiv.find('#jqueryCycle').responsiveSlides({
					pager: true,
					speed: 500,
					maxwidth: 943
		});
		prop.ReinitializeAddThis()
		prop.bindEvents();
		tab.bindEvents();
		prop.resetItem(id, 'id', 'data-newid', 'class', 'data-newclass' );


	},
	resetItem: function(id, a, b, c, d) {

		var newhtml = prop.El.ajaxContainer.find('#'+id);

		$(newhtml).find("*").each(function(index, element) {   // And all inner elements.
			if($(element).attr(a))
			{		
			$(element).attr(b, $(element).attr(a)).attr(a, '');
			}
			if($(element).attr(c))
			{	
			$(element).attr(d, $(element).attr(c)).attr(c, '');
			}
		});




		return newhtml;

	},
	hideMobilemenu: function() {
	ww = document.body.clientWidth;

		if (ww <= 720) {

			if($('.banner-nav-li').is(':visible'))
				$('.banner-nav-li').hide();
		}
	}
};

/* Address Change Location */

 var addInit = prop.get_address_path(); 
  
	$.address.state(addInit).init(function () {

	}).internalChange(function (event) { 
		prop.hideMobilemenu();
		if(prop.El.Clicked) {
			var ajax_id = prop.get_page_id(event.path);
			prop.El.AjaxParams.ajax_id = ajax_id;  
			if($('#'+ajax_id).length ==0) {
				prop.AjaxCall(prop.El.AjaxParams, 'return');
			} else { 
				
				tab.a.cathed =true;
				prop.get_cachitem(ajax_id);
			}
		}
	}).externalChange(function(e) {
prop.hideMobilemenu(); 	
		var addressHash = $.address.hash(); 
		if(!addressHash && prop.El.loaded) {

			var ajax_id = prop.get_page_id(e.path);
			
			
			if($('#'+ajax_id).length ==0) {
				window.location.href=window.location.href;
		}else { tab.a.cathed = true;
				prop.get_cachitem(ajax_id);

		}

		}

		else { 			
			var ajax_id = prop.get_page_id(e.path);

			if(prop.El.loaded==false) {
				prop.appendItem(ajax_id);
			}

			else if($('#'+ajax_id).length ==0) {
				window.location.href=window.location.href;
			}else { 

				if(!$('#'+ajax_id).hasClass('current') && $('.cong-content').find('#'+addressHash).length == 0 ) {
				tab.a.cathed = true;
				prop.get_cachitem(ajax_id);
				}

			}
			prop.El.loaded=true;

		}

	}); 


(function($) {

prop.init();

/* Object Trigger for Tabs */
tab.init();


})(jQuery);

