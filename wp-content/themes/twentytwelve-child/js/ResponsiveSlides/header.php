<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]> <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]> <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--[if lt IE 9]><script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<html class='no-js' lang='en'>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>

<script type="text/javascript" src="http://fast.fonts.com/jsapi/4a6b6a4a-3ff6-4aa9-927d-65e10982b716.js"></script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo child_template_directory; ?>/css/jquery-ui.css" /> -->
<link rel="stylesheet" type="text/css" href="<?php echo child_template_directory; ?>/js/chosen/chosen.css" />
<link rel="stylesheet" type="text/css" href="<?php echo child_template_directory; ?>/js/ResponsiveSlides/responsiveslides.css" />

<?php wp_head(); ?>

</head>
<body>

<input type="hidden" value="<?php echo site_url(); ?>" name="siteurl" id="siteurl" />
<?php

$meg_url = get_option('magazine_url'); // some IP address
$iparr = split ("=", $meg_url); 
global $pdfpath, $filename;

$pdfpath = wp_get_attachment_url( $iparr[1] );
$filename = basename($pdfpath);      

?>
<?php if (is_page('congress-hall') OR is_page('gallery') OR is_page('dining') OR is_page('view-detail') OR is_page('directions') OR is_page('contact-us') OR is_page('view-details') ) { ?>
<div class="wrapper cong-wrapper">

<?php }  elseif (is_page('beach-shack') OR is_page('beach-shack-contact') OR is_page('beach-shack-directions') OR is_page('beach-shack-gallery') OR is_page('beach shack specials')) { ?>
<div class="wrapper beach-wrapper"> 
<?php }  else { ?> 

<div class="wrapper">
<?php } ?>

<div class="wrapper-top">
<div class="wrapper-bottom">
<div class="wrapper-middle">
<div class="wr-bot">
<div class="wr-top">
<div class="main">
<!--header starts here-->
<header> 

<?php if (is_page('congress-hall') OR is_page('gallery') OR is_page('dining') OR is_page('view-detail') OR is_page('directions') OR is_page('contact-us') OR is_page('view-details') ) { ?>
<div class="logo c-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/con-images/c-logo.png" alt="Cape resorts Logo"/></a></h1>

<?php }  elseif(is_page('beach-shack') OR is_page('beach-shack-contact') OR is_page('beach-shack-directions') OR is_page('beach-shack-gallery') OR is_page('beach shack specials')){?>
<div class="logo bs-logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/bs-logo.png" alt="Cape resorts Logo"/></a></h1>



<?php }  else{?>
<div class="logo">
<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo child_template_directory; ?>/img/con-images/logo.png" alt="Cape resorts Logo"/></a></h1>
<?php } ?>
</div>
<nav>
<div class="nav-top">
<form id="reservations">
<ul>
<li><a href="#">book a room</a></li>
<li class="ngt">

<?php  echo hotels_list('dropdown');  ?>

<!-- <a href="#">hotel<span><img src="<?php echo child_template_directory; ?>/img/con-images/down-arrow.png" alt="Cape resorts room booking nights"/></span></a> -->



</li>
<li class="arr">
<input type="text" name="arrivaldate" id="arrivaldate" />
<!-- <a href="#" id="arrivaldate">arrival date<span><img src="<?php echo child_template_directory; ?>/img/con-images/date-bg.png" alt="Cape resorts room booking date"/></span></a>-->
</li>
<li class="ngt">
<select name="nights" data-placeholder="Nights" id="nights" class="chosen-select">
	<option value=''></option>";
	<option value='1'>1</option>";
	<option value='2'>2</option>";
	<option value='3'>3</option>";
	<option value='4'>4</option>";
	<option value='5'>5</option>";
	<option value='6'>6</option>";
	<option value='7'>7</option>";
	<option value='8'>8</option>";
	<option value='9'>9</option>";
	<option value='10'>10</option>";
</select>
<!-- <a href="#">nights<span><img src="<?php echo child_template_directory; ?>/img/con-images/down-arrow.png" alt="Cape resorts room booking nights"/></span></a> -->
</li>
<li class="adl">

<select name="adults" data-placeholder="adults" id="adults" class="chosen-select">
	<option value=''></option>";
	<option value='1'>1</option>";
	<option value='2'>2</option>";
	<option value='3'>3</option>";
	<option value='4'>4</option>";
	<option value='5'>5</option>";
	<option value='6'>6</option>";
	<option value='7'>7</option>";
	<option value='8'>8</option>";
	<option value='9'>9</option>";
	<option value='10'>10</option>";
</select>

<!--<a href="#">adults<span><img src="<?php echo child_template_directory; ?>/img/con-images/down-arrow.png" alt="Adults"/></span></a> -->
</li>
<li class="ngt">
<select name="children" data-placeholder="children" id="children" class="chosen-select">
	<option value=''></option>";
	<option value='1'>1</option>";
	<option value='2'>2</option>";
	<option value='3'>3</option>";
	<option value='4'>4</option>";
	<option value='5'>5</option>";
	<option value='6'>6</option>";
	<option value='7'>7</option>";
	<option value='8'>8</option>";
	<option value='9'>9</option>";
	<option value='10'>10</option>";
</select>
<!-- <a href="#">children<span><img src="<?php echo child_template_directory; ?>/img/con-images/down-arrow.png" alt="Cape resorts room booking nights"/></span></a> -->
</li>
<li class="res">
<a href="javascript:void(0);" class="reserve_button">Reservations<span><img src="<?php echo child_template_directory; ?>/img/con-images/right-arrow.png" alt="Cape resorts room reservations"/></span></a>
</li>
</ul>
</form> <!-- End Form Reservation -->
</div>
<div class="star">
<span class="hr-line"></span>
<img src="<?php echo child_template_directory; ?>/img/con-images/star.png" alt="Capre resorts star"/>
</div>

<div class="top-menu">
<a class="toggleMenu" href="#">Menu</a>
<ul id="menu-menu" class="dropdown">
<li><a href="#">hotels</a>
	<ul class="sub-menu">
		<li><a href="http://sritsprojects.com/stage/caperesorts/capemay/congresshall/">CONGRESS HALL</a></li>
		<li><a href="#">THE VIRGINIA</a></li>
		<li><a href="#">THE VIRGINIA COTTAGES</a></li>
		<li><a href="http://sritsprojects.com/stage/caperesorts/beach-shack/">BEACH SHACK</a></li>
		<li><a href="#">THE STAR</a></li>
		<li><a href="#">THE SAND PIPER BEACH CLUB</a></li>
		<li><a href="#">BARONS COVE SAG HARBOR</a></li>
		<li><a href="#">GIFT CARDS</a></li>
	</ul>
</li>
<li><a href="#">restaurants</a>

	<ul class="sub-menu">
		<li><a href="#">BLUE PIG TAVERN</a></li>
		<li><a href="#">EBBITT ROOM</a></li>
		<li><a href="#">BROWN ROOM</a></li>
		<li><a href="#">BOILER ROOM</a></li>
		<li><a href="#">RUSTY NAIL</a></li>
		<li><a href="#">TOMMY’S FOLLY COFFEE SHOP</a></li>
		<li><a href="#">THE STAR COFFEE SHOP</a></li>
		<li><a href="#">THE BARON'S TABLE</a></li>
		<li><a href="#">GIFT CARDS</a></li>
	</ul>

</li>
<li><a href="#">experiences</a>

	<ul class="sub-menu">
		<li><a href="#">SEA SPA</a></li>
		<li><a href="#">BEACH SERVICE</a></li>
		<li><a href="#">TOMMY’S FOLLY</a></li>
		<li><a href="#">BEACH PLUM FARM</a></li>
		<li><a href="#">WEST END GARAGE</a></li>
		<li><a href="#">SEASONAL</a></li>
		<li><a href="#">SHOPPING</a></li>
		<li><a href="#">GIFT CARDS</a></li>
	</ul>


</li>
<li id="concierge-nav"><a href="<?php echo site_url(); ?>/concierge/" data-type="con-home">concierge</a>

	<ul class="sub-menu">
		<li><a href="<?php echo get_permalink(9); ?>" data-type="page" >PRE-ARRIVAL</a></li>
		<li><a href="<?php echo site_url(); ?>/concierge-calender/?dn=Wednesday&amp;mo=05&amp;d=15&amp;y=2013">DAILY CALENDAR</a></li>
		<li><a href="<?php echo get_permalink(85); ?>" data-type="all-posts" data-taxnomy="stories_category" data-post="stories">STORIES</a></li>
		<li><a href="<?php echo get_permalink(72); ?>" data-type="all-posts" data-taxnomy="activity_category" data-post="activities">ACTIVITIES</a></li>
		<li><a href="<?php echo get_permalink(80); ?>" data-type="all-posts" data-taxnomy="seasonalevents_category" data-post="seasonalevents">SEASONAL EVENTS</a></li>
		<li><a href="<?php echo get_permalink(83); ?>" data-type="all-posts" data-taxnomy="entertainment_category" data-post="entertainment">ENTERTAINMENT</a></li>
		<li><a href="<?php echo get_permalink(87); ?>" data-type="all-posts" data-taxnomy="offers_category" data-post="specialoffers">SPECIAL OFFERS</a></li>
		<li><a href="<?php echo $pdfpath; ?>" download="<?php echo $filename; ?>" target="_blank">CONCIERGE MAGAZINE</a></li>
	</ul>

</li>
<li><a href="#">weddings &amp; events</a>

	<ul class="sub-menu">
		<li><a href="#">CAPE MAY OVERVIEW</a></li>
		<li><a href="#">CONGRESS HALL MEETING</a></li>
		<li><a href="#">VIRGINIA MEETINGS</a></li>
		<li><a href="#">CONGRESS HALL WEDDINGS</a></li>
		<li><a href="#">VIRGINIA WEDDINGS</a></li>
		<li><a href="#">BEACH SHACK WEDDINGS</a></li>
	</ul>


</li>
<li class="spl"><a href="#">specials</a>

	<ul class="sub-menu">
		<li><a href="#">ALL</a></li>
		<li><a href="http://sritsprojects.com/stage/caperesorts/capemay/congresshall/">CONGRESS HALL</a></li>
		<li><a href="#">THE VIRGINIA</a></li>
		<li><a href="#">THE VIRGINIA COTTAGES</a></li>
		<li><a href="http://sritsprojects.com/stage/caperesorts/beach-shack/">BEACH SHACK</a></li>
		<li><a href="#">THE STAR</a></li>
		<li><a href="#">THE SANDPIPER BEACH CLUB</a></li>
		<li><a href="#">GIFT CARDS</a></li>
	</ul>


</li>
</ul>
</div>
</nav>
</header>


