<?php
	/*
	Template Name: Congress-Hall
	*/
?>
<?php get_header();

global $p_id;
$p_id = get_field('pageproperties');

global $subMenu;  
	$subMenu = urldecode($wp_query->query_vars['subMenu']);
	if(!$subMenu)
		$subMenu='home';
	$menu = getMenuBySlug($p_id, $subMenu);
?>
<div class="CH-home">

	<!-- Center_content bar -->
		<?php get_template_part( 'center', 'content' ); ?>

</div> <!-- CH-home -->


</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content">
    <div class="tabsDiv">

	  <?php $tabs = get_all_tabs($p_id, $menu->nid); ?>

	 <!-- <div id="underDevelopment" style="display: none;margin-top: 104px;
margin-left: 96px;"><h3 style="font-size: 35px;">Under Development</h3></div> -->


      <div id="tabs">
 
    <ul>
		<?php foreach($tabs as $tab): ?>

			<li><a href="#<? echo $tab->slug; ?>"><?php echo $tab->t_name; ?></a></li>
			


		<?php endforeach; ?>


    </ul>
    <div class="all-tabs">


		<?php foreach($tabs as $tab): ?>


	<?php if($tab->type==0) { ?>

		<div id="<? echo $tab->slug; ?>" class="tabs_div">
			<?php if($tab->header): ?>
			<h3><?php echo $tab->header; ?>
				<?php if($tab->sub_header){ ?>
				<br><?php echo $tab->sub_header; ?>
				<?php } ?>
			</h3>
			<?php endif; ?>
			<?php
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			echo wpautop($post['post_content']); ?>
		</div>

<?php		} elseif($tab->type==1) { ?>


		<div id="<? echo $tab->slug; ?>" class="events tabs_div" >
			<?php if($tab->header): ?>
			<h3><?php echo $tab->header; ?>
			<?php if($tab->sub_header){ ?>
			<br><?php echo $tab->sub_header; ?>
			<?php } ?>
			</h3>
			<?php endif; ?>
			<?php
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			echo wpautop($post['post_content']); ?>
		</div>

<?php		}

		elseif($tab->type==2) {

?>
		<div id="<? echo $tab->slug; ?>" class="events tabs_div">

		<?php if($tab->post_type=='seasonalevents') { ?>

			   <p class="all-events"><a href="<?php echo site_url().'/concierge/seasonal-events/?property='.$p_id; ?>">all events</a><a href="<?php echo site_url().'/concierge/concierge-calender/?property='.$p_id; ?>">daily calendar</a></p>

		<?php } 

		?>

			<?php if($tab->header): ?>
			<h3><?php echo $tab->header; ?>
			<?php if($tab->sub_header){ ?>
			<br><?php echo $tab->sub_header; ?>
			<?php } ?>
			</h3>
			<?php endif; ?>
			<?php

		$args['post_type']='seasonalevents';
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		$args[ 'meta_value'] = $p_id;



		$activities = new WP_Query( $args );

while ( $activities->have_posts() ) : $activities->the_post();
		?>

    <div>

      <h4><?php the_title(); ?></h4>
      <h5><?php the_field('subtitle1'); ?></h5>

		<?php $newcontent= get_the_content(); ?>

      	<p><?php echo cr_trim_chars( $newcontent, 460 ) ?>... <a href="<?php echo get_permalink(); ?>" class="readmore" data-id="<?php the_ID(); ?>">READ MORE</a></p>

      <p class="view-links">

		<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="<?php the_title(); ?>" st_url="<?php echo get_permalink(); ?>"></span></a>

        <span class="v-gallery">

		<?php if(get_field('button_1')): ?>

		<?php $text =  get_field('button_1');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 echo "<a href='$pieces[1]' ".$onclick." target='_bank'>$pieces[0]</a>";

		?>
		<?php endif; ?>
		<?php if(get_field('button_2')): ?>

		<?php $text =  get_field('button_2');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 echo "<a href='$pieces[1]' ".$onclick." target='_bank'>$pieces[0]</a>";

		?>
		<?php endif; ?>
		<?php if(get_field('button_3')): ?>

		<?php $text =  get_field('button_3');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 echo "<a href='$pieces[1]' ".$onclick." target='_bank'>$pieces[0]</a>";

		?>
		<?php endif; ?>
		<?php if(get_field('button_4')): ?>

		<?php $text =  get_field('button_4');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 echo "<a href='$pieces[1]' ".$onclick." target='_bank'>$pieces[0]</a>";

		?>
		<?php endif; ?>

        </span>
      </p>

      </div>


	<?php endwhile; ?>

		</div>		

<?php		} ?>



		<?php endforeach; ?>
    </div>
</div>      
    </div>


	<?php if($menu->promos==1): ?>
     <!--<div class="rt-bar">
      <div class="spring">
        <h2>book a wedding</h2>
        <h3>in 2013 and receive a $1000.00 gift card and one night stay.</h3>
        <p><a href="#">learn more</a></p>
      </div>
      <div class="sea">
        <h2>winter weddings</h2>
        <h3>free bar package when you book an 01/14 or 02/14 date.</h3>
        <p><a href="#">learn more about this package</a></p>
      </div>
      <div class="retreat">
        <h2>winter wedding packages available</h2>
        <h3></h3>
        <p><a href="#">january through march 2014 packages starting at $125.</a></p>
      </div>
      <div class="resort-video">
        <span>Watch the Cape Resorts Video</span>
      </div>
    </div>
  </div> -->
  <?php endif; ?>
  
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('congresshall')) { ?>
<style>

.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>
