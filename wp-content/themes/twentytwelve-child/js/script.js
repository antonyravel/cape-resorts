var $=jQuery;
var row =0;
var emp_row=0;
var ww=window.innerWidth
|| document.documentElement.clientWidth
|| document.body.clientWidth;
	var content_div;

$(document).ready(function() {

$('#ajax-pageLoader').hide();

$('.wrapper').fadeTo('slow', 1); 

	
	content_div=$('.content');
	resizing();
	var site_url = $('#siteurl').val();

	$("#menu-menu li a").each(function() {
		if ($(this).next().length > 0) {
			$(this).addClass("parent");
		};
	})
	
	$(".toggleMenu").click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$("#menu-menu").toggle();
	});
	adjustMenu();

	 var collapsed = $("#side-accordian li.mainSidenav.active").index();
	
		if(collapsed < 0)  
			collapsed=1; 

	$("#side-accordian").accordion({heightStyle: "content", active: collapsed});

/* Header reservation DatePicker */

	$( "#arrivaldate" ).datepicker({
		showOn: "button",
		buttonImage: site_url+"/wp-content/themes/twentytwelve-child/img/con-images/date-bg.png",
		buttonImageOnly: true,
		dateFormat: 'mm/dd/yy',
		showAnim:'slideDown',
		minDate: 0
	});


$( "#dp1373363262549" ).datepicker({
		dateFormat: 'mm/dd/yy',
		showAnim:'slideDown',
		minDate: 0
	});
$( "#bookreqdate" ).datepicker({
		dateFormat: 'mm/dd/yy',
		showAnim:'slideDown',
		minDate: 0
	});


	$('.pressnews').click(function(e) {
		if($(this).hasClass("exterurl")){}else{e.preventDefault();
		$('#newstitle').text($(this).text());
		$('.heading').hide();
		$('.imgtitle').hide();
		$(this).closest("li").addClass('odd Innerodd');
		$('#presstitle').show();
		$(this).closest(".imgtitle").show();
		$(this).closest("li").find('.contarea').show();}

	});

	$('#backlink').click(function(e) {
		e.preventDefault();
		$('#presstitle').hide();
		$('.contarea').hide();
		$('.press-container li').removeClass('odd');
		$('.press-container li').removeClass('Innerodd');
		$('.press-container li:even').addClass('odd');
		$('.heading').show();
		$('.imgtitle').show();
	});
/* Reservation Form */

var reserve={
		a: $('.reserve_button')
}

reserve.a.bind('click', function(e) {e.preventDefault();

	var form = $('#reservations'),
			hotel=$("#head_hotel", form).val(),
			arrivaldate=$("#arrivaldate", form).val(),
			nights=$("#nights", form).val(),
			adults=$("#adults", form).val(),
			children=$("#children", form).val();
			reservurl=$(".reserve_button").attr('href');
	var params='',newurl = reservurl,ehot ='',edate ='';
	if(reservurl.indexOf("&Hotel=") > -1){
		var newurl = reservurl.replace(/&Hotel=([^&]$|[^&]*)/i, "");		
		params += '&Hotel='+ hotel;
	}else if(!hotel || hotel=="") {
			alert("Please select hotel"); 
			ehot = 'error';
	}else {	params += '&Hotel='+ hotel; }	
 
	if(arrivaldate!='arrival') {
		params+='&Arrive='+arrivaldate;
	}else{alert("Please select arrival date"); edate = 'error'; }
	if(nights) {
			params+='&Nights='+nights;
		}
	if(adults) {
			params+='&Adult='+adults;
		}
	if(children) {
			params+='&Child='+children;
		}
	//alert(newurl+params);
//console.log(ehot+' '+edate);
	if(ehot!='error' && edate!='error'){//console.log('In');
	reservationurl = newurl+params;
_gaq.push(['_link',reservationurl]);
	window.location.href = reservationurl;
	}
});

/* reservation Form Ends */

    var res= $("#jqueryCycle,#jqueryCycle1").responsiveSlides({
        pager: true,
        speed: 500,
        maxwidth: 943
      });

$('.smrdmore').each(function() { var more = $(this).find(".readmore").clone();  $(this).find(".readmore").remove(); $(this).find(":last").append(more);});
$('.smrdless p:last-child').append('<a href="Javascript:void(0)" class="readless" data-id="137"> read less</a>');
$('.smrdmore a.readmore').unbind('click').bind('click', function(e){
e.preventDefault(); $el = $(this);
	$el.parents('.smrdmore').hide();
	$el.parents('.smrdmore').next('.smrdless').show();
});
$('.smrdless a.readless').unbind('click').bind('click', function(e){ 
e.preventDefault(); $el = $(this); 
	$el.parents('.smrdless').hide();
	$el.parents('.smrdless').prev('.smrdmore').show();
	
});

}); /* Document ready Function Ends Here */

/* edit menu start here */
$(document).ready(function() {

	content_div=$('.content');

	$('.tabsDiv.diningDiv').last().css('margin-bottom', '0'); 
	$('.tabs_div').each(function() { 
		
			$(this).find('li').last().css("cssText", "padding-bottom: 0px !important;");
			$(this).find('p').last().css("cssText", "margin-bottom: 0px !important;");
	})
$('.all-tabs').css("cssText", "padding-bottom: 0px !important;");
	var site_url = $('#siteurl').val();

	$(".banner-nav-li li a").each(function() {
		if ($(this).next().length > 0) {
			$(this).addClass("parent");
		};
	})
	
	$(".banner-toggle-menu").click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$(".banner-nav-li").toggle();
	});


	adjustMenu1();

	var collapsed = $("#side-accordian li.mainSidenav.active").index();

	if(collapsed < 0)  
		collapsed=1; 

	$("#side-accordian").accordion({heightStyle: "content", active: collapsed});


	/* Chosen */

	$(".chosen-select").chosen();



/* Careers page Javascript */

$('.job-table .show').unbind('click').bind('click', function(e){ 
	e.preventDefault(); 

	var hiddenEl = $(this).next();

	if(hiddenEl.is(':visible')) 
		hiddenEl.hide();
	else 
		hiddenEl.show();

});


$('.upload_button').parent().before('<p style="padding: 0px 0 0px 13px;margin-top: 6px;">Valid Formats: <span style="color: red;">.doc, .docx, .odt, .rtf, .txt, .wpd, .wps, .pdf</p>');
$('.upload_button').after('<input style="margin-left: 13px;" id="reset_button" type="reset" value="Reset">');
$('.upload input').bind('change', function(){ 
  	
	  var resume = this.value;
      var exts = ['doc','docx','rtf','odt','txt','wpd','wps','pdf'];
      // first check if file field has any value
      if ( resume ) {
        // split file name at dot
        var get_ext = resume.split('.');
        // reverse name to check extension
        get_ext = get_ext.reverse();
        // check file type is valid as given in 'exts' array
        if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
           $(this).parents('.upload').find('.imagename').text(resume);
        } else {
          alert( 'Invalid file! Please upload valid file formats' ); return false;
        }
      } 

});

	$('#reset_button').bind('click', function() {

 	  $el = $(this);
      $el.parent().parent().find('.upload-resume input').val('');

	   $el.parent().parent().find('.upload-resume .imagename').text('Browse to upload resume');

	   $el.parent().parent().find('.upload-cover input').val('');

	   $el.parent().parent().find('.upload-cover .imagename').text('Browse to upload cover letter');


	});
 $('.file_submit').on('submit', function(e)
    {   e.preventDefault();
		this.action = ajaxLoad.Url;
        $el =$(this);

		var appemail = $el.find('.upload-email input').val();
		var resume = $el.find('.upload-resume input').val();

		var cover = $el.find('.upload-cover input').val();

      var exts = ['doc','docx','rtf','odt','txt','wpd','wps','pdf'];
      // first check if file field has any value
      if ( appemail ) {
	if( !isValidEmailAddress( appemail ) ) { alert('Please enter a valid email address'); return false; }
      }else{alert('Please enter email address'); return false;} 


      if ( resume ) {
        // split file name at dot
        var get_ext = resume.split('.');
        // reverse name to check extension
        get_ext = get_ext.reverse();
        // check file type is valid as given in 'exts' array
        if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
          //alert( 'Allowed extension!' ); return false;
        } else {
          alert( 'Invalid file! Please upload valid file formats' ); return false;
        }
      } 

 if ( cover ) {
        // split file name at dot
        var get_ext = cover.split('.');
        // reverse name to check extension
        get_ext = get_ext.reverse();
        // check file type is valid as given in 'exts' array
        if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
          //alert( 'Allowed extension!' ); return false;
        } else {
          alert( 'Invalid file!' ); return false;
        }
		if(resume=="") {
			alert('In order to submit your cover letter, you must also submit your resume (mandatory).');
			return false;
		}
      } 

		if(resume=="") {
			alert('Please upload your resume and cover letter');
			return false;
		}
        $el.find('file').attr('disabled', 'disabled'); 
        $el.find('.output').html('<span style="padding:10px">Uploading...</span>');
        $el.ajaxSubmit({
        target: $el.find('.output'),
        success:  function() { 
			$el.resetForm();  // reset form 
			$el.find('.imagename').eq(0).text('Browse to upload Resume');
			$el.find('.imagename').eq(1).text('Browse to upload Cover letter');
			$el.find('file').removeAttr('disabled'); //enable submit button
		}

        });
    });





$('#add_school').bind('click', function(e) {
e.preventDefault();
row++;

var newField = '<tr id="row_'+row+'"><td><input type="text" name="jobman-school[name][]" value=""></td><td><input type="text" name="jobman-school[location][] value=""></td><td><input type="text" name="jobman-school[graduate][] value=""></td><td><input type="text" name="jobman-school[type][]" value=""></td><td><button title="Remove Education" data-id="row_'+row+'" onclick="remove_field(this);return false;"></button></td></tr>';

	$("#schoolTable tr").last().after(newField);
	$('#total_school').val(row);		
	});


$('#add_employee').bind('click', function(e) {
e.preventDefault();
	emp_row++;

var field ='<table class="job-apply-table table5 employers" id="emp_'+emp_row+'"><tbody><tr><td colspan="2" style="display:block; width:100%;float:right;"><button title="Remove Employer" data-id="emp_'+emp_row+'" onclick="remove_employee(this);return false;" style="margin:0px 177px 10px 0px; float:right;"></button></td></tr><tr><th scope="row">Name of Employer</th><td><input type="text" name="jobman-emp[name][]" value=""></td></tr><tr class="row5 totalrow45 field50 odd"><th scope="row">Phone Number</th><td><input type="text" name="jobman-emp[phone][]" value=""><span style="padding-left:10px;">Ex: xxx-xxx-xxxx</span></td></tr><tr><th class="addr">Employment Dates</th></tr><tr class="row7 totalrow47 field52 odd"><th scope="row">Start</th><td> <input type="text" class="datepicker" placeholder="mm/dd/yy" name="jobman-emp[start][]" value="" id="dp1373363262552"></td></tr><tr class="row8 totalrow48 field53 even"><th scope="row">End</th><td> <input type="text" class="datepicker" placeholder="mm/dd/yy" name="jobman-emp[end][]" value="" id="dp1373363262553"></td></tr><tr><th class="addr">Rate of Pay</th></tr><tr class="row9 totalrow49 field54 odd"><th scope="row">Start</th><td> <input type="text" class="datepicker" placeholder="mm/dd/yy" name="jobman-emp[paystart][]" value="" id="dp1373363262554"></td></tr><tr class="row10 totalrow50 field55 even"><th scope="row">End</th><td> <input type="text" class="datepicker" placeholder="mm/dd/yy" name="jobman-emp[payend][]" value="" id="dp1373363262555"></td></tr><tr class="row11 totalrow51 field56 odd"><th scope="row">Supervisor’s Name &amp; Title</th><td><input type="text" name="jobman-emp[supervisor][]" value=""></td></tr><tr><th colspan="2" class="addr">Address</th></td></tr><tr class="row1 totalrow52 field58 odd"><th scope="row">street</th><td><input type="text" name="jobman-emp[address][street][]" value=""></td></tr><tr class="row2 totalrow53 field59 even"><th scope="row">city</th><td><input type="text" name="jobman-emp[address][city][]" value=""></td></tr><tr class="row3 totalrow54 field60 odd"><th scope="row">State</th><td><input type="text" name="jobman-emp[address][state][]" value=""></td></tr><tr class="row4 totalrow55 field61 even"><th scope="row">zip</th><td><input type="text" name="jobman-emp[address][zip][]" value=""></td></tr><tr class="row5 totalrow56 field62 odd"><th scope="row">Reasons for Leaving</th><td> <textarea name="jobman-emp[reason][]"></textarea></td></tr><tr class="row6 totalrow57 field63 even"><th scope="row">Your Titles &amp; Duties</th><td> <textarea name="jobman-emp[duty][]"></textarea></td></tr><tr class="row7 totalrow58 field64 odd"><th scope="row">Lapse of time?</th><td><input type="text" name="jobman-emp[lapse][]" value=""></td></tr></tbody></table>';

	 $()
	$(".employers").last().after(field);
	$('#emp_'+emp_row).find('.datepicker').datepicker({dateFormat: 'mm/dd/yy', changeMonth: true, changeYear: true, gotoCurrent: true});

	$('#total_employee').val(emp_row);	





});

}); // Document ready End



function remove_field(el) { 
	
	$('#'+$(el).attr('data-id')).remove();
		//$('#total_school').val(row);	

}
function remove_employee(el) {

	$('#'+$(el).attr('data-id')).remove();
		//$('#total_school').val(emp_row);	

}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
/* edit menu end here */

$(window).bind('load', function() {


if(window.location.href.indexOf("/specials") > -1 && window.location.hash=='' && window.location.search.substring(1)) {
var first = getUrlVars()["offer"];
			if(first){ hash = '#'+first; var sploff=true;}
			//console.log(first);
		if(hash && sploff){
				if($('li a[href="#specialoffers"]').length > 0){var $tab = $('li a[href="#specialoffers"]'); }
				else if($('li a[href="#specials"]').length > 0){var $tab = $('li a[href="#specials"]'); }
				else if($('li a[href="#beachspecials"]').length > 0){var $tab = $('li a[href="#beachspecials"]'); }
				else{var $tab = $('li a');}
				$tab.parent().addClass('active');
				var d_class = $tab.attr('data-class');  
				if(d_class) {
				$('.tabsDiv').addClass(d_class);
				}	
				var p = $(hash);
				p.find('.initial-content').hide()
				p.find('.final-content').show();		
				var position = p.position();//console.log(position.top);
				var howitswork = position.top+550;//console.log(howitswork);
				$('body,html').animate({scrollTop: howitswork});
		} 
}

  $('.sliderDiv.ourhistory .banner-nav-li').find('li').last().css('background','none');
  $('.sliderDiv .banner-nav-li').find('li').last().css('background','none');
  $('#tabs ul').find('li').last().css('background','none');
  $('.directions').find('li').last().css('background','none');
  

});









$(window).bind('resize orientationchange', function() {
	ww = document.body.clientWidth;
	
	if (ww <= 720) {
    $('#tabs').find('ul:eq(0)').addClass('Test');

}	else {
    $('#tabs').find('ul:eq(0)').removeClass('Test');

}
	
	
	adjustMenu();
	adjustMenu1();
	resizing();
});

var adjustMenu = function() {
	if (ww <= 768) {
		$(".toggleMenu").css("display", "inline-block");
		if (!$(".toggleMenu").hasClass("active")) {
			$("#menu-menu").hide();
		} else {
			$(".nav").show();
		}
		$("#menu-menu li").unbind('mouseenter mouseleave');
		$("#menu-menu li a.parent").unbind('click').bind('click', function(e) {
			// must be attached to anchor element to prevent bubbling
			e.preventDefault();
			$(this).parent("li").toggleClass("hover");
		});
	} 
	else if (ww >= 768) {
		$(".toggleMenu").css("display", "none");
		$("#menu-menu").show();
		$("#menu-menu li").removeClass("hover");
		$("#menu-menu li a").unbind('click');
		$("#menu-menu li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
		 	// must be attached to li so that mouseleave is not triggered when hover over submenu
		 	$(this).toggleClass('hover');
		});
	}
}

/* for navigation menu */

var adjustMenu1 = function() {
	if (ww <= 720) {
		$(".banner-toggle-menu").css("display", "block");
		if (!$(".banner-toggle-menu").hasClass("active")) {
			$(".banner-nav-li").hide();
		} else {
			$(".nav").show();
		}
		$(".banner-nav-li li").unbind('mouseenter mouseleave');
		$(".banner-nav-li li a.parent").unbind('click').bind('click', function(e) {
			// must be attached to anchor element to prevent bubbling
			e.preventDefault();
			$(this).parent("li").toggleClass("hover");
		});
	} 
	else if (ww >= 720) {
		$(".banner-toggle-menu").css("display", "none");
		$(".banner-nav-li").show();
		$(".banner-nav-li li").removeClass("hover");
		//$(".banner-nav-li li a").unbind('click');
		$(".banner-nav-li li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
		 	// must be attached to li so that mouseleave is not triggered when hover over submenu
		 	$(this).toggleClass('hover');
		});
	}
}

/* for navigation end here */
function resizing() { 	content_div=$('.content');
	if(content_div.length > 0) {
		con_left = content_div.offset().left,
		con_width = content_div.width();
	 page.loader.css('left', con_left+(con_width/2));
	}
}

/* Hash Change Events */

var tab = {
   a: {
	z:null,
	y:null,
	x: null,
	w: null,
	identify:null,
	cathed: false
    },
   init: function() {

    this.bindEvents(); 
	
    },

	bindEvents: function() {
	tab.a.z=$('.all-tabs > div'),
	tab.a.y=$('#tabs ul:eq(0) li'),
	tab.a.x= $('#tabs ul:eq(0) li a'),
	tab.a.w= $('.all-tabs > div').first(),
	tab.a.identify=$('.identify');
	tab.a.directions=$('#directions').clone();
	tab.a.contactus=$('#contactUs').clone();
	if($('#mailinglist').length == 0) {

$.ajax({
    url: $('#siteurl').val()+"/mailinglist.html",
    success: function (data) { $('.cong-content').append(data); },
    dataType: 'html'
});

} else if(tab.a.mailing) { $('.cong-content').append(tab.a.mailing);}
	tab.a.mailing = $('#mailinglist').clone();


	tab.a.x.unbind('click').bind('click', function(e) {
		$el = $(this); 
		var currentTab = this.hash;
		if(!$el.parent().hasClass('active')) {

		
		if((currentTab =='#contact' || currentTab =='#rfp') && ($(currentTab).find('form').length == 0)  ) { 
			

		}else if((currentTab =='#history') && ($(currentTab).find('form').length == 0) && $('.sliderDiv.ourhistory').length ==1 ) { 
		tab.a.y.removeClass('active');
		$el.parent().addClass('active');
		
		tab.a.z.hide();
		$(currentTab).show();
		$('.tabsDiv').removeClass('diningDiv gly form');
		var d_class = $el.attr('data-class');  
		if(d_class) {
			$('.tabsDiv').addClass(d_class);
		}
		if($el.attr('data-promo') =='1') { 
			$('.rt-bar').html($(currentTab).find('.hided_promos').html()).show();
		}
		else {
			$('.rt-bar').hide();
		}
			$('.sliderDiv.ourhistory').hide();$('.sliderDiv.ourhistpic').show();
			//alert($('.sliderDiv.ourhistory').length);

		}else if((currentTab !='#history') && ($(currentTab).find('form').length == 0) && $('.sliderDiv.ourhistory').length ==1 ) { 
		tab.a.y.removeClass('active');
		$el.parent().addClass('active');
		
		tab.a.z.hide();
		$(currentTab).show();
		$('.tabsDiv').removeClass('diningDiv gly form');
		var d_class = $el.attr('data-class');  
		if(d_class) {
			$('.tabsDiv').addClass(d_class);
		}
		if($el.attr('data-promo') =='1') { 
			$('.rt-bar').html($(currentTab).find('.hided_promos').html()).show();
		}
		else {
			$('.rt-bar').hide();
		}
			$('.sliderDiv.ourhistpic').hide();$('.sliderDiv.ourhistory').show();
			

		} else {

		tab.a.y.removeClass('active');
		$el.parent().addClass('active');
		
		tab.a.z.hide();
		$(currentTab).show();
		$('.tabsDiv').removeClass('diningDiv gly form');
		var d_class = $el.attr('data-class');  
		if(d_class) {
			$('.tabsDiv').addClass(d_class);
		}
		if($el.attr('data-promo') =='1') { 
			$('.rt-bar').html($(currentTab).find('.hided_promos').html()).show();
		}
		else {
			$('.rt-bar').hide();
		} }
$(".promovideo").colorbox({iframe:true, innerWidth:853, innerHeight:480, onOpen: function(){
           $("#colorbox").addClass("youtubepromo");
       }});
		if(history.pushState) {
			history.pushState(null, null, this.hash);
		 if((currentTab =='#contact' || currentTab =='#rfp') && ($(currentTab).find('form').length == 0) ) { location.reload(); }
			return false;
		} 
} else { 	return false; }
	});

	$(".mailing-list .mail-a, .mailing-list button").bind('click', function(e) { 
		e.preventDefault();
			if(!$('#mailinglist').is(':visible')) {  prop.El.BannerNav.find('a.selected').addClass('currentMenu');
			prop.El.tabsEl.fadeTo('fast', 0, function() {
			prop.El.tabsEl.hide(); $('#directions').hide();
			$('#contactUs').hide();
			$('#mailinglist').show();

		if(history.pushState) { 
			history.pushState(null, null, '#mailinglist');
			return false;
		} 
 		else { return false; }

		});
	} 

	});



tab.a.identify.unbind('click');
	tab.a.identify.bind('click',  function(e) {   
		e.preventDefault(); prop.El.BannerNav.find('a.selected').addClass('currentMenu');
prop.El.BannerNav.find('a').removeClass('selected');
		$el = $(this); 
$('#mailinglist').hide();
		if(!$el.hasClass('selected')) { 
		tab.a.identify.removeClass('selected'); $el.addClass('selected');
		var currentTab = $el.attr('data-href'); 
		var cl = $el.attr('data-class');
		    $($el.attr('data-hide')).hide();
	
		//$.address.value(currentTab.toLowerCase());
		prop.El.tabsEl.fadeTo('fast', 0, function() {
			prop.El.tabsEl.hide();$(currentTab).css('opacity', 0);
			$('.currentContact').hide().removeClass('currentContact');
			$(currentTab).addClass('currentContact').fadeTo('fast', 1);
		}) 
		}  
if(history.pushState) { 
			history.pushState(null, null, currentTab.toLowerCase());
			return false;
		} else
		return false;
	});

	$('.points').unbind('click').bind('click', function(e) {

		$El = $(this);
		if($El.next().is(':visible')) {

			$El.next().hide();

		} else {
			$El.next().show();

		}
	});


$('.faq-ques a').unbind('click').bind('click', function(e){
e.preventDefault();
if($(this).parent().find('.faq-ans').css('display')=='none'){
$('.faq-ans').css("display","none");
$(this).parent().find('.faq-ans').css("display","block");
}else{
$(this).parent().find('.faq-ans').css("display","none");
}
});

var pinit;
$('.initial-content, .final-content').find('p').css('text-align', 'left');
$('.cong-content .initial-content').each(function() { pinit=$(this).find('p').eq(0); if(pinit.text()=='') pinit.remove();
		var more = $(this).find(".readmore").clone(); $(this).find(".readmore").remove(); 
	var last_child = $(this).find(":last");
	if(last_child.length > 0) {
		if(last_child[0].tagName !='BR' )
			last_child.append(more);
		else
			last_child.parent().append(more);
	}
  });

$('.final-content').each(function() { 
if($(this).find('.readless').length == 0) {
var less = ' <a href="Javascript:void(0)" class="readless" data-id="137">READ LESS</a>';
var last_child = $(this).find(":last");
if(last_child.length > 0) {

	if ($(last_child[0]).css('display') == 'inline' ) {
			last_child.parent().append(less);
	}

	else if(last_child[0].tagName !='BR' )
		last_child.append(less);
	
	else
		last_child.parent().append(less);
}
}
 });


$('.initial-content a.readmore').unbind('click').bind('click', function(e){
e.preventDefault(); $el = $(this);
	$el.parents('.initial-content').hide();
	$el.parents('.initial-content').next('.final-content').show();
});

$('.final-content a.readless').unbind('click').bind('click', function(e){ 
e.preventDefault(); $el = $(this); 
	$el.parents('.final-content').hide();
	$el.parents('.final-content').prev('.initial-content').show();
	
});
$('.weddingspecialist').unbind('click').bind('click', function(e){ 
e.preventDefault(); $el = $(this);
$('#weddingcontact').trigger('click');
});

		$(window).unbind('hashchange').bind('hashchange', function() {  

		if(tab.a.cathed) {
			var hash= $.address.hash(); 
			if(hash)
				hash = '#'+hash;
 			tab.a.cathed=false;
		}else {
			var hash = window.location.hash;			
		} 
		if(!$('.sliderDiv.ourhistory').is(':visible')) {
			$('.sliderDiv.ourhistory').show(); $('.sliderDiv.ourhistpic').hide();
		}
	

		if(hash) {
			if(hash == '#directions' || hash == '#contactus' || hash == '#mailinglist') { 
				tab.a.y.removeClass('selected');
				if(hash=='#directions')
					tab.a.identify.eq(0).trigger('click');
				else if(hash == '#mailinglist') 
					$('.mail-a').trigger('click');
				else
					tab.a.identify.eq(1).trigger('click');
				return false;
			}else if(hash == '#history' && $('.sliderDiv.ourhistory').length ==1){
							tab.a.y.removeClass('active');
			var $tab = $('li a[href="'+hash+'"]'); 
			$tab.parent().addClass('active');
			tab.a.z.hide();
			var d_class = $tab.attr('data-class');  
			if(d_class) {
			$('.tabsDiv').addClass(d_class);
			}
			if($tab.attr('data-promo') =='1') { 
				$('.rt-bar').html($(hash).find('.hided_promos').html()).show();
			}
			else {
			$('.rt-bar').hide();
			}
			$(hash).show();
			$('.sliderDiv.ourhistory').hide();$('.sliderDiv.ourhistpic').show();
			}else { 

			tab.a.y.removeClass('active');
			var $tab = $('li a[href="'+hash+'"]'); 
		
			$tab.parent().addClass('active');
			tab.a.z.hide();
			var d_class = $tab.attr('data-class');  
			if(d_class) {
			$('.tabsDiv').addClass(d_class);
		}
			if($tab.attr('data-promo') =='1') { 
				$('.rt-bar').html($(hash).find('.hided_promos').html()).show();
		}

		else {
			$('.rt-bar').hide();
		}
			$(hash).show();

		}

		}else {  
			tab.a.z.hide(); tab.a.y.removeClass('active');
			var d_class = tab.a.y.first().find('a').attr('data-class');
			$('.tabsDiv').removeClass('diningDiv gly form');
			$('.tabsDiv').addClass(d_class);
			tab.a.w.show();
			if(tab.a.y.first().find('a').attr('data-promo') =='1') {
				$('.rt-bar').html($(tab.a.w.find('.hided_promos').html())).show();
			}
			else {
				$('.rt-bar').hide();
			}
			tab.a.y.first().addClass('active');
		}
	});


	$(window).trigger('hashchange');

	var slider;


$(".pretty_photo").unbind('click');
$(".pretty_photo").bind('click',  function(e) {  

	if(history.pushState) {
			history.pushState(null, null, this.hash);
		} 

});
var open = true;var bxArea;  
 if(window.location.hash.indexOf("-gallery") > -1) { 
	 open=true; }
	else {
	 open=false; }

	$(".pretty_photo").colorbox({
		inline:true,
		height:"710px",
		width:"1080px",
		fixed:true,
		href:window.location.hash,
		open: open,
	onComplete: function(el) {
		bxArea = $('#cboxContent').find('.bxslider').clone();
	
		slider = $('#cboxContent').find('.bxslider').bxSlider({
			mode: 'fade',
			captions: true,
			pagerCustom: '.bx-pager-images'
		}); 

		jQuery('#cboxContent').find('.bx-pager-images').jcarousel({
		vertical: true,
		scroll: 2
		}); 

	
	},
	onClosed: function(el) {  
		$(this).parents('li').find('.gallery_popup').html(bxArea);
	$(".pretty_photo").colorbox({ href:''});
		history.pushState('', document.title, window.location.pathname);
	}

	});

	$(".capacity-chart").colorbox({fixed:true, maxWidth:'95%', maxHeight:'95%'});
$(".promovideo").colorbox({iframe:true, innerWidth:853, innerHeight:480, onOpen: function(){
           $("#colorbox").addClass("youtubepromo");
       }});
//$('.muldate-pick').attr('disabled', 'disabled');
$('.calenderimg')
		.datePicker(
			{
				createButton:false,
				displayClose:true,
				closeOnSelect:false,
				selectMultiple:true,
clickInput:true
			}
		)
		.bind(
			'click',
			function()
			{
				$(this).dpDisplay(); 
				this.blur();
				return false;
			}
		)
		.bind(
			'dateSelected',
			function(e, selectedDate, $td, state)
			{	
				
			}
		)
		.bind(
			'dpClosed',
			function(e, selectedDates)
			{
				var hiddenField = $('.muldate-pick'), val, con;
						hiddenField.val('');
						$.each( selectedDates, function( key, value ) {

							val=hiddenField.val();
							if(val=="") {
								con = '';
							}else { con = ' / '; }
							
							hiddenField.val(hiddenField.val()+con+$.datepicker.formatDate('mm-dd-yy', value));

						});
			}
		);
	}
}																																
