<?php
	/*
	Template Name: tf-directions
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(888)944-1816</p>
    
    
    <ul id="jqueryCycle">
<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>

	</ul>
    
    
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/tommys-folly/">home</a></li>
        <li><a href="#">tommy's folly gift</a></li>
        <li><a href="#">tommy's folly home</a></li>
        <li><a href="#">tommy's folly boutique</a></li>
        <li><a href="#">congress hall</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>200 congress place</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/tommy-folly-directions/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/tommy-folly-contact/">contact us</a></li>
    </ul>
    <ul class="social-icons">     
      <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/vector-icon.png" alt="corner-img" /></a></li>      
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/youtube.png" alt="corner-img" /></a></li> 
      <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/facebook.png" alt="corner-img" /></a></li>      
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/twitter.png" alt="corner-img" /></a></li>      
        <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/pinit.png" alt="corner-img" /></a></li>      
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="JOIN OUR MAILING LIST" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content drt">
    <h3>Directions</h3>
    <div class="directionDiv">
		    
		    <div class="direct-content">
		        <div class="dir-left">
		          <h2>Congress Hall</h2>
		          <h2>200 Congress Place</h2>
		          <h2>Cape May, New Jersey  08204</h2>
		          <h2>(888) 944-1816</h2>
		          <div class="find-us">
		            <h2>its easy for find us!</h2>
		            <p>Cape May is easily accessible from the Garden State Parkway and the Cape May-Lewes Ferry. The hotel is located right in the heart of the Historic District, right across the street from the beach and the Washington Street Mall.</p>
		            <p><b>Valet Parking</b> is provided. Please pull up in front of the hotel and an attendant will assist you with the luggage and car.</p>
		          </div>
		        </div>
		        <div class="google-map">
		          <h2>from google maps:</h2>
		          <span>View Google Map &amp; Enter Your Starting Point</span>
		          <div class="specific">
		            <h2>from specific points</h2>
		            <a href="#">from the garden state parkway</a>
		            <a href="#">from the cape may lewes ferry</a>
		            <a href="#">from new york</a>
		            <a href="#">from washington/baltimore</a>
		            <a href="#">from philadelphia</a>
		            
		          </div>
		        </div>
		    </div>
    </div>
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('tommy-folly-directions')) { ?>
<style>
body{background:#F9F9F9;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>

