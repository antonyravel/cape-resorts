<?php

	/*
	Template Name: Careers
	*/


get_header(); ?>

<div id="content" role="main">
	<section>

		<aside>

			<ul id="carrer_sidebar">
			<?php dynamic_sidebar('Contact Page'); ?>
			</ul>

		</aside>
	<?php while ( have_posts() ) : the_post(); ?>


	<div class="content carrier_container">
		<h2 class="heading"><span class="parent_cat"><?php echo the_title(); ?></span></h2>
		<?php the_content(); ?>

	</div>

	<?php endwhile; // end of the loop. ?>
	</section>
</div><!-- #content -->


</div></div></div></div></div></div>
<?php get_footer(); ?>
