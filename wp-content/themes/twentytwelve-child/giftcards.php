<?php
	/*
	Template Name: Gift cards
	*/
?>
<?php get_header(); ?> 
<style type="text/css">

table.giftTable {
	font-size:13px; width:auto;
}
table.giftTable td {
	border:1px solid #CCC;
}
h1 {
	font-size:18px;
}
p {
	font-size:13px;
	margin:0px 0 15px 0;
}
.price {
	font-size:16pt;
	font-weight:bold;
}
.phone {
	font-size:12pt;
	font-weight:bold;
}
.notes {
	font-size:8pt;
	color:#666;
	margin:10px 0 0 0;
}
.override #googlecart-widget-list {
	border:1px solid #CCC;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>/giftcardsf/congresshall.css" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>/giftcardsf/nav.css" />
<table class="gift-table" width="800" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <!--<td valign="middle"><a href="http://www.congresshall.com"><img src="<?php echo site_url(); ?>/giftcardsf/images/layout_01.gif" alt="" border="0" width="166" height="36"></a></td>-->
    <td colspan="2" align="right" valign="bottom"><h3>Call 888.944.1816</h3></td>
  </tr>
  <tr>
    <td colspan="2"><p style="text-align:justify;">Share the warmth and history of the Cape Resorts Experience with your loved ones. Purchase a gift card in any denomination or choose a customized certificate for one of our specialized retreats. </p></td>
  </tr>
  <tr>
    <td colspan="2"><table width="800" cellpadding="5" border="0" cellspacing="2" class="giftTable">
        <tr>
          <td colspan="3" valign="middle"><h1>Gift Cards</h1></td>
        </tr>
        <tr>
          <td width="200"><img src="<?php echo site_url(); ?>/giftcardsf/images/gift_congressGC.jpg" alt="" width="200" height="133" border="0"></td>
          <td><b>Congress Hall Gift Card</b><br>
            For use like a standard gift card throughout the resort <br>
            <br>
            Please enter the dollar amount and click order. A shopping cart will open in a new window, presenting you with additional options such as changing the quantity for your order.<br/>
            Please note that you DO NOT have to use a PayPal account to place an order. We accept four major credit-cards.</span><br/>
            <br/>
            <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" style="position:relative;float:left;">
              <span class="style41">Amount: $</span>
              <input type="text" name="amount" value="25" style="width: 50px; height: 23px;"/>
              <input type="hidden" name="add" value="1">
              <input type="hidden" name="bn" value="PP-ShopCartBF">
              <input type="hidden" name="cmd" value="_cart">
              <input type="hidden" name="business" value="CongressHallGiftCard@congresshall.com">
              <input type="hidden" name="item_name" value="Congress Hall Gift Card">
              <input type="hidden" name="no_shipping" value="0">
              <input type="hidden" name="no_note" value="1">
              <input type="hidden" name="currency_code" value="USD">
              <input type="hidden" name="lc" value="US">
              <input type="hidden" name="bn" value="PP-ShopCartBF">
              <input name="order" type="submit" style="height: 23px; width: 100px; border-style: solid; border-width: 1px; border-color: #bbb; background-color:#fff;" value="Add to Cart">
            </form>
            <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" style="position:relative;float:left;width:80px; margin-left: 10px;">
              <input type="submit" value="View Cart" name="submit" style="height: 23px; width: 80px; border-style: solid; border-width: 1px; border-color: #bbb; background-color: #fff;"/>
              <input type="hidden" name="cmd" value="_cart">
              <input type="hidden" name="business" value="CongressHallGiftCard@congresshall.com">
              <input type="hidden" name="display" value="1">
            </form></td>
          <td align="center" width="150"><span class="price">Choose your denomination</span><br>
            <span class="number">Call 609-884-8421</span> <br></td>
        </tr>
        <tr>
          <td width="200"><img src="<?php echo site_url(); ?>/giftcardsf/images/gift_virginiaGC.jpg" alt="" width="200" height="133" border="0"></td>
          <td><b>Virginia Hotel Gift Card</b><br>
            For use like a standard gift card throughout the hotel. <br>
            <br>
            Please enter the dollar amount and click order. A shopping cart will open in a new window, presenting you with additional options such as changing the quantity for your order.<br/>
            Please note that you DO NOT have to use a PayPal account to place an order. We accept four major credit-cards.</span><br/>
            <br/>
            <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" style="position:relative;float:left;">
              <span class="style41">Amount: $</span>
              <input type="text" name="amount" value="25" style="width: 50px; height: 23px;"/>
              <input type="hidden" name="add" value="1">
              <input type="hidden" name="bn" value="PP-ShopCartBF">
              <input type="hidden" name="cmd" value="_cart">
              <input type="hidden" name="business" value="CongressHallGiftCard@congresshall.com">
              <input type="hidden" name="item_name" value="Virginia Hotel Gift Card">
              <input type="hidden" name="no_shipping" value="0">
              <input type="hidden" name="no_note" value="1">
              <input type="hidden" name="currency_code" value="USD">
              <input type="hidden" name="lc" value="US">
              <input type="hidden" name="bn" value="PP-ShopCartBF">
              <input name="order" type="submit" style="height: 23px; width: 100px; border-style: solid; border-width: 1px; border-color: #bbb; background-color:#fff;" value="Add to Cart">
            </form>
            <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" style="position:relative;float:left;width:80px; margin-left: 10px;">
              <input type="submit" value="View Cart" name="submit" style="height: 23px; width: 80px; border-style: solid; border-width: 1px; border-color: #bbb; background-color: #fff;"/>
              <input type="hidden" name="cmd" value="_cart">
              <input type="hidden" name="business" value="CongressHallGiftCard@congresshall.com">
              <input type="hidden" name="display" value="1">
            </form></td>
          <td align="center" width="150"><span class="price">Choose your denomination</span><br>
            <span class="number">Call 609-884-5700</span></td>
        </tr>
        <tr>
          <td width="200"><img src="<?php echo site_url(); ?>/giftcardsf/images/gift_beachshackGC.jpg" alt="" width="200" height="133" border="0"></td>
          <td><b>Beach Shack Gift Card</b><br>
            For use like a standard gift card throughout the resort <br>
            <br>
            Please enter the dollar amount and click order. A shopping cart will open in a new window, presenting you with additional options such as changing the quantity for your order.<br/>
            Please note that you DO NOT have to use a PayPal account to place an order. We accept four major credit-cards.</span><br/>
            <br/>
            <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" style="position:relative;float:left;">
              <span class="style41">Amount: $</span>
              <input type="text" name="amount" value="25" style="width: 50px; height: 23px;"/>
              <input type="hidden" name="add" value="1">
              <input type="hidden" name="bn" value="PP-ShopCartBF">
              <input type="hidden" name="cmd" value="_cart">
              <input type="hidden" name="business" value="CongressHallGiftCard@congresshall.com">
              <input type="hidden" name="item_name" value="Seaspa Gift Card">
              <input type="hidden" name="no_shipping" value="0">
              <input type="hidden" name="no_note" value="1">
              <input type="hidden" name="currency_code" value="USD">
              <input type="hidden" name="lc" value="US">
              <input type="hidden" name="bn" value="PP-ShopCartBF">
              <input name="order" type="submit" style="height: 23px; width: 100px; border-style: solid; border-width: 1px; border-color: #bbb; background-color:#fff;" value="Add to Cart">
            </form>
            <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" style="position:relative;float:left;width:80px; margin-left: 10px;">
              <input type="submit" value="View Cart" name="submit" style="height: 23px; width: 80px; border-style: solid; border-width: 1px; border-color: #bbb; background-color: #fff;"/>
              <input type="hidden" name="cmd" value="_cart">
              <input type="hidden" name="business" value="CongressHallGiftCard@congresshall.com">
              <input type="hidden" name="display" value="1">
            </form></td>
          <td align="center" width="150"><span class="price">Choose your denomination</span><br>
            <span class="number">Call 609-884-6568</span> <br></td>
        </tr>
        
        
        <tr>
          <td width="200"><img src="<?php echo site_url(); ?>/giftcardsf/images/SeaSpa-GiftCard.jpg" alt="" width="200" height="133" border="0"></td>
          <td><b>Sea Spa Gift Card</b><br>
            For use like a standard gift card at the Congress Hall Sea Spa <br>
            <br>
            Please enter the dollar amount and click order. A shopping cart will open in a new window, presenting you with additional options such as changing the quantity for your order.<br/>
            Please note that you DO NOT have to use a PayPal account to place an order. We accept four major credit-cards.</span><br/>
            <br/>
            <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" style="position:relative;float:left;">
              <span class="style41">Amount: $</span>
              <input type="text" name="amount" value="25" style="width: 50px; height: 23px;"/>
              <input type="hidden" name="add" value="1">
              <input type="hidden" name="bn" value="PP-ShopCartBF">
              <input type="hidden" name="cmd" value="_cart">
              <input type="hidden" name="business" value="CongressHallGiftCard@congresshall.com">
              <input type="hidden" name="item_name" value="Sea Spa Gift Card">
              <input type="hidden" name="no_shipping" value="0">
              <input type="hidden" name="no_note" value="1">
              <input type="hidden" name="currency_code" value="USD">
              <input type="hidden" name="lc" value="US">
              <input type="hidden" name="bn" value="PP-ShopCartBF">
              <input name="order" type="submit" style="height: 23px; width: 100px; border-style: solid; border-width: 1px; border-color: #bbb; background-color:#fff;" value="Add to Cart">
            </form>
            <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" style="position:relative;float:left;width:80px; margin-left: 10px;">
              <input type="submit" value="View Cart" name="submit" style="height: 23px; width: 80px; border-style: solid; border-width: 1px; border-color: #bbb; background-color: #fff;"/>
              <input type="hidden" name="cmd" value="_cart">
              <input type="hidden" name="business" value="CongressHallGiftCard@congresshall.com">
              <input type="hidden" name="display" value="1">
            </form></td>
          <td align="center" width="150"><span class="price">Choose your denomination</span><br>
            <span class="number">Call 609-884-6568</span> <br></td>
        </tr>
        
        
      </table>
      <br>
      <table width="800" cellpadding="5" border="0" cellspacing="2" class="giftTable">
        <tr>
          <td  valign="middle"><h1>Popular Gifts</h1></td>
        </tr>
        <tr>
          <td><script src="https://www-sgw-opensocial.googleusercontent.com/gadgets/ifr?url=https%3A%2F%2Fstoregadgetwizard.appspot.com%2Fservlets%2FgadgetServlet%3Fkey%3D0AoNre3eshQiPdG1WYzJxd0MzLURCYktna3o1eUd3UUE%26mid%3D955381042815981%26currency%3DUSD%26sandbox%3Dfalse%26gadget%3DLARGE&amp;container=storegadgetwizard&amp;w=785&amp;h=780&amp;title=&amp;brand=none&amp;output=js"></script></td>
        </tr>
      </table>
      <br>
      <br></td>
  </tr>
</table>
<br>
</div></div></div></div>
<?php get_footer(); ?>
