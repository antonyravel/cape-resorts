<?php
/*
Template Name: Concierge-details
*/
?>
<?php get_header(); ?>
<section>
<!-- concierge Left Side bar Starts -->
		<?php get_sidebar('concierge'); ?>

<div class="content details">

  <div class="details-banner">
    <img src="<?php echo child_template_directory; ?>/img/det-images/det-banner.png" alt="dtl-banner" />
  </div>
  <div class="details-content act-content">
  <h3>The Bike Shop at Congress Hall</h3>
  <h4>The Bike Room, March 15, 9PM-11PM</h4>
  <i>$24 adults, $12 children</i>
  <span>Free gifts after noon</span>
  <p>Saetosus fiducia suis miscere chirographi, et saburre fermentet quadrupei, quod incredibiliter adlaudabilis saburre iocari vix fragilis cathedras.</p>
  <p>
  Bellus syrtes conubium santet matrimonii. Zothecas fermentet Medusa, ut ossifragi conubium santet tremulus syrtes. Verecundus matrimonii miscere saetosus apparatus bellis, etiam umbraculi corrumperet syrtes, quamquam agricolae pessimus frugaliter praemuniet concubine. Catelli amputat quadrupei, utcunque verecundus chirographi circumgrediet catelli, etiam fragilis umbraculi celeriter adquireret tremulus oratori, quamquam apparatus bellis satis spinosus agnascor Aquae Sulis. Zothecas insectat fiducia suis. Catelli iocari saburre, semper apparatus bellis adquireret incredibiliter perspicax zothecas, et umbraculi lucide suffragarit cathedras. Vix parsimonia quadrupei vocificat umbraculi, quamquam tremulus ossifragi suffragarit chirographi, quod fiducia suis fermentet fragilis matrimonii, ut quinquennalis syrtes aegre divinus miscere apparatus bellis.
  </p>
  <a href="#" class="learn">Learn More</a>
  <a href="#" class="share">Share</a>  
  </div>

</div>
</section>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
<?php get_footer(); ?>
