<?php
	/*
	Template Name: Sea-spa-directions
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reserve a Treatment&nbsp;(609)884-6543</p>
    
    
    <ul id="jqueryCycle">
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider2.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider3.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider4.png" alt="slider" class="slider-img" /></li>
	</ul>
    
    
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/sea-spa/">home</a></li>
        <li><a href="<?php echo site_url(); ?>/sea-spa-menu/">spa menu</a></li>
        <li><a href="<?php echo site_url(); ?>/sea-spa-specials/">spa specials</a></li>
        <li><a href="<?php echo site_url(); ?>/sea-spa-gallery/">gallery</a></li>
        <li><a href="#">book a treatment</a></li>
        <li><a href="#">congress hall</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>200 congress place</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/sea-spa-directions/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/sea-spa-contactus/">contact us</a></li>
    </ul>
    <ul class="social-icons">
      <li><a href="http://www.tripadvisor.com/Hotel_Review-g46341-d92337-Reviews-Congress_Hall-Cape_May_New_Jersey.html" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/vector-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://www.youtube.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/tube-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://www.facebook.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/facebook-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://twitter.com/CongressHall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/twitter-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://pinterest.com/caperesorts/" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/pin-icon.png" alt="corner-img" /></a></li>
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="join our mailing list" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content drt">
    
    <div class="directionDiv">
		    <h3>Directions</h3>
		    <div class="direct-content">
		        <div class="dir-left">
		          <h2>congress hall</h2>
		          <h2>200 congress place</h2>
		          <h2>cape may, new jersy 08204</h2>
		          <h2>(609) 884-6593</h2>
		          <div class="find-us">
		            <h2>Its easy for find us!</h2>
		            <p>Cape May is easily accessible from the Garden State Parkway and the Cape May-Lewes Ferry. The hotel is located right in the heart of the Historic District, right across the street from the beach and the Washington Street Mall</p>
		            <p><b>Valet Parking</b> is provided. Please pull up in front of the hotel and an attendant will assist you with the luggage and car.</p>
		          </div>
		        </div>
		        <div class="google-map">
		          <h2>from google maps:</h2>
		          <span>View Google Map &amp; Enter Your Starting Point</span>
		          <div class="specific">
		            <h2>from specific points</h2>
		            <a href="#">from the garden state parkway</a>
		           
		            <!--<p>Take the Parkway to the end, Exit "0", to 109 South, Follow Route 109 over a large then a small bridge. This becomes Lafayette Street. Follow Lafayette Street through two traffic lights to the end. Lafayette will end at Jackson Street where there is a green liquor store called "Collier's". Turn left onto Jackson Street and immediately turn right onto Mansion Street. Continue to stop sign and turn left onto Perry Street. Make first right onto Congress Place and turn left into hotel reception area. </p>-->
		            <a href="#">from the cape may lewes ferry</a>
		            <a href="#">from new york</a>
		            <a href="#">from washington/baltimore</a>
		            <a href="#">from philadelphia</a>
		            
		          </div>
		        </div>
		    </div>
    </div>
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('sea-spa-directions')) { ?>
<style>
body{background:#EEEBE6;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>



