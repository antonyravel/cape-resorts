<?php
/*
Template Name: Concierge
*/
?>
<?php get_header(); ?>
<section>

 <img src="<?php echo child_template_directory; ?>/img/con-images/concierge.png" class="con-logo" alt="Cape resorts Concierge"/>
<!-- concierge Left Side bar Starts -->
		<?php get_sidebar('concierge'); ?>


<div class="content home">
<h2 class="heading parent_cat">Your guide to what's cool at the cape</h2>
<?php 

$count = 0;
//$post_type = array('activities', 'seasonalevents', 'entertainment', 'stories', 'specialoffers');
$args['post_type']= array( 'seasonalevents','activities','entertainment', 'stories', 'specialoffers' );
	$args[ 'meta_key'] = 'displayhome';
	$args[ 'meta_value'] = 'yes';
$args['post_status']='publish';
	$home_posts = get_option( 'concierege_posts' );
	$home_posts = explode(",", $home_posts); 

	$all_posts = get_posts($args);
	$home_posts = get_option( 'concierege_posts' );
	$home_posts = explode(",", $home_posts); 
$post_ids = array();
	foreach($all_posts as $posts) {
			
		array_push();

		}


if(is_array($home_posts) !="") {

		foreach($all_posts as $posts) {
			if(!in_array($posts->ID, $home_posts))

				array_push($home_posts,$posts->ID);

		}	

		$args['post__in'] = $home_posts;
		$args['orderby'] = 'post__in';

	} 


$myquery = new WP_Query($args); 

while ( $myquery->have_posts() ) : $myquery->the_post();

	 $image_id = get_post_thumbnail_id();
	if ( $count == 0 ) {
	 $image_url = wp_get_attachment_image_src($image_id,'banner_501_300', true);
		?>
		<div class="home-banner">
			<img src="<?php echo $image_url[0]; ?>" alt="<?php the_title(); ?>"/>
			<div class="spring-harvest act-content">
			<h3><a href="<?php the_permalink(); ?>" class="title_tag" data-id="<?php the_ID(); ?>"><?php the_title(); ?></a></h3>
			<h4><?php the_field('subtitle1'); ?></h4>
			<i><?php the_field('subtitle_2'); ?></i>
			<span><?php the_field('subtitle_3'); ?></span>
			<p><?php echo wp_trim_words( get_the_content(), 40 ) ?></p>
			<a href="<?php echo the_permalink(); ?>" class="learn" data-id="<?php the_ID(); ?>">Read More</a>
			<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="Share" st_title="<?php the_title(); ?>" st_url="<?php echo get_permalink(); ?>"></span></a>
				
			</div>
		</div>
		
	<?php	}
	else {
		if($count==1)
			echo '<div>';

	 $image_url = wp_get_attachment_image_src($image_id,'thumb_345_154', true);
?>
<div class="first-div">
<img class="acti-img" src="<?php echo $image_url[0]; ?>" alt="<?php the_title(); ?>" />
<div class="act-content">
<h3><a href="<?php the_permalink(); ?>" class="title_tag" data-id="<?php the_ID(); ?>"><?php the_title(); ?></a></h3>
<h4><?php  the_field('subtitle1'); ?></h4>
<i><?php the_field('subtitle_2'); ?></i>
<span><?php the_field('subtitle_3'); ?></span>
<p><?php echo wp_trim_words( get_the_content(), 12 ) ?></p>

<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="Share" st_title="<?php the_title(); ?>" st_url="<?php echo get_permalink(); ?>"></span></a>
<a href="<?php echo the_permalink(); ?>" class="learn" data-id="<?php the_ID(); ?>">Learn More</a>
</div>
</div>

<?php
	}	
	$count++;
		if($count==$myquery->found_posts)
			echo '</div>';
	endwhile;
?>

</div>
</section>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
<?php get_footer(); ?>
