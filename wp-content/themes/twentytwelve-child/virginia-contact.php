<?php
	/*
	Template Name: the-virgina-contact
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(800)732-4236</p>
    <ul id="jqueryCycle">
<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>

	</ul>    
    <div class="banner-navigation">
      <!--<img src="<?php echo child_template_directory; ?>/img/congress-hall-images/corner-img.png" class="corner-img" alt="corner-img" />-->
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/capemay/virginia/">home</a></li>
        <li><a href="#">reservations</a></li>
        <li><a href="<?php echo site_url(); ?>/capemay/virginia/specials/">specials</a></li>
        <li><a href="#">accommodations</a></li>
        <li><a href="#">resort life</a></li>
        <li><a href="#">gallery</a></li>
        <li><a href="#">the ebbitt room</a></li>
        <li><a href="#">weddings &amp; events</a></li>
        <li><a href="#">meetings</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
    
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>25 Jackson Street</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/capemay/virginia/direction/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/capemay/virginia/contact/">contact us</a></li>
    </ul>
    <ul class="social-icons">
      <li><a href="http://www.tripadvisor.com/Hotel_Review-g46341-d92337-Reviews-Congress_Hall-Cape_May_New_Jersey.html" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/vector-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://www.youtube.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/tube-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://www.facebook.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/facebook-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://twitter.com/CongressHall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/twitter-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://pinterest.com/caperesorts/" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/pinit.jpg" alt="corner-img" /></a></li>
    </ul>
    

    <p class="mailing-list"><input type="text" name="mailing" value="JOIN OUR MAILING LIST" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
 <div class="cong-content cnt">
    <h3>contact us</h3>
    <div class="directionDiv">
		    <div class="direct-content">
		        <div class="gen-information">
		         	<h3>general information:</h3> 
						<h5>Jim Smith xxx xxx-xxxx</h5>
						<h5><a href="#">congress@congresshall.com</a></h5>
						
						<h3>general manager:</h3> 
						<h5><a href="#">jdaily@congresshall.com</a></h5>
						
						<h3>general sales:</h3> 
						<h5><a href="#">jplatt@congresshall.com</a></h5>
						
						<h3>wedding inquiries:</h3> 
						<h4>krista ostrander(609)884-6553</h4>
						<h5><a href="#">click here to fill out an inquiry form</a></h5>
		          </div>
		         <div class="gen-information cntus">
		         	<h3>general information:</h3> 
						<h5>jim smith xxx xxx-xxxx</h5>
						<h5><a href="#">congress@congresshall.com</a></h5>
						
						<h3>general manager:</h3> 
						<h5><a href="#">jdaily@congresshall.com</a></h5>
						
						<h3>general sales:</h3> 
						<h5><a href="#">jplatt@congresshall.com</a></h5>
						
						<h3>wedding inquiries:</h3> 
						<h4>krista ostrander(609)884-6553</h4>
						<h5><a href="#">click here to fill out an inquiry form</a></h5>
		          </div>
						
		          </div>
		         <div class="gen-information reach">
		         <h3>Reach us by phone:</h3>
		         <h5>( 888 ) 944 1816 OR (609) 884-8421</h5>
		         </div>      
		        
		       </div>
		        
		    </div>
    </div>
    
  </div>
<!--content end here-->  
<!--content end here-->
<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('direction')) { ?>
<style>
body{background:#f2f2f2;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>
