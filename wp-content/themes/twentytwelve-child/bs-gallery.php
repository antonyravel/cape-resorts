<?php
	/*
	Template Name: bs-gallery
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(888)944-1816</p>
    <img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider1.png" alt="slider" class="slider-img" />
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/beach-shack/">home</a></li>
        <li><a href="#">reservations</a></li>
        <li><a href="<?php echo site_url(); ?>/beach-shack-specials">specials</a></li>
        <li><a href="<?php echo site_url(); ?>/guest-room">guest rooms</a></li>
        <li><a href="#">resort life</a></li>
        <li><a href="<?php echo site_url(); ?>/beach-shack-gallery">gallery</a></li>
        <li><a href="#">rusty nail</a></li>
        <li><a href="#">weddings &amp; events</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li><a href="#">205 beach avenue</a></li>
      <li><a href="#">cape may, nj 08204</a></li>
      <li class="dr"><a href="http://sritsprojects.com/stage/caperesorts/beach-shack-directions/">directions</a></li>
      <li class="ct"><a href="http://sritsprojects.com/stage/caperesorts/beach-shack-contact/">contact us</a></li>
    </ul>
    <ul class="social-icons">
      <li><a href="http://www.tripadvisor.com/Hotel_Review-g46341-d92337-Reviews-Congress_Hall-Cape_May_New_Jersey.html" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/vector-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://www.youtube.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/tube-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://www.facebook.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/facebook-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://twitter.com/CongressHall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/twitter-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://pinterest.com/caperesorts/" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/pin-icon.png" alt="corner-img" /></a></li>
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="join our mailing list" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content gly">
  <h3>Gallery</h3>
    <div class="gallery-container">
      
      <div class="gallery-images">
         <ul>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/guest-room.png" alt="corner-img" /></a>
          <div class="galler-text">
            <h3>guest rooms</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/dining.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>dining</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/pool.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>pool</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/easter.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>easter weekend</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/beach.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>beach</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/guest-room.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>mothers day weekend</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/dining.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>guest rooms</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/pool.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>guest rooms</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
          <li><a href="#"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/easter.png" alt="corner-img" /></a>
            <div class="galler-text">
            <h3>guest rooms</h3>
            <span>optional informational header</span>
            <a href="#">view</a>
          </div>
          </li>
         
          
         </ul>
         
         <div class="pop-up">
				         
         </div>
      </div>
    </div>
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('beach-shack-gallery')) { ?>
<style>
body{background:#E7E6E4;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>

