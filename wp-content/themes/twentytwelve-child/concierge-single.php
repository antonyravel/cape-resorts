<?php
/*
Template Name: Concierge-single
*/
?>
<?php get_header(); ?>
<section>
<!-- concierge Left Side bar Starts -->
		<?php get_sidebar('concierge'); ?>
<div class="content activites">
<h2 class="heading">Activites&nbsp;:&nbsp;<span>All</span></h2>
<div>
<div class="first">
<div>
<img src="<?php echo child_template_directory; ?>/img/con-images/act-img1.png" alt="act"/>
<div class="act-content">
<h3>Mother's Day Brunch</h3>
<h4>the bike room march 15, 9pm-11pm</h4>
<i>$24 adults, $12 children</i>
<span>free gifts after noon</span>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Every May Cape May is the site of one of nature’s most fascinating animal migrations. </p>
<a href="#" class="share">Share</a>
<a href="<?php echo get_permalink(20); ?>" class="learn">Learn More</a>
</div>
</div>
<div>
<img src="<?php echo child_template_directory; ?>/img/con-images/act-img2.png" alt="act"/>
<div class="act-content">
<h3>Mother's Day Brunch</h3>
<h4>the bike room march 15, 9pm-11pm</h4>
<i>$24 adults, $12 children</i>
<span>free gifts after noon</span>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Every May Cape May is the site of one of nature’s most fascinating animal migrations. </p>
<a href="#" class="share">Share</a>
<a href="<?php echo get_permalink(20); ?>" class="learn">Learn More</a>
</div>
</div>
</div>
<div class="first">
<div>
<img src="<?php echo child_template_directory; ?>/img/con-images/act-img3.png" alt="act"/>
<div class="act-content">
<h3>Mother's Day Brunch</h3>
<h4>the bike room march 15, 9pm-11pm</h4>
<i>$24 adults, $12 children</i>
<span>free gifts after noon</span>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Every May Cape May is the site of one of nature’s most fascinating animal migrations. </p>
<a href="#" class="share">Share</a>
<a href="<?php echo get_permalink(20); ?>" class="learn">Learn More</a>
</div>
</div>
<div>
<img src="<?php echo child_template_directory; ?>/img/con-images/act-img4.png" alt="act"/>
<div class="act-content">
<h3>Mother's Day Brunch</h3>
<h4>the bike room march 15, 9pm-11pm</h4>
<i>$24 adults, $12 children</i>
<span>free gifts after noon</span>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Every May Cape May is the site of one of nature’s most fascinating animal migrations. </p>
<a href="#" class="share">Share</a>
<a href="<?php echo get_permalink(20); ?>" class="learn">Learn More</a>
</div>
</div>
</div>
<div class="first">
<div>
<img src="<?php echo child_template_directory; ?>/img/con-images/act-img3.png" alt="act"/>
<div class="act-content">
<h3>Mother's Day Brunch</h3>
<h4>the bike room march 15, 9pm-11pm</h4>
<i>$24 adults, $12 children</i>
<span>free gifts after noon</span>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Every May Cape May is the site of one of nature’s most fascinating animal migrations. </p>
<a href="#" class="share">Share</a>
<a href="<?php echo get_permalink(20); ?>" class="learn">Learn More</a>
</div>
</div>
<div>
<img src="<?php echo child_template_directory; ?>/img/con-images/act-img4.png" alt="act"/>
<div class="act-content">
<h3>Mother's Day Brunch</h3>
<h4>the bike room march 15, 9pm-11pm</h4>
<i>$24 adults, $12 children</i>
<span>free gifts after noon</span>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Every May Cape May is the site of one of nature’s most fascinating animal migrations. </p>
<a href="#" class="share">Share</a>
<a href="<?php echo get_permalink(20); ?>" class="learn">Learn More</a>
</div>
</div>
</div>
</div>

 

</div>
</section>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
<?php get_footer(); ?>
