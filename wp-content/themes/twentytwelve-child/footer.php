<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
<?php global $filename; ?>
</div></div> </div>
<footer>



<div class="footer-inner">

<div class="footer-logo">
<a href="<?php echo site_url(); ?>">
<img src="<?php echo child_template_directory; ?>/img/con-images/footer-logo.png" alt="Cape resorts Logo"/>
</a>
</div>
<div class="footer-container">
<div>
<h3>hotels</h3>
<ul>
<li><a href="<?php echo site_url(); ?>/hotels/capemay/congresshall/">CONGRESS HALL</a></li>
<li><a href="<?php echo site_url(); ?>/hotels/capemay/virginiahotel/">THE VIRGINIA</a></li>
<li><a href="<?php echo site_url(); ?>/hotels/capemay/cottages">THE VIRGINIA COTTAGES</a></li>
<li><a href="<?php echo site_url(); ?>/hotels/capemay/beachshack/">BEACH SHACK</a></li>
<li><a href="<?php echo site_url(); ?>/hotels/capemay/sandpiper">SANDPIPER BEACH CLUB</a></li>
<li><a href="<?php echo site_url(); ?>/hotels/capemay/thestar">THE STAR</a></li>	
<li><a href="<?php echo site_url(); ?>/hotels/capemay/baronscove/">BARONS COVE SAG HARBOR</a></li>
<li><a href="<?php echo site_url(); ?>/giftcards">GIFT CARDS</a></li>
</ul>
</div>
<div>
<h3>Restaurants</h3>
<ul>
<li><a href="<?php echo site_url(); ?>/restaurants/capemay/bluepigtavern">THE BLUE PIG TAVERN</a></li>
<li><a href="<?php echo site_url(); ?>/restaurants/capemay/ebbittroom">THE EBBITT ROOM</a></li>
<li><a href="<?php echo site_url(); ?>/restaurants/capemay/rustynail">THE RUSTY NAIL</a></li>
<li><a href="<?php echo site_url(); ?>/restaurants/capemay/boilerroom/">THE BOILER ROOM</a></li>
<li><a href="<?php echo site_url(); ?>/restaurants/capemay/brownroom/">THE BROWN ROOM</a></li>
<li><a href="<?php echo site_url(); ?>/restaurants/capemay/verandabar/">VERANDA BAR</a></li>
<li><a href="<?php echo site_url(); ?>/experiences/capemay/tommysfolly/tommysfollycoffeeshop">TOMMY’S FOLLY COFFEE SHOP</a></li>
<li><a href="<?php echo site_url(); ?>/hotels/capemay/thestar/dining">THE STAR COFFEE SHOP</a></li>
<li><a href="<?php echo site_url(); ?>/giftcards">GIFT CARDS</a></li>
</ul>
</div>
<div class="exp">
<h3>Experiences</h3>
<ul>
<li><a href="<?php echo site_url(); ?>/experiences/capemay/seaspa/">SEA SPA</a></li>
<li><a href="<?php echo site_url(); ?>/experiences/capemay/beachservice/">BEACH SERVICE</a></li>
<li><a href="<?php echo site_url(); ?>/experiences/capemay/tommysfolly">TOMMY’S FOLLY</a></li>
<li><a href="<?php echo site_url(); ?>/experiences/capemay/beachplumfarm">BEACH PLUM FARM</a></li>
<li><a href="<?php echo site_url(); ?>/experiences/capemay/westendgarage">WEST END GARAGE</a></li>
<li><a href="<?php echo site_url(); ?>/experiences/capemay/seasonal">SEASONAL EXPERIENCES</a></li>
<li><a href="<?php echo site_url(); ?>/giftcards">SHOPPING</a></li>
<!--<li><a href="<?php echo site_url(); ?>/experiences/shopping">SHOPPING</a></li>-->
<li><a href="<?php echo site_url(); ?>/giftcards">GIFT CARDS</a></li>
</ul>
</div>
<div class="conci">
<h3>CONCIERGE</h3>
<ul>
<?php global $pdfpath; ?>
<li><a href="<?php echo get_permalink(9); ?>">PRE-ARRIVAL</a></li>
		<li><a href="<?php echo get_permalink(47); ?>">DAILY CALENDAR</a></li>
		<li><a href="<?php echo get_permalink(72); ?>">ACTIVITIES</a></li>
		<li><a href="<?php echo get_permalink(80); ?>">SEASONAL EVENTS</a></li>
		<li><a href="<?php echo get_permalink(83); ?>">ENTERTAINMENT</a></li>
		<li><a href="<?php echo get_permalink(85); ?>">OUR STORIES</a></li>
		<li><a href="http://issuu.com/congresshall.com/docs/concierge_2013?e=1173389/2612690" target="_blank">CONCIERGE MAGAZINE</a></li>
</ul>
</div>
<div class="events">
<h3>WEDDINGS & EVENTS</h3>
<ul>
		<li><a href="<?php echo site_url(); ?>/capemay/weddingsevents/">CAPE MAY OVERVIEW</a></li>
		<li><a href="<?php echo site_url(); ?>/hotels/capemay/congresshall/meetings">CONGRESS HALL MEETING</a></li>
		<li><a href="<?php echo site_url(); ?>/hotels/capemay/virginiahotel/meetings">VIRGINIA MEETINGS</a></li>
		<li><a href="<?php echo site_url(); ?>/hotels/capemay/congresshall/weddingevents">CONGRESS HALL WEDDINGS</a></li>
		<li><a href="<?php echo site_url(); ?>/hotels/capemay/virginiahotel/weddingevents">VIRGINIA WEDDINGS</a></li>
		<li><a href="<?php echo site_url(); ?>/hotels/capemay/beachshack/weddingevents">BEACH SHACK WEDDINGS</a></li>
</ul>
</div>
<div class="specials">
<h3>specials</h3>
	<ul>
		<li><a href="<?php echo site_url(); ?>/concierge/capemay/specialoffers/">ALL</a></li>
		<li><a href="<?php echo site_url(); ?>/hotels/capemay/congresshall/specials/">CONGRESS HALL</a></li>
		<li><a href="<?php echo site_url(); ?>/hotels/capemay/virginiahotel/specials/">THE VIRGINIA</a></li>
		<li><a href="<?php echo site_url(); ?>/hotels/capemay/cottages/specials/">THE VIRGINIA COTTAGES</a></li>
		<li><a href="<?php echo site_url(); ?>/hotels/capemay/beachshack/specials/">BEACH SHACK</a></li>
		<li><a href="<?php echo site_url(); ?>/hotels/capemay/thestar/specials/">THE STAR</a></li>
		<li><a href="<?php echo site_url(); ?>/hotels/capemay/sandpiper/specials/">SANDPIPER BEACH CLUB</a></li>
		<li><a href="http://www.caperesorts.com/giftcards">GIFT CARDS</a></li>
	</ul>
</div>
</div>
<div class="footer-mailing-list">
  <p><a href="#"id="txtEmail">Join our Mailing List</a><input type="button" id="button" onclick="checkEmail();" /></p>
  <a href="https://www.facebook.com/caperesorts" title="Facebook" target="_blank" ><img src="<?php echo child_template_directory; ?>/img/bookingimages/mailing-facebook.png" style="width:26px;height:24px;" alt="facebook" /></a>
  <a href="https://twitter.com/CapeResorts" title="Twitter" target="_blank"><img src="<?php echo child_template_directory; ?>/img/bookingimages/mailing-twt.png" style="width:26px;height:24px;" alt="Twitter" /></a>
  <!--<p class="copy">&copy;2013 Cape Resorts, Incorporated. All Rights Reserved.</p>-->
</div>
<div class="footer-bottom">
	<ul>
		<li class="fr"><a href="<?php echo site_url(); ?>/about">About Cape Resorts</a></li>

		<li><a href="<?php echo site_url(); ?>/inthenews">In the News</a></li>
		<li><a href="<?php echo site_url(); ?>/development">Development</a></li>
		<li><a href="<?php echo site_url(); ?>/careers">Careers</a></li>
		<!--<li><a href="<?php echo site_url(); ?>/terms">Terms</a></li>-->
		<li><a href="<?php echo site_url(); ?>/contactus">Contact Us</a></li>
		<li class="ls"><a href="<?php echo site_url(); ?>/privacy">Privacy</a></li>
	</ul>
	<p class="copy">&copy;2013 Cape Resorts, Incorporated. All Rights Reserved.</p>

</div>
</div>
</footer>
</div><!-- .wrapper Ends -->
<div id="ajax_content" style="display:none;"></div>
	<div class="ajax-loader"><span>Loading...</span></div>
	<div id="ajax-pageLoader"><span>Loading...</span></div>
<?php	if(!is_front_page()) { ?>
<script>jQuery(document).ready(function() {stLight.options({publisher: "c058d568-dc16-43ad-937b-402f7cdf9598",doneScreen:false}); 
function fun1(event,service){
		if(event=="click" && service!="pinterest" && service!="Email"){
			$('.stclose').trigger('click');$('#shmain').show();
		}{
			//$('.stclose').trigger('click');$('#shmain').show();
		}//alert("service called is:"+service);
		//alert("popup closed:");
	}
	
	stLight.subscribe("click",fun1);

$("#closeX").click(function(){ $('#shmain').hide(); });
});
</script>
<?php } ?>
<script type="text/javascript" src="<?php echo child_template_directory; ?>/js/script.js"></script>
<?php wp_footer(); ?>
<div id="shmain" style="display:none;">
<div id="closeX" class="closeX"></div>
<span id="shpopular" class="popular">Share this with friends!</span>
<div class="shpageContainer" id="shdonePage">
<div id="shsuccessBox" class="shagainContainer">
<div id="shmsg_share_success" class="shsuccess">Your message was successfully shared</div>
</div>
</div>
</div>
</body>
</html>
