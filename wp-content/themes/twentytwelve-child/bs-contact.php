<?php
	/*
	Template Name: bs-contact-us
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(888)944-1816</p>
    <img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider1.png" alt="slider" class="slider-img" />
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/beach-shack/">home</a></li>
        <li><a href="#">reservations</a></li>
        <li><a href="<?php echo site_url(); ?>/beach-shack-specials">specials</a></li>
        <li><a href="<?php echo site_url(); ?>guest-room">guest rooms</a></li>
        <li><a href="#">resort life</a></li>
        <li><a href="<?php echo site_url(); ?>/beach-shack-gallery">gallery</a></li>
        <li><a href="#">rusty nail</a></li>
        <li><a href="#">weddings &amp; events</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>205 beach avenue</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/beach-shack-directions/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/beach-shack-contact/">contact us</a></li>
    </ul>
    <ul class="social-icons">
      <li><a href="http://www.tripadvisor.com/Hotel_Review-g46341-d92337-Reviews-Congress_Hall-Cape_May_New_Jersey.html" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/vector-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://www.youtube.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/tube-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://www.facebook.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/facebook-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://twitter.com/CongressHall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/twitter-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://pinterest.com/caperesorts/" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/pin-icon.png" alt="corner-img" /></a></li>
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="join our mailing list" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content cnt">
    <h3>contact us</h3>
    <div class="directionDiv">
		    <div class="direct-content">
		        <div class="gen-information">
		         	<h3>general information:</h3> 
						<h5>jim smith xxx xxx-xxxx</h5>
						<h5><a href="#">congress@congresshall.com</a></h5>
						
						<h3>general manager</h3> 
						<h5><a href="#">jdaily@congresshall.com</a></h5>
						
						<h3>general sales:</h3> 
						<h6><a href="#">jplatt@congresshall.com</a></h6>
						
						<h3>wedding inquiries:</h3> 
						<h6>krista ostrander(609)884-6553</h6>
						<h6><a href="#">click here to fill out an inquiry form</a></h6>
		          </div>
		          <div class="gen-information rt">
		         	<h3>beach shack</h3> 
		         	<h3>205 congress place</h3>
						  <h3>capemay, newjersy 08204</h3>
						  <h3>(877) 742-2507</h3>
						<div class="find-us">
						<h3>its easy for find us!</h3> 
						<p>Cape May is easily accessible from the Garden State Parkway and the Cape May-Lewes Ferry. The hotel is located right in the heart of the Historic District, right across the street from the beach and the Washington Street Mall</p>
		            <p>Valet Parking is provided. Please pull up in front of the hotel and an attendant will assist you with the luggage and car.</p>
						<p></p>
						</div>
						
		          </div>
		        	        
		        
		       </div>
		        
		    </div>
    </div>
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('beach-shack-contact')) { ?>
<style>
body{background:#E7E6E4;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>



