<?php
	/*
	Template Name: bs-specials
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(888)944-1816</p>
    <img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider1.png" alt="slider" class="slider-img" />
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/beach-shack/">home</a></li>
        <li><a href="#">reservations</a></li>
        <li><a href="<?php echo site_url(); ?>/beach-shack-specials">specials</a></li>
        <li><a href="<?php echo site_url(); ?>/guest-room">guest rooms</a></li>
        <li><a href="#">resort life</a></li>
        <li><a href="<?php echo site_url(); ?>/beach-shack-gallery">gallery</a></li>
        <li><a href="#">rusty nail</a></li>
        <li><a href="#">weddings &amp; events</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>205 beach avenue</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/beach-shack-directions/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/beach-shack-contact/">contact us</a></li>
    </ul>
    <ul class="social-icons">
      <li><a href="http://www.tripadvisor.com/Hotel_Review-g46341-d92337-Reviews-Congress_Hall-Cape_May_New_Jersey.html" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/vector-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://www.youtube.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/tube-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://www.facebook.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/facebook-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://twitter.com/CongressHall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/twitter-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://pinterest.com/caperesorts/" target="_blank"><img src="<?php echo child_template_directory; ?>/img/beach-shack-images/pin-icon.png" alt="corner-img" /></a></li>
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="join our mailing list" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content spl">
    <h3>specials</h3>
    <div class="spec">
      <ul>
        <li class="dining-content">
          <div>
          <img src="http://sritsprojects.com/stage/caperesorts/wp-content/themes/twentytwelve-child/img/congress-hall-images/dining-samp1.jpg" alt="dining">
         <div>
         <h4>clamorama!</h4>
         <h5>experience capemay clam fest and get a great deal</h5>
         <h5>book early and save an additional 15%</h5>
         <p>Within a year, the owners rebuilt the hotel, this time in brick rather than wood, and business blossomed once again. The hotel and Cape May proved so popular that they gained renown as a summer.</p>
         <p class="view-links">
         <span class="v-gallery"><a href="#">learn more</a><a href="#">book now</a></span>
      </p>
     </div>
</div>
        </li>
        <li class="dining-content">
          <div>
          <img src="http://sritsprojects.com/stage/caperesorts/wp-content/themes/twentytwelve-child/img/congress-hall-images/dining-samp1.jpg" alt="dining">
         <div>
         <h4>surfs up!</h4>
         <h5>experience capemay clam fest and get a great deal</h5>
         <h5>book early and save an additional 15%</h5>
         <p>Within a year, the owners rebuilt the hotel, this time in brick rather than wood, and business blossomed once again. The hotel and Cape May proved so popular that they gained renown as a summer.</p>
         <p class="view-links">
         <span class="v-gallery"><a href="#">learn more</a><a href="#">book now</a></span>
      </p>
     </div>
     </div>
        </li>
        <li class="dining-content">
          <div>
          <img src="http://sritsprojects.com/stage/caperesorts/wp-content/themes/twentytwelve-child/img/congress-hall-images/dining-samp1.jpg" alt="dining">
         <div>
          <h4>bed, breakfast and the beach</h4>
         <h5>experience capemay clam fest and get a great deal</h5>
         <h5>book early and save an additional 15%</h5>
         <p>Within a year, the owners rebuilt the hotel, this time in brick rather than wood, and business blossomed once again. The hotel and Cape May proved so popular that they gained renown as a summer.</p>
         <p class="view-links">
         <span class="v-gallery"><a href="#">learn more</a><a href="#">book now</a></span>
      </p>
     </div>
     </div>
        </li>
        <li class="dining-content">
          <div>
          <img src="http://sritsprojects.com/stage/caperesorts/wp-content/themes/twentytwelve-child/img/congress-hall-images/dining-samp1.jpg" alt="dining">
         <div>
         <h4>surfs up!</h4>
         <h5>experience capemay clam fest and get a great deal</h5>
         <h5>book early and save an additional 15%</h5>
         <p>Within a year, the owners rebuilt the hotel, this time in brick rather than wood, and business blossomed once again. The hotel and Cape May proved so popular that they gained renown as a summer.</p>
         <p class="view-links">
         <span class="v-gallery"><a href="#">learn more</a><a href="#">book now</a></span>
      </p>
     </div></div>
        </li>
      </ul>
  </div>
    
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('beach shack specials')) { ?>
<style>
 body{background:#E7E6E4;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>

