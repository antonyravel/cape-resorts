<?php
	/*
	Template Name: Press
	*/
?>
<?php get_header(); ?> 
<section>
<aside>
<div class="aside-content press-details">
<?php dynamic_sidebar( 'sidebar-5' ); ?>
</div>
</aside>
<div class="content press-container">
<h2 class="heading">Cape Resorts In the News</h2>
<div id="presstitle" style="border-bottom: 1px solid #9b733f; padding-bottom: 13px; margin-bottom: 14px;color: #6d6d65; font-size: 24px; font-weight: normal; font-family: 'Conv_06232ed9'; margin-bottom: 10px; clear: both; opacity: 0.8;display:none;">Cape Resorts Press ><span id="newstitle"></span><a href="#" id="backlink" style="float:right;" >&lt;Back</a></div>
<ul>
<?php 
$args = array(
	'post_type' => 'inthenews',
	'posts_per_page' => -1,
	'orderby'=>'date',
	'post_status'=>'publish'
	);
$myquery = new WP_Query( $args );
$i=0;
while ( $myquery->have_posts() ) : $myquery->the_post();

	 $image_id = get_post_thumbnail_id();
	 $image_url = wp_get_attachment_image_src($image_id,'thumbnail', true);
if($i%2==0){	?>
<li class="odd">
<?php }else{ ?>
<li>
<?php } ?>
<div class="imgtitle">    
<img src="<?php echo $image_url[0]; ?>" alt="image">
<?php if(get_field('pdf_file_path')){$pageurl = get_field('pdf_file_path');$prclass ='exterurl';$targ='target="_blank"';
}elseif(get_field('external_url')){$pageurl = get_field('external_url');$prclass ='exterurl';$targ='target="_blank"';
}else{$pageurl = get_permalink();$prclass ='';$targ='';} 

?>
    <span><a href="<?php echo $pageurl; ?>" class="pressnews <?php echo $prclass; ?>" <?php echo $targ; ?> data-id="<?php the_ID(); ?>"><?php echo str_replace(array('<p>','</p>'),array('',''),get_field('news_title')); ?></a></span>
</div>
<div class="contarea" style="float: left;display: none;padding: 6px;"><?php the_content(); ?>
<?php if(get_field('video_upload')){ ?>
<video width="600"  controls="controls" >
<source src="<?php echo get_field('video_upload'); ?>" type="video/mp4">
<object data="" width="600" >
<embed width="600" src="<?php echo get_field('video_upload'); ?>">
</object>
</video>
<?php } if(get_field('embeded_video_url')){ ?>
<object width="600" height="500">
<param name="movie" value="<?php echo get_field('embeded_video_url'); ?>" />
<embed src="<?php echo get_field('embeded_video_url'); ?>" type="application/x-shockwave-flash" width="600"  height="500" />
</object>
<?php } ?>
</div>
</li>
<?php $i++; endwhile; ?>
</ul>
</div>
</section>
</div> </div> </div>
<?php get_footer(); ?>
