<?php
	/*
	Template Name: Cape Resort Home Template
	*/
?>
<?php get_header(); ?>
<div class="CH-home">
<div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(888)944-1816</p>
<ul>
<li><img src="http://caperesorts.com/staging/wp-content/gallery/congress-hall-home/home-slider1.png" alt="Cape Resort" class="slider-img"></li>
</ul>
</div>
<div class="socialDiv home">
<p>From small inns to classic hotels, rambling cottages to modern condominiums, each Cape Resort has its own style defined by a chic sensibility</p>
</div>
</div> <!-- CH-home -->
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<div class="cong-content">
<ul class="home_specials01">
<li>
<p>enjoy</p>
<div class="wedding_home">
<p class="winter">Wedding specials</p>
<p>Wedding specials on select dates in 2013</p>
<strong><a href="#">Learn More.</a></strong></div></li>

<li>
<p style="padding: 0 0 0 20px;">special savings</p>
<div class="wedding_home middle">
<p class="winter">Winter wonderland</p>
<p>Join us this holiday season and experience the magic of Cape May! Enjoy the annual tree lighting.<strong><a href="#"> Learn More.</a></strong></p>
</div></li>

<li>
<p>don't miss out</p>
<div class="wedding_home">
<p class="winter">125th Congressional Retreat</p>
<p>Getaway with dinner, drinks and a relaxing spa treatement. Starting at $145</p>
<strong><a href="#">Learn More</a></strong></div></li>

</ul>
</div>
<p class="star08"><img src="http://caperesorts.com/staging/wp-content/themes/twentytwelve-child/img/con-images/star.png" alt="Capre resorts star"><img src="http://caperesorts.com/staging/wp-content/themes/twentytwelve-child/img/con-images/star.png" alt="Capre resorts star"><img src="http://caperesorts.com/staging/wp-content/themes/twentytwelve-child/img/con-images/star.png" alt="Capre resorts star"></p>

<!--content starts here-->

			
		


  </div> 

<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('cape-resort-home')) { ?>
<style>
#tabs .all-tabs{min-height: 20px;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>
