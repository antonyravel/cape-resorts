<?php
	/*
	Template Name: Sea-spa-specials
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(888)944-1816</p>
    
    
    <ul id="jqueryCycle">
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider2.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider3.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider4.png" alt="slider" class="slider-img" /></li>
	</ul>
    
    
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/sea-spa/">home</a></li>
        <li><a href="<?php echo site_url(); ?>/sea-spa-menu/">spa menu</a></li>
        <li><a href="<?php echo site_url(); ?>/sea-spa-specials/">spa specials</a></li>
        <li><a href="<?php echo site_url(); ?>/sea-spa-gallery/">gallery</a></li>
        <li><a href="#">book a treatment</a></li>
        <li><a href="#">congress hall</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>200 congress place</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/sea-spa-directions/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/sea-spa-contactus/">contact us</a></li>
    </ul>
    <ul class="social-icons">
      <li><a href="http://www.tripadvisor.com/Hotel_Review-g46341-d92337-Reviews-Congress_Hall-Cape_May_New_Jersey.html" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/vector-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://www.youtube.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/tube-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://www.facebook.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/facebook-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://twitter.com/CongressHall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/twitter-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://pinterest.com/caperesorts/" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/pin-icon.png" alt="corner-img" /></a></li>
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="Join our Mailing List" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content">
  
   <div class="tabsDiv">
   <div class="all-tabs spa">
   <h3>spa specials</h3>
    <div>
    <h4>lorem ipsom</h4>
    <h5>Optional Informational Header (with Square footage)</h5>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the.</p>
<p class="view-links">  
<a href="javascript:void(0);" class="share"><span class="st_sharethis" displaytext="SHARE" st_title="4th of July Celebration" st_url="http://sritsprojects.com/stage/caperesorts/?post_type=seasonalevents&amp;p=278"></span></a>
<span class="v-gallery"><a href="#">book now</a></span>
</p></div>
<div>
    <h4>lorem ipsom</h4>
    <h5>Optional Informational Header</h5>
    <h5>Optional Second Informational Header</h5>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the.</p>
<p class="view-links">  
<a href="javascript:void(0);" class="share"><span class="st_sharethis" displaytext="SHARE" st_title="4th of July Celebration" st_url="http://sritsprojects.com/stage/caperesorts/?post_type=seasonalevents&amp;p=278"></span></a>
<span class="v-gallery"><a href="#">book now</a></span>
</p></div>
<div>
    <h4>lorem ipsom</h4>
    <h5>Optional Informational Header (with Square footage)</h5>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the.</p>
<p class="view-links">  
<a href="javascript:void(0);" class="share"><span class="st_sharethis" displaytext="SHARE" st_title="4th of July Celebration" st_url="http://sritsprojects.com/stage/caperesorts/?post_type=seasonalevents&amp;p=278"></span></a>
<span class="v-gallery"><a href="#">book now</a></span>
</p></div>


</div>
   </div>
   <div class="rt-bar">
    <div class="spring">
      <h6>try our</6>
      <h3>spa sampler package</h3>
      <p>available through May 1, 2013</p>
    </div>
    <span class="hr-w"></span>
    <div class="sea">
      <p>40% off</p>
      <h2>aveda</h2>
      <h6>products</h6>
    </div>
    <span class="hr-w"></span>
    <div class="retreat">
      <h6>easter weekend</h6>
      <h3>extended hours</h3>
      <p>Open Daily Thursday&nbsp;3/28 - Sunday 4/7</p>
      <p> Closed Easter Sunday, March 31</p>
    </div>
    <span class="hr-w"></span>
    <div class="resort-video">
      <a href="#">downlaod the <b>spa menu</b></a>
    </div>
   </div>
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('sea-spa-specials')) { ?>
<style>
body{background:#E7E6E4;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>



