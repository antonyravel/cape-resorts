<?php
	/*
	Template Name: sandpiper-contactus
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(800)732-7816</p>
    <ul id="jqueryCycle">
<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
	</ul>
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/capemay/sandpiper/">home</a></li>
        <li><a href="<?php echo site_url(); ?>/capemay/sandpiper/sandpiper-specials/">SPECIALS</a></li>
        <li><a href="#">ACCOMMODATIONS</a></li>
        <li><a href="#">RESORT LIFE</a></li>
        <li><a href="#">GALLERY</a></li>
        <li><a href="#">OWNER’S SIGNIN</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>11 West Beach Ave</li>
      <li>Cape May, NJ 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/capemay/sandpiper/sandpiper-direction">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/capemay/sandpiper/sandpiper-contact">contact us</a></li>
    </ul>
    <ul class="social-icons">     
       <li class="vec-icon"><a href="#" target="_blank"></a></li>      
       <li class="you-icon"><a href="#" target="_blank"></a></li> 
      <li class="fac-icon"><a href="#" target="_blank"></a></li>      
       <li class="twi-icon"><a href="#" target="_blank"></a></li>      
        <li class="pin-icon"><a href="#" target="_blank"></a></li>           
    </ul>
    
    <p class="mailing-list"><a class="mail-a" href="#">JOIN OUR MAILING LIST</a>
    <button class="mail-bt"> </button>
    </p>    
  </div>
</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content cnt">
    <h3>contact us</h3>
    <div class="directionDiv">
		    <div class="direct-content">
		        <div class="gen-information">
		         	<h3>general information:</h3> 
						<h5>jim smith xxx xxx-xxxx</h5>
						<h5><a href="#">congress@congresshall.com</a></h5>
						
						<h3>general manager</h3> 
						<h5><a href="#">jdaily@congresshall.com</a></h5>
						
						<h3>general sales:</h3> 
						<h5><a href="#">jplatt@congresshall.com</a></h5>						
		          </div>
		         <div class="gen-information cntus">
		         	<h3>general information:</h3> 
						<h5>jim smith xxx xxx-xxxx</h5>
						<h5><a href="#">congress@congresshall.com</a></h5>
						
						<h3>general manager</h3> 
						<h5><a href="#">jdaily@congresshall.com</a></h5>
						
						<h3>general sales:</h3> 
						<h5><a href="#">jplatt@congresshall.com</a></h5>						
		          </div>
						
		          </div>
		         <div class="gen-information reach">
		         <h3>REACH US BY PHONE:</h3>
		         <h5>( 888 ) 944 1816 OR (609) 884-8421</h5>
		         </div>      
		        
		       </div>
		        
		    </div>
    </div>
    
  </div>
<!--content end here-->  
<!--content end here-->


<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('sandpiper-contact')) { ?>
<style>
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>

