<?php
	/*
	Template Name: tommy-folly
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(888)944-1816</p>
    <ul id="jqueryCycle">
<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/slider-1.png" alt="slider" class="slider-img" /></li>

	</ul>
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/tommys-folly/">home</a></li>
        <li><a href="#">tommy's folly gift</a></li>
        <li><a href="#">tommy's folly home</a></li>
        <li><a href="#">tommy's folly boutique</a></li>
        <li><a href="#">congress hall</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>200 congress place</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/tommy-folly-directions/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/tommy-folly-contact/">contact us</a></li>
    </ul>
    <ul class="social-icons">     
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/vector-icon.png" alt="corner-img" /></a></li>      
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/youtube.png" alt="corner-img" /></a></li> 
      <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/facebook.png" alt="corner-img" /></a></li>      
       <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/twitter.png" alt="corner-img" /></a></li>      
        <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/tommy-folly-images/pinit.png" alt="corner-img" /></a></li>           
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="JOIN OUR MAILING LIST" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>    
  </div>
</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content">
    <div class="tabsDiv">
    <div class="tommy-cont">
    <h2>Welcome</h2>
    <h3>INTRODUCTORY HEADER ABOUT TOMMY'S FOLLY<br>
    SECONDARY SUB-HEADER			
    </h3>								
    <p>
    Octavius libere insectat lascivius oratori, ut tremulus catelli imputat Medusa, quod fragilis zothecas senesceret quadrupei, et Octavius conubium santet parsimonia matrimonii, etiam tremulus oratori lucide corrumperet adlaudabilis concubine. Agricolae circumgrediet apparatus bellis. Catelli imputat umbraculi, ut rures corrumperet tremulus umbraculi
    </p>
    <p>
      quamquam apparatus bellis satis spinosus agnascor Aquae Sulis. Zothecas insectat fiducia suis. Catelli iocari saburre, semper apparatus bellis adquireret incredibiliter perspicax zothecas, et umbraculi lucide suffragarit.
    </p>
    <p>
      Parsimonia matrimonii, etiam tremulus oratori lucide corrumperet adlaudabilis concubine. Agricolae circumgrediet apparatus bellis. Catelli imputat umbraculi, ut rures corrumperet tremulus umbraculi. Arsimonia matrimonii, etiam tremulus oratori lucide corrumperet adlaudabilis concubine. Agricolae circumgrediet apparatus bellis. Catelli imputat umbraculi, ut rures corrumperet tremulus umbraculi.
    </p>

   </div>
   </div>
   <div class="rt-bar">
    <div class="spring">
      <h2>Spring Break</h2>
      <h3>Enjoy the our fun getaways & activities</h3>
      <p>
      <a href="http://www.congresshall.com/content/winter-weddings.php" target="_blank">learn more</a>
      </p>
    </div>
    <div class="spring">
      <h2>Sea spa</h2>
      <h3>Relax, unwind, and treat yourself to a spa day.</h3>
      <p>
      <a href="http://www.congresshall.com/content/winter-weddings.php" target="_blank">learn more about this package</a>
      </p>
    </div>
    <div class="spring">
      <h2>125th congress <br>hall retreat</br></h2>
      <p>
      <a href="http://www.congresshall.com/content/winter-weddings.php" target="_blank">Getaway....with dinner, drinks and a relaxing spa treatment.starting at $125.</a>
      </p>
    </div>
    <div class="resort-video"><a href="http://caperesortsgroup.com/video.html" target="">Watch the Cape Resorts Video</a></div>
   </div>
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('tommys-folly')) { ?>
<style>
body{background:#F9F9F9;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>



