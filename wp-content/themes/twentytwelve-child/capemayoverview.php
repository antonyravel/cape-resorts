<?php
	/*
	Template Name: Cape May Overview
	*/
?>
<?php get_header();

global $p_id;
$p_id = get_field('pageproperties');

global $subMenu; 
global $menu; 

	$subMenu = urldecode($wp_query->query_vars['subMenu']);
	if(!$subMenu)
		$subMenu='home';
	$menu = getMenuBySlug($p_id, $subMenu);
?>
<div class="CH-home">

	<!-- Center_content bar -->
		<?php get_template_part( 'center', 'capemayoverviewcontent' ); ?>

</div> <!-- CH-home -->


</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->

	<?php if($subMenu=='gallery'): 
			$congclass= 'cong-content gly congresshall';
		  else:
			$congclass= 'cong-content congresshall';
		endif;
		?>
  <div class="<?php echo $congclass; ?>">


		<?php echo get_cong_template($menu, $p_id, $subMenu); ?>


		<div id="contactUs" class="hide"> 
		
		 <?php get_contact_temp($p_id, 'contactus'); ?>
		
		</div> 
		<div id="directions" class="hide"> 
		
		 <?php get_address_temp($p_id, 'address');  ?>
		
		</div>


  </div> 

<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('congresshall')) { ?>
<style>

.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>
