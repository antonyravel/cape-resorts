<?php
	/*
	Template Name: sandpiper-beach-home
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(800)732-7816</p>
    <ul id="jqueryCycle">
<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/sandpiper-images/slider.png" alt="slider" class="slider-img" /></li>
	</ul>
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/capemay/sandpiper/">home</a></li>
        <li><a href="<?php echo site_url(); ?>/capemay/sandpiper/sandpiper-specials/">SPECIALS</a></li>
        <li><a href="#">ACCOMMODATIONS</a></li>
        <li><a href="#">RESORT LIFE</a></li>
        <li><a href="#">GALLERY</a></li>
        <li><a href="#">OWNER’S SIGNIN</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>11 West Beach Ave</li>
      <li>Cape May, NJ 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/capemay/sandpiper/sandpiper-direction">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/capemay/sandpiper/sandpiper-contact">contact us</a></li>
    </ul>
    <ul class="social-icons">     
       <li class="vec-icon"><a href="#" target="_blank"></a></li>      
       <li class="you-icon"><a href="#" target="_blank"></a></li> 
      <li class="fac-icon"><a href="#" target="_blank"></a></li>      
       <li class="twi-icon"><a href="#" target="_blank"></a></li>      
        <li class="pin-icon"><a href="#" target="_blank"></a></li>           
    </ul>
    
    <p class="mailing-list"><a class="mail-a" href="#">JOIN OUR MAILING LIST</a>
    <button class="mail-bt"> </button>
    </p>    
  </div>
</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content">
    <div class="tabsDiv">
    <div class="sandpiper-cont">
      <h3>Welcome to the Sandpiper Beach Club</h3>
     <p>
   Saburre fermentet bellus matrimonii. Fiducia suis fortiter suffragarit oratori, ut saburre miscere apparatus bellis, semper vix tremulus syrtes senesceret Pompeii, ut chirographi agnascor agricolae, utcunque quinquennalis quadrupei imputat rures. Umbraculi frugaliter suffragarit apparatus bellis, quamquam parsimonia syrtes adquireret fiducia suis. Gulosus oratori vocificat pessimus parsimonia agricolae, etiam quadrupei iocari saburre, quamquam utilitas fiducia suis infeliciter fermentet perspicax agricolae. Octavius libere insectat lascivius oratori, ut tremulus catelli imputat Medusa, quod fragilis zothecas senesceret quadrupei, et Octavius conubium santet parsimonia matrimonii, etiam tremulus oratori lucide corrumperet adlaudabilis concubine. Agricolae circumgrediet apparatus bellis. Catelli imputat umbraculi, ut rures corrumperet tremulus umbraculi. Saetosus apparatus bellis insectat zothecas. Bellus concubine infeliciter fermentet aegre verecundus zothecas. Adlaudabilis umbraculi imputat ossifragi. Pretosius syrtes fermentet Pompeii, semper agricolae iocari umbraculi.</p>
    <p>
    Saetosus fiducia suis miscere chirographi, et saburre fermentet quadrupei, quod incredibiliter adlaudabilis saburre iocari vix fragilis cathedras.
    </p>
    <p>
    Bellus syrtes conubium santet matrimonii. Zothecas fermentet Medusa, ut ossifragi conubium santet tremulus syrtes. Verecundus matrimonii miscere saetosus apparatus bellis, etiam umbraculi corrumperet syrtes, quamquam agricolae pessimus frugaliter praemuniet concubine. Catelli amputat quadrupei, utcunque verecundus chirographi circumgrediet catelli, etiam fragilis umbraculi celeriter adquireret tremulus oratori, quamquam apparatus bellis satis spinosus agnascor Aquae Sulis
    </p>

   </div>
   </div>
   <div class="rt-bar">
    <div class="spring">
      <h2>Free Night</h2>
      <h3>STAY THREE NIGHTS AND GET A free fourth night</h3>
      <p>
      <a href="http://www.congresshall.com/content/winter-weddings.php" target="_blank">learn more</a>
      </p>
    </div>
    <div class="spring">
      <h2>Get Spring Fever</h2>
      <h3>indulge in the ultimate spring break experience.</h3>
      <p>
      <a href="http://www.congresshall.com/content/winter-weddings.php" target="_blank">learn more about this package</a>
      </p>
    </div>
    <div class="spring">
      <h2>125th Congressional<br>Retreat</br></h2>
      <p>
      <a href="http://www.congresshall.com/content/winter-weddings.php" target="_blank">GETAWAY...WITH DINNER, DRINKS AND A RELAXING SPA TREATMENT. STARTING AT $125</a>
      </p>
    </div>
    <div class="resort-video"><a href="http://caperesortsgroup.com/video.html" target="">Watch the Cape Resorts Video.</a></div>
   </div>
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>

<!-- for body background image -->
<?php if(is_page('sandpiper')) { ?>
<style>
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>



