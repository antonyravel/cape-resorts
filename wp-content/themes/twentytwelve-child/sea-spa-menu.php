<?php
	/*
	Template Name: Sea-spa-menu
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(888)944-1816</p>
    
    
    <ul id="jqueryCycle">
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider1.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider2.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider3.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider4.png" alt="slider" class="slider-img" /></li>
	</ul>
    
    
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/sea-spa/">home</a></li>
        <li><a href="<?php echo site_url(); ?>/sea-spa-menu/">spa menu</a></li>
        <li><a href="<?php echo site_url(); ?>/sea-spa-specials/">spa specials</a></li>
        <li><a href="<?php echo site_url(); ?>/sea-spa-gallery/">gallery</a></li>
        <li><a href="#">book a treatment</a></li>
        <li><a href="#">congress hall</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>200 congress place</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="http://sritsprojects.com/stage/caperesorts/directions/">directions</a></li>
      <li class="ct"><a href="http://sritsprojects.com/stage/caperesorts/contact-us/">contact us</a></li>
    </ul>
    <ul class="social-icons">
      <li><a href="http://www.tripadvisor.com/Hotel_Review-g46341-d92337-Reviews-Congress_Hall-Cape_May_New_Jersey.html" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/vector-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://www.youtube.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/tube-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://www.facebook.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/facebook-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://twitter.com/CongressHall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/twitter-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://pinterest.com/caperesorts/" target="_blank"><img src="<?php echo child_template_directory; ?>/img/sea-spa-images/pin-icon.png" alt="corner-img" /></a></li>
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="join our mailing list" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content">
    <div class="tabsDiv spa-menu">
    
      <div id="tabs">
        <ul>
          <li><a href="#">massages</a></li>
          <li><a href="#">body treatments</a></li>
          <li><a href="#">facials</a></li>
          <li><a href="#">beauty</a></li>
        </ul>
        <div class="all-tabs">
          <div id="welcome">
            <div>
              <a href="#">Change of Scenery (Anti-Aging)<span>50 minutes $130</span></a>
              <p>A SkinCeuticals reconditioning and firming treatment, with the use of Vitamin C ideal
               for dehydrated, aging,and environmentally damaged skin.</p>
            </div>
            
            <div>
              <a href="#">Running for Congress<span>50 minutes $155; Peel only $85</span></a>
              <p>A glycolic peel that aides in minimizing the appearance of fine lines, evens skin texture, helps to clear acne,and fade pigmentation. Opt for the peel only or pair with a Vitamin C Masque for a firmer appearance.</p>
            </div>
            
            <div>
              <a href="#">Facial add-ons</a>
              <p>Eye Expert <span>$35</span> 10 minutes</p>
              <p>Warm Hand Treatment $10; Feet $15 </p>
            </div>
            
            <div>
              <a href="#">MANI/PEDi</a>
              <p>Manicure <span>$25</span></p>
              <p>Your nails are spruced, cuticles are tailored, a hand to elbow massage, finish up with the polish of your choice.</p>
            </div>
            
            <div>
              <a href="#">Exfoliating Manicure<span>$45</span></a>
              <p>In addition to our Old Fashioned Manicure receive a Salt scrub to the elbow and a warming Algae hand mask.</p>
            </div>
            
            <div>
              <a href="#">Pedicure<span>$55</span></a>
              <p>Your feet will be renewed, this service includes grooming, exfoliation, and a lower leg massage. Finish with the polish of your choice.</p>
              </div>
              <div>
              <a href="#">Pedicure Ritual<span>$75</span></a>
              <p>Includes an Ocean Sensation foot bath, Aquatic foot scrub, warm Basalt stone massage as well as a warming Algae foot mask</p>
            </div>
            <div>
              <a href="#">Gentleman’s Manicure<span>$20</span></a>
              <p>Includes nail grooming a hand to elbow massage and a buff shine.</p>
            </div>
            
            <div>
             <a href="#">Gentleman’s Pedicure<span>$45</span></a>
              <p>Manicure <span>$25</span></p>
              <p>Includes nail grooming, exfoliation, lower leg massage and finished with a buff shine.</p>
            </div>
              
              
            
          </div>

          <div class="tabs_div">
          
          </div>
          
      </div>
    </div>
   </div>
   <div class="rt-bar">
    <div class="spring">
      <h6>try our</6>
      <h3>spa sampler package</h3>
      <p>available through May 1, 2013</p>
    </div>
    <span class="hr-w"></span>
    <div class="sea">
      <p>40% off</p>
      <h2>aveda</h2>
      <h6>products</h6>
    </div>
    <span class="hr-w"></span>
    <div class="retreat">
      <h6>easter weekend</h6>
      <h3>extended hours</h3>
      <p>Open Daily Thursday&nbsp;3/28 - Sunday 4/7</p>
      <p> Closed Easter Sunday, March 31</p>
    </div>
    <span class="hr-w"></span>
    <div class="resort-video">
      <a href="#">downlaod the <b>spa menu</b></a>
    </div>
   </div>
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('sea-spa-menu')) { ?>
<style>
body{background:#E7E6E4;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>



