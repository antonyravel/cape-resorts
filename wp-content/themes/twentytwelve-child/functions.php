<?php
ob_start();
include('cf7-custom-validation.php');
define('child_template_directory', dirname( get_bloginfo('stylesheet_url')) );

/* Next gen Script Exclusions */
define('NGG_SKIP_LOAD_SCRIPTS', true);


/* Next Gen Style Exclusions */

add_action('wp_print_styles','my_deregister_styles', 100);
function my_deregister_styles(){

		wp_deregister_style('NextGEN');
		wp_deregister_style('shutter');
}
/************************/


$termarray =array();
global $termarray;


global $wpdb;
if(!isset( $wpdb->{$table_prefix.'conciergepages'} ) ){
  $wpdb->conciergepages = $table_prefix.'conciergepages';
  
}
    if(!isset( $wpdb->{$table_prefix.'hotels'} ) ){
    $wpdb->hotels = $table_prefix.'hotels';

    }

//=================register Concierge menus======================

 $conciergeMenus = array();
$conciergeMenus['pre-arrival'] = 'PRE-ARRIVAL';
$conciergeMenus['daily-calender'] = 'DAILY CALENDAR';
$conciergeMenus['activities'] = 'ACTIVITIES';
$conciergeMenus['seasonal-events'] = 'SEASONAL EVENTS';
$conciergeMenus['entertainment'] = 'ENTERTAINMENT';
$conciergeMenus['stories'] = 'STORIES';
$conciergeMenus['special-offers'] = 'SPECIAL OFFERS'; 
$conciergeMenus['concierge-magazine'] = 'CONCIERGE MAGAZINE'; 




add_action('init', 'rc_register_posts');

/**********concierge posts **************/

/* activities, seasonalevents, entertainment, stories, specialoffers */

function rc_register_posts()
{
    
    $args = array(
           'label' => __('Activities'),
           'singular_label' => __('Activities'),
           'public' => true,
           'show_ui' => true,
           'capability_type' => 'page',
           'hierarchical' => true,
           'menu_position' => 4,
           'rewrite' => true,
           'supports' => array('title', 'thumbnail', 'editor' , 'page-attributes','custom-fields'),
           'exclude_from_search' => false
         );  

         register_post_type('activities',$args);


    $args1 = array(
           'label' => __('Seasonal events'),
           'singular_label' => __('Seasonal event'),
           'public' => true,
           'show_ui' => true,
           'capability_type' => 'page',
           'hierarchical' => true,
    		'menu_position' => 4,
           'rewrite' => true,
           'supports' => array('title', 'thumbnail', 'editor' , 'page-attributes','custom-fields'),
           'exclude_from_search' => false
         );  

         register_post_type('seasonalevents',$args1);

	 register_taxonomy('seasonalevents_category', 'seasonalevents', array(
		'hierarchical' => true,
		'labels' => array(
			'name' => _x( 'Seasonal events category', 'taxonomy general name' ),
			'singular_name' => _x( 'Seasonal events category', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search seasonalevents_category' ),
			'all_items' => __( 'All seasonalevents_categories' ),
			'parent_item' => __( 'Parent seasonalevents_category' ),
			'parent_item_colon' => __( 'Parent seasonalevents_category:' ),
			'edit_item' => __( 'Edit seasonalevents_category' ),
			'update_item' => __( 'Update seasonalevents_category' ),
			'add_new_item' => __( 'Add New seasonalevents_category' ),
			'new_item_name' => __( 'New seasonalevents_category Name' ),
			'menu_name' => __( 'Seasonal category' ),
		),
		// Control the slugs used for this taxonomy
		'rewrite' => array(
			'slug' => 'seasonalevents_categories', // This controls the base slug that will display before each term
			'with_front' => false, // Don't display the category base before "/locations/"
			'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
		),
	)); 


	register_taxonomy('activity_category', 'activities', array(
		'hierarchical' => true,
		'labels' => array(
			'name' => _x( 'Activity category', 'taxonomy general name' ),
			'singular_name' => _x( 'Activity category', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search activity_category' ),
			'all_items' => __( 'All activity_categories' ),
			'parent_item' => __( 'Parent activity_category' ),
			'parent_item_colon' => __( 'Parent activity_category:' ),
			'edit_item' => __( 'Edit activity_category' ),
			'update_item' => __( 'Update activity_category' ),
			'add_new_item' => __( 'Add New activity_category' ),
			'new_item_name' => __( 'New activity_category Name' ),
			'menu_name' => __( 'Activity category' ),
		),
		// Control the slugs used for this taxonomy
		'rewrite' => array(
			'slug' => 'activity_categories', // This controls the base slug that will display before each term
			'with_front' => false, // Don't display the category base before "/locations/"
			'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
		),
	));


    $args2 = array(
           'label' => __('Entertainment'),
           'singular_label' => __('Entertainment'),
           'public' => true,
           'show_ui' => true,
           'capability_type' => 'page',
           'hierarchical' => true,
	'menu_position' => 4,
           'rewrite' => true,
           'supports' => array('title', 'thumbnail', 'editor' , 'page-attributes','custom-fields'),
           'exclude_from_search' => false
         );  

         register_post_type('entertainment',$args2);

	 register_taxonomy('entertainment_category', 'entertainment', array(
		'hierarchical' => true,
		'labels' => array(
			'name' => _x( 'Entertainments category', 'taxonomy general name' ),
			'singular_name' => _x( 'Entertainment category', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search entertainment_category' ),
			'all_items' => __( 'All entertainments_categories' ),
			'parent_item' => __( 'Parent entertainment_category' ),
			'parent_item_colon' => __( 'Parent entertainment_category:' ),
			'edit_item' => __( 'Edit entertainment_category' ),
			'update_item' => __( 'Update entertainments_category' ),
			'add_new_item' => __( 'Add New entertainment_category' ),
			'new_item_name' => __( 'New entertainment_category Name' ),
			'menu_name' => __( 'Entertainment category' ),
		),
		// Control the slugs used for this taxonomy
		'rewrite' => array(
			'slug' => 'entertainments_categories', // This controls the base slug that will display before each term
			'with_front' => false, // Don't display the category base before "/locations/"
			'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
		),
	)); 





    $args3 = array(
           'label' => __('Stories'),
           'singular_label' => __('Story'),
           'public' => true,
           'show_ui' => true,
           'capability_type' => 'page',
           'hierarchical' => true,
	'menu_position' => 4,
           'rewrite' => true,
           'supports' => array('title', 'thumbnail', 'editor' , 'page-attributes','custom-fields'),
           'exclude_from_search' => false
         );  

         register_post_type('stories',$args3);

	 register_taxonomy('stories_category', 'stories', array(
		'hierarchical' => true,
		'labels' => array(
			'name' => _x( 'Stories category', 'taxonomy general name' ),
			'singular_name' => _x( 'Stories category', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search stories_category' ),
			'all_items' => __( 'All stories_categories' ),
			'parent_item' => __( 'Parent stories_category' ),
			'parent_item_colon' => __( 'Parent stories_category:' ),
			'edit_item' => __( 'Edit stories_category' ),
			'update_item' => __( 'Update stories_category' ),
			'add_new_item' => __( 'Add New story_category' ),
			'new_item_name' => __( 'New story_category Name' ),
			'menu_name' => __( 'Stories category' ),
		),
		// Control the slugs used for this taxonomy
		'rewrite' => array(
			'slug' => 'stories_categories', // This controls the base slug that will display before each term
			'with_front' => false, // Don't display the category base before "/locations/"
			'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
		),
	)); 



   $args4 = array(
           'label' => __('Special offers'),
           'singular_label' => __('Special offer'),
           'public' => true,
           'show_ui' => true,
           'capability_type' => 'page',
           'hierarchical' => true,
	'menu_position' => 5,
           'rewrite' => true,
			'show_in_nav_menus' => true,
           'supports' => array('title', 'thumbnail', 'editor' , 'page-attributes','custom-fields'),
           'exclude_from_search' => false,
         );  

         register_post_type('specialoffers',$args4);

	 register_taxonomy('offers_category', 'specialoffers', array(
		'hierarchical' => true,
		'labels' => array(
			'name' => _x( 'Offers category', 'taxonomy general name' ),
			'singular_name' => _x( 'Offers category', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search offers_category' ),
			'all_items' => __( 'All offers_categories' ),
			'parent_item' => __( 'Parent offers_category' ),
			'parent_item_colon' => __( 'Parent offers_category:' ),
			'edit_item' => __( 'Edit Offers_category' ),
			'update_item' => __( 'Update Offers_category' ),
			'add_new_item' => __( 'Add New Offers_category' ),
			'new_item_name' => __( 'New Offers_category Name' ),
			'menu_name' => __( 'Offers category' ),
		),
		// Control the slugs used for this taxonomy
		'rewrite' => array(
			'slug' => 'offers_categories', // This controls the base slug that will display before each term
			'with_front' => false, // Don't display the category base before "/locations/"
			'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
		),
	)); 


   $args5 = array(
           'label' => __('Menu Tabs'),
           'singular_label' => __('Menu Tab'),
           'public' => true,
           'show_ui' => true,
           'capability_type' => 'page',
           'hierarchical' => true,
           'menu_position' => 4,
           'rewrite' => true,
           'supports' => array('editor', 'title'),
           'exclude_from_search' => false
         );  

         register_post_type('menutab',$args5);

 /*  $args6 = array(
           'label' => __('News'),
           'singular_label' => __('News'),
           'public' => true,
           'show_ui' => true,
           'capability_type' => 'page',
           'hierarchical' => true,
	'menu_position' => 5,
           'rewrite' => true,
			'show_in_nav_menus' => true,
           'supports' => array('title', 'thumbnail', 'editor' , 'page-attributes','custom-fields'),
           'exclude_from_search' => false,
         );  

         register_post_type('news',$args6); */
   $args7 = array(
           'label' => __('In the News'),
           'singular_label' => __('In the News'),
           'public' => true,
           'show_ui' => true,
           'capability_type' => 'page',
           'hierarchical' => true,
	'menu_position' => 5,
           'rewrite' => true,
			'show_in_nav_menus' => true,
           'supports' => array('title', 'thumbnail', 'editor' , 'page-attributes','custom-fields'),
           'exclude_from_search' => false,
         );  

         register_post_type('inthenews',$args7);

  }
  
function get_current_post_type() {
  global $post, $typenow, $current_screen; 


  //we have a post so we can just get the post type from that
  if ( $post && $post->post_type )
    return $post->post_type;
    
  //check the global $typenow - set in admin.php
  elseif( $typenow )
    return $typenow;
    
  //check the global $current_screen object - set in sceen.php
  elseif( $current_screen && $current_screen->post_type )
    return $current_screen->post_type;
  
  //lastly check the post_type querystring
  elseif( isset( $_REQUEST['post_type'] ) )
    return sanitize_key( $_REQUEST['post_type'] );
	
elseif($_REQUEST['post'] ) {
	return get_post_type( $_REQUEST['post'] );

}

  //we do not know the post type!
  return null;
}

function remove_those_menu_items( ){
	$post_type = get_current_post_type();

if ( $post_type != 'activities' ) {
   	remove_menu_page('edit.php?post_type=activities');
}

if ( $post_type != 'seasonalevents' ) {
 	remove_menu_page('edit.php?post_type=seasonalevents');
}

if ($post_type != 'entertainment' ) {
 	remove_menu_page('edit.php?post_type=entertainment');
}

if ( $post_type != 'stories' ) {
 	remove_menu_page('edit.php?post_type=stories');
}

if ( $post_type != 'specialoffers' ) {   
 	remove_menu_page('edit.php?post_type=specialoffers');
}
	
}
add_filter( 'admin_menu', 'remove_those_menu_items' );

add_action( 'login_head', 'login_welcome',30);

function login_welcome() { ?>
<script type="text/javascript">// <![CDATA[
jQuery(document).ready(function() { jQuery("#login h1").before("<p style='margin-bottom: -15px;color: #906c15;font-weight: bold;font-size: 18px;text-align: center;'>Welcome Admin! Please login below:</p>");     
});
// ]]></script>
<?php
}


add_action('admin_print_footer_scripts','wpse57033_add_new_voucher_link');
function wpse57033_add_new_voucher_link(){

    $screen = get_current_screen();

   if( $screen->id == 'edit-activities' || $screen->id == 'activities' ){
		$url = site_url().'/wp-admin/edit-tags.php?taxonomy=activity_category&post_type=activities'; 
   }
elseif( $screen->id == 'edit-seasonalevents' || $screen->id == 'seasonalevents' ){
		$url = site_url().'/wp-admin/edit-tags.php?taxonomy=seasonalevents_category&post_type=seasonalevents'; 
   }
elseif( $screen->id == 'edit-entertainment' || $screen->id == 'entertainment' ){
		$url = site_url().'/wp-admin/edit-tags.php?taxonomy=entertainment_category&post_type=entertainment'; 
   }
elseif( $screen->id == 'edit-stories' || $screen->id == 'stories' ){
		$url = site_url().'/wp-admin/edit-tags.php?taxonomy=stories_category&post_type=stories'; 
   }
elseif( $screen->id == 'edit-specialoffers' || $screen->id == 'specialoffers' ){
		$url = site_url().'/wp-admin/edit-tags.php?taxonomy=offers_category&post_type=specialoffers'; 
   }

	if($url) {       ?>
            <script>
            jQuery('.wrap h2 .add-new-h2').after('<a href="<?php echo $url; ?>" class="add-new-h2">Manage Category</a><a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=conciergepages" class="add-new-h2" style="display:inline-block;">Back to Concierge manage page</a>');
            </script>
        <?php }


   if( $screen->id == 'edit-offers_category' ){
		$url1 = site_url().'/wp-admin/edit.php?post_type=specialoffers'; 
   }
elseif( $screen->id == 'edit-activity_category' ){
		$url1 = site_url().'/wp-admin/edit.php?post_type=activities'; 
   }
elseif( $screen->id == 'edit-entertainment_category' ){
		$url1 = site_url().'/wp-admin/edit.php?post_type=entertainment'; 
   }
elseif( $screen->id == 'edit-stories_category' ){
		$url1 = site_url().'/wp-admin/edit.php?post_type=stories'; 
   }
elseif( $screen->id == 'edit-seasonalevents_category' ){
		$url1 = site_url().'/wp-admin/edit.php?post_type=seasonalevents'; 
   }

	if($url1) {       ?>
            <script>
            jQuery('.wrap h2').css('display', 'inline-block').after('<a href="<?php echo $url1; ?>" class="add-new-h2" style="display:inline-block;">Add/Edit Posts</a><a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=conciergepages" class="add-new-h2" style="display:inline-block;">Back to Concierge manage page</a>');
            </script>
        <?php }
 }

register_sidebars(5);

if (function_exists('register_sidebar')) {
register_sidebar(array(
'name'=>'Contact Page',
'before_widget' => '',
'after_widget' => '',
'before_title' => '',
'after_title' => '',
));
}


add_image_size( 'thumb_345_154', 345, 154, true );
add_image_size( 'banner_501_300', 501, 300, true );
add_image_size( 'med_700_309',698, 310, false );
add_image_size( 'med_274_122',274, 122, true );
//if(!is_admin) {
function custom_admin_js() {
  
	wp_enqueue_script('migrate', 'http://code.jquery.com/jquery-migrate-1.2.1.js', array('jquery'), 'false', null);

wp_enqueue_script('date', child_template_directory.'/js/date.js', array('jquery'), 'false', null);
wp_enqueue_script('MultiSelct Daepicker', child_template_directory.'/js/jquery.date_Picker.js', array('jquery'), 'false', null);

wp_enqueue_script('events-managerCustum', child_template_directory.'/js/events-manager.js', array('jquery'), 'false', null);
 wp_enqueue_style('events-managerCustumr', child_template_directory.'/css/datePicker.css');
}
add_action('admin_enqueue_scripts', 'custom_admin_js');
//}

function wptuts_scripts_with_jquery()
{
	wp_deregister_script( 'comment-reply' );
	wp_deregister_script( 'twentytwelve-navigation' );

	//wp_enqueue_script('modernizr', child_template_directory.'/js/modernizr.js', array('jquery'), 'false', null);
	wp_enqueue_script('css_browser', child_template_directory.'/js/css_browser_selector.js', array('jquery'), 'false', null);


	wp_register_script( 'jquery-user', child_template_directory . '/js/jquery-ui/js/jquery-ui-1.10.3.custom.min.js', array( 'jquery' ) );

	wp_enqueue_script( 'jquery-user' );
	wp_register_script( 'jquery-chosen', child_template_directory . '/js/chosen/chosen.jquery.js', array( 'jquery' ) );
	wp_enqueue_script( 'jquery-chosen' );
	wp_enqueue_script('responsive-Slides', child_template_directory.'/js/ResponsiveSlides/responsiveslides.min.js', array('jquery'), 'false', null);

	if(!is_front_page()) {
	wp_register_script( 'jquery-address', child_template_directory . '/js/jquery.address-1.6.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'jquery-address' );

	wp_enqueue_script('hash-change', child_template_directory.'/js/hashchange.min.js', array('jquery'), 'false', null);
	wp_enqueue_script('light-box', child_template_directory.'/js/colorbox-master/jquery.colorbox.js', array('jquery'), 'false', true);
	wp_enqueue_script('zerotimedate', child_template_directory.'/js/date.js', array('jquery'), 'false', true);
	wp_enqueue_script('multipledatepicker', child_template_directory.'/js/jquery.date_Picker.js', array('jquery'), 'false', true);
	wp_enqueue_script('prettyPhoto', child_template_directory.'/js/jcarousel/lib/jquery.jcarousel.js', array('jquery'), 'false', true);
	wp_enqueue_script('bxslider', child_template_directory.'/js/jquery.bxslider/jquery.bxslider.js', array('jquery'), 'false', true);
	wp_enqueue_script('binit', '//assets.pinterest.com/js/pinit.js', array('jquery'), '', true);
	wp_enqueue_script('sharebar', get_bloginfo('wpurl').'/wp-content/plugins/sharebar/js/sharebar.js',array('jquery'), '', true);
		wp_enqueue_script('buttons', site_url().'/wp-content/themes/twentytwelve-child/js/buttons.js',array('jquery'), '', true); 


		wpcf7_enqueue_scripts();
		wpcf7_enqueue_styles();

}

if(is_page(874)) { 
	wp_enqueue_script( 'jobman-display', site_url() . '/wp-content/plugins/job-manager/js/display.js', false, '', true);
}


	if(!is_page(430) && !is_page(159) && !is_page(424) && !is_page(570) && !is_page(591) && !is_page(599) && !is_page(608) && !is_page(691) && !is_page(700) && !is_page(733) && !is_page(754) && !is_page(781) && !is_page(819) && !is_page(840) && !is_page(867) && !is_page(1088) && !is_page(1065) && !is_page(1125) && !is_page(1298) && !is_page(1738) && !is_page(2069) && !is_page(2104)):
	wp_register_script( 'ajax-main', child_template_directory . '/js/ajax-main.js', array( 'jquery-address' ) );
	wp_enqueue_script( 'ajax-main' );
	$ajax_main ='ajax-main'; 
	else:
	wp_enqueue_script('Property-script', child_template_directory.'/js/script-property.js', array('jquery'), 'false', true);
$ajax_main ='Property-script'; 
	endif;

	wp_localize_script( $ajax_main, 'ajaxLoad', array( 'Url' => admin_url( 'admin-ajax.php' ) ) );

 wp_enqueue_style('events-managerCustumr', child_template_directory.'/css/datePicker.css');
	//wp_enqueue_script('script', child_template_directory.'/js/script.js', array('jquery'), 'false', null);

}
add_action( 'wp_enqueue_scripts', 'wptuts_scripts_with_jquery', 100 );





/* Get Page Slug */

function get_the_slug( $id=null ) {
if($id==null) $id=$post->ID;
$post_data = get_post($id, ARRAY_A);
$slug = $post_data['post_name'];
return $slug; }


// Add a widget in WordPress Dashboard
function wpc_dashboard_widget_function() {
	// Entering the text between the quotes
	echo "<ul>
	<li><a href='".site_url()."/wp-admin/admin.php?page=conciergepages'>Concierge Pages</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=managehotels'>Manage Hotels</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=experiencespages'>Experiences Pages</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=restaurantspages'>Restaurants Pages</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=PageGalleryMapping'>Page Gallery Mapping</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=AlbumGalleryMapping'>Album Gallery Mapping</a></li>
	</ul>";
}
function wpc_dashboard_gallery_widget_function() {
	// Entering the text between the quotes
	echo "<ul>
	<li><a href='".site_url()."/wp-admin/admin.php?page=nextgen-gallery'>Overview</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=nggallery-add-gallery'>Add Gallery / Images </a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=nggallery-manage-gallery'>Manage Gallery</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=nggallery-manage-album'>Album</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=nggallery-tags'>Tags</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=nggallery-options'>Options</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=nggallery-style'>Style</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=nggallery-roles'>Roles</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=nggallery-about'>About</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=nggallery-setup'>Reset / Uninstall</a></li>
	</ul>";
}
function wpc_dashboard_jobmanager_widget_function() {
  echo "<ul>
	<li><a href='".site_url()."/wp-admin/admin.php?page=jobman-conf'>Settings</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=jobman-add-job'>Add Job</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=jobman-list-jobs'>Jobs</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=jobman-list-applications'>Applications</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=jobman-list-emails'>Emails</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=jobman-interviews'>Interviews</a></li>
	</ul>";
}
function wpc_dashboard_promotions_widget_function() {
  echo "<ul>
	<li><a href='".site_url()."/wp-admin/edit.php?post_type=ps_promotion'>Promotions</a></li>
	<li><a href='".site_url()."/wp-admin/post-new.php?post_type=ps_promotion'>Add New Promotions</a></li>
	<li><a href='".site_url()."/wp-admin/edit.php?post_type=ps_promotion&page=reorder-ps_promotion'>Reorder</a></li>
	</ul>";
}
function wpc_dashboard_seo_widget_function() {
	// Entering the text between the quotes
	echo "<ul>
	<li><a href='".site_url()."/wp-admin/admin.php?page=wpseo_dashboard'>Dashboard</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=wpseo_titles'>Titles & Metas / Images </a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=wpseo_social'>Social</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=wpseo_xml'>XML sitemaps</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=wpseo_permalinks'>Permalinks</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=wpseo_internal-links'>Internal Links</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=wpseo_rss'>RSS</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=wpseo_import'>Import $ Export</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=wpseo_files'>Edit files</a></li>
	</ul>";
}
function wpc_dashboard_hotels_function() {
	// Entering the text between the quotes
	echo "<ul>
	<li><a href='".site_url()."/wp-admin/admin.php?page=managehotels&hotel_id=1'>Congress Hall</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=managehotels&hotel_id=2'>The Virginia </a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=managehotels&hotel_id=3'>The Cottages</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=managehotels&hotel_id=4'>Beach Shack</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=managehotels&hotel_id=5'>The Star</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=managehotels&hotel_id=6'>SandPiper Beach Club</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=managehotels&hotel_id=7'>Baron's Cove Sag Harbor Spring 2014</a></li>
	</ul>";
}
function wpc_dashboard_restaurants_function() {
	// Entering the text between the quotes
	echo "<ul>
	<li><a href='".site_url()."/wp-admin/admin.php?page=restaurantspages&hotel_id=15'>The Blue Big Tavern</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=restaurantspages&hotel_id=16'>The Ebbitt Room </a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=restaurantspages&hotel_id=17'>The Rusty Nail</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=restaurantspages&hotel_id=18'>The Boiler Room</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=restaurantspages&hotel_id=19'>The Brown Room</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=restaurantspages&hotel_id=20'>Veranda Bar</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=restaurantspages&hotel_id=21'>Tommy's Folly Coffee Shop</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=restaurantspages&hotel_id=22'>The Star Coffee Shop</a></li>
	</ul>";
}
function wpc_dashboard_experiences_function() {
	// Entering the text between the quotes
	echo "<ul>
	<li><a href='".site_url()."/wp-admin/admin.php?page=experiencespages&hotel_id=8'>Sea Spa</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=experiencespages&hotel_id=9'>Beach Service </a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=experiencespages&hotel_id=10'>Tommy's Folly</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=experiencespages&hotel_id=11'>Beach Plum Farm</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=experiencespages&hotel_id=12'>West End Garage</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=experiencespages&hotel_id=13'>Seasonal Experiences</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=experiencespages&hotel_id=14'>Shopping</a></li>
	</ul>";
}
function wpc_dashboard_concierge_function() {
	// Entering the text between the quotes
	echo "<ul>
	<li><a href='".site_url()."/wp-admin/post.php?post=9&action=edit'>Pre arrival</a></li>
	<li><a href='".site_url()."/wp-admin/edit.php?post_type=activities'>Activities</a></li>
	<li><a href='".site_url()."/wp-admin/edit.php?post_type=seasonalevents'>Seasonal events</a></li>
	<li><a href='".site_url()."/wp-admin/edit.php?post_type=entertainment'>Entertainment</a></li>
	<li><a href='".site_url()."/wp-admin/edit.php?post_type=stories'>Our Stories</a></li>
	<li><a href='".site_url()."/wp-admin/admin.php?page=conciergepages'>Concierge Magazine</a></li>
	</ul>";
}
function wpc_add_dashboard_widgets() {
?>
<style>
#wp_dashboard_pages, #wp_dashboard_gallery_pages, #wp_dashboard_jobmanager_pages, #wp_dashboard_promotions_pages, #wp_dashboard_seo_pages,#wp_dashboard_hotels_pages,#wp_dashboard_restaurants_pages,#wp_dashboard_experiences_pages,#wp_dashboard_concierge_pages {
  float:left;
  height:auto;
  margin:10px 5px auto 5px;
	width: 100px;	
}
.inside{min-height:225px;}
</style>
<?php
	wp_add_dashboard_widget('wp_dashboard_pages', 'Manage Pages', 'wpc_dashboard_widget_function');
	wp_add_dashboard_widget('wp_dashboard_gallery_pages', 'Gallery', 'wpc_dashboard_gallery_widget_function');
	wp_add_dashboard_widget('wp_dashboard_jobmanager_pages', 'Job Manager', 'wpc_dashboard_jobmanager_widget_function');
	wp_add_dashboard_widget('wp_dashboard_promotions_pages', 'Promotions', 'wpc_dashboard_promotions_widget_function');
	wp_add_dashboard_widget('wp_dashboard_seo_pages', 'SEO', 'wpc_dashboard_seo_widget_function');
	wp_add_dashboard_widget('wp_dashboard_hotels_pages', 'Hotels', 'wpc_dashboard_hotels_function');
	wp_add_dashboard_widget('wp_dashboard_restaurants_pages', 'Restaurants', 'wpc_dashboard_restaurants_function');
	wp_add_dashboard_widget('wp_dashboard_experiences_pages', 'Experiences', 'wpc_dashboard_experiences_function');
	wp_add_dashboard_widget('wp_dashboard_concierge_pages', 'Concierge Pages', 'wpc_dashboard_concierge_function');
}
add_action('wp_dashboard_setup', 'wpc_add_dashboard_widgets' );


/* Rewrite Rule */

function my_acf_load_field( $field )
{
	global $wpdb;
	$field['choices'] = array();
	$hotels = $wpdb->get_results("select id, name, slug from $wpdb->hotels order by id asc",ARRAY_A);
	$field['choices'][''] = "SELECT PROPERTY";
	if( is_array($hotels) )
	{
		foreach( $hotels as $hotel )
		{
			$field['choices'][ $hotel['id'] ] = $hotel['name'];
		}
	} 
    return $field;
}
add_filter('acf/load_field/name=property', 'my_acf_load_field');
add_filter('acf/load_field/name=pageproperties', 'my_acf_load_field');
// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

/* Hotes Drop Down */
function hotels_list($type=null, $default="Hotel", $id="head_hotel") {
global $wpdb;
$hotels = $wpdb->get_results("select id, name, slug , page_id , hotelid from $wpdb->hotels where type='hotel' order by id asc",ARRAY_A);

if($type =='dropdown' ) {
		$html ="";
		$html .="<select id='$id' data-placeholder='$default' name='hotels' class='chosen-select'>
				<option value=''>&nbsp;</option>";
				foreach($hotels as $hotel){global $post;
					if($hotel['id'] != 3 && $hotel['id'] != 7) {
					if($post->ID==$hotel['page_id']){$selected = ' selected="selected"';}else{$selected = '';}
					$html .="<option ".$selected." value='".$hotel['hotelid']."'>".$hotel['name']."</option>";
					}
				}
		$html .="</select>";
		return $html;

} else{

	return $hotels;
}

}
 add_filter( 'show_admin_bar', '__return_false' );


//flush_rewrite_rules( )

add_filter('rewrite_rules_array','wp_insertMyRewriteRules');
add_filter('query_vars','wp_insertMyRewriteQueryVars');
add_filter('init','flushRules');

// Remember to flush_rules() when adding rules
function flushRules(){
	global $wp_rewrite;
   	$wp_rewrite->flush_rules();
}

// Adding a new rule
function wp_insertMyRewriteRules($rules)
{
	$newrules = array();
	$newrules['hotels/capemay/congresshall/(.+)'] = 'index.php?pagename=hotels/capemay/congresshall&subMenu=$matches[1]';
	$newrules['hotels/capemay/beachshack/(.+)'] = 'index.php?pagename=hotels/capemay/beachshack&subMenu=$matches[1]';
	$newrules['hotels/capemay/virginiahotel/(.+)'] = 'index.php?pagename=hotels/capemay/virginiahotel&subMenu=$matches[1]';
	$newrules['hotels/capemay/sandpiper/(.+)'] = 'index.php?pagename=hotels/capemay/sandpiper&subMenu=$matches[1]';
	$newrules['hotels/capemay/cottages/(.+)'] = 'index.php?pagename=hotels/capemay/cottages&subMenu=$matches[1]';
	$newrules['hotels/capemay/baronscove/(.+)'] = 'index.php?pagename=hotels/capemay/baronscove&subMenu=$matches[1]';
	$newrules['hotels/capemay/thestar/(.+)'] = 'index.php?pagename=hotels/capemay/thestar&subMenu=$matches[1]';


	$newrules['restaurants/capemay/boilerroom/(.+)'] = 'index.php?pagename=restaurants/capemay/boilerroom&subMenu=$matches[1]';
	$newrules['restaurants/capemay/rustynail/(.+)'] = 'index.php?pagename=restaurants/capemay/rustynail&subMenu=$matches[1]';
	$newrules['restaurants/capemay/brownroom/(.+)'] = 'index.php?pagename=restaurants/capemay/brownroom&subMenu=$matches[1]';
	$newrules['restaurants/capemay/ebbittroom/(.+)'] = 'index.php?pagename=restaurants/capemay/ebbittroom&subMenu=$matches[1]';
	$newrules['restaurants/capemay/bluepigtavern/(.+)'] = 'index.php?pagename=restaurants/capemay/bluepigtavern&subMenu=$matches[1]';
	$newrules['restaurants/capemay/verandabar/(.+)'] = 'index.php?pagename=restaurants/capemay/verandabar&subMenu=$matches[1]';


	$newrules['experiences/capemay/seaspa/(.+)'] = 'index.php?pagename=experiences/capemay/seaspa&subMenu=$matches[1]';
	$newrules['experiences/capemay/tommysfolly/(.+)'] = 'index.php?pagename=experiences/capemay/tommysfolly&subMenu=$matches[1]';
	$newrules['experiences/capemay/westendgarage/(.+)'] = 'index.php?pagename=experiences/capemay/westendgarage&subMenu=$matches[1]';
	$newrules['experiences/capemay/beachplumfarm/(.+)'] = 'index.php?pagename=experiences/capemay/beachplumfarm&subMenu=$matches[1]';
	$newrules['experiences/capemay/beachservice/(.+)'] = 'index.php?pagename=experiences/capemay/beachservice&subMenu=$matches[1]';
	$newrules['experiences/capemay/seasonal/(.+)'] = 'index.php?pagename=experiences/capemay/seasonal&subMenu=$matches[1]';

	$newrules['concierge/capemay/entertainment/(.+)'] = 'index.php?pagename=concierge/capemay/entertainment&post_slug=$matches[1]';
	$newrules['concierge/capemay/activities/(.+)'] = 'index.php?pagename=concierge/capemay/activities&post_slug=$matches[1]';
	$newrules['concierge/capemay/seasonalevents/(.+)'] = 'index.php?pagename=concierge/capemay/seasonalevents&post_slug=$matches[1]';
	$newrules['concierge/capemay/stories/(.+)'] = 'index.php?pagename=concierge/capemay/stories/&post_slug=$matches[1]';
	$newrules['concierge/capemay/specialoffers/(.+)'] = 'index.php?pagename=concierge/capemay/specialoffers&post_slug=$matches[1]';


	$finalrules = $newrules + $rules;
        return $finalrules;
}

// Adding the var so that WP recognizes it
function wp_insertMyRewriteQueryVars($vars)
{
    array_push($vars, 'subMenu');
    array_push($vars, 'post_slug');

    return $vars;
}
// Get Sub Menu  
//urldecode($wp_query->query_vars['subMenu']);

//Stop wordpress from redirecting
remove_filter('template_redirect', 'redirect_canonical');

function cr_trim_chars($content, $len, $strip='') {

	//$content=strip_tags($content); // remove all html tags from content
	//$content=mb_substr($content, 0, $len); //add desired characters instead of 300

	if($strip=='false') { 
		$content=strip_tags($content);
		$content=mb_substr($content, 0, $len);
	}else{
		$content = html_cut($content, $len);
	}

	return $content;
}



function html_cut($text, $max_length)
{
    $tags   = array();
    $result = "";

    $is_open   = false;
    $grab_open = false;
    $is_close  = false;
    $in_double_quotes = false;
    $in_single_quotes = false;
    $tag = "";

    $i = 0;
    $stripped = 0;

    $stripped_text = strip_tags($text);
$smallcont = $max_length-50;
$substring = substr($stripped_text,$smallcont,45);
$pos = strpos($substring, ' ');
$limit_content = $smallcont + $pos;

    while ($i < mb_strlen($text) && $stripped < $limit_content && $stripped < mb_strlen($stripped_text) && $stripped < $max_length)
    {
        $symbol  = $text{$i}; 
        $result .= $symbol;

        switch ($symbol)
        {
           case '<':
                $is_open   = true;
                $grab_open = true;
                break;

           case '"':
               if ($in_double_quotes)
                   $in_double_quotes = false;
               else
                   $in_double_quotes = true;

            break;

            case "'":
              if ($in_single_quotes)
                  $in_single_quotes = false;
              else
                  $in_single_quotes = true;

            break;

            case '/':
                if ($is_open && !$in_double_quotes && !$in_single_quotes)
                {
                    $is_close  = true;
                    $is_open   = false;
                    $grab_open = false;
                }

                break;

            case ' ':
                if ($is_open)
                    $grab_open = false;
                else
                    $stripped++;

                break;

            case '>':
                if ($is_open)
                {
                    $is_open   = false;
                    $grab_open = false;
                    array_push($tags, $tag);
                    $tag = "";
                }
                else if ($is_close)
                {
                    $is_close = false;
                    array_pop($tags);
                    $tag = "";
                }

                break;

            default:
                if ($grab_open || $is_close)
                    $tag .= $symbol;

                if (!$is_open && !$is_close)
                    $stripped++;
        }

        $i++;
    }

    while ($tags) {

        $result .= "</".array_pop($tags).">";
}
    return $result;
}





function get_ID_by_slug($slug) {
	global $wpdb;
	$id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '$slug'");
	return $id;
}

function diplay_concierge() {

$value = get_post_meta( $_GET['post'], 'displayhome', true );
		if($value=='yes') {
			$checked = "checked=checked";
		}else {
$checked='';

		}
		echo '<label for="displayhome">';
		_e( '', 'Display in home' );
		echo '</label> ';
		echo '<input type="checkbox" id="displayhome" '.$checked.' name="displayhome" value="yes" size="25" />';


}

if (is_admin())
	add_action('admin_menu', 'add_some_box');
	add_action( 'save_post', 'save_display' );
function add_some_box() {
	$cus_posts = array( 'seasonalevents','activities','entertainment', 'stories', 'specialoffers' );
foreach ($cus_posts as $cus_post) {
		 add_meta_box( 'displayhome', 'Display in Concierge Home', 'diplay_concierge', $cus_post);
}
}
function save_display() {


$cus_posts = array( 'seasonalevents','activities','entertainment', 'stories', 'specialoffers' );
if ( $_POST['post_type'] =='seasonalevents' || $_POST['post_type'] =='activities' || $_POST['post_type'] =='entertainment' ||  $_POST['post_type'] =='stories' || $_POST['post_type'] =='specialoffers' ) {
				

		$post_ID = $_POST['post_ID'];

		if ( !add_post_meta( $post_ID, 'displayhome', $_POST['displayhome'], true ) ) {
			update_post_meta( $post_ID, 'displayhome', $_POST['displayhome'] );
		}

} else 
	return;
}
function get_con_attachments($id) {
	$html ='';
	$images = get_children( 'post_type=attachment&post_mime_type=image&orderby=menu_order&order=ASC&post_parent='.$id);
	if(count($images) > 0) {

		$class= "jqueryCycle concierge-slides";
	}else {

		$class ="concierge-slides";
	}
	if($images){ 
	$html .= '<ul class="'.$class.'">';
	foreach( $images as $imageID => $imagePost ){

		unset($the_b_img);

		$the_b_img = wp_get_attachment_image($imageID, 'med_700_309', false);

		$html .='<li>'.$the_b_img.'</li>';

	} 

	$html .='</ul>';
	}
	return $html;
}
function customize_text_sizes($initArray){
   $initArray['theme_advanced_font_sizes'] = "10px,11px,12px,13px,14px,15px,16px,17px,18px,19px,20px,21px,22px,23px,24px,25px,26px,27px,28px,29px,30px,32px";
   return $initArray;
}

// Assigns customize_text_sizes() to "tiny_mce_before_init" filter
add_filter('tiny_mce_before_init', 'customize_text_sizes');
function my_order($orderby) { 
 global $wpdb; 
 return "menu_order ASC, ID DESC"; 
 } 
 
add_filter( 'posts_orderby', 'my_order' ); 
add_filter( 'manage_edit-entertainment_columns', 'my_edit_entertainment_columns' ) ;

function my_edit_entertainment_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title' ),
		'date' => __( 'Date' ),
		'property' => __( 'Property' ),
		'subtitle1' => __( 'Subtitle1' ),
		'subtitle_2' => __( 'Subtitle2' ),
		'subtitle_3' => __( 'Subtitle3' )

	);

	return $columns;
}
add_action( 'manage_entertainment_posts_custom_column', 'my_manage_entertainment_columns', 10, 2 );

function my_manage_entertainment_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		/* If displaying the 'duration' column. */
		case 'property' :

			global $wpdb;$proname ='';
			$duration = get_post_meta( $post_id, 'property', true );
			if(is_array($duration)){$i=0;
			foreach ($duration as $dur) { if($i!=0){$proname .=',';}
			$hotels = $wpdb->get_results("select * from $wpdb->hotels where id=$dur",ARRAY_A);
			foreach ($hotels as $singlepost) { 
       			$proname .= $singlepost['name'];
			}$i++;
			}}else{
				$hotels = $wpdb->get_results("select * from $wpdb->hotels where id=$duration",ARRAY_A);
				foreach ($hotels as $singlepost) { 
       				$proname .= $singlepost['name'].' ';
				}

			}
			/* If no duration is found, output a default message. */
			if ( empty( $duration ) )
				echo __( '' );

			/* If there is a duration, append 'minutes' to the text string. */
			else
				echo $proname;

			break;

		case 'subtitle1' :

			/* Get the genres for the post. */
			$terms = get_post_meta( $post_id, 'subtitle1', true );
			if ( empty( $terms ) )
				echo __( '' );

			/* If there is a duration, append 'minutes' to the text string. */
			else
				echo $terms;
			/* If terms were found. */

			break;

		case 'subtitle_2' :

			/* Get the genres for the post. */
			$terms = get_post_meta( $post_id, 'subtitle_2', true );
			if ( empty( $terms ) )
				echo __( '' );

			/* If there is a duration, append 'minutes' to the text string. */
			else
				echo $terms;
			/* If terms were found. */

			break;
		case 'subtitle_3' :

			/* Get the genres for the post. */
			$terms = get_post_meta( $post_id, 'subtitle_3', true );
			if ( empty( $terms ) )
				echo __( '' );

			/* If there is a duration, append 'minutes' to the text string. */
			else
				echo $terms;
			/* If terms were found. */

			break;

		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}
add_filter( 'manage_edit-specialoffers_columns', 'my_edit_specialoffers_columns' ) ;

function my_edit_specialoffers_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title' ),
		'date' => __( 'Date' ),
		'category' => __( 'Category' ),
		'property' => __( 'Property' )
	);

	return $columns;
}
add_action( 'manage_specialoffers_posts_custom_column', 'my_manage_specialoffers_columns', 10, 2 );

function my_manage_specialoffers_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		/* If displaying the 'duration' column. */
		case 'property' :

			global $wpdb;$proname ='';
			$duration = get_post_meta( $post_id, 'property', true );
			if(is_array($duration)){$i=0;
			foreach ($duration as $dur) { if($i!=0){$proname .=',';}
			$hotels = $wpdb->get_results("select * from $wpdb->hotels where id=$dur",ARRAY_A);
			foreach ($hotels as $singlepost) { 
       			$proname .= $singlepost['name'];
			}$i++;
			}}else{
				$hotels = $wpdb->get_results("select * from $wpdb->hotels where id=$duration",ARRAY_A);
				foreach ($hotels as $singlepost) { 
       				$proname .= $singlepost['name'].' ';
				}

			}
			/* If no duration is found, output a default message. */
			if ( empty( $duration ) )
				echo __( '' );

			/* If there is a duration, append 'minutes' to the text string. */
			else
				echo $proname;

			break;

		case 'category' :

			$cate_slug = get_post_taxonomies($post);
			$categories = get_the_terms( $post->id, $cate_slug[0] );
			$separator = ',';
			$output = '';
			if($categories){
				foreach($categories as $category) {
				$output .= $category->name.$separator;
				}
				$categorylist = trim($output, $separator);
			}			
			if ( empty( $categorylist ) )
				echo __( '' );

			/* If there is a duration, append 'minutes' to the text string. */
			else
				echo $categorylist;
			/* If terms were found. */

			break;
		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}
add_filter( 'manage_edit-ps_promotion_columns', 'my_edit_ps_promotion_columns' ) ;

function my_edit_ps_promotion_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title' ),
		'content' => __( 'Body Text' ),
		'date' => __( 'Date' ),
		'property' => __( 'Property' )

	);

	return $columns;
}
add_action( 'manage_ps_promotion_posts_custom_column', 'my_manage_ps_promotion_columns', 10, 2 );

function my_manage_ps_promotion_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		/* If displaying the 'duration' column. */
		case 'property' :

			global $wpdb;$proname ='';
			$duration = get_post_meta( $post_id, 'property', true );
			if(is_array($duration)){$i=0;
			foreach ($duration as $dur) { if($i!=0){$proname .=',';}
			$hotels = $wpdb->get_results("select * from $wpdb->hotels where id=$dur",ARRAY_A);
			foreach ($hotels as $singlepost) { 
       			$proname .= $singlepost['name'];
			}$i++;
			}}else{
				$hotels = $wpdb->get_results("select * from $wpdb->hotels where id=$duration",ARRAY_A);
				foreach ($hotels as $singlepost) { 
       				$proname .= $singlepost['name'].' ';
				}

			}
			/* If no duration is found, output a default message. */
			if ( empty( $duration ) )
				echo __( '' );

			/* If there is a duration, append 'minutes' to the text string. */
			else
				echo $proname;

			break;
		case 'content' :

			/* Get the genres for the post. */			
			$terms = get_post_field('post_content', $post_id);
			if ( empty( $terms ) )
				echo __( '' );

			/* If there is a duration, append 'minutes' to the text string. */
			else
				echo $terms;
			/* If terms were found. */

			break;
		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}
add_action( 'admin_head-post-new.php', 'posttype_admin_css' );
add_action( 'admin_head-post.php', 'posttype_admin_css' );
function posttype_admin_css() {
  global $post_type;
  $post_types = array('ps_promotion');
  if(in_array($post_type, $post_types))
  echo '<style type="text/css">#post-preview, #view-post-btn{display: none;}</style>';
}
	add_filter( 'posts_orderby', 'sort_query_by_post_in', 10, 2 );

	function sort_query_by_post_in( $sortby, $thequery ) {
		if ( !empty($thequery->query['post__in']) && isset($thequery->query['orderby']) && $thequery->query['orderby'] == 'post__in' )
			$sortby = "find_in_set(ID, '" . implode( ',', $thequery->query['post__in'] ) . "')";

		return $sortby;
	}
remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );

//add_action('wp_title', get_cr_title(), 16, 3);


add_action('wp_footer', 'add_googleanalytics');
function add_googleanalytics() {  ?>
<script type="text/javascript"> 
  var _gaq = _gaq || []; 
  _gaq.push(['_setAccount', 'UA-42082326-1']); 
  _gaq.push(['_setDomainName', 'caperesorts.com']); 
  _gaq.push(['_setAllowLinker', true]); 
  _gaq.push(['_trackPageview']); 

  (function() { 
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; 
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; 
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); 
  })(); 

</script>
<?php
}
function mytheme_dequeue_fonts() {
         wp_dequeue_style( 'twentytwelve-fonts' );
      }

add_action( 'wp_enqueue_scripts', 'mytheme_dequeue_fonts', 11 ); 

