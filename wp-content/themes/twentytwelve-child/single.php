<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header();

//print_r($post);

 $post_type = get_post_type();
global $post_type;
global $taxnomy;
global $termarray;
global $parent;
	if($post_type=='activities') {
		$taxnomy='activity_category';		
		$parent =27;
		$title = 'Activities';

}
	elseif($post_type=='seasonalevents') {
		$taxnomy='seasonalevents_category';
		$parent =28;
		$title = 'Seasonal Events';
}		

	elseif($post_type=='entertainment') {
		$taxnomy='entertainment_category';
		$parent =29;
		$title = 'Entertainment';
}
	elseif($post_type=='stories') {
		$taxnomy='stories_category';
		$parent =30;
		$title = 'Our stories';
}
	elseif($post_type=='specialoffers') {
		$taxnomy='offers_category';		
		$parent =34;
		$title = "Special Offers";


}

$terms = wp_get_post_terms( $post->ID, $taxnomy); 




foreach($terms as $term) {
	array_push($termarray, $term->term_id);
}

$args = array(
	'parent'                   => $parent,
	'orderby'                  => 'menu_order',
	'taxonomy'                 => $taxnomy);

 $categories = get_categories( $args );

$catArray = array();
foreach($categories  as $category) {
	array_push($catArray, $category->term_id);
}

?>
<?php get_header(); ?>
<section>

 <img src="<?php echo child_template_directory; ?>/img/con-images/concierge.png" class="con-logo" alt="logo" />

<!-- concierge Left Side bar Starts -->
		<?php get_sidebar('concierge'); ?>

<div class="content details">

<?php $postterms = get_the_terms( $post->ID, $taxnomy ); 

?>

	<h2 class="heading"><span class="parent_cat"><?php echo $title; ?>&nbsp;&nbsp;</span>
<span class="child_cat">

	<?php
	/*	$i=0; 
		if(is_array($postterms)):
		foreach ($postterms as $term) {
	if(in_array($term->term_id, $catArray)) {
		if($i==0) {
			echo ucwords($term->name); 
		}elseif($i > 0) {
			echo ' / '.ucwords($term->name); 
		}
		$i++;
		}
	}
	endif; */
	?>
	</span>
</h2>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content','details' ); ?>

			<?php endwhile; // end of the loop. ?>
</div>
</section>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->

<?php get_footer(); ?>
