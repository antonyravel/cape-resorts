<?php
	/*
	Template Name: Dining
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(888)944-1816</p>
    <img src="<?php echo child_template_directory; ?>/img/congress-hall-images/slider1.png" alt="slider" class="slider-img" />
    <div class="banner-navigation">
      <span class="nav-corner-img"></span>
      <ul>
        <li><a href="#">home</a></li>
        <li><a href="#">reservations</a></li>
        <li><a href="#">specials</a></li>
        <li><a href="#">guest rooms</a></li>
        <li><a href="#">gallery</a></li>
        <li><a href="#">resort life</a></li>
        <li><a href="#">dining</a></li>
        <li><a href="#">sea spa</a></li>
        <li><a href="#">weddings &amp; events</a></li>
        <li><a href="#">meetings</a></li>
      </ul>
    </div>
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li><a href="#">200 congress place</a></li>
      <li><a href="#">cape may, nj 08204</a></li>
      <li><a href="#">directions</a></li>
      <li><a href="#">contact us</a></li>
    </ul>
    <ul class="social-icons">
      <li><a href="http://www.tripadvisor.com/Hotel_Review-g46341-d92337-Reviews-Congress_Hall-Cape_May_New_Jersey.html" target="_blank"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/vector-smart.png" alt="corner-img" /></a></li>
      <li><a href="http://www.youtube.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/tube.png" alt="corner-img" /></a></li>
      <li><a href="https://www.facebook.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/facebook.png" alt="corner-img" /></a></li>
      <li><a href="https://twitter.com/CongressHall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/twitter.png" alt="corner-img" /></a></li>
      <li><a href="#" target="_blank"><img src="<?php echo child_template_directory; ?>/img/congress-hall-images/pinrest.png" alt="corner-img" /></a></li>
    </ul>
    
    <p class="mailing-list"><input type="text" name="mailing" value="join our mailing list" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content">
    <div class="tabsDiv diningDiv">
      <div id="tabs">
    <ul>
      <li><a href="#tabs-1">dining</a></li>
      <li><a href="#tabs-2">nightlife</a></li>
      
    </ul>
    <div class="all-tabs" style="display:block !important;">
    <div id="tabs-1">
      
      <ul>
      <li class="dining-content">
         <div>
          <img src="<?php echo child_template_directory; ?>/img/congress-hall-images/dining-samp1.jpg" alt="dining"/>
         <div>
         <h4>blue pig tavern</h4>
         <h5>super chill piano bar</h5>
         <h5>congress hall, library level</h5>
         <p>Within a year, the owners rebuilt the hotel, this time in brick rather than wood, and business blossomed once again. The hotel and Cape May proved so popular that they gained renown as a summer.</p>
         <p class="view-links"><a href="#">share</a>
         <span class="v-gallery"><a href="#">learn more</a><a href="#">view menus</a><a href="#">reserve</a></span>
      </p>
      </div></div>
      </li>
      <li class="dining-content">
          <div>
          <img src="<?php echo child_template_directory; ?>/img/congress-hall-images/dining-samp1.jpg" alt="dining"/>
          <div>
         <h4>tommy's folly</h4>
         <h5>super chill piano bar</h5>
         <h5>congress hall, library level</h5>
         <p>Within a year, the owners rebuilt the hotel, this time in brick rather than wood, and business blossomed once again. The hotel and Cape May proved so popular that they gained renown as a summer.</p>
         <p class="view-links"><a href="#">share</a>
         <span class="v-gallery"><a href="#">learn more</a><a href="#">view menus</a><a href="#">reserve</a></span>
      </p>
      </div></div>
      </li>
      <li class="dining-content">
        <div>
        <img src="<?php echo child_template_directory; ?>/img/congress-hall-images/dining-samp1.jpg" alt="dining"/>
        <div>
         <h4>room service</h4>
         <h5>optional informational header (with square footage)</h5>
         
         <p>Within a year, the owners rebuilt the hotel, this time in brick rather than wood, and business blossomed once again. The hotel and Cape May proved so popular that they gained renown as a summer Within a year, the owners rebuilt the hotel, this time in brick rather than wood, and business blossomed once again. The hotel and Cape May proved so popular that they gained renown as a summer.</p>
         <p class="view-links"><a href="#">share</a>
         <span class="v-gallery"><a href="#">learn more</a><a href="#">view menus</a><a href="#">reserve</a></span>
      </p>
      </div></div>
      </li>
      </ul>
    </div>
    
    <div id="tabs-2">
    <h3>night hall content</h3>
      <p>For almost two centuries the hotel has offered hospitality to locals and visitors alike. It began life in 1816 as a simple boarding house for summer visitors to one of America's earliest seaside resorts. Its owner, Thomas H. Hughes, called his new boarding house "The Big House". The local people had other ideas, though. Convinced the building was far too large to ever be a success they nicknamed it "Tommy's Folly".</p>
      <p>
      In this first incarnation it was a quite different affair. Downstairs was a single room that served as the dining room for all the guests, who stayed in simply partitioned quarters on the two upper floors. The walls and woodwork were bare and supplies of provisions were at times unreliable.
      </p>
      <p>Guests were undeterred by the Spartan conditions and summer after summer the new hotel was packed to bursting. In 1828 Hughes had been elected to Congress and in honor of his new status his hotel was renamed Congress Hall. </p>
      <p>As Congress Hall's reputation grew, so did Cape May's. By the middle of the 19th Century Cape May had become a booming holiday destination, rivaling Saratoga and Newport for popularity. Congress Hall had doubled in size and was welcoming guests from around the region, but in 1878 the building was destroyed when a huge fire swept through 38 acres of Cape May's seafront.</p>
      <p>Within a year, the owners rebuilt the hotel, this time in brick rather than wood, and business blossomed once again. The hotel and Cape May proved so popular that they gained renown as a summer retreat for the nation's presidents. Ulysses S. Grant, Franklin Pierce, and James Buchanan all chose to vacation here. President Benjamin Harrison made Congress Hall his "summer White House" and conducted the affairs of state from the hotel.</p>
      <a href="#" class="learn">Read more</a>
    </div>
    
    
    </div>
</div>      
    </div>
    
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('dining')) { ?>
<style>

.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>

