<?php
	/*
	Template Name: the-virgina-home
	*/
?>
<?php get_header(); ?> 
<div class="CH-home">
  <div class="sliderDiv">
    <p class="reserve">Reservations&nbsp;(800)732-4236</p>
    <ul id="jqueryCycle">
<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>
    		<li><img src="<?php echo child_template_directory; ?>/img/virginia-images/slider.png" alt="slider" class="slider-img" /></li>

	</ul>    
    <div class="banner-navigation">
      <!--<img src="<?php echo child_template_directory; ?>/img/congress-hall-images/corner-img.png" class="corner-img" alt="corner-img" />-->
      <span class="nav-corner-img"></span>
      <ul class="banner-nav-li banner-menu">
        <li><a href="<?php echo site_url(); ?>/capemay/virginia/">home</a></li>
        <li><a href="#">reservations</a></li>
        <li><a href="<?php echo site_url(); ?>/capemay/virginia/specials/">specials</a></li>
        <li><a href="#">accommodations</a></li>
        <li><a href="#">resort life</a></li>
        <li><a href="#">gallery</a></li>
        <li><a href="#">the ebbitt room</a></li>
        <li><a href="#">weddings &amp; events</a></li>
        <li><a href="#">meetings</a></li>
      </ul>
    </div>
    <a href="#" class="banner-toggle-menu">banner navigation</a>
    
  </div>
  <div class="socialDiv">
    <ul class="directions">
      <li>25 Jackson Street</li>
      <li>cape may, nj 08204</li>
      <li class="dr"><a href="<?php echo site_url(); ?>/capemay/virginia/direction/">directions</a></li>
      <li class="ct"><a href="<?php echo site_url(); ?>/capemay/virginia/contact/">contact us</a></li>
    </ul>
    <ul class="social-icons">
      <li><a href="http://www.tripadvisor.com/Hotel_Review-g46341-d92337-Reviews-Congress_Hall-Cape_May_New_Jersey.html" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/vector-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://www.youtube.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/tube-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://www.facebook.com/congresshall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/facebook-icon.png" alt="corner-img" /></a></li>
      <li><a href="https://twitter.com/CongressHall" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/twitter-icon.png" alt="corner-img" /></a></li>
      <li><a href="http://pinterest.com/caperesorts/" target="_blank"><img src="<?php echo child_template_directory; ?>/img/virginia-images/pinit.jpg" alt="corner-img" /></a></li>
    </ul>
    

    <p class="mailing-list"><input type="text" name="mailing" value="JOIN OUR MAILING LIST" onfocus="if (this.defaultValue==this.value) {this.value='';}" onblur="if (this.value==''){ this.value=this.defaultValue;}"/>
    <button class="mail-bt"> </button>
    </p>
    
  </div>
  

</div>
</div> <!-- .main -->
</div> <!-- .wrapper-middle -->
</div> <!-- .wrapper-bottom -->
</div><!-- .wrapper-top -->
</div>
<!--content starts here-->
<!--content starts here-->
  <div class="cong-content">
    <div class="tabsDiv">
      <div id="tabs">
    <ul>
      <li class="active"><a href="#tabs-1">Welcome to The Virginia</a></li>
      <li><a href="#tabs-2">Our history</a></li>
    </ul>
    <div class="all-tabs">
    <div id="tabs-1">
      <p> The Virginia Hotel & Cottages offers 24 intimate rooms in the main house and 5 spacious cottages along Jackson Street, in the heart of the famed historic district of Cape May. The Virginia hotel is an impeccably restored 1879 landmark building and is the foremost luxury hotel in Cape May. History combines with modern day amenities and superior service to provide guests with a unique Cape May hotel experience. All guests enjoy complimentary breakfast, wireless Internet and valet parking. Our luxury hotel is ideally located just a half a block from the ocean and one block from the downtown Cape May shopping.</p>     
      <p> Enjoy the following amenities at The Virginia Hotel & Cottages:</p>
      <ul>
      <li>24 intimate rooms</li>
      <li>5 individual cottage rentals</li>
      <li>Facilities for meetings and retreats</li>
      <li>Elegant wedding packages</li>
      <li>Complimentary breakfast</li>
      <li>Gourmet dining featuring The Ebbitt Room</li>
      <li>Beach service featuring complimentary umbrellas, chairs, towels</li>
      <li>Complimentary valet parking</li>
      <li>Golf carts available</li>
      <li>Fitness center at Congress Hall</li>
      </ul>
    </div>    
    </div>
</div>      
    </div>
    <div class="rt-bar">
      <div class="spring">
        <h2>Introducing the</h2>
        <h3>2013 dining series at the ebbit room</h3>
        <h4></h4>
        <p><a href="#">learn more</a></p>
      </div>
      <div class="sea">
        <h2>Ebbitt Room</h2>
        <h3>COCKTAILS ON THE PORCH</h3>
        <h4></h4>
        <p><a href="#">LEARN MORE ABOUT THIS PACKAGE</a></p>
      </div>
      <div class="retreat">
        <h2>125th Congress Hall Retreat</h2>
        <h4></h4>
        <p><a href="#">GETAWAY...WITH DINNER, DRINKS AND A RELAXING SPA TREATMENT. STARTING AT $125</a></p>
      </div>
      <div class="resort-video">
        <a href="#">Watch the Cape Resorts Video.</a>
      </div>
    </div>
    
  </div>
<!--content end here-->  
<!--content end here-->

<?php get_footer(); ?>


<!-- for body background image -->
<?php if(is_page('virginia')) { ?>
<style>
body{background:#f2f2f2;}
.wrapper-bottom{padding-bottom: 19px;}
</style>
<?php } ?>

