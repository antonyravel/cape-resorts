If it worked before, it's probably a plugin hooking into the_content filter. Disable your plugins one by one to see if this fixes the issue. Next, check the callback functions for the_content filter.

You can see every callback for the_content by adding the following:

add_action('template_redirect', 'wpse_44152_template_redirect');
function wpse_44152_template_redirect(){
    global $wp_filter;
    print_r($wp_filter['the_content']);
}
Find each of the functions listed by doing a 'find in files' code search on your code editor. A filter callback HAS TO return $content data back for the_content() to work properly.
