<?php

    /*
     * CLASS TO GENRATE CONSTANT ARRAY
     * @author Sourav Ray
     * @version 2.0.0
     * @name arrConst.inc.php
	 * @Note: 	arrConst Version 2.0.0 consist a major change in Get method
				Now with the new get method it is possible to get 
				back the value of a particular element of the Array  Constant
	 * ---------------------------------------------------------------------------
     */ 

	 class arrConst
	{
		
		public static function set($constName, $paramArray)				// setter method: sets the array as a constant
		{
			if($paramArray)
			{
				if(is_array($paramArray))
				{
					if($constName)
					{
						if(defined($constName))
						{
							throw new Exception('arrConst Error: Constant name in use');
						}
						else
						{
							try
							{
								define($constName,var_export($paramArray, true));
							} 
							catch ( Exception  $e)
							{
									throw new Exception('arrConst Error: Unknown error');
							}
						}
					}
					else 
					{
						throw new Exception('arrConst Error: No Const name');
					}
				}
				else
				{
					throw new Exception("arrConst Error: Non array Constan can't be define");
				}
			}
			else 
			{
				throw new Exception('arrConst Error: Void array');
			}
		}
		
		
		public static function get($constName,$element=NULL) 						//getter method: returns the array from the constant
		{
			if( $constName)
			{
				try
				{	
					if(is_null($element))
					{
						return eval('return '.$constName.";");
					}
					else
					{
						$array=eval('return '.$constName.";");
						$keyArr = explode(',',$element);
						$tempReturn=$array[$keyArr[0]];
						for($count=1, $keySize=count($keyArr); $count<=$keySize; $count++ )
						{
							if($keyArr[$count] == NULL || $keyArr[$count] == ' ')
							{
								$tempReturn=$tempReturn;
							}
							else
							{
								$tempReturn=$tempReturn[$keyArr[$count]];
							}
						}
						
						return $tempReturn;
						
					}
				}
				catch ( Exception  $e)
				{
					throw new Exception('arrConst Error: Unknown error');
				}				
			}
			else 
			{
				throw new Exception('arrConst Error: Invalid Constant Name');
			}
		}
	
}

?>