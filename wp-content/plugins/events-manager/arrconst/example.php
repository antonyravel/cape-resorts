<?php

    /*
	 *	 @description This is an example of how to use arrConst Functions V 2.0.0
	 *	 @author Sourav Ray
	 */
	 
	 include('arrConst.inc.php');
	 
	$myArray= array('I know Deepti','for more than a year now',array('She is','my best friend'),'and'=> 'and a amazing programmer too.'); // now have an array
	 	 
	 /* to define the array as constant we will call SET function */
	 	arrConst::set('FRIEND',$myArray);
		
				
	 /* now to retrieve  back the array value from the Constant we will call the GET function */
	
 	echo '<pre>';
 		print_r(arrConst::get(FRIEND));		// printing the array from the Constant
	echo '</pre>';
	
	
	/* 	
		the example bellow is to show the new functionality that has been added to Get method in ver. 2.0
		the following exaple shows the get method re turns a particular elment of the array when the element in specified
		NOTE: the element specification string should not contain any white space of tab
	*/
	echo	arrConst::get(FRIEND, '2,1');	// to get the equivalent out put of $myArray[2][1]
	echo '<br />';
	echo	arrConst::get(FRIEND, 'and');	// to get the equivalent out put of $myArray['and']
	
?>