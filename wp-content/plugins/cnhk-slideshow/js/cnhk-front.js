var $c = jQuery.noConflict();

var ssid_list = [];
var ss_list = [];
var loaded_slide = {};
var scrolling_status = {};
var pending_restart = [];

$c( function() {
	$c( ".cnhk-slideshow" ).each( function() {
		ssid_list.push( $c( this ).attr( 'id' ) );
	} );
	for ( var i in ssid_list ) {
		ss_list.push( new slideshow( ssid_list[i] ) );
		loaded_slide[ssid_list[i]] = 0;
	}
	$c( window ).resize( function(){
		for ( var i in ssid_list ) {
			obj = get_instance( ssid_list[i] );
			width = $c( '*:has(#' + obj.id + '):last' ).width();
			ratio = obj.ratio;
			height = Math.round( width / ratio );
			$c( "#" + obj.id ).width( width );
			$c( "#" + obj.id ).height( height );
			$c( "#" + obj.id + ' img' ).width( width );
			$c( "#" + obj.id + ' img' ).height( height );
			if ( 'scrollHorz' == obj.effect ) {
				if ( 'number' == typeof( scrolling_status[obj.id] ) ) { /* No engaged scrolling */
					time = parseInt( scrolling_status[obj.id] );
					now = new Date().getTime();
					chrono = now - time;
					index = -1;
					$c( "#csnav-" + obj.name +' a' ).each( function( i ) {
						if ( $c( this ).hasClass( 'activeSlide' ) ) {
							index = parseInt( $c( this ).text() ) - 1;
						}
					} );
					obj.restart( index, chrono );
				} else {
					add_restart( obj.id );
				}
			}
			if ( obj.nav ) obj.update_nav_pos();			
		}
	});
} );

function check_restart( identifier ) {
	result = false;
	for ( var i in pending_restart ) {
		if ( identifier == pending_restart[i] ) {
			result = true;
		}
	}
	return result;
}

function add_restart( identifier ) {
	if ( false == check_restart( identifier ) ) {
		pending_restart.push( identifier );
	}
}

function delete_restart( identifier ) {
	var new_array = [];
	for ( var i in pending_restart ) {
		if ( identifier != pending_restart[i] ) {
			result.push( pending_restart[i] );
		}
	}
	pending_restart = new_array;
}

function before_transition ( current, pending, options  ) {
	var identifier = 'csi-' + options['pager'].substr( 7 );	
	scrolling_status[identifier] = 'running';
}

function after_transition ( current, pending, options  ) {
	var identifier = 'csi-' + options['pager'].substr( 7 );
	
	obj = get_instance( identifier );
	
	index = -1;
	$c( "#csnav-" + obj.name +' a' ).each( function( i ) {
		if ( $c( this ).hasClass( 'activeSlide' ) ) {
			index = parseInt( $c( this ).text() ) - 1;
		}
	} );	
	
	scrolling_status[identifier] = new Date().getTime();
	
	if ( check_restart( identifier ) ) {
		delete_restart( identifier );
		obj.restart( index, 0 );
	}	
}

function get_instance( identifier ) {
	var index = -1;
	for( var i in ssid_list ) {
		if ( ssid_list[i] == identifier ) index = i;
	}
	if ( index != -1 ) {
		return ss_list[index];
	} else {
		return false;
	}
}

function one_more_slide( identifier ) {
	obj = get_instance( identifier );
	obj.loaded_slide++;
	if ( obj.src_list.length == obj.loaded_slide ) {
		obj.launch();
	}
}

function slideshow( id ) {

	this.id 		= id;
	this.name 		= id.substr( 4 );
	this.loaded_slide = 0;
	
	this.box_width 	= $c( "#" + id ).outerWidth();
	
	this.fit;
	this.fit_width;
	this.ratio		= -1;
	this.effect		= '';
	this.speed		= -1;
	this.duration	= -1;
	this.nav;
	this.base_url 	= '';
	
	this.src_list 	= [];
	this.ttl_list 	= [];
	this.lnk_list 	= [];
	this.slide_list	= [];
	
	this.background = '';
	
	this.init = function() {
	
		this.fit = true;
		this.fit_width = -1;
		
		var speed, duration, height, width, nav, effect, base_url;
		$c( "#cssettings-" + this.name + " li" ).each( function( i ) {
			switch( i ) {
				case 0:
					effect = $c( this ).text();
					break;
				case 1:
					speed = $c( this ).text();
					break;
				case 2:
					duration = $c( this ).text();
					break;
				case 3:
					height = $c( this ).text();
					break;
				case 4:
					width = $c( this ).text();
					break;
				case 5:
					nav = $c( this ).text();
					break;
				case 6:
					base_url = $c( this ).text();
					break;
				case 7:
					bgr_url = $c( this ).text();
					break;
				default:
					break;
			}
		} );
		var effect_list = {
			Scroll	: 'scrollHorz',
			Fade	: 'fade',
			None	: 'none'
		}
		
		this.effect 	= effect_list[effect];
		this.speed 		= parseInt( speed );
		this.duration 	= parseInt( duration );
		this.ratio 		= parseInt( width ) / parseInt( height );
		this.base_url 	= base_url;
		this.background = bgr_url;
		
		if ( 'true' == nav ) this.nav = true; else this.nav = false;
		
		src_list = [];
		ttl_list = [];
		lnk_list = [];
		
		$c( "#cssrc-" + this.name + " li" ).each( function() {
			src_list.push( $c( this ).text() );
		} );
		$c( "#csttl-" + this.name + " li" ).each( function() {
			ttl_list.push( $c( this ).text() );
		} );
		$c( "#cslnk-" + this.name + " li" ).each( function() {
			lnk_list.push( $c( this ).text() );
		} );
		
		this.src_list = src_list;
		this.ttl_list = ttl_list;
		this.lnk_list = lnk_list;
		
		this.set_box_size();
		this.preload();
		this.preload_background();
	}
	
	this.set_box_size = function() {
		var width = this.box_width;
		var height = Math.round( width / this.ratio );
		var id = "#" + this.id;
		$c( id ).height( height );
		$c( id ).width( width );
	}
	
	this.preload = function() {
		var width = this.box_width;
		var height = Math.round( width / this.ratio );
		base_url = this.base_url;
		var preloader = $c( '<img />' ).attr( {
			src		: base_url + 'images/loading.gif',
			title	: 'Loading',
			alt		: 'Loading'
		} ).css( {
			left	: Math.round( width / 2 ) - 25 + 'px',
			top		: Math.round( height / 2 ) - 25 + 'px'
		} ).addClass( 'cnhk-preloader' );
		var id = "#" + this.id;
		$c( id ).append( preloader );
	}
	
	this.preload_background = function() {
		var obj = this;
		if ( 'none' != obj.background ) {
			bgr = $c( '<img />' ).attr( {
				src	: obj.background
			} ).load( function(){
				obj.loading();
			} );
		} else {
			this.loading();
		}
	}
	
	this.loading = function() {
		src_list = this.src_list;
		ttl_list = this.ttl_list;
		lnk_list = this.lnk_list;
		width = this.box_width;
		height = Math.round( width / this.ratio );
		slide_list = [];
		id = this.id;
		for ( var i in src_list ) {
			image 	= $c( '<img />' ).attr( {
				src		: src_list[i],
				title	: ttl_list[i],
				alt		: ttl_list[i],
				width	: width,
				height	: height
			} ).load( function() {
				one_more_slide( id );
			} ).css( 'max-width', '100%' ).addClass( 'cnhk-slide' );
			lnk 	= $c( '<a />' ).attr( { href : lnk_list[i], target : '_blank' } );
			if ( 'No link' == lnk_list[i] ) {
				slide_list.push( image );
			} else {
				lnk.append( image );
				slide_list.push( lnk );
			}
		}
		this.slide_list = slide_list;
	}
	
	this.launch = function() {
		$c( "#" + this.id ).empty();
		$c( '.cnhk-data:has(#csttl-' + this.name + ')' ).remove();
		slide_list = this.slide_list;
		width = this.box_width;
		height = Math.round( width / this.ratio );
		var obj = this;
        if ( 'none' != this.background ) {
            var bgr = $c( '<img />' ).attr( {src:obj.background} ).css({width: width, height: height});
            $c( '#' + this.id ).append( bgr );
        }
		for ( var i in slide_list ) {
			$c( "#" + this.id ).append( slide_list[i] );
		}
		options = {
			fx				: this.effect,
			speed			: this.speed,
			timeout			: this.duration,
			containerResize	: 1,
			slideResize		: 0,
			fit				: 0,
			manualTrump		: false,
			pause			: true,
			slideExpr		: '.cnhk-slide',
			pager			: "#csnav-" + this.name,
			pagerEvent		: 'click',
			cleartypeNoBg	: 'true'
		}
		
		if ( this.nav ) {
			prev = $c( '<div />' ).attr( 'id', 'prev-' + this.name ).css( 'opacity', 0.4 ).hover( function(){ $c( this ).css( 'opacity', 1 ); }, function(){ $c( this ).css( 'opacity', 0.4 ); } ).addClass( 'cnhk-prev' );
			next = $c( '<div />' ).attr( 'id', 'next-' + this.name ).css( 'opacity', 0.4 ).hover( function(){ $c( this ).css( 'opacity', 1 ); }, function(){ $c( this ).css( 'opacity', 0.4 ); } ).addClass( 'cnhk-next' );
			$c( "#" + this.id ).append( prev ).append( next );
			this.update_nav_pos();
			options['prev'] = '#prev-' + this.name;
			options['next'] = '#next-' + this.name;
		}
		
		if ( 'scrollHorz' == this.effect ) {
			options['before'] 	= before_transition
			options['after']	= after_transition
		}
		$c( "#" + this.id ).cycle( options );
	}
	
	this.update_nav_pos = function() {
		width 			= $c( "#" + this.id ).outerWidth();
		height 			= Math.round( width / this.ratio );
		box_top 		= $c( "#" + this.id ).offset().top;
		box_left 		= $c( "#" + this.id ).offset().left;
		
		prev_top 		= Math.round( height / 2 ) - 15;
		prev_left 		= 5;
		next_top 		= prev_top;
		next_left		= width - 15 - 5;
		
		$c( "#prev-" + this.name ).css( {
			top		: prev_top + 'px',
			left	: prev_left + 'px'
		} );
		$c( "#next-" + this.name ).css( {
			top		: next_top + 'px',
			left	: next_left + 'px'
		} );
	}
	
	this.restart = function( index, elapsed ) {
		if ( 'undefined' == typeof( elapsed ) ) {
			elapsed = 0;
		}
		$c( "#" + this.id ).cycle( 'destroy' );
		
		options = {
			fx				: this.effect,
			speed			: this.speed,
			timeout			: this.duration,
			containerResize	: 1,
			slideResize		: 0,
			fit				: 0,
			manualTrump		: false,
			pause			: true,
			slideExpr		: '.cnhk-slide',
			pager			: "#csnav-" + this.name,
			pagerEvent		: 'click',
			cleartypeNoBg	: 'true',
			before			: before_transition,
			after			: after_transition,
			startingSlide	: index,
			delay			: -elapsed
		}
		
		if ( this.nav ) {
			options['prev'] = '#prev-' + this.name;
			options['next'] = '#next-' + this.name;
		}
		
		$c( "#" + this.id ).cycle( options );
	}
	
	this.init();
	
}