jQuery( function() {
	jQuery( ".cnhk-left-list" ).sortable( {
		connectWith				: ".cnhk-right-list",
		receive 				: function( ev, ui ) {
			jQuery( ui.item ).removeClass( "cnhk-right-item" ).addClass( "cnhk-left-item" );
			update_order();
		},
		placeholder 			: "cnhk-placeholder",
		forcePlaceholderSize	: true
	} );
	
	jQuery( ".cnhk-right-list" ).sortable( {
		connectWith				: ".cnhk-left-list",
		update 					: function( ev, ui ) {
			update_order();
		},
		receive 				: function( ev, ui ) {
			jQuery( ui.item ).removeClass( "cnhk-left-item"  ).addClass( "cnhk-right-item" );
			update_order();
		},
		placeholder 			: "cnhk-placeholder",
		forcePlaceholderSize	: true
	});
	update_order();
	
	jQuery( "#instance" ).change( function(){
		var path = jQuery( "#page_url" ).html();
		var index = jQuery( "#instance" ).val();
		document.location.href = path + '&index=' + index;
	} );
	jQuery( "#cnhk_select_ss" ).change( function(){
		var path = jQuery( "#page_url" ).html();
		var index = jQuery( "#cnhk_select_ss" ).val();
		document.location.href = path + '&index=' + index;
	} );
	jQuery( '#cnhk-fixed' ).change( function() {
		if ( 1 == jQuery( this ).val() ) {
			jQuery( "#cnhk-fixed-width" ).removeAttr( 'disabled' );
			jQuery( "#cnhk-fixed-align" ).removeAttr( 'disabled' );
		} else {
			jQuery( "#cnhk-fixed-width" ).attr( { disabled : 'disabled', value : '' } );
			jQuery( "#cnhk-fixed-align" ).attr( { disabled : 'disabled', value : 'none' } );
		}
	} );
	
});

function update_order() {
		var data = jQuery( ".cnhk-right-list" ).sortable( "serialize" );
		jQuery( "#cnhk-order" ).val( data );
}