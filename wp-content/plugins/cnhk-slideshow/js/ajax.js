var RESPONSE_TIMEOUT = 15000; /* Maximum time awaiting for server response ( in milliseconds ) */

$c = jQuery.noConflict();

$c( window ).click( function() {
	clear_bubble();
	$c( '.cnhkx-popout' ).remove();
} );

$c( function() {
	$c( 'a[id^="slideuse"]' ).live( 'click', function() {
		
		if ( ! cnhk_ajax_settings.pending ) {
			clear_bubble();
			
			id_attr = $c( this ).attr( 'id' );
			
			var DATA = {
				action : 'cnhkx_initial',
				what : 'use',
				nonce : cnhk_ajax_settings.nonce,
				num_id : parse_id( id_attr ).num,
				alpha_id : parse_id( id_attr ).alpha
			}
			
			preload( this );
			
			$c.post(
				cnhk_ajax_settings.url,
				DATA,
				function( response ) {
					clear_preload();
					if ( is_valid_resp( response ) ) {
						if ( cnhk_ajax_settings.pending ) {
							cnhk_ajax_settings.pending = false;
							bbl = $c( '<div />' ).addClass( 'cnhkx-bubble' ).attr( { id: 'repuse-' + response.target[1] } );
							ul = $c( '<ul />' )
							for ( var i in response.value ) {
								
								index = response.value[i]['index'];
								if ( response.value[i]['current'] ) {
									name = '<strong>' + response.value[i]['use'] + '</strong>';
									ul.append( $c( '<li />').append( name ) );
								} else {
									name = response.value[i]['use'];
									link = $c( '<a />' ).attr( {
											href : '#',
											id : "ckrepuse_" + response.target[1] + "_" + response.value[i]['index']
										}
									).text( name );
									li = $c( '<li />' ).append( link );
									ul.append( li );
								}
								
							}
						
						$c( '#slideuse_' + response.target[1] ).after( bbl.append( ul ) );
						
						}
						
					} else {
						cnhk_ajax_settings.pending = false;
						cnhkx_display_error( response );
					}
					/*
					*	End of 'use' response
					*/
				}
			);
			
			return false;
		/*
		*	End of a[id^="slideuse"] 's Section.
		*/
		}
	} );
	
	$c( '.cnhkx-bubble li a' ).live( 'click', function() {
	
		if ( ! cnhk_ajax_settings.pending ) {
			clear_bubble();
			
			id_attr = $c( this ).attr( 'id' );
			id = parse_lnk_repid( id_attr );
			
			var DATA = {
				action : 'cnhkx_resp',
				what :	'use',
				nonce : cnhk_ajax_settings.nonce,
				target : id['target'],
				value : id['value']
			}
			
			preload( $c( '#slideuse_' + id['target'] ) );
			
			$c.post(
				cnhk_ajax_settings.url,
				DATA,
				function( response ) {
					clear_preload();
					if ( is_valid_resp( response ) ) {
						if ( cnhk_ajax_settings.pending ) {
							cnhk_ajax_settings.pending = false;
							
							if ( response.status ) {
								if ( 'Unused' == response.value_name ) {
									$c( '#slideuse_' + response.target ).empty();
									$c( '#slideuse_' + response.target ).text( response.value_name );
								} else {
									$c( '#slideuse_' + response.target ).empty();
									$c( '#slideuse_' + response.target ).append( '<strong />' ).text( response.value_name );
								}
								
								if ( response.old_class != response.new_class ) {
									$c( '.background-pattern:has(img[title="' + response.title + '"])' ).removeClass( response.old_class );
									$c( '.background-pattern:has(img[title="' + response.title + '"])' ).addClass( response.new_class );
								}
								
								$c( 'img[title="' + response.title + '"]' ).width( response.width );
								$c( 'img[title="' + response.title + '"]' ).height( response.height );
									
								target_node_id = 'slideuse_' + response.target;
								cnhk_make_infobubble( target_node_id, 'Data updated' );
								
							} else {
								target_node_id = 'slideuse_' + response.target;
								cnhkx_display_error( response );
							}							
						}
					} else {
						cnhk_ajax_settings.pending = false;
						cnhkx_display_error( response );
					}
				/*
				*	End of 'ckrepuse' response 
				*/
				}
			);
		return false;
		/*
		*	End of a[id^="ckrepuse"] 's Section.
		*/
		}
	} );
	
	$c( 'a[id^="slidetitle"]' ).live( 'click', function() {
		
		if ( ! cnhk_ajax_settings.pending ) {
			
			clear_bubble();
			
			id_attr = $c( this ).attr( 'id' );
			
			var DATA = {
				action : 'cnhkx_initial',
				what : 'title',
				nonce : cnhk_ajax_settings.nonce,
				num_id : parse_id( id_attr ).num,
				alpha_id : parse_id( id_attr ).alpha
			}
			
			preload( this );
			
			$c.post(			
				cnhk_ajax_settings.url,
				DATA,
				function( response ) {
					clear_preload();
					if ( is_valid_resp( response ) ) {
						if ( cnhk_ajax_settings.pending ) {
							cnhk_ajax_settings.pending = false;
							prev_node = $c( '#slidetitle_' + response.target );
							cnhkx_textfield( 'change_title', 'new_title', 'Enter the new title:', 'title', response.target, prev_node , response.title );
						}
					} else {
						cnhk_ajax_settings.pending = false;
						cnhkx_display_error( response );
					}
				
				}			
			);
			
		}
		return false;
		/*
		*	En of a[id^="slidetitle"] 's section
		*/
	} );

		$c( 'a[id^="slidelink"]' ).live( 'click', function() {
			if ( ! cnhk_ajax_settings.pending ) {
				clear_bubble();
				
				id_attr = $c( this ).attr( 'id' );
				
				var DATA = {
					action : 'cnhkx_initial',
					what : 'link',
					nonce : cnhk_ajax_settings.nonce,
					num_id : parse_id( id_attr ).num,
					alpha_id : parse_id( id_attr ).alpha
				}
				
				preload( this );
				
				$c.post(			
					cnhk_ajax_settings.url,
					DATA,
					function( response ) {
						clear_preload();
						if ( is_valid_resp( response ) ) {
							if ( response.status ) {
								if ( cnhk_ajax_settings.pending ) {
									cnhk_ajax_settings.pending = false;
									prev_node = $c( '#slidelink_' + response.target );
									cnhkx_textfield( 'change_link', 'new_link', 'Enter the new link:', 'link', response.target, prev_node , response.link );
								}
							} else {
								cnhkx_display_error( response );
							}
						} else {
							cnhk_ajax_settings.pending = false;
							cnhkx_display_error( response );
						}
					}		
				);
				
			}
			return false;
		});
	
	$c( '#cnhk_file' ).uploadify( {
		'swf'      			: cnhk_ajax_settings.cnhk_url + 'uploadify/uploadify.swf',
		'uploader' 			: cnhk_ajax_settings.cnhk_url + 'uploadify/uploadify.php',
        'multi'             : false,
        'queueSizeLimit'    : 1,
		'checkExisting'		: false,
		'formData' 			: {
								'sid'			: cnhk_ajax_settings.sid,
								'cnhk_dir' 		: cnhk_ajax_settings.cnhk_dir,
								'cnhk_url' 		: cnhk_ajax_settings.cnhk_url,
								'ajaxnonce' 	: cnhk_ajax_settings.nonce,
								'page'			: cnhk_ajax_settings.page,
								'what'			: 'slide'
							},
		'onUploadSuccess' 	: function( file, sdata, response ) {
		
								data = JSON.parse( sdata);
								
								if ( is_valid_resp( data ) ) {
									if ( data.status ) {
										insert_in_db( 'slide', data.file );
									} else {
										cnhkx_display_error( data );
									}
								}
							},
		'onSWFReady' 		: function() {
								$c( '.c_upload_button' ).remove();
							}
	});
	
} );

function insert_in_db( what, target ) {
	switch ( what ) {
		case 'slide' :
				$c.post(
					cnhk_ajax_settings.url,
					{
						action	: 'afterupload',
						nonce	: cnhk_ajax_settings.nonce,
						what	: 'slide',
						file	: target
					},
					function ( re ) {
						if ( is_valid_resp( re ) ) {
							if ( re.status ) {
								append_slide( re.index, re.src, re.title, re.udate, re.width, re.height, re.wideclass, re.key );
								cnhk_make_infobubble( 'slideuse_' + re.index, 'New slide added' );
							}
						} else {
							cnhkx_display_error( response );
						}
					}
				);
			break;
		case 'bgr' :
			break;
		default	:
	}
}

function append_slide( index, src, title, udate, width, height, wideclass, token ) {
	tr = $c( '<tr />' );
	tdsrc = $c( '<td />' ).append( $c( '<div />' ).addClass( 'background-pattern' + wideclass ).append( $c( '<img />' ).attr({
		src		: src,
		title	: title,
		alt		: title,
		width	: width,
		height	: height
	}) ) );
	tduse = $c( '<td />' ).addClass( 'alternate' ).append( $c( '<a />' ).text( 'Unused' ).attr({
		href	: '#',
		id		: 'slideuse_' + index,
		title	: 'Change'
	}) );
	tdtitle = $c( '<td />' ).append( $c( '<a />' ).addClass( 'row-title' ).text( title ).attr({
		href	: '#',
		title	: "Edit " + title + "'s title",
		id		: 'slidetitle_' + index
	}) ).append( $c( '<br />' ) ).append( $c( '<span />' ).append( $c( '<a />' ).text( 'Edit' ).attr({
		id		: 'linkedit_' + index,
		href	: cnhk_ajax_settings.admin_slides_url + '&action=edit&obj=' + title + '&key=' + token,
		title	: 'Edit "' + title + '"'
	}) ).append( ' | ' ).append( $c( '<a />' ).text( 'Delete' ).attr({
		id		: 'linkdelete_' + index,
		href	: cnhk_ajax_settings.admin_slides_url + '&action=delete&obj=' + title + '&key=' + token,
		title	: 'Delete "' + title + '"'
	}) ) );
	tdlink = $c( '<td />' ).addClass( 'alternate' ).append( $c( '<a />' ).attr({
		id		: 'slidelink_' + index,
		href	: '#'
	}).text( 'No link' ) );
	tddate = $c( '<td />' ).attr({ title : udate }).text( udate	);
	tr.append( tdsrc ).append( tduse ).append( tdtitle ).append( tdlink ).append( tddate );
	$c( '#slides-list tbody' ).append( tr );
}

function post_textfield( subject, target, value ) {

	if ( ! cnhk_ajax_settings.pending ) {
		
		clear_bubble();
		
		DATA = {};
		atag = '';
		
		switch ( subject ) {
			case 'title' :
				
				atag = 'slidetitle_' + target;
				DATA = {
					action : 'cnhkx_resp',
					what : 'textfield_submited',
					subject : subject,
					target : target,
					value : value,
					nonce : cnhk_ajax_settings.nonce
				}
				
				break;
			case 'link' :
			
				atag = 'slidelink_' + target;
				DATA = {
					action : 'cnhkx_resp',
					what : 'textfield_submited',
					subject : subject,
					target : target,
					value : value,
					nonce : cnhk_ajax_settings.nonce
				}
				
				break;
			default:
		}
		
		preload( atag );
		
		$c.post(
			cnhk_ajax_settings.url,
			DATA,
			function ( response ) {
				clear_preload();
				if ( is_valid_resp( response ) ) {
				
					if ( cnhk_ajax_settings.pending ) {
						cnhk_ajax_settings.pending = false;
						
						if ( response.status ) {
							switch( response.subject ) {
							
								case 'title' :
									$c( '#slidetitle_' + response.target ).text( response.new_value );
									
									for ( var i in response.outofdate_links ) {
										href = response.outofdate_links[i].href;
										id = response.outofdate_links[i].id;
										title = response.outofdate_links[i].title;
										if ( title )
											$c( '#' + id  ).attr( { 'title' : title } );
										if ( href )
											$c( '#' + id ).attr( { 'href' : href } );
									}
									
									cnhk_make_infobubble( 'slidetitle_' + response.target, 'Data updated' );
									break;
								case 'link' :
									$c( '#slidelink_' + response.target ).text( response.new_value );
									cnhk_make_infobubble( 'slidelink_' + response.target, 'Data updated' );
									break;
								default :
								
							}
						} else {
							cnhkx_display_error( response );
						}
						
					}
				} else {
					cnhk_ajax_settings.pending = false;
					cnhkx_display_error( response );
				}
			}
		);
		
	}
	return false;
	
}

function preload( atag_obj ) {

		loading = $c( '<img />' ).attr( {
			src : cnhk_ajax_settings.imgurl + '/arrows16.gif'
		} ).addClass( 'cnhkx-preloader' );
		$c( atag_obj ).append( loading );
		cnhk_ajax_settings.pending = true;
		
		cdelay = setTimeout( "toolong()", RESPONSE_TIMEOUT );
		
}

function clear_preload() {
	if ( 'undefined' != typeof( cdelay ) ) {
		clearTimeout( cdelay );
	}
	$c( '.cnhkx-preloader' ).remove();
}

function parse_id( id ) {
	arr = id.split( '_' );
	if ( 2 == arr.length ) {
		return { 'alpha': arr[0], 'num' : arr[1] };
	} else {
		return false;
	}
}

function parse_lnk_repid( id ) {
	arr = id.split( '_' );
	if ( 3 == arr.length ) {
		return { 'alpha' : arr[0], 'target' : arr[1], 'value' : arr[2] };
	} else {
		return false;
	}
}

function toolong() {
	clear_preload();
	cnhk_ajax_settings.pending = false;
	cnhkx-popout( "<h3>Timeout</h3><p>It seems that the server takes too long time to treat your request. Try to use the edit link to edit slide's parameters.</p>", true );
}

function clear_bubble() {
	$c( '.cnhkx-bubble' ).remove();
}

function debug( obj ) {
	var text = "\nObject {\n";
	
	for ( var i in obj ) {
		if ( i !== obj ) { 
			if ( 'object' == typeof( obj[i] ) ) {
				text += " [" + i + "] => " + debug( obj[i] ) + "\n"; 
			} else {
				text += " [" + i + "] => " + obj[i] + "\n";  
			}
		}
	}
	
	result = text + "}";
	return result;
}

function is_valid_resp( resp ) {
	if ( 'undefined' != typeof( resp.status ) ) {
		return true;
	} else {
		return false;
	}
}

function remove_infobubble() {
	setTimeout( 'remove_infobubble_internal()', 2500 );
}

remove_infobubble_internal = function() {
	$c( '.infobubble' ).fadeOut( 1500, function() { 
		$c( '.infobubble' ).remove();
	} );
}

function cnhk_make_infobubble( prev_node_id, content, additional_classes ) {
	if ( 'string' != typeof( additional_classes ) ) {
		additional_classes = '';
	}
	classes = 'infobubble ';
	if ( '' == additional_classes ) {
		classes += ' info-success';
	} else {
		classes += ' ' + trim( additional_classes );
	}
	div = $c( '<div />' ).addClass( classes ).html( content );
	$c( '#' + prev_node_id ).after( div );
	remove_infobubble();
}

function cnhkx_textfield( form_id, field_name, label, subject, target, prev_node_jqobj, old_value ) {
	if ( 'undefined' == typeof old_value )
		old_value = '';
	
	bbl = $c( '<div />' ).addClass( 'cnhkx-bubble' );
	f = $c( '<div />' ).addClass( 'cnhkx-form' ).attr( { id: form_id } ).click( function() { return false; } );
	
	f.append( $c( '<label />' ).text( label ) );
	
	f.append( $c( '<input />' ).attr( {
		type: 'hidden',
		name: 'subject',
		value: subject		
	} ) );
	f.append( $c( '<input />' ).attr( {
		type: 'hidden',
		name: 'target',
		value: target		
	} ) );
	f.append( $c( '<input />' ).attr( {
		type: 'text',
		name: field_name,
		value: old_value	
	} ).keypress( function ( evt ) {
		code = evt.which || evt.keyCode;
		if ( 13 == code ) {
			new_value = $c( '#' + form_id + ' input[name="' + field_name + '"]' ).val();
			post_textfield( subject, target, new_value );
		}
	}) );
	f.append( $c( '<a />' ).attr( {
		href: '#',
		id: 'submit_' + field_name
	} ).text( 'Save' ).addClass( 'button-primary' ).click( function() {
		new_value = $c( '#' + form_id + ' input[name="' + field_name + '"]' ).val();
		post_textfield( subject, target, new_value );
		return false;
	} ) );
	bbl.append( f );

	prev_node_jqobj.after( bbl );
	
}

function cnhkx_popout( content, modal ) {
	if ( 'undefined' == typeof( modal ) ) {
		modal = false;
	}
	
	mainbox = $c( '<div />' ).addClass( 'cnhkx-popout' ).css(
		{
			zIndex: 99999
		}	
	);
	
	w_width = $c( window ).width();
	w_height = $c( window ).height();
	
	close_img = $c( '<img />' ).attr(
		{
			src : cnhk_ajax_settings.imgurl + '/close16.png' 
		}
	).addClass( 'close-popout close-img' );
	
	close_link = $c( '<a />' ).attr(
		{
			href : '#',
		}
	).addClass( 'close-popout close-link' ).text( 'Close' ).css(
		{
			color: '#aaa'
		}
	).hover(
		function() {
			$c( this ).css(
				{ color: '#fff' }
			);
		},
		function() {
			$c( this ).css(
				{ color: '#aaa' }
			);
		}
	);
	
	var message = false;
	if ( content.selector ) {
		message = content.addClass( 'popout-message' );
	} else {
		message = $c( '<div />' ).html( content ).addClass( 'popout-message' );
	}
	
	container = $c( '<div />' ).addClass( 'popout-container' ).append( close_img ).append( message ).append( close_link );
	
	if ( modal ) {
		mainbox.addClass( 'modal' ).append( container );
		$c( 'body' ).append( mainbox );
		
	} else {
		mainbox.append( container );
		$c( 'body' ).prepend( mainbox );
	}
	
	refresh_popout_position();
	$c( window ).scrollTop( 0 );
	
	$c( '.close-popout' ).click( function() {
			$c( '.cnhkx-popout' ).remove();
			return false;
		}
	);	
	$c( window ).resize( function() {
		refresh_popout_position();
	} );
}

function refresh_popout_position() {

	w_width = $c( window ).width();
	w_height = $c( window ).height();
	
	d_width = $c( document ).width();
	d_height = $c( document ).height();
	
	scroll = $c( window ).scrollTop();
	if ( $c( '.cnhkx-popout' ).hasClass( 'modal' ) ) {
	
		$c( '.cnhkx-popout' ).css(
			{
				top		: 0,
				left	: 0,
				width	: d_width + 'px',
				height	: d_height + 'px',
			}
		).click( function() { return false; } );		
	}
	$c( '.popout-container' ).css(
		{
			left	: Math.round( w_width/3 ) + 'px',
			top		: Math.round( w_height/3 ) + 'px',
			width	: Math.round( w_width/3 ) + 'px',
			height	: Math.round( w_height/3 ) + 'px'
		}
	);
	$c( '.popout-message' ).css(
		{
			height	: Math.round( w_height/6 ) + 'px',
			scroll	: 'auto'
		}
	);
}

function cnhkx_display_error( response ) {
	
	if ( is_valid_resp( response ) ) {
		if ( response.error ) {
			if ( 'undefined' != typeof( response.subject ) ) {
				switch( response.subject ) {
					case 'slide-upload' :
						cnhk_make_infobubble( 'slideupload', response.error, 'info-error' );
						break;
					default :
						cnhk_make_infobubble( 'slide' + response.subject + '_' + response.target, response.error, 'info-error' );
				}
			} else {
				if ( response.modal ) {
					cnhkx_popout( response.error, true );
				} else {
					cnhkx_popout( response.error );
				}
			}
		}
	} else {
		cnhkx-popout( "<h3>Invalid response</h3><p>The response received from the server is invalid. Try to use the edit link to edit slide's parameters.</p>", true );
	}
	
}

function trim( value ) {
	reg = /^\s+|\s+$/g;
	result = value.replace( reg, '' );
	return result;
}