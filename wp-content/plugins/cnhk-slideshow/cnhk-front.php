<?php if ( isset( $index ) && is_int( $index ) && -1 < $index ) {

	$options_ss 	= get_option( 'cnhk_options_ss' );
	$options_slides = get_option( 'cnhk_options_slides' );
	$options 		= get_option( 'cnhk_options' );

	global $ss_count;

	if ( ! is_array( $ss_count ) ) {		$ss_count = array(); 
	}
	
	array_push( $ss_count, $index );
	$key = array_keys( $ss_count, $index, true );	
	$id = count( $key ) -1;
	if ( 0 < count( $options_ss['elem'][$index] ) ) {
		if ( is_int( $fixed_width ) ) {
			$align_list = array( 'none', 'left', 'right', 'center', 'float-left', 'float-right' );
			$head = '';
			switch ( $fixed_align ) {
				case 'left':
					$head .= "<div class='cnhk-container'><div id='cbx-" . $options[0][$index] . "-$id' style='width: {$fixed_width}px; float: left;'>";
					break;
				case 'right':
					$head .= "<div class='cnhk-container'><div id='cbx-" . $options[0][$index] . "-$id' style='width: {$fixed_width}px; float: right;'>";
					break;
				case 'center':
					$head .= "<div class='cnhk-container'><div id='cbx-" . $options[0][$index] . "-$id' style='width: {$fixed_width}px; margin-left: auto; margin-right: auto;'>";
					break;
				case 'float-left':
					$head .= "<div id='cbx-" . $options[0][$index] . "-$id' style='width: {$fixed_width}px; float: left; margin: 5px 10px;'>";
					break;
				case 'float-right':
					$head .= "<div id='cbx-" . $options[0][$index] . "-$id' style='width: {$fixed_width}px; float: right; margin: 5px 10px;'>";
					break;
				default: /* for 'none' */
					$head .= "<div id='cbx-" . $options[0][$index] . "-$id' style='width: {$fixed_width}px;'>";
					break;
			}
			echo $head;
		}
		echo "<div class='cnhk-data'>"; /* Data block */
		
		echo "<ul id='cssettings-". $options[0][$index] . "-$id'>";
		echo "<li>" . $options[1][$index] . "</li>";
		echo "<li>" . $options[2][$index] . "</li>";
		echo "<li>" . $options[3][$index] . "</li>";
		echo "<li>" . $options[4][$index] . "</li>";
		echo "<li>" . $options[5][$index] . "</li>";
		$nav = ( $options[6][$index] ) ? 'true' : 'false';
		echo "<li>$nav</li>";
		echo "<li>" . CNHK_URL . "</li>";
		$background = ( $options[10][$index] ) ? cnhk_background_image_url( $index ) : 'none';
		echo "<li>" . $background . "</li>";
		echo "</ul>";
		
		echo "<ul id='cssrc-". $options[0][$index] . "-$id'>";
		foreach ( $options_ss['elem'][$index] as $value ) {
			echo "<li>" . CNHK_URL . 'uploads/' . $options_slides[0][$value] . "</li>";
		}
		echo "</ul>";
		
		echo "<ul id='csttl-". $options[0][$index] . "-$id'>";
		foreach ( $options_ss['elem'][$index] as $value ) {
			echo "<li>" . $options_slides[2][$value] . "</li>";
		}
		echo "</ul>";
		
		echo "<ul id='cslnk-". $options[0][$index] . "-$id'>";
		foreach ( $options_ss['elem'][$index] as $value ) {
			echo "<li>" . urldecode( $options_slides[3][$value] ) . "</li>";
		}
		echo "</ul>";
		
		echo "</div>";/*End of cnhk-data*/
		
		echo "<div class='cnhk-slideshow' id='csi-" . $options[0][$index] . "-$id'></div>"; /* Container */
			
		if ( $options[6][$index]  ) {
			echo "<div id='csnav-" . $options[0][$index] .  "-$id' class='cnhk-nav'>";
			echo "</div>";
		} else {
			
			echo "<div id='csnav-" . $options[0][$index] .  "-$id' class='cnhk-navx'>";
			echo "</div>";
		}
		if ( is_int( $fixed_width ) ) {
			echo "</div><!-- cbx-{$options[0][$index]}-$id -->";
			if ( 'left' == $fixed_align || 'right' == $fixed_align || 'center' == $fixed_align  ) {
				echo '</div><!-- cnhk-container -->';
				echo "<div class='cnhk-clear-floating'></div>";
			}
		}
	}
}