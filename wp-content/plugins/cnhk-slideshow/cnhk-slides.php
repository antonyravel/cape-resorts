<?php
if ( ! current_user_can ( 'manage_options' ) ) {
	wp_die ( 'You do not have sufficient permissions to access this page.' );
}
?>
<div class="wrap">
	<div id="icon-upload" class="icon32"></div>
	<h2>Slides</h2>
	<?php
/*
*$_GET['action'] == edit
*/
	if ( isset( $_GET['action'] ) && 'edit' == $_GET['action'] ) {
		if ( $_GET['key'] == $_SESSION['cnhk']['token_edit'] ) {
		
			$obj = urldecode( $_GET['obj'] );
			$opt = get_option( 'cnhk_options_slides' );
			
			if ( array_search( $obj, $opt[2] ) !== false ) {
				?>
				<h3>Edit Slide <em>"<?php echo $obj; ?>"</em></h3>
				<table class="wp-list-table widefat"><thead><tr><th>Image</th><th>Infos</th></tr></thead><tr>
				<?php
				$index = array_search( $obj, $opt[2] );
				
				$snap_size = cnhk_get_snapsize( $index );
				$snap_width = $snap_size['width'];
				$snap_height = $snap_size['height'];
				$class = $snap_size['class'];
				?>
				<td><div class="background-pattern <?php echo $class; ?>"><img src="<?php echo esc_url( CNHK_URL . 'uploads/' . $opt[0][$index] ); ?>" title="<?php echo esc_attr( $opt[2][$index] ); ?>" alt="<?php echo esc_attr( $opt[2][$index] ); ?>" width="<?php echo $snap_width; ?>" height="<?php echo $snap_height; ?>" /></div></td>
				<td class="alternate"><div class="form-wrap">
				<form id="cnhk_edit_form" method="post" action="<?php echo CNHK_ADMIN_SLIDE;?>&updated=updated&obj=<?php echo $index; ?>">
				
				<?php
				$token_update = md5( uniqid( rand (), true ) );
				$_SESSION['cnhk']['token_update'] = $token_update;
				?>
				
				<input type="hidden" name="cnhk_token_update" value="<?php echo $token_update; ?>" />
				<input type="hidden" name="cnhk_update_index" value="<?php echo $index; ?>" />
				<div class="form-field">
				<label for="cnhk_update_title">Title</label>
				<input type="text" name="cnhk_update_title" id="cnhk_update_title" value="<?php echo $opt[2][$index]; ?>" />
				<p>The title is used to identify the slide. It's also used in the filename. You can use alphanumeric characters, blank spaces (" "), hyphens ("-") and underscores ("_")</p>
				</div>
				<div class="form-field">
				<label for="cnhk_update_link">Link</label>
				<input type="text" name="cnhk_update_link" id="cnhk_update_link" value="<?php echo urldecode($opt[3][$index]); ?>" />
				<p>The link toward which this slide leads</p>
				
				<label for="cnhk_slide_ss">Use</label>
				<select name="use">
				<?php
				$ss = get_option( 'cnhk_options' );
				if ( count( $ss[0] ) > 0 ) {
					for ( $i = -1; $i < count( $ss[0] ); $i++ ) {
						$selected = ( $i == $opt[1][$index] ) ? " selected='selected'" : '';
						$slideshow = ( $i == -1 ) ? 'Unused' : $ss[0][$i];
						echo "<option value='$i'$selected>$slideshow</option>";
					}
				} else {
					echo "<option value='-1'>Unused</option>";
				}
				?>
				</select>				
				</div>
				<div id="cnhk-edit-buttons">
				<input type="submit" name="cnhk_edit_ok" value="Update" class="button-primary" />
				<a href="<?php echo esc_url( CNHK_ADMIN_SLIDE ); ?>" class="button">Cancel</a>
				</div>
				</form></div></td>
				</tr></table>
				<?php
			} else {
				echo "<div class='error'><p>The slide \"{$obj}\" was not found on this server.</p></div>";
			}
		} else {
			echo "<div class='error'><p>No data updated</p></div>";
		}
	} elseif ( isset( $_GET['action'] ) && 'delete' == $_GET['action'] ) {

/*
*$_GET['action'] == delete
*/

		if ( isset( $_GET['key'] ) && isset( $_SESSION['cnhk']['token_edit'] ) && $_GET['key'] == $_SESSION['cnhk']['token_edit'] ) {
			$obj = urldecode( $_GET['obj'] );
			$opt = get_option( 'cnhk_options_slides' );
			$ss_setting = get_option( 'cnhk_options' );
			$index = array_search( $obj, $opt[2] ) ;
			if ( $index !== false ) {
				
				$imgsrc = $opt[0][$index];
				$title = $opt[2][$index];
				$link = urldecode( $opt[3][$index] );
				$use = ( -1 == $opt[1][$index] ) ? 'Unused' : $ss_setting[0][$opt[1][$index]];
			
				$snap_size = cnhk_get_snapsize( $index );
				$snap_width = $snap_size['width'];
				$snap_height = $snap_size['height'];
				$class = $snap_size['class'];
				?>
				<table class="wp-list-table widefat">
				<thead><tr><th>Image</th><th>Infos</th></tr></thead>
				<tbody><tr><td><div class="background-pattern <?php echo $class; ?>"><img src="<?php echo esc_url( CNHK_URL . "uploads/$imgsrc" ); ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" width="<?php echo $snap_width; ?>" height="<?php echo $snap_height; ?>" /></div></td>
				<td><ul style="list-style-type : none;">
				<li>Title : <strong><?php echo $title; ?></strong></li>
				<li>Link : <strong><?php echo $link; ?></strong></li>
				<li>Used in : <strong><?php echo $use; ?></strong></li>
				</ul></td></tr>
				</tbody>
				</table>
				<p><span style="font-size : 1.2em;">Are you sure to delete <span style="text-decoration:underline; font-weight:bold;">permanently</span> the slide <em>"<?php echo $title; ?>"</em>?</span></p>
				<p><em>If you are not sure, keep the slide and just drop if out of the slideshow.</em></p>
				<div class="form-wrap">
				<form method="post" action="<?php echo esc_url( CNHK_ADMIN_SLIDE ); ?>&updated=deleted">
				<?php
				$token_delete = md5( uniqid( rand (), true ) );
				$_SESSION['cnhk']['token_delete'] = $token_delete;
				?>
				<input type="hidden" name="cnhk_token_delete" value="<?php echo $token_delete; ?>" />
				<input type="hidden" name="cnhk_delete_index" value="<?php echo $index;?>" />
				<div id="cnhk-edit-buttons">
				<input type="submit" name="cnhk_delete_ok" value="Delete" class="button-primary" />
				<a href="<?php echo esc_url( CNHK_ADMIN_SLIDE ); ?>" class="button">Cancel</a>
				</div>
				</form>
				</div>
				<?php
			} else {
				echo "<div class='error'><p>The slide \"{$obj}\" was not found on this server.</p></div>";
			}
			
		} else {
			echo "<div class='error'><p>No data updated</p></div>";
		}
	} else {

/*
*$_GET['action'] is empty
*/

		if ( isset( $_GET['updated'] ) && 'updated' == $_GET['updated'] ) {
		
/*
*$_GET['updated'] == 'updated'
*/
		
			if ( isset( $_SESSION['cnhk']['token_update'] ) && isset( $_POST['cnhk_token_update'] ) && $_SESSION['cnhk']['token_update'] == $_POST['cnhk_token_update'] ) {
				
				$index = intval( $_POST['cnhk_update_index'] );
				$opt = get_option( 'cnhk_options_slides' );
				$old_title = $opt[2][$index];
				$new_title = trim( $_POST['cnhk_update_title'] );
				$old_name = $opt[0][$index];
				$old_use = $opt[1][$index];
				
				$ext = strtolower( strrchr( $opt[0][$index], '.' ) );
				
				$new_link = ( $_POST['cnhk_update_link'] == '') ? rawurlencode( 'No link' ) : rawurlencode( $_POST['cnhk_update_link'] );
				if ( cnhk_is_valid_title( $new_title ) ) {
					$new_name = cnhk_make_slug( $new_title ) . $ext;
					$name_jpg = CNHK_DIR . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . cnhk_make_slug( $new_title ) . '.jpg';
					$name_png = CNHK_DIR . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . cnhk_make_slug( $new_title ) . '.png';
					$new_use = intval( trim( $_POST['use'] ) );
					if ( ! ( ( file_exists( $name_jpg ) || file_exists( $name_png ) ) && $new_title != $old_title ) ) {
						try {
							$basedir = CNHK_DIR .DIRECTORY_SEPARATOR. "uploads". DIRECTORY_SEPARATOR ;
							rename( $basedir.$old_name, $basedir.$new_name );
							$opt[0][$index] = $new_name;
							$opt[1][$index] = $new_use;
							$opt[2][$index] = $new_title;
							$opt[3][$index] = $new_link;
							if ( -1 != $old_use ) {
								$ss = get_option( 'cnhk_options_ss' );
								$offset = array_search( $index, $ss['elem'][$old_use] );
								if ( false !== $offset ) {
									array_splice( $ss['elem'][$old_use], $offset, 1 );
									update_option( 'cnhk_options_ss', $ss );
								}
							}						
							update_option( 'cnhk_options_slides', $opt );
							echo "<div class='updated'><p><strong>Data updated.</strong></p></div>";
							
						} catch ( Exception $e ) {
							echo "<div class='error'><p>The file $old_name can not be renamed. No data updated.</p></div>";
						}
					} else {
						echo "<div class='error'><p>The title \"$new_title\" matches with an existing filename. Please choose an other one (A slight change may be sufficient). No data updated.</p></div>";
					}
				} else {
					echo "<div class='error'><p>The title can not contain any special characters. \"<strong>$new_title\"</strong> is invalid.<strong> No data updated.</strong></p></div>";
				}
			} elseif ( isset( $_SESSION['cnhk']['token_delete'] ) && isset( $_POST['cnhk_token_delete'] ) && $_SESSION['cnhk']['token_delete'] != $_POST['cnhk_token_delete'] ) {
				echo "<div class='error'><p> No data updated</p></div>";
			}
		}
		if ( isset( $_GET['updated'] ) && 'deleted' == $_GET['updated'] ) {
		
/*
*$_GET['updated'] == 'deleted'
*/
		
			if ( isset( $_SESSION['cnhk']['token_delete'] ) && isset( $_POST['cnhk_token_delete'] ) && $_SESSION['cnhk']['token_delete'] == $_POST['cnhk_token_delete'] ) {
				$index = $_POST['cnhk_delete_index'];
				$opt = get_option( 'cnhk_options_slides' );
				$option_ss = get_option( 'cnhk_options_ss' );
				$size = count( $opt[0] );
				$filename = array();
				$use = array();
				$title = array();
				$link = array();
				$date = array();
				for ( $i = 0; $i < $size; $i++ ) {
					if ( $i != intval( $index ) ){
					array_push( $filename, $opt[0][$i] );
					array_push( $use, $opt[1][$i] );
					array_push( $title, $opt[2][$i] );
					array_push( $link, $opt[3][$i] );
					array_push( $date, $opt[4][$i] );
					}
				}
				$new_opt = array( $filename, $use, $title, $link, $date );
				
				$new_options_ss = array( 'ss' => array(), 'elem' => array() );
				
				$length = count( $option_ss['ss'] );
				
				for ( $i = 0; $i < $length; $i++ ) {
					$new_elem = array();
					
					array_push( $new_options_ss['ss'], $option_ss['ss'][$i] );
					$cnt = count( $option_ss['elem'][$i] );
					for ( $j = 0; $j < $cnt; $j++ ) {
						if ( $option_ss['elem'][$i][$j] != intval( $index ) ) {
							if ( $option_ss['elem'][$i][$j] < intval( $index ) ) {
								array_push( $new_elem, $option_ss['elem'][$i][$j] );
							} else {
								array_push( $new_elem, $option_ss['elem'][$i][$j] -1 );
							}
						}
					}
					array_push( $new_options_ss['elem'], $new_elem );
				}
				
				try {
					unlink( CNHK_DIR . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . $opt[0][$index] );
					update_option( 'cnhk_options_slides', $new_opt );
					update_option( 'cnhk_options_ss', $new_options_ss );
					echo "<div class='updated'><p><strong>Slide deleted.</strong></p></div>";
				} catch ( Exception $e ) {
					echo "<div class='error'><p>The file " . $opt[0][$index] . " can not be deleted. No data updated.</p></div>";
				}
			}
		}
		
/*
*End of ( $_GET['updated'] == 'deleted' )
*/

	?>
	<form method="post" enctype="multipart/form-data" action="<?php echo admin_url( 'admin.php?page=cnhk-slide' );?>">
		<input type="hidden" name="cnhk_token_file" value="<?php
		$token_file = md5( uniqid( rand (), true ) );
		$_SESSION['cnhk']['token_file'] = $token_file;
		echo $token_file;
		?>" />
		<fieldset class="cnhk-form" id="slideupload">
			<?php
			if ( isset( $_SESSION['cnhk']['upload_error_upload'] ) && $_SESSION['cnhk']['upload_error_upload'] != '') {
				echo "<div class='error'>An error occurs during upload!</div>";
			}
			if ( isset( $_SESSION['cnhk']['upload_error_extension'] ) && $_SESSION['cnhk']['upload_error_extension'] != '') {
				echo "<div class='error'>Invalid extension! .jpg (or .jpeg) and .png files only</div>";
			}
			if ( isset( $_SESSION['cnhk']['upload_error_size'] ) && $_SESSION['cnhk']['upload_error_size'] != '' ) {
				echo "<div class='error'>The file is too big!</div>";
			}
			$_SESSION['cnhk']['upload_error_upload'] = '';
			$_SESSION['cnhk']['upload_error_extension'] = '';
			$_SESSION['cnhk']['upload_error_size'] = '';
			?>
			<legend>New slide</legend>
			<label for="cnhk_file">Choose a file to upload : </label>
			<input type="file" name="cnhk_file" id="cnhk_file" />
			<input type="submit" class="c_upload_button" value="Upload file" class="button-secondary" /> <span class="upload-limits">(*.jpg, *.jpeg, *.png up to 2MB)<span><br />
		</fieldset>
	</form>
	<h3 id="slideslist-head">Available Slides : </h3>
	<table class="wp-list-table widefat" id="slides-list">
		<thead>
			<tr><th width="300">Slide image</th><th width="160">Used in</th><th>Slide title</th><th>Slide link</th><th>Upload Date</th></tr>
		</thead>
		<tfoot>
			<tr><th width="300">Slide image</th><th>Used in</th><th>Slide title</th><th>Slide link</th><th>Upload Date</th></tr>
		</tfoot>
		<tbody>
		<?php
		$opt = get_option( 'cnhk_options_slides' );
		$ss = get_option( 'cnhk_options' );

		$token_edit = md5( uniqid( rand (), true ) );
		$_SESSION ['cnhk']['token_edit'] = $token_edit;
		
		for ( $i = 0; $i < count( $opt[0] ); $i++ ) {
			
			$snap_size = cnhk_get_snapsize( $i );
			$snap_width = $snap_size['width'];
			$snap_height = $snap_size['height'];
			$wideclass = $snap_size['class'];
			
			$slideshow = $opt[1][$i];
			
			$slide_usage_id = "slideuse_$i";
			?>
			<tr>
			<td><div class="background-pattern <?php echo $wideclass;?>"><img src="<?php echo CNHK_URL; ?>uploads/<?php echo $opt[0][$i]; ?>" title="<?php echo $opt[2][$i]; ?>" alt="<?php echo $opt[2][$i] ; ?>" width="<?php echo $snap_width; ?>" height="<?php echo $snap_height; ?>" /></div></td>
			<?php if ( $slideshow == -1 ) { ?>
				<td class="alternate"><a href="#" id="<?php echo $slide_usage_id; ?>">Unused</a></td>
			<?php } else {
				$use = ( $opt[1][$i] < count( $ss[0] ) ) ? $ss[0][$slideshow] : "<span style='color: #f20; font-weight: bold'>Data corrupted</strong>";
			?>
				<td class="alternate"><a href="#" id="<?php echo $slide_usage_id; ?>" title="Change"><?php echo $use ;?></a></td>
			<?php } ?>
			<td>
			<a href="#" class="row-title" title="Edit <?php echo $opt[2][$i]; ?>&#039;s title" id="slidetitle_<?php echo $i; ?>"><?php echo $opt[2][$i]; ?></a><br />
			<span><a id="linkedit_<?php echo $i;?>" href="<?php echo CNHK_ADMIN_SLIDE . '&action=edit&obj=' . rawurlencode($opt[2][$i]) . '&key=' . $_SESSION['cnhk']['token_edit'] ?>" title='Edit "<?php echo $opt[2][$i]?>"'>Edit</a> | <a id="linkdelete_<?php echo $i;?>" href="<?php echo CNHK_ADMIN_SLIDE . "&action=delete&obj=" . rawurlencode($opt[2][$i]) . "&key=" . $_SESSION ['cnhk']['token_edit']; ?>" title='Delete "<?php echo $opt[2][$i] ?>"'>Delete</a></span>
			<?php 
			$goto = urldecode( $opt[3][$i] );
			if ( 'No link' == $goto) { ?>
				</td><td class="alternate"><a href="#" id="slidelink_<?php echo $i ?>">No link</a></td>
			<?php } else { ?>
				</td><td class="alternate"><a href="<?php echo esc_url( $goto ); ?>" id="slidelink_<?php echo $i ?>" target="_blank"><?php echo esc_attr( $goto ); ?></a></td>
			<?php }
			$DT = gmdate( "d/m/Y - h:i A", $opt[4][$i] ) . ' GMT';
			$DT_title = gmdate( "d/m/Y - h:i A", $opt[4][$i] ) . ' GMT';
			?>
			<td title="<?php echo $DT_title ?>"><?php echo $DT ?></td>
			</tr>
			<?php
		}
		?>
		</tbody>
	</table>
<?php } /*END $_GET['action'] is empty*/ ?>
</div>