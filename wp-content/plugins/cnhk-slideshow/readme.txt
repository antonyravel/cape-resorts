=== Cnhk Slideshow ===
Contributors: cnhk_systems
Donate link: http://cnhk-systems.webege.com
Tags: easy, slideshow, images, jquery, jquery cycle, drag and drop, fluid width, multiple slideshows
Requires at least: 3.4.2
Tested up to: 3.5.1
Stable tag: 1.1.1
License: GPLv2 or later

Easy to use Wordpress slideshow plugin with a drag and drop system for editing slides order.

== Description ==

Cnhk Slideshow is a slideshow plugin that uses jQuery cycle and is compatible with fluid width design. It uses an easy drag and drop system for managing slideshow. The plugin supports using multiple slideshows on the same page or post. Integration by shortcode, widget or template tag.

== Installation ==
Upload the cnhk-slideshow folder to your /wp-content/plugins/ directory or go to Plugins -> Add New from the Dashboard.
Activate the plugin through the Plugins menu.
Use the Settings link or go to Slideshow -> Settings to access the Cnhk Slideshow settings.

= How to use it =

After installation, the first thing to do is to create a slideshow. On the *'settings'* page enter a name in the *'new slideshow'* section.
This name must be unique for each slideshow. Once the slideshow created, upload all image files you need. Each file can be used in at most one slideshow.
Go to the *'slides'* pages to upload your files and for each slide click on the *'edit'* link then on the *'Use'* field select the slideshow's name in which you want this slide to be used. You can also set an URL this slide should link to and a title.
Return to the *'settings'* page and adjust slideshow's settings to your needs. Go to the *'slideshow'* page to arrange displaying order of slides and save the changes.
Now ***you have 3 way to display your slideshow***:

***In sidebar using widget***

A widget called *'cnhk slideshow'* is now available in the widget page of your admin panel. Drag it in your sidebar and select the slideshow you want to display.

***In your post using shortcode***

In your post you can use a shortcode like [cnhk_slideshow name='my_slideshow' width='300' align='float-left'] where :

* **my_slideshow** is the name that you defined on the 'settings' page.

* **300** is the fixed width in pixels of the container (if you want it to be a fixed width slideshow). If set this value will override the one you've entered in the settings page for this slideshow. Then a fluid width slideshow will have locally a fixed width

* **align-left** is the alignment of the container (only for fixed width). If set this value will override the one you've entered in the settings page. (Possible values : left, right, center, float-left, float-right).

* **Only the name parameter is required**


***Using template tag***

Directly in your theme's files with `<?php if ( function_exists( 'cnhk_slideshow' ) ) cnhk_slideshow( 'my_slideshow', 300, 'align-left' ); ?>`
Meanings of all parameters are the same as for the shortcode and only the name parameter is required. Pay attention on the width parameter which is passed as an integer to the function (no wrapping quotes).

= Settings =

By default, any created slideshow has a fluid width and takes all available width in its layout. *'Width'* and *'height'* in the left column of the *'settings'* page are used to scale slides when the browser is resized.
You can restrain the slideshow in a fixed width container by choosing fixed width on the right column. To define this fixed width enter the value (in pixels) into the *'The fixed width'*.  You can also choose an alignment for the slideshow.
The value you entered is overridden when you use the 'width' or the 'align' parameters in shortcodes or template tags. Doing so allows you to fix locally the width for a basicaly fluid width slideshow.
You can add a background image for each slideshow. This image will fit the ratio defined in *'settings'* page and will be visible through transparent areas of '.png' slides
*Tip* : Use shortcode in a post and preview this post to try your slideshow without publishing it on the front end.

== Frequently Asked Questions ==

= How should I define dimensions for the slideshow =

Slideshow takes all the width available in the block in which you put it, and is resized each time the window (web browser) is resized.
The slideshow's height is calculated using the width/height ratio of your settings (width and height that you entered on the settings page).
For each slideshow, use images with approximatively the same width/height ratio to display it correctly. Then take width and height from one of those images and enter them in the *'settings'* screen.

= Can the plugin be used to display two or more slideshow =

Yes! The plugin supports multiple slideshows and many slideshows can appear in the same page/post, even the same slideshow with different *'width'* or *'align'* values.

= When I'm about to set slides order I can't find some of these slides =

* You've probably make some mistakes when editing slides properties. Slides you're searching for may be used in an other slideshow than the one you've expected.

= I've created a slideshow, performed all settings but nothing appears =

* Ensure that you put something in the right column on *'slideshow'* page.
* If you use a shortcode or a template tag, check the *'name'* parameter. This name is case sensitive.

== Screenshots ==

1. Slideshow preview
2. Slideshow admin page
3. Settings page
4. Slides page

== Changelog ==

= 1.1.1 =

* Fixed bugs. An image with 'none' was added if here is no background.
* Background is now resized to fit the container before the slideshow starts.
* Fixed bug in ajax.php which causes problem when trying to set a slide as 'Unused' while it is still used (and ordered) inside a slideshow.

= 1.1 =

* Added transparency support *(.png files)*
* Added dynamic edition *(ajax)* of slide's properties
* Added optional background image
* Added new navigation buttons
* Added uninstall.php for a separate (from deactivation) uninstallation process

= 1.0 =

* initial release

== Upgrade Notice ==

= 1.1.1 =

Fixes bugs with background images and ajax edition.

= 1.1 =

Version 1.1 adds .png format support, dynamic edition of slide's properties, background image, new navigation buttons and uninstall process.

= 1.0 =

* initial release