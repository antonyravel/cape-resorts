<?php
if ( is_admin() ) {

	add_action( 'wp_ajax_cnhkx_initial', 'cnhk_ajax_initial' );

	function cnhk_ajax_initial() {
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( 'Not enough permissions.' );
		}
		if ( isset( $_POST['nonce'] ) && $_POST['nonce'] == $_SESSION['cnhk']['ajaxnonce'] ) {
		
			$settings = get_option( 'cnhk_options' );
			$slides = get_option( 'cnhk_options_slides' );
			$slideshows = get_option( 'cnhk_options_ss' );
			
			switch( $_POST['what'] ) {
			
				case 'use' :
					if ( intval( $_POST['num_id'] ) < count( $slides[0] ) ) {
						$slide_id = intval( $_POST['num_id'] );
						$current_usage = $slides[1][$slide_id];
						
						$response = array( 'status' => true, 'target' => array( 'use', $slide_id ), 'value' => array() );
						
						if ( -1 == $current_usage ) {
							array_push( $response['value'], array( 'index' => -1, 'use' => 'Unused', 'current' => true ) );
						} else {
							array_push( $response['value'], array( 'index' => -1, 'use' => 'Unused', 'current' => false ) );
						}
						
						foreach	( $settings[0] as $key => $value ) {
							if ( $current_usage == $key ) {
								array_push( $response['value'], array( 'index' => $key, 'use' => $value, 'current' => true ) );
							} else {
								array_push( $response['value'], array( 'index' => $key, 'use' => $value, 'current' => false ) );
							}
						}					
						
						$output = json_encode( $response, JSON_FORCE_OBJECT );
						
						header( "Content-Type: application/json" );
						echo $output;
						
						exit;
					} else {
						$error = '<h3>Data error</h3><p>The plugin may need to be reinstalled</p>';
						$output = json_encode( array( 'status' => false, 'error' => $error ) );			
						header( "Content-Type: application/json" );
						echo $output;
						exit;
					}
					
					break;
				case 'title' :
					if ( intval( $_POST['num_id'] ) < count( $slides[0] ) ) {
						$slide_id = $_POST['num_id'];
						
						$title = $slides[2][$slide_id];
						$response = array( 'status' => true, 'title' => $title, 'subject' => 'title', 'target' => $slide_id );
						
						$output = json_encode( $response, JSON_FORCE_OBJECT );
						header( "Content-Type: application/json" );
						echo $output;
						exit;
						
					} else {
					
						$error = '<h3>Data error</h3><p>The plugin may need to be re-installed</p>';
						$output = json_encode( array( 'status' => false, 'error' => $error ) );
						header( "Content-Type: application/json" );
						echo $output;
						exit;
					}
					break;

				case 'link' :
					if ( intval( $_POST['num_id'] ) < count( $slides[0] ) ) {
						$target = intval( $_POST['num_id'] );
						$link = urldecode( $slides[3][$target] );
						
						$response = array(
							'status' 	=> true,
							'link'		=> $link,
							'subject'	=> 'link',
							'target'	=> $target
						);
						
						$output = json_encode( $response, JSON_FORCE_OBJECT );
						header( "Content-type: application/json" );
						echo $output;
						exit;
						
					}
					break;
					
				default:		
			}
		} else {
			
			$error = '<h3>Security error</h3><p>Try to reload this page and make sure to use the cancel button for canceling an action.</p>';
			$output = json_encode( array( 'status' => false, 'error' => $error, 'modal' => true ) );			
			header( "Content-Type: application/json" );
			echo $output;
			exit;
			
		}
		
		exit;
	/*
	*	End of cnhk_ajax_initial()
	*/
	}
	
	add_action( 'wp_ajax_cnhkx_resp', 'cnhk_ajax_resp' );
	
	function cnhk_ajax_resp() {
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( 'Not enough permissions.' );
		}
		if ( isset( $_POST['nonce'] ) && $_POST['nonce'] == $_SESSION['cnhk']['ajaxnonce'] ) {
		
			$settings = get_option( 'cnhk_options' );
			$slides = get_option( 'cnhk_options_slides' );
			$slideshows = get_option( 'cnhk_options_ss' );	
			
			switch ( $_POST['what'] ) {
			
				case 'use' :
					if ( intval( $_POST['target'] ) < count( $slides[0] ) && intval( $_POST['value'] ) < count( $settings[0] ) ) {
						
						$target = intval( $_POST['target'] );
						$value = intval( $_POST['value'] );
						$old_value = $slides[1][$target];
						$size = cnhk_get_snapsize( $target );
						$old_class = $size['class'];
						
						if ( -1 != $old_value ) {
							$offset = array_search( $target, $slideshows['elem'][$old_value] );
							if ( false !== $offset ) {
								array_splice( $slideshows['elem'][$old_value], $offset, 1  );
								update_option( 'cnhk_options_ss', $slideshows );
							}
						}
						
						$slides[1][$target] = $value;
						update_option( 'cnhk_options_slides', $slides );
						
						$value_name = ( -1 == $value ) ? 'Unused' : $settings[0][$value];
						$snap_size = cnhk_get_snapsize( $target );
						
						$response = array(
							'status' => true,
							'target' => $target,
							'value_name' => $value_name,
							'width' => $snap_size['width'],
							'height' => $snap_size['height'],
							'old_class' => $old_class,
							'new_class' => $snap_size['class'],
							'title' => $slides[2][$target]
						);
						
						$output = json_encode( $response, JSON_FORCE_OBJECT );
						header( "Content-Type: application/json" );
						echo $output;
						exit;
						
					} else {
						$error = '<h3>Data error</h3><p>The plugin may need to be reinstalled</p>';
						$output = json_encode( array( 'status' => false, 'error' => $error ) );			
						header( "Content-Type: application/json" );
						echo $output;
						exit;
					}
					break;
					
				case 'textfield_submited' :
					if ( intval( $_POST['target'] ) < count( $slides[0] ) && intval( $_POST['value'] ) < count( $settings[0] ) ) {
						
						$target = intval( $_POST['target'] );
						$new_value = trim( $_POST['value'] );
						$subject = $_POST['subject'];
						switch ( $subject ) {
							case 'title' :
								$new_title = $new_value;
								if ( cnhk_is_valid_title( $new_title ) ) {
								
									$ext = strtolower( strrchr( $slides[0][$target], '.' ) );
									$new_name = cnhk_make_slug( $new_title ) . $ext;
									$name_jpg = CNHK_DIR . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . cnhk_make_slug( $new_title ) . '.jpg';
									$name_png = CNHK_DIR . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . cnhk_make_slug( $new_title ) . '.png';
									$old_name = $slides[0][$target];
									$old_title = $slides[2][$target];
									
									if ( ! ( ( file_exists( $name_jpg ) || file_exists( $name_png ) ) && $new_title != $old_title ) ) {
									
										try {
											$basedir = CNHK_DIR .DIRECTORY_SEPARATOR. "uploads". DIRECTORY_SEPARATOR ;
											rename( $basedir.$old_name, $basedir.$new_name );
											$slides[0][$target] = $new_name;
											$slides[2][$target] = $new_title;
											update_option( 'cnhk_options_slides', $slides );
											
											$outofdate_links = array(
												array(
													'id'	=> "linkedit_$target",
													'href'	=> CNHK_ADMIN_SLIDE . "&action=edit&obj=" . rawurlencode($slides[2][$target]) . "&key=" . $_SESSION ['cnhk']['token_edit'],
													'title'	=> 'Edit "' . $slides[2][$target] . '"'
												),
												array(
													'id'	=> "linkdelete_$target",
													'href'	=> CNHK_ADMIN_SLIDE . "&action=delete&obj=" . rawurlencode($slides[2][$target]) . "&key=" . $_SESSION ['cnhk']['token_edit'],
													'title'	=> 'Delete "' . $slides[2][$target] . '"'
												),
												array(
													'id'	=> "slidetitle_$target",
													'href'	=> false,
													'title'	=> "Edit {$slides[2][$target]}'s title"
												)
											);
											
											$response = array(
												'status' => true,
												'new_value' => $new_title,
												'subject' => $subject,
												'target' => $target,
												'outofdate_links' => $outofdate_links
											);
											
											$output = json_encode( $response, JSON_FORCE_OBJECT );	
											header( "Content-Type: application/json" );
											echo $output;
											exit;
											
										} catch ( Exception $e ) {
											$response = array(
												'status' 	=> false,
												'error' 	=> "<h3>No data updated</h3></p>The file <strong>\"" . $old_name . "\"</strong> can not be renamed.</p>",
												'target'	=> $target,
												'subject'	=> $subject
											);
											$output = json_encode( $response, JSON_FORCE_OBJECT );	
											header( "Content-Type: application/json" );
											echo $output;
											exit;
											
										}
									
									} else {
										$response = array(
											'status' => false,
											'error' => "<h3>No data updated</h3><p>This title matches an existing file. Please choose another one.</p>",
											'target'	=> $target,
											'subject'	=> $subject
										);
										$output = json_encode( $response, JSON_FORCE_OBJECT );	
										header( "Content-Type: application/json" );
										echo $output;
										exit;
									
									}
								} else {
									$response = array(
										'status' 	=> false,
										'error' 	=> "<h3>No data updated</h3><p>The title should not be empty and can not contain any special characters. <strong>\"" . $new_value . "\"</strong> is invalid.</p>",
										'target'	=> $target,
										'subject'	=> $subject
									);
									$output = json_encode( $response, JSON_FORCE_OBJECT );	
									header( "Content-Type: application/json" );
									echo $output;
									exit;
								}
								break;
								
							case 'link' :
							
								$new_link = $new_value;
								if ( 0 == strcasecmp( 'no link', $new_link ) || empty( $new_link ) ) {
									$new_link = 'No link';
								}
								$slides[3][$target] = rawurlencode( $new_link );
								
								update_option( 'cnhk_options_slides', $slides );
								
								$response = array(
									'status' 	=> true,
									'subject'	=> $subject,
									'target' 	=> $target,
									'new_value'	=> $new_link
								);
								$output = json_encode( $response, JSON_FORCE_OBJECT );	
								header( "Content-Type: application/json" );
								echo $output;
								exit;
									
								break;
								
							default:
							
								$response = array(
									'status' => false,
									'error' => "<h3>No data updated</h3><p>No matching subject.</p>"
								);
								$output = json_encode( $response, JSON_FORCE_OBJECT );	
								header( "Content-Type: application/json" );
								echo $output;
								exit;
							
								break;
								
						} /* End switch ( $subject ) */
					} else {
						$error = '<h3>Data error</h3><p>The plugin may need to be re-installed</p>';
						$output = json_encode( array( 'status' => false, 'error' => $error ) );			
						header( "Content-Type: application/json" );
						echo $output;
						exit;
					}
					break;
					
				default :
			}
		
		} else {
		
			$error = '<h3>Security error</h3><p>Try to reload this page and make sure to use the cancel button for canceling an action.</p>';
			$output = json_encode( array( 'status' => false, 'error' => $error, 'modal' => true ) );			
			header( "Content-Type: application/json" );
			echo $output;
			exit;
			
		}
	/*
	*	End of cnhk_ajax_resp()
	*/
	}
	
	add_action( 'wp_ajax_afterupload', 'cnhk_ajax_afterupload' );
	
	function cnhk_ajax_afterupload() {
	if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( 'Not enough permissions.' );
		}
		if ( isset( $_POST['nonce'] ) && $_POST['nonce'] == $_SESSION['cnhk']['ajaxnonce'] ) {
		
			$slides = get_option( 'cnhk_options_slides' );
			
			switch( $_POST['what'] ) {
				case 'slide' :
					$file = $_POST['file'];
					preg_match( "#slide(\d+)\.(jpg|png)#i", $file, $elem );
					$nb = $elem[1];
					$ext = '.' . $elem[2];
					
					array_push( $slides[0], "slide{$nb}{$ext}" );
					array_push( $slides[1], -1 );
					array_push( $slides[2], "slide{$nb}" );
					array_push( $slides[3], rawurlencode("No link") );
					array_push( $slides[4], time() );
					update_option( 'cnhk_options_slides', $slides );
					$new_index = count( $slides[0] ) -1;
					
					$snap_size = cnhk_get_snapsize( $new_index );
					$title = "slide$nb";
					$src = CNHK_URL . "/uploads/{$file}";
					$DT = gmdate( "d/m/Y - h:i A", $slides[4][$new_index] ) . ' GMT';
					
					$response = json_encode( array(
						'status'			=> true,
						'subject'			=> 'slide',
						'index'				=> $new_index,
						'src'				=> $src,
						'title'				=> $title,
						'udate'				=> esc_attr( $DT ),
						'width'				=> $snap_size['width'],
						'height'			=> $snap_size['height'],
						'wideclass'			=> $snap_size['class'],
						'key'				=> $_SESSION ['cnhk']['token_edit']
					), JSON_FORCE_OBJECT );
					
					header( "Content-Type: application/json" );
					echo $response;
					exit;
					
					break;
				default :
			}
		} else {
		
			$error = '<h3>Security error</h3><p>Try to reload this page and make sure to use the cancel button for canceling an action.</p>';
			$output = json_encode( array( 'status' => false, 'error' => $error, 'modal' => true ) );			
			header( "Content-Type: application/json" );
			echo $output;
			exit;
			
		}
	}
}
