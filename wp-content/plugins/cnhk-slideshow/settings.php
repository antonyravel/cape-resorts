<?php
if ( ! current_user_can ( 'manage_options' ) ) {
	wp_die ( 'You do not have sufficient permissions to access this page.' );
}
?>
<div class='wrap'><div id='icon-options-general' class='icon32'></div><h2>Settings</h2>
<?php

if ( isset( $_SESSION['cnhk']['upload_error_upload'] ) && '' != $_SESSION['cnhk']['upload_error_upload'] ) {
	echo "<div class='error'>An error occurs during upload!</div>";
}
if ( isset( $_SESSION['cnhk']['upload_error_extension'] ) && '' !=  $_SESSION['cnhk']['upload_error_extension'] ) {
	echo "<div class='error'>Invalid extension! .jpg (or .jpeg) and .png files only</div>";
}
if ( isset( $_SESSION['cnhk']['upload_error_size'] ) && '' != $_SESSION['cnhk']['upload_error_size'] ) {
	echo "<div class='error'>The file is too big!</div>";
}
$_SESSION['cnhk']['upload_error_upload'] = '';
$_SESSION['cnhk']['upload_error_extension'] = '';
$_SESSION['cnhk']['upload_error_size'] = '';

$setting = get_option( 'cnhk_options' );
/*
*	Check the validity of the plugin's options.
*/
if ( ! is_array( $setting ) || 11 != count( $setting ) ) {
	wp_die ( "<p class='error'>Data corrupted. Cnhk Slideshow must be reinstalled.</p>" );
}

$error_notice = '';
$success_notice = '';

if ( isset( $_POST['cnhk_token_new_ss'] ) && isset( $_SESSION['cnhk']['token_new_ss'] ) && $_POST['cnhk_token_new_ss'] == $_SESSION['cnhk']['token_new_ss'] )  {
	
	$name = trim( $_POST['name'] );
	$pattern = "#^[\p{L}0-9\.?\!_ -]{2,}$#iu";
	if ( preg_match( $pattern, $name ) == 1) {
		$sch = array( "#(\s){1,}#iu", "#[éêèë]#iu", "#[îï]#iu", "#[öô]#iu", "#[ùüû]#iu", "#[àâä]#iu", "#[ç]#iu", "#[^éêèëïîöôùüûàâäç0-9a-zA-Z_ -]#iu" );
		$rpl = array( "_", "e", "i", "o", "u", "a", "c", "-" );
		$name = preg_replace( $sch, $rpl, $name );
		
		$setting = get_option( 'cnhk_options' );

		if ( ! in_array( $name, $setting[0] ) ) {
		
			array_push( $setting[0], $name );
			array_push( $setting[1], 'Scroll' );
			array_push( $setting[2], 1000 );
			array_push( $setting[3], 4000 );
			array_push( $setting[4], 220 );
			array_push( $setting[5], 956 );
			array_push( $setting[6], true );
			array_push( $setting[7], false );
			array_push( $setting[8], null );
			array_push( $setting[9], null );
			array_push( $setting[10], false );
			
			update_option( 'cnhk_options', $setting );
			
			$ss = get_option( 'cnhk_options_ss' );
			array_push( $ss['ss'], $name );
			array_push( $ss['elem'], array() );
			update_option( 'cnhk_options_ss', $ss );
			
		} else {
			$error_notice .= "This name is already used for another slideshow. <strong>No data updated</strong><br />";
		}
		
	} else {
		if ( empty( $name ) ) {
			$error_notice .= "Slideshow's name must not be <strong>empty</strong><br />";
		} else {
			$error_notice .= "Slideshow's name can not contains any special characters. <strong>No data updated</strong><br />";
		}
	}
}

if ( isset( $_POST['cnhk_token_setting'] ) && ( $_SESSION['cnhk']['token_setting'] ) && $_POST['cnhk_token_setting'] == $_SESSION['cnhk']['token_setting'] ) {
	$index = intval( trim( $_GET['index'] ) );
	$effect_list = array( 'Scroll', 'Fade', 'None' );
	$effect = trim( $_POST['effect'] );
	$transition_speed = intval( trim( $_POST['transition_speed'] ) );
	$slide_duration = intval( trim( $_POST['slide_duration'] ) );
	$height = intval( trim( $_POST['height'] ) );
	$width = intval( trim( $_POST['width'] ) );
	
	$setting = get_option( 'cnhk_options' );
	
	if ( $index < count( $setting[0] ) ) {
		if ( ! in_array( $effect, $effect_list ) ) {
			$error_notice .= " - Choose a valid effect <br />";
		}
		if ( ! is_numeric( $_POST['transition_speed'] ) ) {
			$error_notice .= " - Transition speed must be an integer <br />";
		}
		if ( ! is_numeric( $_POST['slide_duration'] ) ) {
			$error_notice .= " - Slide duration must be an integer <br />";
		}
		if ( ! is_numeric( $_POST['height'] ) ) {
			$error_notice .= " - Height must be an integer <br />";
		}
		if ( ! is_numeric( $_POST['width'] ) ) {
			$error_notice .= " - Width must be an integer <br />";
		}
		
		$fixed = ( isset( $_POST['fixed'] ) && 1 == intval( trim( $_POST['fixed'] ) ) ) ? true : false;
		$fixed_width = null;
		$fixed_align = null;
		
		if ( isset( $_POST['fixed'] ) && 1 == intval( trim( $_POST['fixed'] ) ) ) {
			$fixed_align = trim( $_POST['fixed-align'] );
			if ( ! is_numeric( $_POST['fixed-width'] ) ) {
				$error_notice .= ' - Fixed width must be an integer.<br />';
			}
			$fixed_width = intval( trim( $_POST['fixed-width'] ) );
			$align_list = array( 'none', 'left', 'right', 'center', 'float-left', 'float-right' );			
			if ( ! in_array( $fixed_align, $align_list ) ) {
				$error_notice .= " - Choose a valid alignment <br />";
			}
		}
	
		if ( '' == $error_notice ) {
			$setting[1][$index] = $effect;
			$setting[2][$index] = $transition_speed;
			$setting[3][$index] = $slide_duration;
			$setting[4][$index] = $height;
			$setting[5][$index] = $width;
			$ckbox = ( isset( $_POST['nav'] ) && true == $_POST['nav'] ) ? true : false;
			$setting[6][$index] = $ckbox;
			$setting[7][$index] = $fixed;
			$setting[8][$index] = $fixed_width;
			$setting[9][$index] = $fixed_align;
			
			update_option( 'cnhk_options', $setting );
			$success_notice .= "<p>Data saved</p>";			
		} else {
			$error_notice .= "<strong><p>No data updated</p></strong>";	
		}
	
	} else {
		$error_notice .= "<strong><p>No data updated</p></strong>";
	}	
}

if ( isset( $_POST['token_del_ss'] ) && $_POST['token_del_ss'] == $_SESSION['cnhk']['token_del_ss'] ) {
	if ( 'delete' == $_GET['action'] ) {
		$instance = intval( $_POST['instance'] );
		$options = get_option( 'cnhk_options' );
		if ( $instance < count( $options[0] ) ) {
            $snapsize = cnhk_get_snapsize( $instance );
			echo "<h3>Slideshow: {$options[0][$instance]} </h3>";
				$slides = get_option( 'cnhk_options_slides' );
				$slide_list = array( 'name' => array(), 'title' => array(), 'link' => array(), 'date' => array() );
				for ( $i = 0; $i < count( $slides[0] ); $i++ ) {
					if ( $slides[1][$i] == $instance ) {
						array_push( $slide_list['name'], $slides[0][$i] );
						array_push( $slide_list['title'], $slides[2][$i] );
						array_push( $slide_list['link'], urldecode( $slides[3][$i] ) );
						array_push( $slide_list['date'], gmdate( "d/m/Y - h:i A", $slides[4][$i] ) . ' GMT' );
					}
				}
				echo "<table class='wp-list-table widefat'>";
				echo "<thead><th>Slides in '{$options[0][$instance]}'</th><th>Title</th><th>Link</th><th>Upload date</th></thead>";
				echo "<tbody>";
				if ( count( $slide_list['name'] ) > 0 ) {
					for ( $i = 0; $i < count( $slide_list['name'] ); $i++ ) {
						echo "<tr>";
						echo "<td><img src='" . CNHK_URL . "uploads/{$slide_list['name'][$i]}' alt='{$slide_list['title'][$i]}' width='{$snapsize['width']}' height='{$snapsize['height']}'/></td>";
						echo "<td>{$slide_list['title'][$i]}</td><td>{$slide_list['link'][$i]}</td><td>{$slide_list['date'][$i]}</td>";
						echo "</tr>";
					}			
				} else {
					echo "<tr><td colspan='4' align='center' syle='font-size: 1.2em; font-weight: bold;'><em>This slideshow does not contain any slides</em></td></tr>";
				}
				echo "</tbody>";
				echo "</table>";
			$disable = ( 0 == count( $options[0] ) ) ? "disabled='disbaled'" : '';
			
			$token_del_ss = md5( uniqid( rand (), true ) );
			$_SESSION['cnhk']['token_del_ss'] = $token_del_ss;
			
			echo "<form method='post' class='cnhk-form' action='" . CNHK_ADMIN_SETTINGS . "&action=update'>";
			echo "<input type='hidden' name='token_del_ss' value='$token_del_ss' />";
			echo "<input type='hidden' name='instance' value='$instance' /><br />";
			echo "<label><p>Are you sure to delete? <em>(All slides used in this slideshow will be set as unused)</em></p></label>";
			echo "<input type='submit' name='confirm' value='Delete' class='button-primary' $disable/>";
			echo "<a href='" . CNHK_ADMIN_SETTINGS . "&index={$instance}' class='button'>Cancel</a>";
			echo "</form>";
		}	
	} else {
		$instance = intval( $_POST['instance'] );
		$options = get_option( 'cnhk_options' );
		$slides = get_option( 'cnhk_options_slides' );
		$ss = get_option( 'cnhk_options_ss' );
		if ( $instance < count( $options[0] ) ) {
			for ( $i= 0; $i < count( $slides[0] ); $i++ ) {
				if ( $slides[1][$i] == $instance ) {
					$slides[1][$i] = -1;
				} elseif ( $slides[1][$i] > $instance ) {
					$slides[1][$i]--;
				}
			}
			$ss_index = array_search( $options[0][$instance], $ss['ss'] );
			if ( false === $ss_index ) {
				$error_notice .= "Instance of slideshow not found. No data updated.<br />";
			} else {
				array_splice( $options[0], $instance, 1 );
				array_splice( $options[1], $instance, 1 );
				array_splice( $options[2], $instance, 1 );
				array_splice( $options[3], $instance, 1 );
				array_splice( $options[4], $instance, 1 );
				array_splice( $options[5], $instance, 1 );
				array_splice( $options[6], $instance, 1 );
				array_splice( $options[7], $instance, 1 );
				array_splice( $options[8], $instance, 1 );
				array_splice( $options[9], $instance, 1 );
				array_splice( $options[10], $instance, 1 );
				array_splice( $ss['ss'], $ss_index, 1 );
				array_splice( $ss['elem'], $ss_index, 1 );
                
                $bgr = cnhk_get_background_image_path( $instance );
                
                if ( $bgr ) {
                    unlink( $bgr );
                }
                
				update_option( 'cnhk_options', $options );
				update_option( 'cnhk_options_slides', $slides );
				update_option( 'cnhk_options_ss', $ss );
				$success_notice .= 'Data updated<br />';
			}
		} else {
			$error_notice .= 'No data updated<br />';
		}
	}
}

if ( isset( $_POST['del_bgr'] ) ) {
	if ( isset( $_POST['token_del_bgr'] ) && $_POST['token_del_bgr'] == $_SESSION['cnhk']['token_del_bgr'] ) {
		$options = get_option( 'cnhk_options' );
		$path = cnhk_get_background_image_path( intval( $_POST['target'] ) );
		if ( unlink( $path ) ) {
			$options[10][intval( $_POST['target'] )] = false;
			update_option( 'cnhk_options', $options );
		}
	}
}

if ( '' != $error_notice ) {
	echo "<div class='error'>$error_notice</div>";
}
if ( '' != $success_notice ) {
	echo "<div class='updated'>$success_notice</div>";
}

if ( ! isset( $_POST['token_del_ss'] ) || ( isset( $_POST['token_del_ss'] ) && isset( $_GET['action'] ) && 'delete' != $_GET['action']) ) : ?>
<div class='cnhk-data' id='page_url'>
	<?php echo CNHK_ADMIN_SETTINGS; ?>
</div>
<form action="<?php echo CNHK_ADMIN_SETTINGS; ?>&action=new" method="post">
	<fieldset class='cnhk-form'>
		<legend>New slideshow</legend>
		<label for='cnhk_new_name'><strong>Name</strong> <em>(unique, case sensitive)</em>: </label>
		<input type='hidden' name='cnhk_token_new_ss' value='<?php
		$token_new_ss = md5( uniqid( rand (), true ) );
		$_SESSION['cnhk']['token_new_ss'] = $token_new_ss;
		echo $token_new_ss;
		?>'/>
		<input type='text' name='name' id='cnhk_new_name' style='font-size : 1.1em; width: 25%; font-weight: bold;' />
		<input type='submit' name='submit_new' class='button' value='Add slideshow' />
	</fieldset>
</form>
<div class='form-wrap'>
<?php
	$options = get_option( 'cnhk_options' );
	$value = array( 
		'name' 				=> null,
		'effect' 			=> null,
		'transition_speed' 	=> null,
		'slide_duration' 	=> null,
		'height' 			=> null,
		'width' 			=> null,
		'fixed' 			=> null,
		'fixed_width' 		=> null,
		'fixed_align'		=> null
	);
	$size = count( $options[0] );	
	$disabled = ( 0 == $size ) ?  " disabled='disabled'" : '';
	$index;
	if ( isset( $_GET['index'] ) ) {
		$index = intval( trim( $_GET['index'] ) );
	} else {
		$index = 0;
	}
	$disabled_fixed = ( 0 < $size && true == $options[7][$index] ) ?   '' : " disabled='disabled'";
	if ( 0 < $size) {
		$value = array(
			'name' 				=> $options[0][$index],
			'effect'			=> $options[1][$index],
			'transition_speed' 	=> $options[2][$index],
			'slide_duration' 	=> $options[3][$index],
			'height' 			=> $options[4][$index],
			'width' 			=> $options[5][$index],
			'nav'				=> $options[6][$index],
			'fixed'				=> $options[7][$index],
			'fixed_width'		=> $options[8][$index],
			'fixed_align'		=> $options[9][$index],
		);
	}
	
	$checked = '';
	if ( 0 < $size && true == $value['nav'] ) $checked = ' checked';
?>	
<form method="post" action="<?php echo CNHK_ADMIN_SETTINGS; ?>&action=delete">
	<?php
	$token_del_ss = md5( uniqid( rand (), true ) );
	$_SESSION['cnhk']['token_del_ss'] = $token_del_ss;
	?>
	<input type="hidden" name="token_del_ss" value="<?php echo $token_del_ss; ?>" />
	<fieldset class='cnhk-form'>
		<legend>Choose a slideshow to edit</legend>
		<?php if ( 0 == $size ) { ?>
			<label for='instance'>No slideshow found.</label>
		<?php } else { ?>
			<label for='instance'>Existing slideshows</label>
		<?php } ?>
		<select name='instance' id='instance' style='font-weight: bold; font-size: 1.2em; color: #666;'>
		<?php
		if ( 0 != $size ) {
			for( $i = 0; $i < $size; $i++ ) {
				if ( $i == $index ) {
					?><option value='<?php echo $i; ?>' selected='selected'><?php echo $setting[0][$i]; ?></option><?php
				} else {
					?><option value='<?php echo $i; ?>'><?php echo $setting[0][$i]; ?></option><?php
				}
			}
		}
		?>
		</select>
		<input type='submit' class='button-primary' name='del' value='Delete slideshow'<?php echo $disabled; ?> />
	</fieldset>
</form>
	<?php if ( 0 < $size ) { ?>
<?php
	/*
	*	If there is any slideshow, display the "upload background" form, and print the opening tag for the general setting form.
	*	Otherwise unvalidate the general setting form by outputing an <form> tag without method and action attributes.
	*/
?>
<form method='post' action='<?php echo CNHK_ADMIN_SETTINGS; ?>&action=upload&index=<?php echo $index; ?>' enctype="multipart/form-data"><?php /* The "upload background form" */ ?>
<?php
	$token_background = md5( uniqid( rand (), true ) );
	$_SESSION['cnhk']['token_background'] = $token_background;
	$options = get_option( 'cnhk_options' );
	$label = ( $options[10][$index] ) ? 'Change the background image' : 'Choose a file to upload';
?>
	<fieldset class="cnhk-form">
	<legend>Background image for <em>'<?php echo $setting[0][$index]; ?>'</em> :</legend>
		<input type="hidden" name="cnhk_token_background" value="<?php echo $token_background; ?>" />
		<input type="hidden" name="target" value="<?php echo $index; ?>" />
		<label for="background-upload"><strong><?php echo $label; ?></strong>: <em>( *.jpg, *.jpeg or *.png up to 2MB )</em></label>
		<input type="file" id="background-upload" name="background-upload" />
		<input type="submit" name="submit-background" value="Upload background" class="button-secondary" />
		<?php if ( $options[10][$index] ) { ?>
			<?php
				$bgr_size = cnhk_get_backgroundsize( $index );
				$bgr_width = $bgr_size['width'];
				$bgr_height = $bgr_size['height'];
				$bgr_class = $bgr_size['class'];
			?>
			<div class="background-container <?php echo $bgr_class; ?>">
				<div class="background-pattern <?php echo $bgr_class; ?>">
					<img src="<?php echo esc_attr( cnhk_background_image_url( $index ) ); ?>" alt="" width="<?php echo $bgr_width; ?>" height="<?php echo $bgr_height; ?>" />
				</div><!-- .background-pattern -->
			</div><!-- .background-container -->
			<?php
				$token_del_bgr = md5( uniqid( rand (), true ) );
				$_SESSION['cnhk']['token_del_bgr'] = $token_del_bgr;
			?>
			<input type="hidden" name="token_del_bgr" value="<?php echo $token_del_bgr; ?>" />
			<input type="submit" name="del_bgr" value="Remove background image" class="button-secondary" />
		<?php } ?>
	</fieldset>
</form>
<form method='post' action='<?php echo CNHK_ADMIN_SETTINGS; ?>&action=edit&index=<?php echo $index; ?>'>
	<?php } else { ?>
<form><?php /* Not a working form if there is no slideshow. In addition the submit button is disabled. */ ?>
	<?php } ?>
	<fieldset class='cnhk-form'>
		<?php if ( 0 != $size ) echo "<legend>Settings for <em>'{$setting[0][$index]}'</em> :</legend>";?>
		<div id='col-container'>		
			<div id='col-right'>
				<label for='cnhk-fixed'>Fluid / fixed width</label>
				<select name='fixed' id='cnhk-fixed'<?php echo $disabled; ?>>
					<?php
						$selected_fixed0 = ( is_array( $value ) && $value['fixed'] == true ) ? " selected='selected'" : '';
						$selected_fixed1 = ( '' != $selected_fixed0 ) ? " selected='selected'" : '';
					?>
					<option value='0'<?php echo $selected_fixed0; ?>>Fluid width</option>
					<option value='1'<?php echo $selected_fixed1; ?>>Fixed width</option>
				</select><p>Select fixed width to restrain the slideshow in a fixed width container</p>
				<label for='cnhk-fixed-width'>The fixed width</label>
				<input name='fixed-width' id='cnhk-fixed-width' type='text' value='<?php echo $value['fixed_width']; ?>' <?php echo $disabled_fixed; ?> /><em>(in pixels)</em>
				<p>Width of the container</p>
				<label for='cnhk-fixed-align'>Alignment</label>
				<select name='fixed-align' id='cnhk-fixed-align'<?php echo $disabled_fixed; ?>>
				<?php
					$align_list = array( 'none', 'left', 'right', 'center', 'float-left', 'float-right' );
					foreach ( $align_list as $elem ) {
						$select = ( $elem == $value['fixed_align'] ) ? " selected='selected'" : '';
						echo "<option value='$elem'$select>$elem</option>";
					}
				?>
				</select>
				<p>Select an alignment for the container</p>
			</div><!-- col-right -->
			
			<div id='col-left'>
				<?php
					$token_setting = md5( uniqid( rand (), true ) );
					$_SESSION['cnhk']['token_setting'] = $token_setting;
					$effect_list = array( 'Scroll', 'Fade', 'None' );
				?>
				<input type='hidden' name='cnhk_token_setting' value='<?php echo $token_setting; ?>' />
				<label for='effect'>Transition effect</label>
				<select name='effect' name='effect' id='effect' <?php echo $disabled; ?>>
				<?php
					foreach ( $effect_list as $elem	) {
						if ( 0 != $size && $value['effect'] == $elem ) {
							echo "<option value='$elem' selected='selected'>$elem</option>";
						} else {
							echo "<option value='$elem'>$elem</option>";		
						}
					}
				?>
				</select><br />
				<p>Visual effect used for switching to the next slide.</p>
				<label for='transition_speed'>Transition speed</label>
				<input type='text' name='transition_speed' id='transition_speed'<?php if ( 0 != $size ) echo " value='{$value['transition_speed']}'$disabled />"; ?><em>(in milliseconds)</em><br />
				
				<p>Visual effect duration when switching between two slides.</p>
				<label for='slide_duration'>Slide duration</label>
				<input type='text' name='slide_duration' id='slide_duration'<?php if ( 0 != $size ) echo " value='{$value['slide_duration']}'$disabled />"; ?><em>(in milliseconds)</em><br />
				
				<p>Display duration of each slide.</p>
				<label for='width'>Width</label>
				<input type='text' name='width' id='width'<?php if ( 0 != $size ) echo " value='{$value['width']}'$disabled />"; ?><em>(in pixels)</em><br />
				
				<p>Width used for the slideshow's ratio.</p>
				<label for='height'>Height</label>
				<input type='text' name='height' id='height'<?php if ( 0 != $size ) echo " value='{$value['height']}'$disabled />"; ?><em>(in pixels)</em><br />
				
				<p>Height used for the slideshow's ratio.</p>
				<span style='margin-left: 15px;'>Display navigation buttons?<input type='checkbox' name='nav' id='nav'<?php echo "$checked$disabled"; ?> /></span><br />
				<p>Check if you want to display navigation buttons for this slideshow.</p>
				<input type='submit' name='submit_setting' id='submit_setting' value='Save changes ?' class='button-primary'<?php echo $disabled; ?> /><br />
			</div><!-- col-left -->
		</div><!-- col-container -->
	</fieldset>
	</form>
<?php endif; /* $_POST is empty */ ?>
</div><!-- .form-wrap -->