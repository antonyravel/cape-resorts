<?php
if ( ! current_user_can( 'manage_options' ) ) {
	wp_die ( 'You do not have sufficient permissions to access this page.' );
}

$error_notice = '';
$success_notice = '';

if ( isset( $_POST['cnhk_token_ss'] ) && isset( $_SESSION['cnhk']['token_ss'] ) && $_POST['cnhk_token_ss'] == $_SESSION['cnhk']['token_ss'] ) {
	$pattern = "#cnhkadminslide\[\]=([0-9]){1,}#i";
	$result = '';
	
	if ( ! isset( $_POST['index'] ) ) {
		$error_notice = "<p>No data updated.</p>";
	} else {
	
		$index = intval( trim( $_POST['index'] ) );
		
		preg_match_all( $pattern, trim( $_POST['cnhk-order'] ), $result, PREG_PATTERN_ORDER );
		if ( is_array( $result ) ) {
			$elems = array_map( 'numerize', $result[1] );
			$options = get_option( 'cnhk_options' );
			$ss = get_option( 'cnhk_options_ss' );
			$subject = $options[0][$index];
			$ss_index = array_search( $subject, $ss['ss'] );
			$ss['elem'][$ss_index] = $elems;
			update_option( 'cnhk_options_ss', $ss );
			echo "<div class='updated'><p>Data updated</p></div>";
		}else{
			echo "<div class='error'><p>No data updated!</p></div>";
		}
	}
}

function numerize( $nbr ) {
	return intval( $nbr );
}

$general_options = get_option( 'cnhk_options' );
$has_slideshow = ( 0 < count( $general_options[0] ) ) ? true : false;

$cnhk_token_ss = md5( uniqid( rand(), true) );
$_SESSION['cnhk']['token_ss'] = $cnhk_token_ss;
?>
<div class="wrap">
	<?php
		$options = get_option( 'cnhk_options' );
		$index = ( isset( $_GET['index'] ) ) ? intval( trim( $_GET['index'] ) ) : 0;
		
		if ( $has_slideshow && $options[10][$index] ) {
			?>
			<div class="updated"><p style="font-weight: bold;">This sideshow has a background image.</p></div>
			<?php
		}
	?>
	<div id="icon-tools" class="icon32">
	</div>
	<h2>Slideshow</h2>
<?php
if ( '' != $error_notice ) {
	echo "<div class='error'>$error_notice</div>";
}
if ( '' != $success_notice ) {
	echo "<div class='updated'>$success_notice</div>";
}
?>
	<div class='cnhk-data' id='page_url'><?php echo CNHK_ADMIN_MAIN; ?></div>
	<p style="font-size: 0.9em;"><em>Just drag and drop snapshots to put or rearrange slides within the slideshow.</em></p>
	<form method="post" action="<?php echo CNHK_ADMIN_MAIN; if ( isset( $_GET['index'] ) ) echo "&index=" . $_GET['index']; ?>" id="cnhk_ss_form">
		<fieldset class='cnhk-form'>
		<legend>Select a slideshow to edit</legend>
<?php
$options = get_option( 'cnhk_options' );
if ( 0 == count( $options[0] ) ) {
	echo "<label for='cnhk_select_ss'>Available slideshows</label><br />";
}
$disabled = ( ! $has_slideshow ) ? " disabled='disabled'" : '';
echo "<select name='index' id='cnhk_select_ss' style='font-size: 1.1em; margin: 5px 15px 15px 15px;'$disabled>";

$options = get_option( 'cnhk_options' );
for ( $i = 0; $i < count( $options[0] ); $i++ ) {
	$selected = ( $i == $index ) ? " selected='selected'" : '';
	echo "<option value='$i'$selected>{$options[0][$i]}</option>";
}
echo "</select>";
echo "<input type='submit' value='Save changes' class='button-primary'$disabled />";
if ( $has_slideshow ) {
	$target = ( ! isset( $_GET['index'] ) ) ? '' : '&index=' . $_GET['index'];
	echo "<a href='" . CNHK_ADMIN_MAIN . $target . "' class='button'>Cancel</a>";
} else {
	echo "<strong><em>You need to create slideshow first.</em></strong>";
}
echo "<input type='hidden' name='cnhk_token_ss' value='" . $_SESSION['cnhk']['token_ss'] ."' />";
echo "<input type='hidden' name='cnhk-order' id='cnhk-order' value='' />";
echo "</fieldset><br /></form><br />";
if ( 0 < count( $options[0] ) ) {
 ?>
	<table class="wp-list-table widefat">
		<thead>
			<tr><th>Available Slides for <?php
			$options = get_option( 'cnhk_options' );
			$index = ( isset( $_GET['index'] ) ) ? intval( trim( $_GET['index'] ) ) : 0;
			echo "'" . $options[0][$index] . "'";
			?></th><th>Slides order <em>(From Top to Bottom)</em></th></tr>
		</thead>
		<tbody>
<?php
$index = ( isset( $_GET['index'] ) ) ? intval( trim( $_GET['index'] ) ) : 0;
$opt 	= get_option( 'cnhk_options' );
$opt1 	= get_option( 'cnhk_options_slides' );
$opt2 	= get_option( 'cnhk_options_ss' );
echo "<tr><td id='cnhk-left-col'><ul class='cnhk-left-list'>";
if ( 0 < count( $opt[0] ) ) {
	$free_slide_list = array();
	$used_slide_list = array();
	$subject = $opt[0][$index];
	$ss_index = array_search( $subject, $opt2['ss'] );
	
	for( $i = 0; $i < count( $opt1[0] ); $i++ ) {
		if ( $opt1[1][$i] == $index ) {
			if ( ! in_array( $i, $opt2['elem'][$ss_index] ) ) {
				array_push( $free_slide_list, $i );
			}
		}
	}
	
	$used_slide_list = $opt2['elem'][$ss_index];
	
	$lngLeft = count( $free_slide_list );
	$lngRight = count( $used_slide_list );
	
	$h 		= $opt[4][$index];
	$w 		= $opt[5][$index];
	$ratio 	= $h/$w;
	$width 	= 400;
	$height = round( $width*$ratio );
	
	$wideclass = ( 1 > $ratio ) ? ' wide-400' : ' wide-200';
	
	if ( 1 < $ratio ) {
		$width = 200;
		$height = round( $width * $ratio );
	}
	
	$slide_class = ( $opt[10][$index] ) ? ' class="with-background"' : '';
	$container_height = ( $opt[10][$index] ) ? " style='height: {$height}px'" : '';
	
	for ( $i = 0; $i < $lngLeft; $i++ ) {
		echo "<li id='cnhkadminslide-{$free_slide_list[$i]}' class='cnhk-left-item'><div class='background-pattern $wideclass'$container_height>";
		if ( $opt[10][$index] ) {
			?><img src="<?php echo esc_url( cnhk_background_image_url( $index ) ); ?>" alt="" width="<?php echo $width; ?>" height="<?php echo $height; ?>" /><?php
		}
		echo "<img src='" . CNHK_URL . "uploads/" . $opt1[0][$free_slide_list[$i]] . "' width='$width' height='$height'$slide_class /></div></li>";
	}
	echo "</ul></td><td id='cnhk-right-col' class='alternate'><ul class='cnhk-right-list'>";

	for ( $i = 0; $i < $lngRight; $i++ ) {
		echo "<li id='cnhkadminslide-{$used_slide_list[$i]}' class='cnhk-right-item'><div class='background-pattern $wideclass'$container_height>";
		if ( $opt[10][$index] ) {
			?><img src="<?php echo esc_url( cnhk_background_image_url( $index ) ) ?>" alt="" width="<?php echo $width; ?>" height="<?php echo $height; ?>" /><?php
		}
		echo "<img src='" . CNHK_URL . "uploads/" . $opt1[0][$used_slide_list[$i]] . "' width='$width' height='$height'$slide_class /></div></li>";
	}

	echo "</ul></td></tr>";
}
?>
		</tbody>
	</table>
<?php 
}
?>
</div>
