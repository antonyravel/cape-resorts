<?php

/*
Plugin Name: Cnhk Slideshow
Description: Cnhk Slideshow is a simple jQuery slideshow plugin with a drag and drop system for editing slides order. Images files are stored in the plugin directory ( Not mixed with WordPress's Media Library ).
Version: 1.1.1
Author: Rija Rajaonah
License: GLPv2 or later
*/

if ( '' == session_id() ) {
	session_start();
}

$cnhk_token_snap = md5( uniqid( rand (), true ) );
$_SESSION['cnhk']['token_snap'] = $cnhk_token_snap;

register_activation_hook( __FILE__, 'cnhk_install' );
register_deactivation_hook( __FILE__, 'cnhk_uninstall' );

define( 'CNHK_DIR', dirname( realpath( __FILE__ ) ) );
define( 'CNHK_ADMIN_SLIDE', admin_url('admin.php?page=cnhk-slide') );
define( 'CNHK_ADMIN_MAIN', admin_url('admin.php?page=cnhk_mainpage') );
define( 'CNHK_ADMIN_SETTINGS', admin_url('admin.php?page=cnhk-settings') );
define( 'CNHK_URL', plugin_dir_url( __FILE__ ) );

add_action( 'admin_menu', 'cnhk_admin_menu' );
add_action( 'admin_init', 'cnhk_init' );
add_action( 'admin_enqueue_scripts', 'cnhk_admin_script' );
add_action( 'wp_enqueue_scripts', 'cnhk_front_script' );
add_action( 'widgets_init', 'cnhk_widget' );
add_filter( 'plugin_action_links', 'cnhk_add_link', 10, 2 );

/**
*	Add setting link in the plugin list
*/

include_once( CNHK_DIR . DIRECTORY_SEPARATOR . 'ajax.php' );

function cnhk_add_link( $lnk, $file ) {
	static $cnhk_this_plugin;
	if ( ! isset( $cnhk_this_plugin ) ) $cnhk_this_plugin = plugin_basename( __FILE__ );
	if ( $file == $cnhk_this_plugin ) {
		$settings_link = '<a href="admin.php?page=cnhk-settings">Settings</a>';
		array_unshift($lnk, $settings_link);
	}
	return $lnk;
}

/**
*	Avoid re-sending form after refreshing a page
*/

function cnhk_redirect() {
    if ( isset( $_POST['del_bgr'] ) || ! empty( $_POST['token_del_ss'] ) || ! empty( $_POST['cnhk_token_setting'] ) || ! empty( $_POST['cnhk_token_new_ss'] ) || ! empty( $_POST['cnhk_token_update'] ) || ! empty( $_POST['cnhk_token_delete'] ) ||  ! empty( $_POST['cnhk_token_edit'] ) || ! empty( $_POST['cnhk_token_ss'] ) ) {
		$_SESSION['cnhk']['savepost'] = $_POST;
		
		$running = $_SERVER['PHP_SELF'];
		if ( ! empty( $_SERVER['QUERY_STRING'] ) )
		{
			$running .= '?' . $_SERVER['QUERY_STRING'];
		}
		wp_redirect( $running );
		die();
	}
	
	if ( isset( $_POST['cnhk_token_file'] ) && trim( $_POST['cnhk_token_file'] ) == $_SESSION['cnhk']['token_file'] ) {
		$referer = admin_url ('admin.php?page=cnhk-slide');
		if ( isset ( $_SERVER['HTTP_REFERER'] ) && $_SERVER['HTTP_REFERER'] == $referer ) {
			$has_error = false;
			if ( $_FILES['cnhk_file']['error'] > 0 ) {
				$_SESSION['cnhk']['upload_error_upload'] = 'err';
				$has_error = true;
			}
			
			$valid_extensions = array( 'image/jpeg', 'image/png' );
			
			if ( ! in_array( $_FILES['cnhk_file']['type'], $valid_extensions ) ) {
				$_SESSION['cnhk']['upload_error_extension'] = 'err';
				$has_error = true;
			}
			
			if ( 2*1024*1024 < $_FILES['cnhk_file']['size'] ) {
				$_SESSION['cnhk']['upload_error_size'] = 'err';
				$has_error = true;
			}
			
			if ( ! $has_error ) {
				$nb = get_file_number();
				$ext_list = array( 'image/jpeg' => '.jpg', 'image/png' => '.png' );
				$res = CNHK_DIR . DIRECTORY_SEPARATOR .'uploads'. DIRECTORY_SEPARATOR .'slide' . $nb . $ext_list[$_FILES['cnhk_file']['type']];
				if ( move_uploaded_file( $_FILES['cnhk_file']['tmp_name'], $res ) ) {
					$_SESSION['cnhk']['upload_error_upload'] = '';
					$_SESSION['cnhk']['upload_error_extension'] = '';
					$_SESSION['cnhk']['upload_error_size'] = '';
					$opt = get_option( 'cnhk_options_slides' );
					array_push( $opt[0], "slide{$nb}" . $ext_list[$_FILES['cnhk_file']['type']] );
					array_push( $opt[1], -1 );
					array_push( $opt[2], "slide{$nb}" );
					array_push( $opt[3], rawurlencode("No link") );
					array_push( $opt[4], time() );
					update_option( 'cnhk_options_slides', $opt );
				}
			}
			
			$running = $_SERVER['PHP_SELF'];
			if ( ! empty( $_SERVER['QUERY_STRING'] ) )
			{
				$running .= '?' . $_SERVER['QUERY_STRING'];
			}
			wp_redirect( $running );
			die();		
		}
	}
	
	if ( isset( $_POST['cnhk_token_background'] ) && $_POST['cnhk_token_background'] == $_SESSION['cnhk']['token_background'] ) {
		$referer = admin_url( 'admin.php?page=cnhk-settings' );
		if ( isset( $_SERVER['HTTP_REFERER'] ) && false !== stripos( $_SERVER['HTTP_REFERER'], $referer ) ) {
			$has_error = false;
			if ( $_FILES['background-upload']['error'] > 0 ) {
				$_SESSION['cnhk']['upload_error_upload'] = 'err';
				$has_error = true;
			}
				
			$valid_types = array( 'image/jpeg', 'image/png' );
			$ext_list = array( 'image/jpeg' => '.jpg', 'image/png' => '.png' );
			$extension = '.xxx';
			
			if ( ! in_array( $_FILES['background-upload']['type'], $valid_types ) ) {
				$_SESSION['cnhk']['upload_error_extension'] = 'err';
				$has_error = true;
			} else {
				$extension = $ext_list[$_FILES['background-upload']['type']];
			}
			
			if ( 2*1024*1024 < $_FILES['background-upload']['size'] ) {
				$_SESSION['cnhk']['upload_error_size'] = 'err';
				$has_error = true;
			}
			
			$destination = CNHK_DIR . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'bgr' . DIRECTORY_SEPARATOR;
			$target = intval( trim( $_POST['target'] ) );
			if ( ! $has_error ) {
				unlink( $destination . "bgr-$target" . '.jpg' );
				unlink( $destination . "bgr-$target" . '.jpeg' );
				unlink( $destination . "bgr-$target" . '.png' );
				if ( move_uploaded_file( $_FILES['background-upload']['tmp_name'], $destination . "bgr-$target" . $extension ) ) {
					
					$_SESSION['cnhk']['upload_error_upload'] = '';
					$_SESSION['cnhk']['upload_error_extension'] = '';
					$_SESSION['cnhk']['upload_error_size'] = '';
					$opt = get_option( 'cnhk_options' );
					$opt[10][$target] = true;
					update_option( 'cnhk_options', $opt );
					
				}
			}
			
			$running = $_SERVER['PHP_SELF'];
			if ( ! empty( $_SERVER['QUERY_STRING'] ) )
			{
				$running .= '?' . $_SERVER['QUERY_STRING'];
			}
			wp_redirect( $running );
			die();	
		}
	}
	
	if ( isset( $_SESSION['cnhk']['savepost'] ) )
	{
		$_POST = $_SESSION['cnhk']['savepost'];		
		unset( $_SESSION['cnhk']['savepost']);
	}
}

function get_file_number() {
	$i = 1;
	$slide_path = CNHK_DIR . DIRECTORY_SEPARATOR . "uploads". DIRECTORY_SEPARATOR;
	while ( file_exists( $slide_path . "slide{$i}.jpg" ) || file_exists( $slide_path . "slide{$i}.png" ) ) {
		$i++;
	}
	return $i;
}

/**
*	Contextual Help
*/

function cnhk_admin_help() {
	
	$screen = get_current_screen();
	global $cnhk_main_page;
	global $cnhk_settings_page;
	global $cnhk_slides_page;
	
	if ( $cnhk_main_page == $screen->id ) {
		$help = "<h4>Slideshow :</h4>";
		$help .= "Just drag and drop snapshots to put or reoder slides within the slideshow. ";
		$help .= "You can also drag a slide out of the slideshow, without permanently deleting it.<br />";
		$help .= "<strong><em>Do not forget to select the right slideshow.</em></strong>";
		
		$screen->add_help_tab( array(
			'id'		=> 'Overview',
			'title'		=> 'Overview',
			'content'	=> $help
		) );
		
	}
	if ( $cnhk_settings_page == $screen->id ) {
		$new = "<h4>Adding new slideshow</h4>";
		$new .= "You can create multiples slideshow but each slideshow name must be unique(case sensitive).<br /> When you delete a slideshow, all slides used in it are set to 'Unused'.";
		$select = "<h4>Selecting slideshow</h4>";
		$select .= "This dropdown list contains all existing slideshow. It allows you to select a slideshow to edit (or delete). When you delete a slideshow, all slides used in it will be set as unused.<br />";
		$settings = "<h4>Settings</h4>";
		$settings .= "<ul>";
		$settings .= "<li>By default, all slideshow has a fluid width. It means that the slideshow will take all available width and is resized each time the page is resized (even partially) ";
		$settings .= "in accordance with the <em>width/height 's ratio</em> that you defined in this screen. ";
		$settings .= "In other terms, all images that you use in this slideshow will be stretched to fit this ratio.</li>";
		$settings .= "<li>If you choose the fix width, the slideshow will be wrapped inside a container with the fixed with that you defined. ";
		$settings .= "You can also choose an alignment for this container to make the slideshow look better when inserted in a post for example.</li>";
		$settings .= "</ul>";
		$display = "<h4>Displaying slideshow</h4>";
		$display .= "You can display slideshows by:";
		$display .= "<ul>";
		$display .= "<li><h5>Using widget</h5>";
		$display .= "Go to the widget menu. Add a <em>cnhk slideshow</em> widget to a sidebar. Then select one of the slideshow you've created and save changes.</li>";
		$display .= "<li><h5>Using a shortcode</h5>";
		$display .= "In your post you can use a shortcode like <code>[cnhk_slideshow name='my_slideshow' width='300' align='float-left']</code> where :<ul>";
		$display .= "<li><strong><em>my_slideshow</em></strong> is the <em>name parameter</em> that you defined on the settings page.</li>";
		$display .= "<li><strong><em>300</em></strong> is the fixed width in pixels of the container (if you want it to be a fixed width slideshow).";
		$display .= " If set, this value will override the one you've entered in the settings page for this slideshow. Then a fluid width slideshow will have locally a fixed width</li>";
		$display .= "<li><strong><em>align-left</em></strong> is the alignment of the container (only for fixed width). If set, this value will override the one you've entered in the settings page. ";
		$display .= "<br />Possible values : <strong>'left', 'right', 'center', 'float-left', 'float-right'.</strong></li></ul><strong><em>Only the name parameter is required.</em></strong></li>";
		$display .= "<li><h5>Using template tag</h5>";
		$display .= "Directly in your theme's files with <code>&lt;?php if ( function_exists( 'cnhk_slideshow' ) ) cnhk_slideshow( 'my_slideshow', 300, 'align-left' ); ?&gt;</code><br />";
		$display .= "Meanings of all parameters are the same as for shortcode and <strong><em>only the name parameter is required.</em></strong> Pay attention on the width parameter which is passed ";
		$display .= "as an integer to the function (no wrapping quotes).</li></ul>";
		$bgr = "<h4>Optional Background</h4>";
		$bgr .= "You can add a background image for each slideshow. This image will fit the ratio defined in this page and will be visible through transparent areas of '.png' slides";
		
		
		$screen->add_help_tab( array(
			'id'		=> 'Overview',
			'title'		=> 'Overview',
			'content'	=> $settings
		) );
		
		$screen->add_help_tab( array(
			'id'		=> 'Creating',
			'title'		=> 'Creating Slideshow',
			'content'	=> $new
		) );
		
		$screen->add_help_tab( array(
			'id'		=> 'Select',
			'title'		=> 'Selecting Slideshow',
			'content'	=> $select
		) );
		
		$screen->add_help_tab( array(
			'id'		=> 'Display',
			'title'		=> 'Displaying Slideshow',
			'content'	=> $display
		) );
		$screen->add_help_tab( array(
			'id'		=> 'Background',
			'title'		=> 'Optional Background',
			'content'	=> $bgr
		) );
	}
	if ( $cnhk_slides_page == $screen->id ) {
		$upload = "<h4>Uploading new slide:</h4>";
		$upload .= "All uploaded images stay inside the plugin's directory, and will not mix with images in the WordPress Media Library.<br />";
		$delete = "<h4>Deleting slide:</h4>";
		$delete .= "This will delete the image file (from the plugin's directory) and the corresponding data in WordPress's database.<br />";
		$delete .= "If you mean to reuse the slide, just drop it out of the slideshow in the <a href='" . CNHK_ADMIN_MAIN . "'><em>\" slideshow \"</em></a> screen instead of permanently deleting it.<br />";
		$help = "<h4>Overview :</h4>";
		$help .= "You can edit each slide's title, link and usage in this screen. You can use each slide in only one slideshow.<br />";
		$help .= "You can click on values listed below to edit them or click on the 'Edit' link.";
		
		$screen->add_help_tab( array(
			'id'		=> 'Overview',
			'title'		=> 'Overview',
			'content'	=> $help
		) );
		
		$screen->add_help_tab( array(
			'id'		=> 'Upload',
			'title'		=> 'Uploading new file',
			'content'	=> $upload
		) );
		
		$screen->add_help_tab( array(
			'id'		=> 'Delete',
			'title'		=> 'Deleting slide',
			'content'	=> $delete
		) );
	}
}

/**
*	Add stylesheet and scripts for the front end
*/

function cnhk_front_script() {
	wp_register_style( 'cnhk_front_css', plugins_url( 'css/cnhk-front.css', __FILE__ ) );
	wp_enqueue_style( 'cnhk_front_css' );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'jquery-cycle', plugins_url( 'js/jquery.cycle.all.js', __FILE__ ), array( 'jquery' ) );
	wp_enqueue_script( 'cnhk_front_js', plugins_url( 'js/cnhk-front.js', __FILE__ ), array( 'jquery-cycle' ) );
}

/**
*	Add stylesheets and scripts for admin pages
*/

function cnhk_admin_script() {
	global $pagenow;
	if ( 'admin.php' == $pagenow ) {
	$page_list = array( 'cnhk_mainpage', 'cnhk-slide', 'cnhk-settings' );
		if ( isset( $_GET['page'] ) && in_array( $_GET['page'], $page_list )  ) {
			wp_register_style( 'cnhk-slideshow_css', plugins_url( 'css/cnhk-ss.css', __FILE__ ) );
			wp_register_style( 'cnhk-uploadify_css', plugins_url( 'uploadify/uploadify.css', __FILE__ ) );
			wp_enqueue_style( 'cnhk-slideshow_css' );
			wp_enqueue_style( 'cnhk-uploadify_css' );
			wp_enqueue_script( 'jquery' );
			wp_enqueue_script( 'jquery-ui-core' );
			wp_enqueue_script( 'jquery-ui-widget' );
			wp_enqueue_script( 'jquery-ui-mouse' );
			wp_enqueue_script( 'jquery-ui-sortable' );
			wp_enqueue_script( 'cnhk-uploadify', plugins_url( 'uploadify/jquery.uploadify-3.1.min.js', __FILE__ ), array( 'jquery' ) );
			wp_enqueue_script( 'cnhk_admin_js', plugins_url( 'js/cnhk-admin.js', __FILE__ ), array(  'jquery', 'jquery-ui-sortable' ) );
			wp_enqueue_script( 'cnhk-ajax', plugins_url( 'js/ajax.js', __FILE__ ), array( 'jquery' ) );
		}
	}
}

/**
*	Makes some variables accessible by javascript
*/
function cnhk_admin_head_callback() {
	if ( isset( $_GET['page'] ) ) {
		$_SESSION['cnhk']['ajaxnonce'] = md5( uniqid( rand (), true ) );
		global $pagenow;
		$root = preg_replace( "#[\\/]#", DIRECTORY_SEPARATOR, $_SERVER['DOCUMENT_ROOT'] );
		$cnhk_url = CNHK_URL . '/uploads';
		?>
			<script type="text/javascript">
				/* <![CDATA[ */
					var cnhk_ajax_settings = {
						url	:"<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>",
						sid : "<?php echo session_id(); ?>",
						imgurl :"<?php echo esc_url( CNHK_URL . 'images' ); ?>",
						cnhk_dir :"<?php echo rawurlencode( CNHK_DIR ); ?>",
						cnhk_url :"<?php echo CNHK_URL; ?>",
						admin_slides_url : "<?php echo CNHK_ADMIN_SLIDE; ?>",
						nonce :'<?php echo $_SESSION['cnhk']['ajaxnonce']; ?>',
						pagenow :'<?php echo $pagenow; ?>',
						page :'<?php echo $_GET['page']; ?>',
						pending :false
					}
				/* ]]> */
			</script>
		<?php
	}
}

/**
*	Initialisation
*/
function cnhk_init() {
	cnhk_redirect();
}

/**
*	Activation
*/
function cnhk_install() {
	if ( ! get_option( 'cnhk_options' ) || ! get_option( 'cnhk_options_slides' ) || ! get_option( 'cnhk_options_ss' ) ) {
		cnhk_default();
	}
}

/**
*	Deactivation
*/
function cnhk_uninstall() {
	unregister_widget( 'Cnhk_Ss_Widget' );
}

/**
*	Default values for options
*/
function cnhk_default(){
	
	$name				= array();
	$effect				= array();
	$transition_speed	= array();
	$slide_duration		= array();
	$height				= array();
	$width				= array();
	$nav				= array();
	$fixed				= array();
	$fixed_width		= array();
	$fixed_align		= array();
	$background			= array();
	
	$image 	= array();
	$use	= array();
	$title 	= array();
	$link 	= array();
	$date 	= array();
	
	$arr = array( $name, $effect, $transition_speed, $slide_duration, $height, $width, $nav, $fixed, $fixed_width, $fixed_align, $background );
	$arr2 	= array( $image, $use, $title, $link, $date );
	$ss 	= array( 'ss' => array(), 'elem' => array() );
	
	update_option( 'cnhk_options', $arr );
	update_option( 'cnhk_options_slides', $arr2 );
	update_option( 'cnhk_options_ss', $ss );
}

/**
*	Creates the admin menu
*/
function cnhk_admin_menu() {
	
	global $cnhk_main_page;
	global $cnhk_settings_page;
	global $cnhk_slides_page;
	
	$cnhk_main_page = add_menu_page( 'Slideshow', 'Slideshow', 'manage_options', 'cnhk_mainpage', 'cnhk_main_menu_fn', '', 14 );
	$cnhk_settings_page = add_submenu_page( 'cnhk_mainpage', 'Settings', 'Settings', 'manage_options', 'cnhk-settings', 'cnhk_setting_fn' );
	$cnhk_slides_page = add_submenu_page( 'cnhk_mainpage', 'Slides', 'Slides', 'manage_options', 'cnhk-slide', 'cnhk_slide_fn' );
	
	add_action( 'admin_head-' . $cnhk_slides_page, 'cnhk_admin_head_callback' );
	add_action( 'admin_head-' . $cnhk_settings_page, 'cnhk_admin_head_callback' );
	add_action( 'admin_head-' . $cnhk_main_page, 'cnhk_admin_head_callback' );
	
	add_action( 'load-' . $cnhk_slides_page, 'cnhk_admin_help' );
	add_action( 'load-' . $cnhk_settings_page, 'cnhk_admin_help' );
	add_action( 'load-' . $cnhk_main_page, 'cnhk_admin_help' );

}

/**
*	Display "Slideshow" page in admin panel
*/
function cnhk_main_menu_fn() {
	include_once( 'cnhk-slideshow.php' );
}

/**
*	Display "Slides" page in admin panel
*/
function cnhk_slide_fn() {
	if ( ! current_user_can( 'manage_options' ) ) {
		wp_die( 'You do not have sufficient permissions to access this page.' );
	}
	include_once( 'cnhk-slides.php' );
}

/**
*	Display slideshow in the front end
*/
function cnhk_slideshow( $instance, $width = null, $align = null ) {
	$index;
	$fixed_width = $width;
	$fixed_align = $align;
	$ss = get_option( 'cnhk_options_ss' );
	$setting = get_option( 'cnhk_options' );
	if ( count( $ss['ss'] ) > 0 ) {
		if ( is_string( $instance ) ) {
			$index = array_search( $instance, $ss['ss'] );
		} else {
				$index = false;
		}
	
		if ( is_int( $index ) && $setting[7][$index] ) {
			if ( ! is_int( $fixed_width ) ) {
				$fixed_width = $setting[8][$index];
			}
			if ( ! is_string( $fixed_align ) ) {
				$fixed_align = $setting[9][$index];
			}
		}
		
		include ( 'cnhk-front.php' );
	}
}

/**
*	Display "Settings" page
*/
function cnhk_setting_fn() {
	if ( ! current_user_can( 'manage_options' ) ) {
		wp_die( 'You do not have sufficient permissions to access this page.' );
	}
	require_once( 'settings.php' );
}

/**
*	Creates widget
*/
function cnhk_widget() {
	register_widget( 'Cnhk_Ss_Widget' );
}

class Cnhk_Ss_Widget extends WP_Widget {

	function Cnhk_Ss_Widget() {
		$widget_ops = array( 'classname' => 'cnhk-ss-widget', 'description'	=> 'Use this widget to add one slideshow to one of your sidebar.'	);		
		$control_ops = array( 'id_base' => 'cnhk-ss-widget' );		
		$this->WP_Widget( 'cnhk-ss-widget', 'Cnhk Slideshow', $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );
		$title 		= apply_filters( 'widget_title', $instance['title'] );
		$slideshow 	= $instance['slideshow'];
		
		echo $before_widget;
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		
		if ( '' != $slideshow ) {
			cnhk_slideshow( $slideshow );
		}
		echo $after_widget;
	}
	
	function form( $instance ) {
		$defaults = array( 'title' => '', 'slideshow' => '' );		
		$instance = wp_parse_args( ( array ) $instance, $defaults );
		$opt = get_option( 'cnhk_options' );
		if ( 0 < count( $opt[0] ) ) {
			echo '<p><label for="' . $this->get_field_id( 'title' ) . '">Title</label>
			<input type="text" class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" value="' . $instance['title'] . '" /></p>';
			
			echo "<p><label for='" . $this->get_field_id( 'slideshow' ) . "'>Slideshow to display</label></p>";
			echo "<select name='" . $this->get_field_name( 'slideshow' ) . "' id='" .$this->get_field_id( 'slideshow' ) . "'>";
				echo "<option value=''"; 
				if ( !in_array( $instance['slideshow'], $opt[0] ) ) echo " selected='selected'";
				echo"></option>";
				for ( $i = 0; $i < count( $opt[0] ); $i++ ) {
					$selected = ( '' != $instance['slideshow'] && $instance['slideshow'] == $opt[0][$i] ) ? " selected='selected'" : '';
					echo "<option value='{$opt[0][$i]}'$selected>" . $opt[0][$i] . "</option>";
				}
			echo "</select>";
		} else {
			echo "<p class='error'>You need to create at least one slideshow. <a href='" . CNHK_ADMIN_SETTINGS . "'>Create one ?</a></p>";
		}
		
	}
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] 		= strip_tags( $new_instance['title'] );
		$instance['slideshow'] 	= $new_instance['slideshow'];
		return $instance;
	}
	
}

/**
*	Add Shortcode
*/
function cnhk_shortcode_fn( $atts, $content = null ) {
	extract( shortcode_atts( array( 'name' => '', 'width' => null, 'align' => null ), $atts ) );
	if ( ! empty( $width ) ) $width = intval( $width );
	if ( ! empty( $content ) ) {
		ob_start();
		cnhk_slideshow( $content, $width, $align );
		$output = ob_get_clean();
		return $output;
	} else {
		ob_start();
		cnhk_slideshow( $name, $width, $align );
		$output = ob_get_clean();
		return $output;
	}
}

add_shortcode( 'cnhk_slideshow', 'cnhk_shortcode_fn' );

/**
*	Utility functions
*/
function cnhk_get_snapsize( $slide_index ) {
	$slides = get_option( 'cnhk_options_slides' );
	$settings = get_option( 'cnhk_options' );
	
	$usage = $slides[1][$slide_index];
	$setting_height;
	$setting_width;
	
	if ( -1 != $usage ) {
		$setting_height = intval( $settings[4][$usage] );
		$setting_width = intval( $settings[5][$usage] );
	} else {
		list( $setting_width, $setting_height ) = getimagesize( CNHK_DIR . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . $slides[0][$slide_index] );
	}
	$snap_width;
	$snap_height;	
	
	$setting_ratio = $setting_width / $setting_height;

	if ( $setting_height < $setting_width ) {
		$snap_width = 300;
		$snap_height = round( 300 / $setting_ratio ); 
	} else {
		$snap_width = 200;
		$snap_height = round( 200 / $setting_ratio ); 
	}
	$wideclass = ( 200 == $snap_width ) ? ' wide-200' : ' wide-300';
	
	$result = array( 'width' => $snap_width, 'height' => $snap_height, 'class' => $wideclass );
	return $result;
	
}

function cnhk_get_backgroundsize ( $slideshow_index ) {
	$settings = get_option( 'cnhk_options' );
	$setting_height;
	$setting_width;
	$setting_height = intval( $settings[4][$slideshow_index] );
	$setting_width = intval( $settings[5][$slideshow_index] );
	$bgr_width;
	$bgr_height;	
	
	$setting_ratio = $setting_width / $setting_height;

	if ( $setting_height < $setting_width ) {
		$bgr_width = 300;
		$bgr_height = round( 300 / $setting_ratio ); 
	} else {
		$bgr_width = 200;
		$bgr_height = round( 200 / $setting_ratio ); 
	}
	$wideclass = ( 200 == $bgr_width ) ? ' wide-200' : ' wide-300';
	
	$result = array( 'width' => $bgr_width, 'height' => $bgr_height, 'class' => $wideclass );
	return $result;
}

function cnhk_background_image_url( $slideshow_index ) {
	$bgr_dir = CNHK_DIR . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'bgr' . DIRECTORY_SEPARATOR;
	$bgr_dir_url = CNHK_URL . 'uploads/bgr/';
	
	if ( file_exists( $bgr_dir . "bgr-{$slideshow_index}.jpg" ) ) {
		return $bgr_dir_url . "bgr-{$slideshow_index}.jpg";
	} elseif ( file_exists( $bgr_dir . "bgr-{$slideshow_index}.png" ) ) {
		return $bgr_dir_url . "bgr-{$slideshow_index}.png";
	}
	
	return false;
}

function cnhk_get_background_image_path( $slideshow_index ) {
	$bgr_dir = CNHK_DIR . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'bgr' . DIRECTORY_SEPARATOR;
	
	if ( file_exists( $bgr_dir . "bgr-{$slideshow_index}.jpg" ) ) {
		return $bgr_dir . "bgr-{$slideshow_index}.jpg";
	} elseif ( file_exists( $bgr_dir . "bgr-{$slideshow_index}.png" ) ) {
		return $bgr_dir . "bgr-{$slideshow_index}.png";
	}
	
	return false;
}

function cnhk_is_valid_title( $title ) {
	$pattern = "#^[\p{L}0-9?\!\. _-]{2,}$#iu";
	if ( preg_match( $pattern, $title ) == 1 ) {
		return true;
	} else {
		return false;
	}
}

function cnhk_make_slug( $value ) {
	$sch = array( "#(\s){1,}#iu", "#[éêèë]#iu", "#[îï]#iu", "#[öô]#iu", "#[ùüû]#iu", "#[àâä]#iu", "#[ç]#iu", "#[^éêèëïîöôùüûàâäç0-9a-zA-Z_ \.-]#iu" );
	$rpl = array( "_", "e", "i", "o", "u", "a", "c", "-" );
	return preg_replace( $sch, $rpl, $value );
}