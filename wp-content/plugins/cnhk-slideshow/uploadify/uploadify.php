<?php

if ( isset( $_POST['sid']) ) {
	session_id( $_POST['sid'] );
	session_start();
} else {
	$error = '<h3>Security error</h3><p>Session can not be retrieved.</p>';
	$output = json_encode( array( 'status' => false, 'error' => $error, 'modal' => true ) );			
	header( "Content-Type: application/json" );
	echo $output;
	exit;
}

if ( isset( $_POST['what'] ) && isset( $_POST['page'] ) ) {
	if ( isset( $_POST['ajaxnonce'] ) && $_POST['ajaxnonce'] == $_SESSION['cnhk']['ajaxnonce'] ) {

		$targetFolder = rawurldecode( $_POST['cnhk_dir'] ) . DIRECTORY_SEPARATOR . 'uploads'; // Relative to the root

		if ( ! empty( $_FILES ) ) {
			switch ( $_POST['what'] ) {
			
				case 'slide' :
					if ( isset( $_POST['page'] ) && 'cnhk-slide' == $_POST['page'] ) {
						$tempFile = $_FILES['Filedata']['tmp_name'];
						$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
						
						$nb = 1;
						while ( file_exists( $targetFolder . DIRECTORY_SEPARATOR . "slide{$nb}.jpg" ) || file_exists( $targetFolder . DIRECTORY_SEPARATOR . "slide{$nb}.png" ) ) {
							$nb++;
						}
						
						$fileTypes = array( 'jpg', 'jpeg', 'png' ); // File extensions
						$fileParts = pathinfo( $_FILES['Filedata']['name'] );
						
						$has_error = false;
						$error_html = '';
						
						if ( ! in_array( $fileParts['extension'], $fileTypes ) ) {
							$has_error = true;
							$error_html .= $fileParts['extension'] . ' files are not allowed.<br />';
						}
						if ( 2*1024*1024 < $_FILES['Filedata']['size'] ) {
							$has_error = true;
							$error_html	.= "The file is too big. 2MB max.";				
						}
						
						$extension = '.png';
						if ( 'jpg' == $fileParts['extension'] || 'jpeg' == $fileParts['extension'] )
							$extension = '.jpg';
						
						$targetFileName = 'slide' . $nb . $extension;
						
						$targetFile = $targetFolder . DIRECTORY_SEPARATOR . $targetFileName;
						
						if ( ! $has_error ) {
							move_uploaded_file( $tempFile, $targetFile );
							
							header( "Content-Type: application/json" );
							
							$out = json_encode( array( 'status' => true, 'file' => $targetFileName ), JSON_FORCE_OBJECT );
							echo $out;
							
							exit;
							
						} else {
							$out = json_encode( array(
								'status' 	=> false,
								'error'		=> $error_html,
								'subject'	=> 'slideupload'
							), JSON_FORCE_OBJECT );
							
							header( "Content-Type: application/json" );
							echo $out;
							exit;
						}
					} else {
						$error = '<h3>Security error</h3><p>Try to reload this page and make sure to use the cancel button for canceling an action.</p>';
						$output = json_encode( array( 'status' => false, 'error' => $error, 'modal' => true ) );			
						header( "Content-Type: application/json" );
						echo $output;
						exit;
					}
					break;
				case 'bgr' :
					break;
				default:
					exit;
			}
		}
		
	} else {
		$error = '<h3>Security error</h3><p>Try to reload this page and make sure to use the cancel button for canceling an action.</p>';
		$output = json_encode( array( 'status' => false, 'error' => $error, 'modal' => true ) );			
		header( "Content-Type: application/json" );
		echo $output;
		exit;
	}
} else {
	$response = array(
			'status' => false,
			'error' => "<h3>No data updated</h3><p>No matching subject.</p>"
		);
		$output = json_encode( $response, JSON_FORCE_OBJECT );	
		header( "Content-Type: application/json" );
		echo $output;
		exit;
}