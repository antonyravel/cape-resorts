function jobman_apply_filter() {
	var ii, field, value, type;
	var empty = new Array();

	jQuery('.carrier_container input, .carrier_container textarea').unbind('focus').bind('focus', function() {

		jQuery(this).css('border-color', '#ccc');

	});

	jQuery('.carrier_container input').css('border-color', '#ccc');
	for( ii = 0; ii < jobman_mandatory_ids.length; ii++ ) {

		field = jQuery('[name="jobman-field-' + jobman_mandatory_ids[ii] + '"]');

		// jQuery 1.6 introduces .prop(), and breaks .attr() backwards compatibility
		// TODO: Remove the .attr() code when we stop suporting jQuery < 1.6
		if( typeof field.prop == 'function' ) {
			value = field.prop('value');
			type = field.prop('type');
		}
		else {
			value = field.attr('value');
			type = field.attr('type');
		}		

		if( 1 == field.length && '' == value ) { 		//alert(ii+' '+value);
			field.css('border-color', 'red');
			empty.push( jobman_mandatory_labels[ii] );
		} else if(ii==7) { 
			if(!value .match(/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/)) {

							field.css('border-color', 'red');
				empty.push( jobman_mandatory_labels[ii] );

			} }
else if(ii==8) { 


 var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(value)) {
   
				field.css('border-color', 'red');
				empty.push( jobman_mandatory_labels[ii] );

			
		}
		}
		if( 'radio' == type || 'checkbox' == type ) {
			var checked = false;
			
			for( var jj = 0; jj < field.length; jj++ ) {
				if( field[jj].checked ) {
					checked = true;
					break;
				}
			}
			
			if( ! checked ) {
				empty.push( jobman_mandatory_labels[ii] );
			}
		}
	}
	
	if( empty.length > 0 ) {
		var error = jobman_strings['apply_submit_mandatory_warning'] + ":\n";
		for( ii = 0; ii < empty.length; ii++ ) {
			error += empty[ii] + "\n";
		}
		alert( error );
		return false;
	}
	else {
		return true;
	}


}
