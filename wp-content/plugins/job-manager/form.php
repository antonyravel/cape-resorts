
<div id="form_inner">

<div id="personal" class="form_division">
<p class="mand"><span class="required">*</span>Mandatory fields</p>
<h3>Personal Information</h3>
<table class="job-apply-table table1">
<tbody><tr class="row1 totalrow1 field2 odd"><th scope="row">First Name<span class="required">*</span> </th><td><input type="text" name="jobman-field-2" value=""></td></tr><tr class="row2 totalrow2 field3 even"><th scope="row">Last Name<span class="required">*</span></th><td><input type="text" name="jobman-field-3" value=""></td></tr><tr class="row3 totalrow3 field4 odd"><th scope="row">Address<span class="required">*</span></th><td> <textarea name="jobman-field-4"></textarea></td></tr><tr class="row4 totalrow4 field5 even"><th scope="row">City<span class="required">*</span></th><td><input type="text" name="jobman-field-5" value=""></td></tr><tr class="row5 totalrow5 field6 odd"><th scope="row">State/Province<span class="required">*</span></th><td><input type="text" name="jobman-field-6" value=""></td></tr><tr class="row6 totalrow6 field7 even"><th scope="row">Zip/Postal Code<span class="required">*</span></th><td><input type="text" name="jobman-field-7" value=""></td></tr><tr class="row7 totalrow7 field8 odd"><th scope="row">Country<span class="required">*</span></th><td><input type="text" name="jobman-field-8" value=""></td></tr><tr class="row8 totalrow8 field9 even"><th scope="row">Telephone Number<span class="required">*</span></th><td><input type="text" name="jobman-field-9" value=""><span style="padding-left:10px;">Ex: xxx-xxx-xxxx</span></td></tr><tr class="row9 totalrow9 field10 odd"><th scope="row">Email Address<span class="required">*</span></th><td><input type="text" name="jobman-field-10" value=""></td></tr></tbody></table>

</div> <!-- personal Ends -->

<div id="employment" class="form_division">

<h3>Employment Related Information</h3>

<h5>Position(s) Applying For<span class="required">*</span></h5>

<table class="job-apply-table table2">
<tbody><tr class="row1 totalrow10 field13 odd"><th scope="row">1.</th><td><input type="text" name="jobman-field-13" value=""></td></tr><tr class="row2 totalrow11 field14 even"><th scope="row">2.</th><td><input type="text" name="jobman-field-14" value=""></td></tr><tr class="row3 totalrow12 field15 odd"><th scope="row">Have you worked for Cape Resorts before?<span class="required">*</span> </th><td> <input type="radio" name="jobman-field-15" value="Yes"> Yes &nbsp;&nbsp;
 <input type="radio" name="jobman-field-15" value="No"> No<br></td></tr><tr class="row4 totalrow13 field16 even"><th scope="row">If yes, when</th><td><input type="text" name="jobman-field-16" value=""></td></tr><tr class="row5 totalrow14 field17 odd"><th scope="row">What property?</th><td> <input type="text" name="jobman-field-17"></td>
</tr><tr class="row6 totalrow15 field18 even"><th scope="row">What position?</th><td><input type="text" name="jobman-field-18" value=""></td></tr><tr class="row7 totalrow16 field19 odd"><th scope="row">Do you have any relatives, spouses and/or partners working for Cape Resorts?</th><td> <input type="radio" name="jobman-field-19" value="Yes"> Yes &nbsp;&nbsp;
<input type="radio" name="jobman-field-19" value="No"> No<br></td></tr><tr class="row8 totalrow17 field20 even"><th scope="row">Name</th><td><input type="text" name="jobman-field-20" value=""></td></tr><tr class="row9 totalrow18 field21 odd"><th scope="row">Relationship</th><td><input type="text" name="jobman-field-21" value=""></td></tr><tr class="row10 totalrow19 field22 even"><th scope="row">Job Title</th><td><input type="text" name="jobman-field-22" value=""></td></tr><tr class="row11 totalrow20 field23 odd"><th scope="row">Location</th><td><input type="text" name="jobman-field-23" value=""></td></tr><tr class="row12 totalrow21 field24 even"><td colspan="2"><p class="relative"><em>Relatives, spouses and/or partners will not be employed under direct supervision of one another and the company reserves the right to determine if they will be placed in the same department if, in Cape Resorts’ opinion, this could result in potential conflict of interest.</em></p></td></tr><tr class="row13 totalrow22 field25 odd"><th scope="row">Which of the following employment status will you accept:</th><td> <input type="checkbox" name="jobman-field-25[]" value="Full Time"> Full Time
<br> <input type="checkbox" name="jobman-field-25[]" value="Part Time"> Part Time
<br> <input type="checkbox" name="jobman-field-25[]" value="Temporary"> Temporary
<br></td>
</tr>

<tr class="row15 totalrow24 field27 odd"><td colspan="2"><p>If you are interested in seasonal work, what is your availability?
</p></td></tr><tr class="row16 totalrow25 field28 even"><th scope="row">(start date)</th><td> <input type="text" class="datepicker" name="jobman-field-28" placeholder="mm/dd/yy" value="" id="dp1373363262549"></td></tr><tr class="row17 totalrow26 field29 odd"><th scope="row">(end date)</th><td> <input type="text" class="datepicker" placeholder="mm/dd/yy" name="jobman-field-29" value="" id="dp1373363262550"></td></tr><tr class="row18 totalrow27 field30 even"><th scope="row">If hired, on what date will you be available to start work?</th><td> <input type="text" class="datepicker" placeholder="mm/dd/yy" name="jobman-field-30" value="" id="dp1373363262551"></td></tr><tr class="row19 totalrow28 field31 odd"><th scope="row">Are you at least 18 years of age?<span class="required">*</span> </th><td> <input type="radio" name="jobman-field-31" value="Yes"> Yes &nbsp;&nbsp; <input type="radio" name="jobman-field-31" value="No"> No<br></td></tr><tr class="row20 totalrow29 field32 even"><th scope="row">Are you a citizen of the U.S.?</th><td> <input type="radio" name="jobman-field-32" value="Yes"> Yes &nbsp;&nbsp;
<input type="radio" name="jobman-field-32" value="No"> No<br></td></tr><tr class="row21 totalrow30 field33 odd"><th scope="row">Do you have a lawful right to work in the U.S.?<span class="required">*</span></th><td> <input type="radio" name="jobman-field-33" value="Yes"> Yes &nbsp;&nbsp;
<input type="radio" name="jobman-field-33" value="No"> No<br></td></tr><tr class="row22 totalrow31 field34 even"><th scope="row">Other than a traffic violation, have you ever been convicted of a felony or misdemeanor?<span class="required">*</span></th><td> <input type="radio" name="jobman-field-34" value="Yes"> Yes &nbsp;&nbsp;
<input type="radio" name="jobman-field-34" value="No"> No<br></td></tr><tr class="row23 totalrow32 field35 odd"><th scope="row">Identify Charge</th><td><input type="text" name="jobman-field-35" value=""></td></tr></tbody></table>


</div> <!-- #employment-->







<div id="Education" class="form_division">

<h3>Education and Skills</h3>

<p><em>Cape Resorts requires all new-hires either to have earned a high school diploma/GED or be currently
enrolled in high school.</em></p>
<h4>List below schools attended starting with high school.</h4>


<table class="job-apply-table table4" id="schoolTable">

<tbody>
<thead>

<tr class="row4 totalrow36 field40 even">

<th class="scl-name">School Name</th>

<th class="scl-loc">Location</th>

<th class="gra">Did you graduate?</th>
<th class="dip">Type of Diploma/Degree</th>
<th class="remove-th">&nbsp;</th>
</tr>
</thead>

<tbody class="grad">

<tr>

<td><input type="text" name="jobman-school[name][]" value="" class="std-text" /></td>

<td><input type="text" name="jobman-school[location][]" value="" class="std-text" /></td>

<td><input type="text" name="jobman-school[graduate][]" value="" class="std-text" /></td>

<td><input type="text" name="jobman-school[type][]" value="" class="std-text" /></td>

</tr>
</tbody>
</table>
<input type="hidden" name="total_school" value="1" id="total_school">
<button id="add_school">Add Education</button>

<table class="job-apply-table table5">
<tbody>
<tr><th>Licenses/Certifications Held</th><td><input type="text" name="jobman-field-43" value=""></td></tr>
<tr class="row8 totalrow40 field44 even"><th scope="row" style="width:270px; padding-right:30px;">If the position for which you have applied requires a valid driver’s license, can you provide one?</th><td>

<input type="radio" name="jobman-field-44" value="Yes"> Yes &nbsp;&nbsp;
<input type="radio" name="jobman-field-44" value="No"> No<br></p>

</td></tr></tbody></table>

<div> <!-- #Education -->

<div id="emp_record" class="form_division">

<h3>Employment Record</h3>

<p><em>If you are not submitting your resume and cover letter, please complete your employment history in detail, starting with your present or most recent employer. Explain any lapse of time not accounted for.</em></p>



<p>May we contact the employers below?</p>

<input type="radio" name="jobman-field-47" value="Yes"> Yes &nbsp;&nbsp;
<input type="radio" name="jobman-field-47" value="No"> No<br></p>


<p> If not, list the one(s) you do not</p>
<p> <textarea name="jobman-field-48" style="width:100%; max-width:490px;"></textarea></p>


<table class="job-apply-table table5 employers">

<tbody>
<tr>
<th scope="row">Name of Employer</th>
<td><input type="text" name="jobman-emp[name][]" value=""></td></tr>


<tr class="row6 totalrow46 field51 even">
<th scope="row">Phone Number</th>
<td><input type="text" name="jobman-emp[number][]" value=""><span style="padding-left:10px;">Ex: xxx-xxx-xxxx</span></td></tr>

<tr><th class="addr">Employment Dates</th></tr>

<tr class="row7 totalrow47 field52 odd">
<th scope="row">Start</th><td> <input type="text" class="datepicker" placeholder="mm/dd/yy" name="jobman-emp[start][]" value="" id="dp1373363262552"></td>
</tr>
<tr class="row8 totalrow48 field53 even"><th scope="row">End</th>
<td> <input type="text" class="datepicker" name="jobman-emp[end][]" placeholder="mm/dd/yy" value="" id="dp1373363262553"></td>
</tr>

<tr class="row9 totalrow49 field54 odd">
<tr><th class="addr">Rate of Pay</th></tr>
<th scope="row">Start</th>
<td> <input type="text" class="datepicker" name="jobman-emp[paystart][]" placeholder="mm/dd/yy" value="" id="dp1373363262554"></td>
</tr>
<tr class="row10 totalrow50 field55 even">
<th scope="row">End</th>
<td> <input type="text" class="datepicker" name="jobman-emp[payend][]" placeholder="mm/dd/yy" value="" id="dp1373363262555"></td>
</tr>
<tr class="row11 totalrow51 field56 odd">
<th scope="row">Supervisor’s Name &amp; Title</th><td><input type="text" name="jobman-emp[supervisor][]" value=""></td></tr>

<tr><th colspan="2" class="addr">Address</th></td></tr>
<tr class="row1 totalrow52 field58 odd">
<th scope="row">Street</th>
<td><input type="text" name="jobman-emp[address][street][]" value=""></td></tr>

<tr class="row2 totalrow53 field59 even"><th scope="row">City</th><td><input type="text" name="jobman-emp[address][city][]" value=""></td></tr>

<tr class="row3 totalrow54 field60 odd"><th scope="row">State</th><td><input type="text" name="jobman-emp[address][state][]" value=""></td></tr>

<tr class="row4 totalrow55 field61 even"><th scope="row">Zip</th><td><input type="text" name="jobman-emp[address][zip][]" value=""></td></tr>


<tr class="row5 totalrow56 field62 odd"><th scope="row">Reasons for Leaving</th><td> <textarea name="jobman-emp[reason][]"></textarea></td></tr>

<tr class="row6 totalrow57 field63 even"><th scope="row">Your Titles &amp; Duties</th><td> <textarea name="jobman-emp[duty][]"></textarea></td></tr>

<tr class="row7 totalrow58 field64 odd"><th scope="row">Lapse of time?</th><td><input type="text" name="jobman-emp[lapse][]" value=""></td></tr>
<tr><td colspan="2"></td></tr>
</tbody></table>
<input type="hidden" name="total_employee" value="1" id="total_employee">
<button id="add_employee">Add Employer</button>

</div> <!-- emp_record -->



<table class="bt-content"><tbody><td colspan="2">

<p>Cape Resorts is an equal opportunity employer.</p>


<p>The facts set forth in my application for employment are true and complete. I understand that if employed, false
statements on this</p>
<p>application shall be considered sufficient cause for dismissal. You are hereby authorized to make any investigation of
my personal history, previous employment, financial and credit record though any investigative or credit agencies or bureaus of your choice.</p>
<p>In making this application for employment I also understand that an investigative consumer report may be made whereby information is obtained through personal interviews with my neighbors, friends, or others with whom I am acquainted. This inquiry, if made, may include information as to my character and general reputation. I understand that I have the right to make a written request within a reasonable period of time to receive additional, detailed information about the nature and scope of any such investigative report that is made.</p></td></tr>

<tr class="hear-a row9 totalrow60 field66 odd">
<th scope="row" class="hear-label">How did you hear about us?</th><td><input type="text" name="jobman-field-66" value=""></td></tr><tr><td colspan="2">&nbsp;</td></tr><tr><td colspan="2" class="submit"><input type="submit" name="submit" class="button-primary" value="Submit Your Application"></td></tr></tbody></table>

</div><!--Inner Wrapper Ends -->


