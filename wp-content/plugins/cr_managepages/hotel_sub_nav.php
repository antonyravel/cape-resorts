<?php 

class menuManagement
{
	
    // property declaration
    public $sub_nav_id = '';
	
    public function loadingPage($h_id, $menu_id, $gallery_id) { ?>
		<?php global $functions; ?>

		<div id="menu_management">
			
			<h4>Global settings</h4>			
				

<div id="manage_tabs" class="jquery_tabs">
	<ul>
		<li><a href="#manage_gallery"><span>Manage Gallery</span></a></li>
		<li><a href="#manage_menus"><span>Manage tabs</span></a></li>

	</ul>



<div id="manage_gallery">
		
		<form class="hotelgallery">
			<input type="hidden" name="hotel_id" value="<?php echo $h_id; ?>">
			<input type="hidden" name="menu_id" value="<?php echo $menu_id; ?>">
			<input type="hidden" name="action" value="menugallery">
			<table>
				<tr>
					<td scope="row" valign="top"><label for="gallery">Select Gallery</label></td>
					<td>
						<?php $functions->get_gallery_by_hotel($gallery_id); ?>		
					</td>
				</tr>
<?php $seoval = $functions->get_seotitle($h_id, $menu_id); ?>
				<tr>
					<td scope="row" valign="top"><label for="gallery">Seo Title</label></td>
					<td>
<input type="text" name="seotitle" size="40" value="<?php echo $seoval->seotitle; ?>">	
					</td>
				</tr>
				<tr>
					<td scope="row" valign="top"><label for="gallery">Meta Description</label></td>
					<td>
<textarea name="meta_description" rows="4" cols="50"><?php echo $seoval->meta_description; ?></textarea>
					</td>
				</tr>
				<tr>
					<td scope="row" valign="top"><label for="gallery">Focus Keywords</label></td>
					<td>
<textarea name="focus_keywords" rows="4" cols="50"><?php echo $seoval->focus_keywords; ?></textarea>
					</td>
				</tr>
				<tr><td align="left" colspan="2"><button class="submit_details">Save</button><span class="loader">Processing..</span></td></tr>
			</table>
		</form>
</div>


<div id="manage_menus">
<?php if($_GET['edit']){ ?>
<div>
			<h5>Update tab</h5>
			<form id="managetab">
			<input type="hidden" name="hotel_id" value="<?php echo $h_id; ?>">
			<input type="hidden" name="menu_id" value="<?php echo $menu_id; ?>">
			<input type="hidden" name="tab_action" value="tab">
			<input type="hidden" name="tab_id" value="<?php echo $_GET['edit']; ?>">
			<input type="hidden" name="action" value="menugallery">
			<input type="hidden" name="data_action" value="edit">
			<?php $tabs = $functions->get_tab_details($h_id, $menu_id,$_GET['edit']);  ?>
			<?php foreach($tabs as $tab): ?>
			<table>
				<tr>
					<td scope="row" valign="top" width="25%"><label for="t_name">Name</label></td>
					<td><input type="text" name="t_name" size="40" value="<?php echo $tab->t_name; ?>" /></td>
				 </tr>
				
				<tr class="post_header">
					<td scope="row" valign="top" width="25%"><label for="header">Header</label></td>
					<td><input type="text" value="<?php echo $tab->header; ?>" name="header" size="60" /></td>
				 </tr>
				
				<tr>
					<td scope="row" valign="top" width="25%"><label for="sub_header">Sub Header</label></td>
					<td><input type="text" value="<?php echo $tab->sub_header; ?>" name="subheader" size="60" /></td>
				 </tr>
				
				<tr>
					<td scope="row" valign="top" width="25%"><label for="slug">Slug</label></td>
					<td><input type="text" value="<?php echo $tab->slug; ?>" name="slug" size="60" /></td>
				 </tr>

				<tr> <td scope="row" valign="top" width="25%" style="padding-top:20px;padding-bottom:20px;"><label for="promos">Display Promos&nbsp; &nbsp; &nbsp;</label></td>
					<td style="padding-top:20px;padding-bottom:20px;"><input type="checkbox" <?php if($tab->promos==1){ echo 'checked="checked"';} ?> name="promos" value='1' ></td>
				 </tr>




<tr>
<td colspan="2">
<input type="hidden" name="promos_list" />
<?php
	
	$promos = explode(",", $tab->promo_lists);
?>
<?php $posts = query_posts(array('post_type' => 'ps_promotion', 'post__in' => $promos,  'orderby' => 'post__in' )); ?>


<div class="widget-top"><h3>Selected Promos</h3></div> 
<ul id="sortable1" class="connectedSortable" style="min-height:100px;">
 <?php 
	if(count($posts) > 0) {  ?>

		<?php foreach($posts as $post): ?>
<?php $property_page = get_field('property',$post->ID);
$separator = ',';
$output = '';
if($property_page){
	foreach($property_page as $category) {
	global $wpdb;
	$pagename = $wpdb->get_var("SELECT name FROM $wpdb->hotels WHERE id = $category");
	$output .= $pagename.$separator;
	}
$properypage = trim($output, $separator);
} ?>
			 <li class="ui-state-highlight" id="array_<?php echo $post->ID; ?>" ><div class="promodet"><span class="protitle"><?php echo $post->post_title; ?></span><span class="procont"><?php $newcontent= $post->post_content; echo cr_trim_chars( $newcontent, 30); ?></span><span class="procont"><?php echo $properypage; ?></span><span class="prodate"><?php echo date("F j, Y",strtotime($post->post_date)); ?></span></div></li>
<?php	 endforeach; }  ?>

</ul>


<div class="widget-top"><h3>Drag to Above box</h3></div>  
<?php
		$args['post_type']='ps_promotion';
		$args['posts_per_page']=-1;
		$args['post__not_in']=$promos;
		$args['meta_query'] = array(
		array(
			'key' => 'property',
			'value' => 's:'.strlen($h_id).':"'.$h_id.'"',
			'compare' => 'LIKE'
			)
		);
		$promotions = new WP_Query( $args );
?>

<ul id="sortable2" class="connectedSortable" style="min-height:100px;">
  <?php while ( $promotions->have_posts() ) : $promotions->the_post(); ?>
<?php $property_page = get_field('property');
$separator = ',';
$output = '';
if($property_page){
	foreach($property_page as $category) {
	global $wpdb;
	$pagename = $wpdb->get_var("SELECT name FROM $wpdb->hotels WHERE id = $category");
	$output .= $pagename.$separator;
	}
$properypage = trim($output, $separator);
} ?>
	 <li class="ui-state-highlight" id="array_<?php the_ID(); ?>" ><div class="promodet"><span class="protitle"><?php the_title(); ?></span><span class="procont"><?php $newcontent= get_the_content(); echo cr_trim_chars( $newcontent, 30); ?></span><span class="procont"><?php echo $properypage; ?></span><span class="prodate"><?php echo get_the_date(); ?></span></div></li>
 <?php endwhile; ?>

</ul>


<script>
jQuery(document).ready(function() {
  $(function() {
    $( "#sortable1, #sortable2" ).sortable({
      connectWith: ".connectedSortable"
    }).disableSelection();
  });
});
function serialize_promos() {

	serial = jQuery('#sortable1').sortable('serialize'); 
	jQuery('input[name=promos_list]').val(serial);

}
</script>




</div>











</td>
</tr>













				<tr><td align="left" colspan="2"><button class="submit_details">Update</button><span class="loader">Processing..</span></td></tr>
			</table>
				<?php endforeach; ?>
			</form>















</div>
<?php }else{ ?>
<div class="add_tab">
			<h5>Add New tab</h5>
			<form id="addmanagetab">
			<input type="hidden" name="hotel_id" value="<?php echo $h_id; ?>">
			<input type="hidden" name="menu_id" value="<?php echo $menu_id; ?>">
			<input type="hidden" name="tab_action" value="tab">
			<input type="hidden" name="action" value="menugallery">
			<table>
				<tr>
					<td scope="row" valign="top" width="25%"><label for="t_name">Name</label></td>
					<td><input type="text" name="t_name" size="40" /></td>
				 </tr>
				<tr>
					<td scope="row" valign="top" style="padding-top:20px;"><label for="type">Select type</label></td>
					<td style="padding-top:20px;">
						<input type="radio" name="type" value="0" class="select_type" />&nbsp;Page&nbsp; &nbsp; &nbsp; 
						<input type="radio" name="type" value="1" class="select_type"/>&nbsp;Post&nbsp; &nbsp; &nbsp;
						<input type="radio" name="type" value="3" class="select_type" />&nbsp;gallery&nbsp; &nbsp; &nbsp;
						<input type="radio" name="type" value="2" class="select_type" id="fromConcierge" />&nbsp;From concierge posts&nbsp; &nbsp; &nbsp;

					<select name="concierge_post" style="visibility:hidden;" id="concierge_post">
						<option value="" >Please Select</option>
						<option value="activities">ACTIVITIES</option>
						<option value="seasonalevents">SEASONAL EVENTS</option>
						<option value="specialoffers">SPECIAL OFFERS</option>
						<option value="daily-calender">DAILY CALENDER</option>
					 </select>
					<td>
				</tr>

				<tr class="post_header">
					<td scope="row" valign="top" width="25%"><label for="header">Header</label></td>
					<td><input type="text" name="header" size="60" /></td>
				 </tr>
				
				<tr>
					<td scope="row" valign="top" width="25%"><label for="sub_header">Sub Header</label></td>
					<td><input type="text" name="subheader" size="60" /></td>
				 </tr>

				<tr> <td scope="row" valign="top" width="25%" style="padding-top:20px;padding-bottom:20px;"><label for="promos">Display Promos&nbsp; &nbsp; &nbsp;</label></td>
					<td style="padding-top:20px;padding-bottom:20px;"><input type="checkbox" name="promos" value='1' ></td>
				 </tr>

				<tr><td align="left" colspan="2"><button class="submit_details">Save</button><span class="loader">Processing..</span></td></tr>
			</table>
			</form>
</div>
<?php }  ?>
<div>
			<h5>Tabs</h5>
			<form>
			<input type="hidden" name="hotel_id" value="<?php echo $h_id; ?>">
			<input type="hidden" name="menu_id" value="<?php echo $menu_id; ?>">
			<input type="hidden" name="tab_action" value="tab">
			<input type="hidden" name="action" value="menugallery">
			<?php $tabs = $functions->get_all_tabs($h_id, $menu_id);  ?>

			<table class="gridtable">
				<tr>
					<th style="text-align:center;" width="16%">Name</th>
					<th colspan="3" style="text-align:center;" colspan="3" width="20%">Actions</th>
				 </tr>
			</table><table class="gridtable" id="table-1">
				<?php foreach($tabs as $tab): ?>
				<tr id="<?php echo $tab->prop_id.'_'.$tab->menu_id.'_'.$tab->id; ?>">
					<td style="text-align:center;" width="60%"><label for="t_name"><?php echo $tab->t_name; ?></label></td>
					
						<?php if($tab->type == 0) {
								$link = site_url().'/wp-admin/post.php?post='.$tab->page_id.'&action=edit';

							} else if($tab->type == 1) {
								$link = site_url().'/wp-admin/edit.php?post_type='.$tab->post_type;
						}else if($tab->type == 2) {
								$link = site_url().'/wp-admin/edit.php?post_type='.$tab->post_type;
						}  
						?>
					<td style="text-align:center;" width="30%"><a href="<? echo $link; ?>" class="edit_tabs">Manage Pages</a></td>
					<td style="text-align:center;" width="20%"><a href="<? echo site_url().'/wp-admin/admin.php?page=managehotels&hotel_id='.$h_id.'&submenu_id='.$menu_id.'&edit='.$tab->id.'#manage_menus'; ?>" class="edit_tabs">Edit</a></td>
					<td style="text-align:center;" width="20%"><a href="javascript:void(0);" data-action="delete" data-tab="<?php echo $tab->id; ?>" data-type ="<?php echo $tab->type; ?>" data-menu="<?php echo $tab->menu_id; ?>" data-prop="<?php echo $tab->prop_id; ?>" class="actionTabs" data-page="<?php echo $tab->page_id; ?>" data-post_type="<?php echo $tab->post_type; ?>">Delete</a></td>
					<td style="text-align:center;" width="20%"><a href="javascript:void(0);" data-action="update" data-tab="<?php echo $tab->id; ?>" data-type ="<?php echo $tab->type; ?>" data-menu="<?php echo $tab->menu_id; ?>" data-prop="<?php echo $tab->prop_id; ?>" class="actionTabs"><?php echo $tab->active==0? 'Deactivate': 'Activate' ; ?></a></td>
				 </tr>
				<?php endforeach; ?>

				<!-- <tr><td align="left" colspan="2"><button class="submit_details">Save</button><span class="loader">Change options..</span></td></tr> -->
			</table>
<div class="result"></div>
<a class="add_tab" href="<? echo site_url().'/wp-admin/admin.php?page=managehotels&hotel_id='.$h_id.'&submenu_id='.$menu_id.'#manage_menus'; ?>">Add New tab<a/>
			</form>
</div>


</div>


			



		</div>
<?php
    }
}

?>

<!--				<div id="galleryContainer" class="widget-holder target">
				<div id="gid-1" class="groupItem">
				<div class="innerhandle">
					<div class="item_top ">
						<a href="#" class="min" title="close">[-]</a>
						ID: 1 | Congress Hall - Home
					</div>
					<div class="itemContent">
							
							<p><strong>Name : </strong>congress-hall-home</p>
							<p><strong>Title : </strong>Congress Hall - Home</p>
							<p><strong>Page : </strong>---</p>
							
						</div>
				</div>
			   </div> -->
