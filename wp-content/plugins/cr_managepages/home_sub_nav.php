<?php 

class menuManagement
{
	
    // property declaration
    public $sub_nav_id = '';
	
    public function loadingPage($h_id, $menu_id, $gallery_id) { ?>
		<?php global $functions; ?>

		<div id="menu_management">
			
			<h4>Global settings</h4>			
				

<div id="manage_tabs" class="jquery_tabs">
	<ul>
		<li><a href="#manage_gallery"><span>Manage Gallery</span></a></li>
		<li><a href="#manage_menus"><span>Manage Content</span></a></li>

	</ul>



<div id="manage_gallery">
		
		<form class="hotelgallery">
			<input type="hidden" name="hotel_id" value="<?php echo $h_id; ?>">
			<input type="hidden" name="menu_id" value="<?php echo $menu_id; ?>">
			<input type="hidden" name="action" value="menugallery">
			<table>
				<tr>
					<td scope="row" valign="top"><label for="gallery">Select Gallery</label></td>
					<td>
						<?php $functions->get_gallery_by_hotel($gallery_id); ?>		
					</td>
				</tr>
				<tr><td align="left" colspan="2"><button class="submit_details">Save</button><span class="loader">Processing..</span></td></tr>
			</table>
		</form>
</div>


<div id="manage_menus">
<?php if($_GET['edit']){ ?>
<div>
			<h5>Update Name</h5>
			<form>
			<input type="hidden" name="hotel_id" value="<?php echo $h_id; ?>">
			<input type="hidden" name="menu_id" value="<?php echo $menu_id; ?>">
			<input type="hidden" name="tab_action" value="tab">
			<input type="hidden" name="tab_id" value="<?php echo $_GET['edit']; ?>">
			<input type="hidden" name="action" value="menugallery">
			<input type="hidden" name="data_action" value="edit">
			<?php $tabs = $functions->get_tab_details($h_id, $menu_id,$_GET['edit']);  ?>
			<?php foreach($tabs as $tab): ?>
			<table>
				<tr>
					<td scope="row" valign="top" width="25%"><label for="t_name">Name</label></td>
					<td><input type="text" name="t_name" size="40" value="<?php echo $tab->t_name; ?>" /></td>
				 </tr>
				<tr><td align="left" colspan="2"><button class="submit_details">Update</button><span class="loader">Processing..</span></td></tr>
			</table>
				<?php endforeach; ?>
			</form>
</div>
<?php }else{ ?>
<div class="add_tab" style="display:none;">
			<h5>Add New tab</h5>
			<form id="addmanagetab">
			<input type="hidden" name="hotel_id" value="<?php echo $h_id; ?>">
			<input type="hidden" name="menu_id" value="<?php echo $menu_id; ?>">
			<input type="hidden" name="tab_action" value="tab">
			<input type="hidden" name="action" value="menugallery">
			<table>
				<tr>
					<td scope="row" valign="top" width="25%"><label for="t_name">Name</label></td>
					<td><input type="text" name="t_name" size="40" /></td>
				 </tr>
				<tr>
					<td scope="row" valign="top" style="padding-top:20px;"><label for="type">Select type</label></td>
					<td style="padding-top:20px;">
						<input type="radio" name="type" value="0" class="select_type" />&nbsp;Page&nbsp; &nbsp; &nbsp; 
						<input type="radio" name="type" value="1" class="select_type"/>&nbsp;Post&nbsp; &nbsp; &nbsp;						
					<td>
				</tr>			

				<tr><td align="left" colspan="2"><button class="submit_details">Save</button><span class="loader">Processing..</span></td></tr>
			</table>
			</form>
</div>
<?php }  ?>
<div>
			<form>
			<input type="hidden" name="hotel_id" value="<?php echo $h_id; ?>">
			<input type="hidden" name="menu_id" value="<?php echo $menu_id; ?>">
			<input type="hidden" name="tab_action" value="tab">
			<input type="hidden" name="action" value="menugallery">
			<?php $tabs = $functions->get_all_tabs($h_id, $menu_id);  ?>

			<table class="gridtable">
				<tr>
					<th style="text-align:center;" width="16%">Name</th>
					<th colspan="3" style="text-align:center;" colspan="3" width="20%">Actions</th>
				 </tr>
			</table><table class="gridtable" id="table-1">
				<?php foreach($tabs as $tab): ?>
				<tr id="<?php echo $tab->prop_id.'_'.$tab->menu_id.'_'.$tab->id; ?>">
					<td style="text-align:center;" width="60%"><label for="t_name"><?php echo $tab->t_name; ?></label></td>
					
						<?php $link = site_url().'/wp-admin/post.php?post='.$tab->page_id.'&action=edit'; ?>

					<td style="text-align:center;" width="30%"><a href="<? echo $link; ?>" class="edit_tabs">Manage Content</a></td>
					<td style="text-align:center;" width="20%"><a href="<? echo site_url().'/wp-admin/admin.php?page=homepage&hotel_id='.$h_id.'&submenu_id='.$menu_id.'&edit='.$tab->id.'#manage_menus'; ?>" class="edit_tabs">Edit</a></td>
				 </tr>
				<?php endforeach; ?>

				<!-- <tr><td align="left" colspan="2"><button class="submit_details">Save</button><span class="loader">Change options..</span></td></tr> -->
			</table>
<div class="result"></div>
			</form>
</div>


</div>


			



		</div>
<?php
    }
}

?>

<!--				<div id="galleryContainer" class="widget-holder target">
				<div id="gid-1" class="groupItem">
				<div class="innerhandle">
					<div class="item_top ">
						<a href="#" class="min" title="close">[-]</a>
						ID: 1 | Congress Hall - Home
					</div>
					<div class="itemContent">
							
							<p><strong>Name : </strong>congress-hall-home</p>
							<p><strong>Title : </strong>Congress Hall - Home</p>
							<p><strong>Page : </strong>---</p>
							
						</div>
				</div>
			   </div> -->
