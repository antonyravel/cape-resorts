<?php
$con = new Concierge();
class Concierge {
		var $args=array();
		var $slugname ='All';
		var $pagetitle ='';
		var $pageClass="";
		var $pageId ="";
	
    public function Concierge() {

		add_action('wp_ajax_concierge_page', array(&$this, 'concierge_content'));
		add_action('wp_ajax_nopriv_concierge_page', array(&$this, 'concierge_content'));


/* Get Nav Content */

		add_action('wp_ajax_navContent',  array(&$this, 'get_navContent'));
		add_action('wp_ajax_nopriv_navContent',  array(&$this, 'get_navContent'));

/* Get Nav Content */

		add_action('wp_ajax_careerfileupload',  array(&$this, 'careerfileupload'));
		add_action('wp_ajax_nopriv_careerfileupload',  array(&$this, 'careerfileupload'));


    }

  public function get_navContent() {

		$cong_temp = new Congresshall();

		$p_id = $_GET['prop'];
		$menu_id = $_GET['menu_id'];
		$subMenu= $_GET['menu'];

		$slides = get_prop_slides($p_id, $_GET['menu']);
		$slider =""; 
			if(count($slides) > 0) {
			foreach($slides as $slide):

				$slider .='<li><img src="'.site_url().'/'.$slide->path.'/'.$slide->filename.'" alt="'.$slide->alttext.'" class="slider-img" /></li>';

		 	endforeach; 
		}
	$menu = getMenuBySlug($p_id, $subMenu);

		//$tabsDiv = $this->get_tab_temp($p_id, $menu_id);

		$func ='Congresshall_'.$subMenu;

		$tabsDiv = $cong_temp->$func($menu, $p_id, $subMenu);

		$result['slides'] = $slider;
		$result['tabs']=$tabsDiv;
		$result['promos']=''; 
		$result['type'] = "success";

		if($menu->seotitle) {
			$result['title'] = $menu->seotitle;  
		} else {
			$result['title'] ='';
		}

		$result = json_encode($result);

		echo $result; 
	die();

	}


    public function concierge_content() {

	$data='';
	$this->pagetitle='';
	$this->pageClass='';


        $temptype = $_GET['tempType'];
		$taxnomy = $_GET['taxnomy'];
		$post_type = $_GET['postType'];
		$cat = $_GET['cat'];
		$page_slug = $_GET['slug'];
		$page_slug = str_replace('/', '', $page_slug);
		$ID = $this->get_ID_by_slug($page_slug); 
		$this->pageId = $ID;

		if(is_numeric($ID)) {
			$this->pagetitle=$this->con_page_title($ID);
		}
		switch( $temptype ){
			case "con-home":

				$data=$this->get_con_home();

			break;
		
			case "all-posts":

				$data=$this->get_con_posts($taxnomy, $post_type);

			break;

			case "specific-posts":

				$data=$this->get_con_posts($taxnomy, $post_type, $cat);

			break;

			case "post-details":

				$data=$this->get_con_details();

			break;

			case "page":

				$data=$this->get_con_page();

			break;
			case "calender":

				$data=$this->get_con_calender();

			break;

	}

	$result['data']=$data;
	$result['title']=$this->pagetitle;
	$result['pageClass']=$this->pageClass;

	//$result['keywords']=$keywords;
	//$result['description']=$description;

	$result = json_encode($result);

	echo $result; 

die();
    }
    public function get_con_home() {

		$this->pageClass='home';
		$content="";
		$content.='<h2 class="heading parent_cat">Your guide to what&#39;s cool at the cape</h2>';


$args['post_type']= array( 'seasonalevents','activities','entertainment', 'stories', 'specialoffers' );

	$args[ 'meta_key'] = 'displayhome';
	$args[ 'meta_value'] = 'yes';
	$args['posts_per_page']=-1;
$args['post_status']='publish';
	$all_posts = get_posts($args);
	$home_posts = get_option( 'concierege_posts' );
	$home_posts = explode(",", $home_posts); 

	if(is_array($home_posts) !="") {

		foreach($all_posts as $posts) {
			if(!in_array($posts->ID, $home_posts))

				array_push($home_posts,$posts->ID);


		}	




		$args['post__in'] = $home_posts;
		$args['orderby'] = 'post__in';




	} 

$myquery = new WP_Query( $args ); 



		$count = 0;
		while ( $myquery->have_posts() ) : $myquery->the_post();

			$image_id = get_post_thumbnail_id();
			if ( $count == 0 ) {
				$image_url = wp_get_attachment_image_src($image_id,'banner_501_300', true);

				$content.='<div class="home-banner">
				<img src="'.$image_url[0].'" alt="'.get_the_title().'" class="title_tag" data-id="'.get_the_ID().'"/>
				<div class="spring-harvest act-content">
				<h3><a href="'.get_permalink().'">'. get_the_title().'</a></h3>
				<h4>'.get_field('subtitle1').'</h4>
				<i>'.get_field('subtitle_2').'</i>
				<span>'.get_field('subtitle_3').'</span>
				<p>'.wp_trim_words( get_the_content(), 40 ).'</p>
				<a href="'.get_permalink().'" class="learn" data-id="'.get_the_ID().'">Read More</a>
				<a href="#" class="share"><span class="st_sharethis" displayText="Share" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>

				</div>
				</div>';

			}
			else {
				if($count==1)
					$content.='<div>';

				$image_url = wp_get_attachment_image_src($image_id,'thumb_345_154', true);

				$content.='<div class="first-div">
				<img src="'.$image_url[0].'" alt="'.get_the_title().'" />
				<div class="act-content">
				<h3><a href="'.get_permalink().'" class="title_tag" data-id="'.get_the_ID().'">'.get_the_title().'</a></h3>
				<h4>'.get_field('subtitle1').'</h4>
				<i>'.get_field('subtitle_2').'</i>
				<span>'.get_field('subtitle_3').'</span>
				<p>'.wp_trim_words( get_the_content(), 12 ).'</p>

				<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="Share" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>
				<a href="'.get_permalink().'" class="learn" data-id="'.get_the_ID().'">Learn More</a>
				</div>
				</div>';
			}	
			$count++;
			if($count==$myquery->found_posts)
				$content.='</div>';
		endwhile;
		return $content;
	}
    public function get_con_posts($tax, $post_type, $slug="") {

		$content="";
		if(isset($slug) && $slug !="") {

			$cat_details = get_term_by( 'slug', $slug, $tax, OBJECT );
			$cat = $cat_details->term_id;
			$this->slugname = $cat_details->name;

			$this->args['tax_query'] = array(
			array(
			'taxonomy' => $tax,
			'field' => 'slug',
			'terms' => $slug
			));
		} else {
			if($tax=='activity_category') {

		$this->args['tax_query'] = array(
			array(
			'taxonomy' => $tax,
			'field' => 'slug',
			'terms' =>'activities'
			));

			$post_type= array('activities', 'seasonalevents');

			}
		}
		$content .='<h2 class="heading"><span class="parent_cat">'.ucwords(get_the_title($this->pageId)).'&nbsp;:&nbsp;</span><span class="child_cat">'.ucwords($this->slugname).'</span></h2><div>';

		$this->args['post_type']=$post_type;
		$this->args['posts_per_page']=-1;
		$this->args['orderby'] = 'menu_order';
$this->args['post_status'] = 'publish';
		$activities = new WP_Query( $this->args );

		while ( $activities->have_posts() ) : $activities->the_post();

			$image_id = get_post_thumbnail_id();
			$image_url = wp_get_attachment_image_src($image_id,'thumb_345_154', true);
			$content .='<div>
			<img class="acti-img" src="'.$image_url[0].'" alt="'.get_the_title().'"/>
			<div class="act-content">
			<h3><a href="'.get_permalink().'" class="title_tag" data-id="'.get_the_ID().'">'.get_the_title().'</a></h3>';


	 $length = 80;
	 if(get_field('subtitle1')) {

	$content .='<h4>'.get_field('subtitle1').'</h4>';
	 }else {
		$length = $length+112;

	} 
	if(get_field('subtitle_2')) {

	$content .='<i>'.get_field('subtitle_2').'</i>';
	 }else {
		$length = $length+56;

	} 

	if(get_field('subtitle_3')) {
	$content .='<span>'.get_field('subtitle_3').'</span>';
	 }else {
		$length = $length+56;
	} 

	$content .='<p>'.cr_trim_chars( get_the_content(), $length, 'false').'...</p>';

			$content .='<a href="'.get_permalink().'" class="learn" data-id="'.get_the_ID().'">Learn More</a>
			<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="Share" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>
			</div>
			</div>';

		endwhile;
		$content .='</div>';
		return $content;

	}
    public function get_con_page() {
		$this->pageClass='details';
		$content ="";
		$this->args['post_type']='page';
		$this->args['p']=9;
		$query = new WP_Query( $this->args );
		while ( $query->have_posts() ) : $query->the_post();
$content .='<h2 class="heading" style="visibility:hidden;"><span class="parent_cat">Pre Arrival</span></h2>';
			$content .='<div class="details-banner">';

			$image_id = get_post_thumbnail_id();
			$image_url = wp_get_attachment_image_src($image_id,'med_700_309', true);

			$content .='<img src="'.$image_url[0].'" alt="'.get_the_title().'" title="'.get_the_title().'" /></div>
			<div class="details-content act-content">
			<h3>'.get_the_title().'</h3>
			<h4>'.get_field('subtitle1').'</h4>
			<i>'. get_field('subtitle_2').' </i>
			<span>'. get_field('subtitle_3').'</span>
			'.get_the_content().'
			</div>';

		endwhile; 
	return $content;
	}
	public function get_con_calender() {
		$this->pageClass='calender';
		$content ="";
global $wpdb;
	$dayname =$_GET['dn'];
	$month =$_GET['month'];
	$day =$_GET['day'];
	$year =$_GET['year'];
	$monthName = date("F", mktime(0, 0, 0, $month, 10));


if($dayname =='ALL') {
  $title_sub = 'ALL';
	$events = $wpdb->get_results("select post_id FROM cr_em_events where event_status=1 AND DATE(`event_end_date`) >= DATE( NOW( ) )");
   $postids=array();

	foreach($events as $event) {

		$postids[] = $event->post_id;
	
	}



} else {




   $postids = $this->get_event_byDate($day, $month, $year);


  $title_sub = $dayname.', '.$monthName.' '.$day.', '.  $year;


}

$content.='<h2 class="heading"><span class="parent_cat">Calendar:&nbsp;</span><span class="child_cat">'.$title_sub.'</span></h2>';
	
	foreach( $postids as $postid) {
	$current = get_post($postid);
	if($current->post_status  == "publish"){
	//while ( $calender->have_posts() ) : $calender->the_post();
	 $image_id = get_post_thumbnail_id($postid);
	 $image_url = wp_get_attachment_image_src($image_id);

	$content.='<div class="calen">
	  <img src="'.$image_url[0].'" title="'.get_the_title($postid).'" alt="'.get_the_title($postid).'" />
	  <div class="act-content">
		<h3><a href="'.get_permalink($postid).'" class="title_tag" data-id="'.$postid.'">'.get_the_title($postid).'</a></h3>
		<h4>'.get_field('subtitle1', $postid).',&nbsp;<i>'.get_field('subtitle_2', $postid).'</i></h4>
		<span>'.get_field('subtitle_3').'</span>
		<p>'.wp_trim_words( get_the_content($postid), 18 ).' <a href="'.get_permalink($postid).'" data-id="'.$postid.'" class="caln_learn">Learn More</a></p>
	  </div>
	</div>';
}
//	endwhile;
}
	return $content;
	}
public function get_con_details() {

	$this->pageClass='details';
	$content ='';

	$post_id =$_GET['post_id'];
$parent_cat =$_GET['parent_cat'];
$child_cat =$_GET['child_cat'];
$this->pagetitle=$this->con_page_title($post_id);
$wp_query->is_single = true;

global $post;


	$post = get_post( $post_id);
	$next_post = get_next_post();
	$previous_post = get_previous_post();

 $post_type = get_post_type();

	if($post_type=='activities') {
		$taxnomy='activity_category';		
		$parent =27;
		$title = 'Activities';

}
	elseif($post_type=='seasonalevents') {
		$taxnomy='seasonalevents_category';
		$parent =28;
		$title = 'Seasonal Events';
}		

	elseif($post_type=='entertainment') {
		$taxnomy='entertainment_category';
		$parent =29;
		$title = 'Entertainment';
}
	elseif($post_type=='stories') {
		$taxnomy='stories_category';
		$parent =30;
		$title = 'Our stories';
}
	elseif($post_type=='specialoffers') {
		$taxnomy='offers_category';		
		$parent =34;
		$title = "Special Offers";
}



$args = array(
	'parent'                   => $parent,
	'orderby'                  => 'menu_order',
	'taxonomy'                 => $taxnomy);

 $categories = get_categories( $args );

$catArray = array();
foreach($categories  as $category) {
	array_push($catArray, $category->term_id);
}

$postterms = get_the_terms( $post_id, $taxnomy );

$content .='<h2 class="heading"><span class="parent_cat">'.$title.'&nbsp;</span>
<span class="child_cat">';

if($post_type != 'specialoffers') {
$content.=':&nbsp;';
	$i=0; 
		if(is_array($postterms)):
		foreach ($postterms as $term) {
	if(in_array($term->term_id, $catArray)) {
		if($i==0) {
			$content .= ucwords($term->name); 
		}elseif($i > 0) {
			$content .= ' / '.ucwords($term->name); 
		}
		$i++;
		}
	}
	endif; 
}

	$content .='</span></h2>';

  	$content .='<div class="details-banner">';

	$conciergerSlides = get_con_attachments(get_the_ID());

	if($conciergerSlides) {
		$content .=$conciergerSlides;
	}
	else {
 	$image_id = get_post_thumbnail_id($post->ID);
 	 $image_url = wp_get_attachment_image_src($image_id,'med_700_309', true);

   $content .='<img src="'.$image_url[0].'" alt="'.get_the_title().'" title="'.get_the_title().'" />';
}
	$content .='</div><nav class="nav-single">';
if($next_post) : 

               $content .=' <span class="nav-next"><a href="'.get_permalink($next_post->ID).'" rel="next" data-id ="'.$next_post->ID.'"> Next<span class="meta-nav"> &gt;</span></a></span>';

endif;

if($previous_post && $next_post) 
$content .= '<span class="slash"> / </span>';

 

if($previous_post) : 
               $content .=' <span class="nav-previous"><a href="'.get_permalink($previous_post->ID).'" rel="prev" data-id ="'.$previous_post->ID.'"><span class="meta-nav">&lt; </span> Previous</a></span>';

endif; 

	$content .='</nav>
  <div class="details-content act-content">
  <h3>'.get_the_title().'</h3>
  <h4>'.get_field('subtitle1').'</h4>
  <i>'.get_field('subtitle_2').'</i>
  <span>'.get_field('subtitle_3').'</span>'.$post->post_content;
$content .='<span class="v-gallery conci">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
$content.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
$content.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
$content.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
$content.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $content.='</span>';

  $content .='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="Share" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a></div>';


return $content;


}

 public function get_ID_by_slug($slug) {
	global $wpdb;
	$id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '$slug'");
	return $id;
}
public function con_page_title($id) {
	global $wpdb; 
	$pageTitle = $wpdb->get_var("SELECT meta_value FROM $wpdb->postmeta WHERE post_id = $id AND meta_key='_yoast_wpseo_title'");
	if($pageTitle=="") {
		$blog_title = get_bloginfo('name');
		$pageTitle= get_the_title($id). ' | '.$blog_title;
	}
	return $pageTitle;
}

public function con_page_desc($id) {
	global $wpdb; 
	$pageDesc = $wpdb->get_var("SELECT meta_value FROM $wpdb->postmeta WHERE post_id = $id AND meta_key='_yoast_wpseo_metadesc'");
	if($pageDesc=="") {
		$blog_title = get_bloginfo('name');
		$pageDesc= get_the_title($id). ' | '.$blog_title;
		
	}
	return $pageDesc;
}

public function get_tab_temp($p_id, $menu_id) {

	$tabs = get_all_tabs($p_id, $menu_id);
    $html .='<ul>';
		 foreach($tabs as $tab): 

			$dataClass= $tab->temp_type;

			$html .='<li><a href="#'.$tab->slug.'" data-tempType="'.$tab->promos.'" class="'.$dataClass.'">'.$tab->t_name.'</a></li>'; 

		endforeach;

    $html .='</ul>';

    $html .='<div class="all-tabs">';

		foreach($tabs as $tab):

			if($tab->type==0) {

		$html .= '<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
		$html .='<h3>'.$tab->header;
				if($tab->sub_header){ 
				$html.='<br>'.$tab->sub_header;
				}
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']);
		$html.='</div>';

	} elseif($tab->type==1) { 

		$html.='<div id="'.$tab->slug.'" class="events tabs_div" >';
			if($tab->header): 
			$html .='<h3>'.$tab->header;
			if($tab->sub_header){
			$html .='<br>'.$tab->sub_header;
			}
			$html .='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$html .=wpautop($post['post_content']); 
		$html .='</div>';

		}

		elseif($tab->type==2) {

		$html .='<div id="'.$tab->slug.'" class="events tabs_div">';

		if($tab->post_type=='seasonalevents') {

			$html .='<p class="all-events"><a href="'.site_url().'/concierge/seasonal-events/?property='.$p_id.'">all events</a><a href="'.site_url().'/concierge/concierge-calender/?property='.$p_id.'">daily calendar</a></p>';

		 } 

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){ 
			$html.='<br>'.$tab->sub_header;
			} 
			$html.='</h3>';
			endif;

		$args['post_type']='seasonalevents';
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		$args[ 'meta_value'] = $p_id;



		$activities = new WP_Query( $args );

while ( $activities->have_posts() ) : $activities->the_post();

    $html.='<div>';

      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';

		$newcontent= get_the_content(); 

      	$html.='<p>'.cr_trim_chars( $newcontent, 460 ).'... <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">READ MORE</a></p>';

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

			$text =  get_field('button_1');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			$html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif; 
		if(get_field('button_2')):

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			$html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			$html .='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')):

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			$html.= '<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;

        $html.= '</span>';
       $html.= '</p>';

      $html.='</div>';

	 endwhile; 

	$html.='</div>';		

	} 

	endforeach;
    $html.='</div>';

	return $html;
}
	public function get_promo_temp($p_id, $menu_id) {
		
		$promo = $this->get_nav_details($p_id, $menu_id, 'promos');

		$html = get_promo_container($promo[0]->promos); 
		return $html;

}
	public function get_nav_details($p_id, $menu_id, $key) {

		global $wpdb; 
   		$res = $wpdb->get_results("SELECT {$key} FROM $wpdb->hotelsnav WHERE nid=$menu_id AND hotel_id=$p_id order by menu_order");

		return $res;

	}

	public function get_event_byDate($date, $month, $year) { 
global $wpdb;

		$date_string = $year.'-'.$month.'-'.$date;
		$post_ids = array();
		
		$multi_events = $wpdb->get_results("select * FROM cr_em_events where event_status=1 AND multiple=1 AND multiple_dates LIKE '%{$date_string}%'");


		foreach($multi_events as $multi_event) {
			$post_ids[] = $multi_event->post_id;
		}

	$date = $year.'-'.$month.'-'.$date;

	$events = $wpdb->get_results("select * FROM cr_em_events where event_status=1 AND multiple=0 AND (STR_TO_DATE( `event_end_date`, '%Y-%m-%d%' ) >= STR_TO_DATE('$date', '%Y-%m-%d%')) ORDER BY event_start_date");


		$matching_days = array(); 
		foreach ($events as $event) {

$start_date = strtotime($event->event_start_date);

		$end_date = strtotime($date);
				
		//$last_date = strtotime($last);

		//if($end_date > $last_date) {
		//	$end_date = $last_date;

		//}
		
 
		$aDay = 86400;  // a day in seconds
		$aWeek = $aDay * 7;	
	if(!$event->recurrence_interval || $event->recurrence_interval ==0) {

$event->recurrence_interval=1;
			}

	switch ( $event->recurrence_freq ){
		
		case 'monthly':

$current_arr = getdate($start_date);
	
			$end_arr = getdate($end_date);

				$end_month_date = strtotime( date('Y-m-t', $end_date) ); //End date on last day of month

				$current_date = strtotime( date('Y-m-1', $end_date) ); //Start date on first day of month
				
				
			//go through the events and put them into a monthly array
			$day = $event->recurrence_byday;


				while( $current_date <= $end_month_date ){ 
					$last_day_of_month = date('t', $current_date);
					//Now find which day we're talking about
					$current_week_day = date('w',$current_date);
					$matching_month_days = array();
					//Loop through days of this years month and save matching days to temp array
					for($day = 1; $day <= $last_day_of_month; $day++){
						if((int) $current_week_day == $event->recurrence_byday){
							$matching_month_days[] = $day; 
							
						}
						$current_week_day = ($current_week_day < 6) ? $current_week_day+1 : 0;							
					}
					//Now grab from the array the x day of the month
					$matching_day = ($event->recurrence_byweekno > 0) ? $matching_month_days[$event->recurrence_byweekno-1] : array_pop($matching_month_days);
					$matching_date = strtotime(date('Y-m',$current_date).'-'.$matching_day);
					if($matching_date >= $start_date && $matching_date == $end_date){
						$matching_days[] = $matching_date; 

if(!in_array($event->post_id, $post_ids)) {

							$post_ids[]=$event->post_id;
						}
						
					}
					//add the number of days in this month to make start of next month
					$current_arr['mon'] += 1;
					if($current_arr['mon'] > 12){
						//FIXME this won't work if interval is more than 12
						$current_arr['mon'] = $current_arr['mon'] - 12;
						$current_arr['year']++;
					}
					$current_date = strtotime("{$current_arr['year']}-{$current_arr['mon']}-1"); 
				}
						
			break;
		case 'weekly':
	$weekdays = explode("_", $event->recurrence_days);

//sort out week one, get starting days and then days that match time span of event (i.e. remove past events in week 1)
				$start_of_week = get_option('start_of_week'); //Start of week depends on WordPress
				//first, get the start of this week as timestamp
				$event_start_day = date('w', $start_date); 
				//then get the timestamps of weekdays during this first week, regardless if within event range
				$start_weekday_dates = array(); //Days in week 1 where there would events, regardless of event date range
				for($i = 0; $i < 7; $i++){
					$weekday_date = $start_date+($aDay*$i); //the date of the weekday we're currently checking
					$weekday_day = date('w',$weekday_date); //the day of the week we're checking, taking into account wp start of week setting

					if( in_array( $weekday_day, $weekdays) ){

		
						

						$start_weekday_dates[] = $weekday_date; //it's in our starting week day, so add it
					}
				}					
				//for each day of eventful days in week 1, add 7 days * weekly intervals
				foreach ($start_weekday_dates as $weekday_date){

				//echo $event->recurrence_interval .'<br />';
				
					//Loop weeks by interval until we reach or surpass end date
					while($weekday_date <= $end_date){

						if( $weekday_date >= $start_date && $weekday_date == $end_date ){


							$matching_days[] = $weekday_date; 
		
if(!in_array($event->post_id, $post_ids)) {

							$post_ids[]=$event->post_id;
						
						

						} }
						$weekday_date = $weekday_date + ($aWeek *  $event->recurrence_interval);
					}
				}//done!
			

				
			break;

		

case 'daily':
				//If daily, it's simple. Get start date, add interval timestamps to that and create matching day for each interval until end date.
				$current_date = $start_date;
			$event->recurrence_interval =1;
				
				while( $current_date <= $end_date ){
						if($current_date == $end_date) {
					$matching_days[] = $current_date; 
						if(!in_array($event->post_id, $post_ids)) {

							$post_ids[]=$event->post_id;
						}
	}
					$current_date = $current_date + ($aDay * $event->recurrence_interval);
				}
				break;

		}

}

  return $post_ids;

}

public function careerfileupload() {


$job = $_POST['job_id'];
$appemail = $_POST['jobman-field-10'];


		$page = array(
				'comment_status' => 'closed',
				'ping_status' => 'closed',
				'post_status' => 'private',
				'post_type' => 'jobman_app',
				'post_content' => '',
				'post_parent' =>$job,
				'post_title' => __( 'Application', 'jobman' ),
			);

	$appid = wp_insert_post( $page );

	$dir = dirname( $_SERVER['SCRIPT_FILENAME'] );

	if( ! file_exists( "$dir/wp-admin/includes/file.php" ) )
		$dir = WP_CONTENT_DIR . '/..';
	
	require_once( "$dir/wp-admin/includes/file.php" );
	require_once( "$dir/wp-admin/includes/image.php" );
	require_once( "$dir/wp-admin/includes/media.php" );


if( is_uploaded_file( $_FILES["jobman-field-16"]['tmp_name'] ) ) {
							$data = media_handle_upload( "jobman-field-16", $appid, array( 'post_status' => 'private' ) );
							if( is_wp_error( $data ) ) {
								// Upload failed, move to next field
								$errors[] = $data;
								continue 2;
							}
							add_post_meta( $appid, 'resume', $data, false );
							add_post_meta( $data, '_jobman_attachment', 1, true );
							add_post_meta( $data, '_jobman_attachment_upload', 1, true );
					}
if( is_uploaded_file( $_FILES["jobman-field-17"]['tmp_name'] ) ) {
							$data = media_handle_upload( "jobman-field-17", $appid, array( 'post_status' => 'private' ) );
							if( is_wp_error( $data ) ) {
								// Upload failed, move to next field
								$errors[] = $data;
								continue 2;
							}
							add_post_meta( $appid, 'cover', $data, false );
							add_post_meta( $data, '_jobman_attachment', 1, true );
							add_post_meta( $data, '_jobman_attachment_upload', 1, true );
					}
if($appemail){add_post_meta( $appid, 'jobman-field-10', $appemail, false );}

	if( NULL != $job ) {
		// Get parent (job) categories, and apply them to application
		$parentcats = wp_get_object_terms( $job, 'jobman_category' );
		foreach( $parentcats as $pcat ) {
			if( is_term( $pcat->slug, 'jobman_category' ) ) {
				wp_set_object_terms( $appid, $pcat->slug, 'jobman_category', false );
				$append = true;
			}
		}
	}


	
		add_post_meta( $appid, 'apptype', 1, false );
		add_post_meta( $appid, 'job', $job, false );

	jobman_email_application( $appid );

	echo "Your application is successfully submitted";

die();
}

}
?>
