<?php

/*
 * Concierge Main Page template
*/


global $wpdb;
?>
<div id="poststuff" class="manage_hotelpage">
	 <div id="post-body" class="metabox-holder columns-2">
		<div id="post-body-content">
			<div id="hotelcustomefields" class="postbox ">

				<div class="handlediv" title="Click to toggle"><br></div>
				<h3 class="hndle"><span>Page Gallery Mapping</span></h3>

				<div class="inside">
					
					<div style="clear:both"></div>
<style>
#pagemapping td, #pagemapping th {
	text-align:left;
}
#pagemapping th {
	background-color: #CCC;
}
</style>

		<table width="100%" cellpadding="8" border="1" style="border-collapse:collapse;" id="pagemapping">

		<tr>
			<th>Property</th>
			<th>Page</th>
			<th>Gallery</th>
			<th>Action</th>
		</tr>
<?php
	$pages=$wpdb->get_results("SELECT * FROM $wpdb->hotelsnav ORDER BY hotel_id", ARRAY_A);
	
	foreach ($pages as $page) {
		$prop = mapping_property($page['hotel_id']);
		$gallery = mapping_gallery($page['gid']);
	?>
		<tr>
			<td><?php echo $prop['name']; ?></td>
			<td><?php echo $page['nav_name']; ?></td>
			<td><?php echo $gallery['gid']. ' - '.stripslashes($gallery['title']); ?></td>
			<td><a href="<?php echo site_url().'/wp-admin/admin.php?page=managehotels&hotel_id='.$page['hotel_id'].'&submenu_id='.$page[nid]; ?>">Edit</a></td>
		</tr>

<?php } ?>
</table>


				</div><!-- .inside   -->
			</div> <!-- #hotelcustomefields  -->
		</div> <!-- #post-body-content  -->
	</div> <!-- #post-body Ends  -->
</div>	<!-- #poststuff Ends  -->
<?php 
 function mapping_property($id) {
global $wpdb;
	$property=$wpdb->get_row("SELECT * FROM $wpdb->hotels WHERE id= $id", ARRAY_A);
	return $property;

}
function mapping_gallery($id) {
global $wpdb;

	$gallery=$wpdb->get_row("SELECT * FROM cr_ngg_gallery WHERE gid= $id", ARRAY_A);

   	return $gallery;

}
	?>


