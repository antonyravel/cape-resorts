<?php

global $p_id;
global $subMenu; 
global $menu; 
global $p_id;

?>


	  <?php $tabs = get_all_tabs($p_id, $menu->nid); ?>
	<?php if($subMenu !== 'gallery') { ?> 
    <div class="tabsDiv">
      <div id="tabs">
    <ul>
		<?php foreach($tabs as $tab): ?>
			<?php $dataClass= $tab->temp_type; ?> 
			<li><a href="#<? echo $tab->slug; ?>"  data-class="<?php echo $dataClass; ?>" data-promo="<? $tab->promos; ?>"><?php echo $tab->t_name; ?></a></li>
		<?php endforeach; ?>
    </ul>


    <div class="all-tabs">

		<?php foreach($tabs as $tab): ?>
	<?php if($tab->type==0) { ?>

		<div id="<? echo $tab->slug; ?>" class="tabs_div">
			<?php if($tab->header): ?>
			<h3><?php echo $tab->header; ?>
				<?php if($tab->sub_header){ ?>
				<br><?php echo $tab->sub_header; ?>
				<?php } ?>
			</h3>
			<?php endif; ?>
			<?php
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			echo wpautop($post['post_content']); ?>
		</div>
		<?php } ?>

<?php if($tab->type==1) { ?> 


		<div id="<? echo $tab->slug; ?>" class="events tabs_div" >
			<ul>

	<?php	

				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;

		 if($tab->post_type=='specialoffers') {

			 $args[ 'meta_key'] = 'property';
		$args[ 'meta_value'] = $p_id;

		 } 

		

	


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		?>
		<li class="dining-content">
		<?php	 $image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); ?>
  <div>
			  <img src="<?php echo $image_url[0]; ?>" alt="dining" />
        <div>
 <h4><?php the_title(); ?>	</h4>
         <h5><?php the_field('subtitle1'); ?></h5>
         <?php $newcontent= get_the_content(); ?>
         <p><?php echo cr_trim_chars( $newcontent, 460 ) ?></p>
         <p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="<?php the_title(); ?>" st_url="<?php echo get_permalink(); ?>"></span></a>
         <span class="v-gallery">		
			<?php if(get_field('button_1')): ?>

		<?php $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 echo "<a href='$pieces[1]' ".$onclick." target='_bank'>$pieces[0]</a>";

		?>
		<?php endif; ?>
		<?php if(get_field('button_2')): ?>

		<?php $text =  get_field('button_2');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 echo "<a href='$pieces[1]' ".$onclick." target='_bank'>$pieces[0]</a>";

		?>
		<?php endif; ?>
		<?php if(get_field('button_3')): ?>

		<?php $text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 echo "<a href='$pieces[1]' ".$onclick." target='_bank'>$pieces[0]</a>";

		?>
		<?php endif; ?>
		<?php if(get_field('button_4')): ?>

		<?php $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 echo "<a href='$pieces[1]' ".$onclick." target='_bank'>$pieces[0]</a>";

		?>
		<?php endif; ?></span>
      </p>
      </div></div>



		</li>
		<?php endwhile; ?>
			</ul>
		</div>

<?php		}

		elseif($tab->type==2) {

?>
		<div id="<? echo $tab->slug; ?>" class="events tabs_div">

		<?php if($tab->post_type=='seasonalevents') { ?>

			   <p class="all-events"><a href="<?php echo site_url().'/concierge/seasonal-events/?property='.$p_id; ?>">all events</a><a href="<?php echo site_url().'/concierge/concierge-calender/?property='.$p_id; ?>">daily calendar</a></p>

		<?php } 

		?>

			<?php if($tab->header): ?>
			<h3><?php echo $tab->header; ?>
			<?php if($tab->sub_header){ ?>
			<br><?php echo $tab->sub_header; ?>
			<?php } ?>
			</h3>
			<?php endif; ?>
			<?php

if($tab->post_type=='calender') {

				$post_type =  array( 'activities', 'seasonalevents', 'entertainment', 'stories', 'specialoffers' );

			} else {

				$post_type = $tab->post_type;
			}
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		$args[ 'meta_value'] = $p_id;



		$activities = new WP_Query( $args );

while ( $activities->have_posts() ) : $activities->the_post();
		?>

    <div>
		


      <h4><?php the_title(); ?></h4>
      <h5><?php the_field('subtitle1'); ?></h5>

		<?php $newcontent= get_the_content(); ?>

      	<p><?php echo cr_trim_chars( $newcontent, 460 ) ?>... <a href="<?php echo get_permalink(); ?>" class="readmore" data-id="<?php the_ID(); ?>">READ MORE</a></p>

      <p class="view-links">

		<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="<?php the_title(); ?>" st_url="<?php echo get_permalink(); ?>"></span></a>

        <span class="v-gallery">

		<?php if(get_field('button_1')): ?>

		<?php $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 echo "<a href='$pieces[1]' ".$onclick." target='_bank'>$pieces[0]</a>";

		?>
		<?php endif; ?>
		<?php if(get_field('button_2')): ?>

		<?php $text =  get_field('button_2');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 echo "<a href='$pieces[1]' ".$onclick." target='_bank'>$pieces[0]</a>";

		?>
		<?php endif; ?>
		<?php if(get_field('button_3')): ?>

		<?php $text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 echo "<a href='$pieces[1]' ".$onclick." target='_bank'>$pieces[0]</a>";

		?>
		<?php endif; ?>
		<?php if(get_field('button_4')): ?>

		<?php $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 echo "<a href='$pieces[1]' ".$onclick." target='_bank'>$pieces[0]</a>";

		?>
		<?php endif; ?>

        </span>
      </p>

      </div>

	<?php endwhile; ?>

		</div>		

<?php		} ?>

<?php	if($tab->type == 3) { ?>

<div class="gallery-container">
      
      <div class="gallery-images">
         <ul>


		<?php 

	

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }
    foreach( $galleries as $key => $galleryitem ){  ?>
		<li>

<?php $gallery = $nggdb->get_gallery($galleryitem['id']);
			$i=0;
			foreach($gallery as $gall) { ?>



<?php		if($i==0) { ?>
				
		<a href="#inline-<?php echo $galleryitem['id']; ?>" class="lightbox"><img src="<?php echo $gall->thumbURL; ?>" title="<?php echo $gall->description; ?>" alt="corner-img" /></a>


		  <div class="galler-text">
            <h3><?php echo $gall->title; ?></h3>
            <span><?php echo $gall->galdesc; ?></span>
            <a href="javascript:void(0);" class="view_gallery">view</a>
          </div>		

			<div style="display:none;" id="inline-<?php echo $galleryitem['id']; ?>" class="bxslider">
	<?php	}  ?>
		
				<div><img src="<?php echo $gall->imageURL; ?>" title="<?php echo $gall->description; ?>" alt="corner-img" /></img></div>

<?php	
			$i++;
			if($i==count($gallery )) { ?>	</div> <!-- .bxslider Ends --> <?php } ?>
<?			} ?>


      </li>

        
 <?php   }




 ?>


 </ul>
      

      </div>
    </div>		






	<?php  }  ?>

		<?php endforeach; ?>
    </div>
</div>      
    </div>


<h3 class="nav-single" style="display:none"></h3>

<div class="gallery-container" style="display:none;">
</div>
    
	<?php }else {
		
				 foreach($tabs as $tab) { ?>

<!-- Empty Container -->

<div class="tabsDiv" style="display: none;">

      <div id="tabs">
</div>      
    </div>
<div class="rt-bar" style="display: none;">

</div>






<h3 class="nav-single"><?php echo $tab->t_name; ?></h3>

<div class="gallery-container">
      
      <div class="gallery-images">
         <ul>


		<?php 

	

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }
    foreach( $galleries as $key => $galleryitem ){  ?>
		<li>

<?php $gallery = $nggdb->get_gallery($galleryitem['id']);
			$i=0;
			foreach($gallery as $gall) { ?>

		
<?php		if($i==0) { ?>
		<a href="<?php echo $gall->imageURL; ?>" data-rel="lightbox[<?php echo $galleryitem['id']; ?>]"><img src="<?php echo $gall->thumbURL; ?>" title="<?php echo $gall->description; ?>" alt="corner-img" /></a>

		  <div class="galler-text">
            <h3><?php echo $gall->title; ?></h3>
            <span><?php echo $gall->galdesc; ?></span>
            <a href="javascript:void(0);" class="view_gallery">view</a>
          </div>		
		<div style="display:none;">

	<?php	} ?>
			<div>
				<a href="<?php echo $gall->imageURL; ?>" data-rel="lightbox[<?php echo $galleryitem['id']; ?>]"><img src="<?php echo $gall->thumbURL; ?>" title="<?php echo $gall->description; ?>" alt="corner-img" /></a>
			</div>

<?php			

			$i++;

			if($i== count($gallery)) {
				echo '</div>';

				}

			} ?>


      </li>






	
		
        
 <?php   }













 ?>



        


 </ul>
      



      </div>
    </div>	



	<?php }	}   ?>  
    
    
    
<?php if($menu->promos != ''): ?>

	
    <div class="rt-bar">

		<?php echo get_promo_container($menu->promos); ?>
   
    </div>

  <?php endif; ?>
		<div id="contactUs" class="hide"> 
		
		 <?php get_contact_temp($p_id, 'contactus'); ?>
		
		</div> 
		<div id="directions" class="hide"> 
		
		 <?php get_address_temp($p_id, 'address');  ?>
		
		</div> 

