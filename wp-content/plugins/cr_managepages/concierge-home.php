<?php

/*
 * Concierge Home Area
*/

global $wpdb;
?>
<div id="poststuff" class="manage_hotelpage">
	 <div id="post-body" class="metabox-holder columns-2">
		<div id="post-body-content">
			<div id="hotelcustomefields" class="postbox ">

				<div class="handlediv" title="Click to toggle"><br></div>
				<h3 class="hndle"><span>Home Concierge Mapping</span></h3>

				<div class="inside">
					
					<div style="clear:both"></div>
<style>
ul li{
	cursor:move;
}
ul {
float: left;
width: 50%;
}
.inside {
	overflow:hidden;
}
</style>
<?php

	$args['post_type']= array( 'seasonalevents','activities','entertainment', 'stories', 'specialoffers' );

	$args[ 'meta_key'] = 'displayhome';
	$args[ 'meta_value'] = 'yes';
	$args['posts_per_page']=-1;
$args['post_status']='publish';
	$all_posts = get_posts($args);
	$home_posts = get_option( 'concierege_posts' );
	$home_posts = explode(",", $home_posts); 

	if(is_array($home_posts) !="") {

		foreach($all_posts as $posts) {
			if(!in_array($posts->ID, $home_posts))

				array_push($home_posts,$posts->ID);


		}	




		$args['post__in'] = $home_posts;
		$args['orderby'] = 'post__in';




	} 

$props = new WP_Query( $args ); ?>
<ul id="sortable2" class="connectedSortable" style="min-height:100px;">
<?php while ( $props->have_posts() ) : $props->the_post();

?>
	<li id="item_<?php the_ID(); ?>" ><?php the_title(); ?> - <?php   echo get_post_type( get_the_ID()); ?></li>
<?php
endwhile;
?>	

</ul>


<script>
jQuery(document).ready(function() {
  jQuery(function() {
//jQuery( "#sortable1").height(jQuery( "#sortable2").height());
    jQuery( "#sortable2" ).sortable({
    }).disableSelection();
  });

jQuery('#submitArea').bind('click', function(e) { 
		
		e.preventDefault(); 
	
	var serial = jQuery('#sortable2').sortable('serialize');
		jQuery('#home_posts').val(jQuery('#sortable2').sortable('serialize'));
	var el = jQuery(this),
		form = el.parents('form').get(0);

	var data = jQuery(form).serialize(); 
	jQuery.ajax({
		url: ajaxurl,
		type: "POST",
		data: data,
		error: function(){
		alert("Theres an error with AJAX");
	},
		success: function(){
		alert("Successfully updated");
	}

});

});
});

</script>
</div>
<div id=""><form><button id="submitArea" style="cursor:pointer;">Update</button><input type="hidden" name="item" id="home_posts"/><input name="action" type="hidden" value="conciergehomepost"/></form></div>
</div></div></div></div>

