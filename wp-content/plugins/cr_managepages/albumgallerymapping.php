<?php

/*
 * Concierge Main Page template
*/


global $wpdb;
?>
<div id="poststuff" class="manage_hotelpage">
	 <div id="post-body" class="metabox-holder columns-2">
		<div id="post-body-content">
			<div id="hotelcustomefields" class="postbox ">

				<div class="handlediv" title="Click to toggle"><br></div>
				<h3 class="hndle"><span>Album Gallery Mapping</span></h3>

				<div class="inside">
					
					<div style="clear:both"></div>
<style>
#pagemapping td, #pagemapping th {
	text-align:left;
}
#pagemapping th {
	background-color: #CCC;
}
</style>

		<table width="100%" cellpadding="8" border="1" style="border-collapse:collapse;" id="pagemapping">

		<tr>
			<th>Album</th>
			<th>Property</th>
			<th>Galleries</th>
		</tr>
<?php
	$albums=$wpdb->get_results("SELECT * FROM cr_ngg_album ORDER BY id ASC", ARRAY_A);
	global $nggdb;
  // 

	foreach ($albums as $album) {
	$galleries = unserialize($album['sortorder']);
	$prop = mapping_property($album['hotel_id']);
	?>
		<tr>
			<td><?php echo $album['id'] .' - '. $album['name']; ?></td>

			<td><?php echo $prop['name']; ?></td>


			<td>
<?php


  	foreach( $galleries as $key => $galleryitem ){ 

 		$gallery = mapping_gallery($galleryitem);
		
		if($gallery['title']) {
		 	echo $galleryitem .' - '. stripslashes($gallery['title']) .'<br /><br />'; 
		}
}
	?>

			</td>
		</tr>

<?php } ?>
</table>


				</div><!-- .inside   -->
			</div> <!-- #hotelcustomefields  -->
		</div> <!-- #post-body-content  -->
	</div> <!-- #post-body Ends  -->
</div>	<!-- #poststuff Ends  -->
<?php 
 function mapping_property($id) {
global $wpdb;
	$property=$wpdb->get_row("SELECT * FROM $wpdb->hotels WHERE id= $id", ARRAY_A);
	return $property;

}
function mapping_gallery($id) {
global $wpdb;

	$gallery=$wpdb->get_row("SELECT * FROM cr_ngg_gallery WHERE gid= $id", ARRAY_A);

   	return $gallery;

}
	?>


