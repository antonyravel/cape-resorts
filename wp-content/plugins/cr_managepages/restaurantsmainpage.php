<?php

/*
 * Concierge Main Page template
*/


global $wpdb;
$title = "Restaurants Pages";
$sub_title = "Global options";

add_action( 'admin_footer', 'admin_scripts_footer' );

function admin_scripts_footer() {
?>
<script type="text/javascript">
  //<![CDATA[
var $ = jQuery;
  jQuery(document).ready(function($) {
var appendEl, points;
	$('.jquery_tabs').tabs({ fx: { opacity: 'toggle' }});
	$('#add_points').bind('click', function(e) {
	e.preventDefault();
	if($('.route').length > 0) {
		appendEl = $('.route');
		points = appendEl.length+1;
	}
	else {
		appendEl = $('#specific_points');
		points=1;
	}
	appendEl.last().after('<tr class="route" id="route_'+points+'"><td align="left" colspan="2"><table style="width:95%;" data-count="'+points+'"><tr><td align="left" colspan="2"><h4>Route</h4></td></tr><tr><td align="left"><label for="from">From</label></td><td><input size="40" type="text" name="points['+points+'][from]"></td></tr><tr><td align="left"><label for="distance">Time</label></td><td><input size="40" type="text" name="points['+points+'][distance]" ></td></tr><tr><td align="left"><label for="distance">Route</label></td><td><textarea rows="6" cols="60" name="points['+points+'][route]"></textarea></td></tr><tr><td colspan="2"><span><button class="removeroute" data-id="'+points+'">Remove</button></span></td></tr></table></td></tr>');
	})

	$('.removeroute').live('click', function() {
		var rEl=$(this).attr('data-id');
		$('#route_'+rEl).remove();
	});
//$("#table-1").tableDnD();
$("#table-1").tableDnD({
onDrop: function(table, row) {
  $.tableDnD.serialize();

$.ajax({
 type: "POST",
 url: "draganddrop.php",
 data: $.tableDnD.serialize(),
 success: function(html){
 }
});
	    }
	});
 	});

	 function change_hotel(el)
	{  
		// var pageSelect = document.getElementById('pages_select'),
		var location =el.value;
		if( location != '' ) {
				window.location.href= location;
			} 
	}

  //]]>
</script>
<?php
}
if($_GET['hotel_id'] || $_GET['submenu_id']) {
	$h_id = $_GET['hotel_id'];

if($_GET['submenu_id']) {

	$h_sub_id= $_GET['submenu_id'];
	$hotel_tabs=$wpdb->get_results("SELECT * FROM $wpdb->hotelsnav WHERE nid=$h_sub_id", ARRAY_A);
	$sub_title = $hotel_tabs[0]['nav_name'];
	$gallery_id = $hotel_tabs[0]['gid'];  
	$sub_title;
	}

	$hotel_navs=$wpdb->get_results("SELECT t1.*, t2.* FROM $wpdb->hotels AS t1 INNER JOIN $wpdb->hotelsnav AS t2 ON t1.id = t2.hotel_id WHERE t1.id=$h_id", ARRAY_A);
	$title ="Restaurants Page " .$hotel_navs[0]['name']; 
	
}
?>
<div id="poststuff" class="manage_hotelpage">
	 <div id="post-body" class="metabox-holder columns-2">
		<div id="post-body-content">
			<div id="hotelcustomefields" class="postbox ">

				<div class="handlediv" title="Click to toggle"><br></div>
				<h3 class="hndle"><span><?php echo $title; ?></span></h3>

				<div class="inside">
					
					<div style="clear:both"></div>

<?php 
if($h_id) {
?>
<div id="hotel_global">
		
		<?php echo get_hotelNav($hotel_navs); ?>

	<h2 class="sub_title"><?php echo $sub_title; ?></h2>
<?php if($h_sub_id) { 
		include_once('hotel_sub_nav_restaurants.php');   
		menuManagement::loadingPage($h_id, $h_sub_id, $gallery_id);	

	?>
<?php } else { ?>
<div id="contact_tabs" class="jquery_tabs">
	<ul>
		<li><a href="#directions"><span>Address</span></a></li>
		<li><a href="#social_links"><span>Social Links</span></a></li>
		<li><a href="#contactus"><span>Contact Us</span></a></li>
		<li><a href="#logo_phone"><span>Hotel Logo</span></a></li>
	</ul>



		<div id="directions">

			<?php $address = get_globalvalues($h_id, 'address');  ?>
			<h2>Address</h2>
			<form id="direction_form">
			<input type="hidden" name="hotel_id" value="<?php echo $h_id; ?>">
			<input type="hidden" name="action" value="address">
			<input type="hidden" name="field_value" value="address">
			<table width="100%" cellpadding="8">
				<tr>
					<td><label for="addressline1">Address Line1</label></td>
					<td align="left"><input size="40" type="text" value="<?php echo $address->addressline1; ?>" name="addressline1" /></td>
				</tr>
				<tr>
					<td><label for="addressline2">Address Line2</label></td>
					<td align="left"><input size="40" type="text" value="<?php echo $address->addressline2; ?>" name="addressline2" /></td>
				</tr>
				<tr>
					<td><label for="addressline3">City</label></td>
					<td align="left"><input size="40" type="text" name="city"  value="<?php echo $address->city; ?>" /></td>
				</tr>
				<tr>
					<td><label for="addressline3">State</label></td>
					<td align="left"><input size="40" type="text" name="state" value="<?php echo $address->state; ?>" /></td>
				</tr>
				<tr>
					<td><label for="addressline3">Zipcode</label></td>
					<td align="left"><input size="40" type="text" name="zipcode" value="<?php echo $address->zipcode; ?>" /></td>
				</tr>
				<tr>
					<td><label for="email">Phone</label></td>
					<td align="left"><input size="40" type="text" id="email" name="phone" value="<?php echo $address->phone; ?>" /></td>
				</tr>
				<tr>
					<td colspan="2"><h2>Directions</h2></td>
				</tr>

				<tr>
					<td><label for="google_map">Google map Link</label></td>
					<td align="left"><input size="40" type="text" id="google_map" name="google_map"  value="<?php echo $address->google_map; ?>"/>						</td>
				</tr>
				<tr id="specific_points">
					<td align="left" ><h4>Route</h4></td>
					<td align="left"><button type="submit" id="add_points" name="add_points" >Add new route</button></td>
				</tr>

<?php
				$count =1;
				foreach($address->points as $point) {
			?>
				<tr class="route" id="route_<?php echo $count; ?>"><td align="left" colspan="2"><table style="width:95%;" data-count="<?php echo $count; ?>"><tr><td align="left" colspan="2"><h4>Route</h4></td></tr><tr><td align="left"><label for="from">From</label></td><td><input size="40" type="text" name="points[<?php echo $count; ?>][from]" value="<?php echo $point->from; ?>"></td></tr><tr><td align="left"><label for="distance">Time</label></td><td><input size="40" type="text" name="points[<?php echo $count; ?>][distance]" value="<?php echo $point->distance; ?>" ></td></tr><tr><td align="left"><label for="distance">Route</label></td><td><textarea rows="6" cols="60" name="points[<?php echo $count; ?>][route]"><?php echo stripslashes($point->route); ?></textarea></td></tr><tr><td colspan="2"><span><button class="removeroute" data-id="<?php echo $count; ?>">Remove</button></span></td></tr></table></td></tr>
	<?php
	$count++;
	}

			?>


				
				<tr>			
					<td colspan="2">
						<h4>Find Us&nbsp;&nbsp;&nbsp;<input size="40" type="text" value="<?php echo $address->find_us_text; ?>" name="find_us_text" /></h4>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<?php the_editor($address->find_us, $id = 'find_us', $prev_id = 'addressline3', $media_buttons = true, $tab_index = 2);
 ?>
					</td>
				</tr>


				<tr><td align="left" colspan="2"><button class="submit_details">Save</button><span class="loader">Processing..</span></td></tr>
			</table>
			</form>
		</div> <!-- #directions Ends -->

		<div id="social_links">
				<?php $links = get_globalvalues($h_id, 'social_links');  ?>
			<h2>Address</h2>
			<form id="social_form">
			<input type="hidden" name="hotel_id" value="<?php echo $h_id; ?>">
			<input type="hidden" name="action" value="address">
			<input type="hidden" name="field_value" value="social_links">
			<table width="100%" cellpadding="8">
				<tr>
					<td><label for="trip_advisor">Trip advisor</label></td>
					<td align="left"><input size="40" type="text" name="trip_advisor" value="<?php echo $links->trip_advisor; ?>"/></td>
				</tr>
				<tr>
					<td><label for="youtube">You tube</label></td>
					<td align="left"><input size="40" type="text" name="youtube" value="<?php echo $links->youtube; ?>" /></td>
				</tr>
				<tr>
					<td><label for="facebook">Facebook</label></td>
					<td align="left"><input size="40" type="text" name="facebook" value="<?php echo $links->facebook; ?>"/></td>
				</tr>
				<tr>
					<td><label for="pinterest">Twitter</label></td>
					<td align="left"><input size="40" type="text" name="twitter" value="<?php echo $links->twitter; ?>" /></td>
				</tr>
				<tr>
					<td><label for="pinterest">Pinterest</label></td>
					<td align="left"><input size="40" type="text" name="pinterest" value="<?php echo $links->pinterest; ?>" /></td>
				</tr>
				<tr><td align="left" colspan="2"><button class="submit_details">Save</button><span class="loader">Processing..</span></td></tr>
			</table>
		</form>
		</div> <!-- #social_links Ends -->


		<div id="contactus">
				<?php $contacts = get_globalvalues($h_id, 'contactus');  ?>
			<form id="contactus_form">
			<input type="hidden" name="hotel_id" value="<?php echo $h_id; ?>">
			<input type="hidden" name="action" value="address">
			<input type="hidden" name="field_value" value="contactus">
			<table width="100%" cellpadding="8">
				<tr>
					<td colspan="2"><h4>Contact1</h2></td>
				</tr>
			
				<tr>
					<td><label for="position">Position:</label></td>
					<td align="left"><input size="40" type="text" name="contact[0][position]" value="<?php echo $contacts->contact[0]->position; ?>"/></td>
				</tr>
				<tr>
					<td><label for="Name">Name:</label></td>
					<td align="left"><input size="40" type="text" name="contact[0][name]" value="<?php echo $contacts->contact[0]->name; ?>" /></td>
				</tr>
				<tr>
					<td><label for="contact_number">Contact Number:</label></td>
					<td align="left"><input size="40" type="text" name="contact[0][contact_number]" value="<?php echo $contacts->contact[0]->contact_number; ?>" /></td>
				</tr>
				<tr>
					<td><label for="email_id">Email:</label></td>
					<td align="left"><input size="40" type="text" name="contact[0][email_id]" value="<?php echo $contacts->contact[0]->email_id; ?>" /></td>
				</tr>

				<tr>
					<td colspan="2"><h4>Contact2</h2></td>
				</tr>
			
				<tr>
					<td><label for="position">Position:</label></td>
					<td align="left"><input size="40" type="text" name="contact[1][position]" value="<?php echo $contacts->contact[1]->position; ?>" /></td>
				</tr>
				<tr>
					<td><label for="Name">Name:</label></td>
					<td align="left"><input size="40" type="text" name="contact[1][name]" value="<?php echo $contacts->contact[1]->name; ?>"/></td>
				</tr>
				<tr>
					<td><label for="contact_number">Contact Number:</label></td>
					<td align="left"><input size="40" type="text" name="contact[1][contact_number]" value="<?php echo $contacts->contact[1]->contact_number; ?>"/></td>
				</tr>
				<tr>
					<td><label for="email_id">Email:</label></td>
					<td align="left"><input size="40" type="text" name="contact[1][email_id]" value="<?php echo $contacts->contact[1]->email_id; ?>" /></td>
				</tr>
				<tr>
					<td colspan="2"><h4>Contact3</h2></td>
				</tr>
			
				<tr>
					<td><label for="position">Position:</label></td>
					<td align="left"><input size="40" type="text" name="contact[2][position]" value="<?php echo $contacts->contact[2]->position; ?>" /></td>
				</tr>
				<tr>
					<td><label for="Name">Name:</label></td>
					<td align="left"><input size="40" type="text" name="contact[2][name]" value="<?php echo $contacts->contact[2]->name; ?>"/></td>
				</tr>
				<tr>
					<td><label for="contact_number">Contact Number:</label></td>
					<td align="left"><input size="40" type="text" name="contact[2][contact_number]" value="<?php echo $contacts->contact[2]->contact_number; ?>" /></td>
				</tr>
				<tr>
					<td><label for="email_id">Email:</label></td>
					<td align="left"><input size="40" type="text" name="contact[2][email_id]" value="<?php echo $contacts->contact[2]->email_id; ?>" /></td>
				</tr>
				<tr>
					<td colspan="2"><h4>Contact4</h2></td>
				</tr>
			
				<tr>
					<td><label for="position">Position:</label></td>
					<td align="left"><input size="40" type="text" name="contact[3][position]" value="<?php echo $contacts->contact[3]->position; ?>" /></td>
				</tr>
				<tr>
					<td><label for="Name">Name:</label></td>
					<td align="left"><input size="40" type="text" name="contact[3][name]" value="<?php echo $contacts->contact[3]->name; ?>"/></td>
				</tr>
				<tr>
					<td><label for="contact_number">Contact Number:</label></td>
					<td align="left"><input size="40" type="text" name="contact[3][contact_number]" value="<?php echo $contacts->contact[3]->contact_number; ?>" /></td>
				</tr>
				<tr>
					<td><label for="email_id">Email:</label></td>
					<td align="left"><input size="40" type="text" name="contact[3][email_id]" value="<?php echo $contacts->contact[3]->email_id; ?>"/></td>
				</tr>


				<tr>
					<td colspan="2"><h4>Contact5</h2></td>
				</tr>
			
				<tr>
					<td><label for="position">Position:</label></td>
					<td align="left"><input size="40" type="text" name="contact[4][position]" value="<?php echo $contacts->contact[4]->position; ?>"/></td>
				</tr>
				<tr>
					<td><label for="Name">Name:</label></td>
					<td align="left"><input size="40" type="text" name="contact[4][name]" value="<?php echo $contacts->contact[4]->name; ?>"/></td>
				</tr>
				<tr>
					<td><label for="contact_number">Contact Number:</label></td>
					<td align="left"><input size="40" type="text" name="contact[4][contact_number]" value="<?php echo $contacts->contact[4]->contact_number; ?>"/></td>
				</tr>
				<tr>
					<td><label for="email_id">Email:</label></td>
					<td align="left"><input size="40" type="text" name="contact[4][email_id]" value="<?php echo $contacts->contact[4]->email_id; ?>"/></td>
				</tr>

				<tr>
					<td colspan="2"><h4>Contact6</h2></td>
				</tr>
			
				<tr>
					<td><label for="position">Position:</label></td>
					<td align="left"><input size="40" type="text" name="contact[5][position]" value="<?php echo $contacts->contact[5]->position; ?>" /></td>
				</tr>
				<tr>
					<td><label for="Name">Name:</label></td>
					<td align="left"><input size="40" type="text" name="contact[5][name]" value="<?php echo $contacts->contact[5]->name; ?>"/></td>
				</tr>
				<tr>
					<td><label for="contact_number">Contact Number:</label></td>
					<td align="left"><input size="40" type="text" name="contact[5][contact_number]" value="<?php echo $contacts->contact[5]->contact_number; ?>"/></td>
				</tr>
				<tr>
					<td><label for="email_id">Email:</label></td>
					<td align="left"><input size="40" type="text" name="contact[5][email_id]" value="<?php echo $contacts->contact[5]->email_id; ?>" /></td>
				</tr>

				
				<tr>
					<td colspan="2"><h4>Inquiry1</h2></td>
				</tr>
			
				<tr>
					<td><label for="position">Position:</label></td>
					<td align="left"><input size="40" type="text" name="inquiry[0][position]" value="<?php echo $contacts->inquiry[0]->position; ?>"/></td>
				</tr>
				<tr>
					<td><label for="Name">Name:</label></td>
					<td align="left"><input size="40" type="text" name="inquiry[0][name]" value="<?php echo $contacts->inquiry[0]->name; ?>"/></td>
				</tr>
				<tr>
					<td><label for="contact_number">Contact Number:</label></td>
					<td align="left"><input size="40" type="text" name="inquiry[0][contact_number]" value="<?php echo $contacts->inquiry[0]->contact_number; ?>"/></td>
				</tr>
				<tr>
					<td><label for="email_id">Email:</label></td>
					<td align="left"><input size="40" type="text" name="inquiry[0][email_id]" value="<?php echo $contacts->inquiry[0]->email_id; ?>"/></td>
				</tr>
				<tr>
				<td v-align="top"><label for="email_id">Inquiry:</label></td>

				<td align="left">
						<span style="padding-right: 10px;">Button Text</span><input size="40" type="text" name="inquiry[0][inquiry_text]" value="<?php echo $contacts->inquiry[0]->inquiry_text; ?>"/><br /><br />
						<span style="padding-right: 54px;">Link</span><input size="70" type="text" name="inquiry[0][inquiry_link]" value="<?php echo $contacts->inquiry[0]->inquiry_link; ?>"/>
				</td>
				</tr>
				<tr>
					<td colspan="2"><h4>Inquiry2</h2></td>
				</tr>
			
				<tr>
					<td><label for="position">Position:</label></td>
					<td align="left"><input size="40" type="text" name="inquiry[1][position]" value="<?php echo $contacts->inquiry[1]->position; ?>"/></td>
				</tr>
				<tr>
					<td><label for="Name">Name:</label></td>
					<td align="left"><input size="40" type="text" name="inquiry[1][name]" value="<?php echo $contacts->inquiry[1]->name; ?>"/></td>
				</tr>
				<tr>
					<td><label for="contact_number">Contact Number:</label></td>
					<td align="left"><input size="40" type="text" name="inquiry[1][contact_number]" value="<?php echo $contacts->inquiry[1]->contact_number; ?>"/></td>
				</tr>
				<tr>
					<td><label for="email_id">Email:</label></td>
					<td align="left"><input size="40" type="text" name="inquiry[1][email_id]" value="<?php echo $contacts->inquiry[1]->email_id; ?>"/></td>
				</tr>
				<tr>
					<td v-align="top"><label for="email_id">Inquiry:</label></td>

					<td align="left">
						<span style="padding-right: 10px;">Button Text</span><input size="40" type="text" name="inquiry[1][inquiry_text]" value="<?php echo $contacts->inquiry[1]->inquiry_text; ?>"/><br /><br />
						<span style="padding-right: 54px;">Link</span><input size="70" type="text" name="inquiry[1][inquiry_link]" value="<?php echo $contacts->inquiry[1]->inquiry_link; ?>"/>
					</td>
				</tr>


				<tr>
					<td colspan="2"><h4>Phone Numbers</h2></td></td>
				</tr>
				<tr>
					<td><label for="phone1">Phone1</label></td>
					<td align="left"><input size="40" type="text" name="phone1" value="<?php echo $contacts->phone1; ?>"/></td>
				</tr>
				<tr>
					<td><label for="phone2">Phone2</label></td>
					<td align="left"><input size="40" type="text" name="phone2" value="<?php echo $contacts->phone2; ?>" /></td>
				</tr>


			</table>

				<div><button class="submit_details">Save</button><span class="loader">Processing..</span></div>
			</form>
		</div> <!-- #contactus Ends -->


		<div id="logo_phone">
				<?php $logo = get_globalvalues($h_id, 'logo_phone');  ?>
			<h2>Address</h2>
			<form id="logophone_form">
			<input type="hidden" name="hotel_id" value="<?php echo $h_id; ?>">
			<input type="hidden" name="action" value="address">
			<input type="hidden" name="field_value" value="logo_phone">
			<table width="100%" cellpadding="8">
				<tr>
					<td scope="row" valign="top"><label for="logo_url">Enter Hotel Logo</label></td>
					<td><input type="text" id="logo_url" name="logo_url" size="36" value="<?php echo $logo->logo_url; ?>" />
						<input class="upload-button" type="button" value="Upload Logo" />
						<div><img src="<?php echo $logo->logo_url; ?>" alt="Logo" /></div>

					</td>
				</tr>
				<tr>
					<td><label for="reservation_number">Reservation Phone number</label></td>
					<td align="left"><input size="40" type="text" name="reservation_number" value="<?php echo $logo->reservation_number; ?>" /></td>
				</tr>

				<tr><td align="left" colspan="2"><button class="submit_details">Save</button><span class="loader">Processing..</span></td></tr>
			</table>
		</form>
		</div> <!-- #social_links Ends -->





		</div> <!-- #contact_tabs Ends -->
		<?php } ?>

</div> <!-- #hotel_global Ends -->

<?php }
else {
$hotels = $wpdb->get_results("select id, name, slug from $wpdb->hotels where type='restaurant' order by id asc",ARRAY_A);
?>
					<table width="100%" cellpadding="8">
						<tr>
							<td align="left">Select Pages</td>
							<td align="left">
							<select id="pages_select" name="hotels" onchange="change_hotel(this)">
							<option value="">Select Page</option>
							<?php 

							foreach($hotels as $hotel){
							?>
							<option value="<?php echo site_url().'/wp-admin/admin.php?page=restaurantspages&hotel_id='. $hotel['id']; ?>"><?php echo $hotel['name']; ?></option>
							<?php
							}
							?>
							</select>
							</td>
							<!-- <td align="left"><button id="addconcierge" onclick="change_hotel()">Add/Edit</button></td> -->
						</tr>

					</table>
<?php } ?>

				</div><!-- .inside   -->
			</div> <!-- #hotelcustomefields  -->
		</div> <!-- #post-body-content  -->
	</div> <!-- #post-body Ends  -->
</div>	<!-- #poststuff Ends  -->



<?php /* Functions  */   

function get_hotelNav($navs) {
          global $wpdb;
          $hotels = $wpdb->get_results("select id, name, slug from $wpdb->hotels where type='restaurant' order by id asc",ARRAY_A);
          ?>

					<table width="100%" cellpadding="8">
					  <tr>
							<td align="left">Select Pages</td>
							<td align="left">
							<select id="pages_select" name="hotels" onchange="change_hotel(this)">
							<option value="">Select Page</option>
							<?php 
							foreach($hotels as $hotel){
							if($hotel['id']  == $_GET['hotel_id']){ $hsl = 'selected="selected"'; }else{$hsl ='';}
							?>
							<option <?php echo $hsl;?> value="<?php echo site_url().'/wp-admin/admin.php?page=restaurantspages&hotel_id='. $hotel['id']; ?>"><?php echo $hotel['name']; ?></option>
							<?php
							}
							?>
							</select>
							</td>
							<!-- <td align="left"><button id="addconcierge" onclick="change_hotel()">Add/Edit</button></td> -->
						</tr>
						<tr>
							<td align="left">Select Submenus</td>
							<td align="left">
							<select id="pages_select" name="hotels" onchange="change_hotel(this)">
							<option value="">Select Sub Menu</option>
							<?php 
							$current_nav=$_GET['submenu_id']; 
							foreach($navs as $nav){
								$selected = $current_nav == $nav['nid'] ? 'selected' : ''; 
								
							?>
							<option <?php echo $selected; ?> value="<?php echo site_url().'/wp-admin/admin.php?page=restaurantspages&hotel_id='. $nav[id].'&submenu_id='.$nav[nid]; ?>"><?php echo $nav['nav_name']; ?></option>
							<?php
							}
							?>
							</select>
							</td>
							<!-- <td align="left"><button id="addconcierge" onclick="change_hotel()">Add/Edit</button></td> -->
						</tr>

					</table>
<?php }

?>
