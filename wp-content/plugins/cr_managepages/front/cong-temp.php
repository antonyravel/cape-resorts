<?php

class Congresshall {
	
    public function Congresshall() {


    }
    public function Congresshall_home($menu, $p_id, $subMenu) {
	$html ='';
	$tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv home ">';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):

		$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']); 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}
		elseif($tab->type==2) {


		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

		$html.='<p class="all-events"><a href="'.site_url().'/concierge/capemay/seasonalevents/">all events</a><a href="'.site_url().'/concierge/capemay/dailycalender/">daily calendar</a></p>';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type = $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();
	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		

		} 

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
    }
   public function Congresshall_accommodations($menu, $p_id, $subMenu) {
		$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']); 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==1) {



	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  $html.='<div>';
			$html.='<img src="'.$image_url[0].'" alt="dining" />';
        $html.='<div>';
 $html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		} 

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
}


  public function Congresshall_resortlife($menu, $p_id, $subMenu) {

	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);

		$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']); 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}
		elseif($tab->type==1) {



	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  $html.='<div>';
			$html.='<img src="'.$image_url[0].'" alt="dining" />';
        $html.='<div>';
 $html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		} 

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;

}

  public function Congresshall_dining($menu, $p_id, $subMenu) {


	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);

		$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']); 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}


		elseif($tab->type==1) {


	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  $html.='<div>';
			$html.='<img src="'.$image_url[0].'" alt="dining" />';
        $html.='<div>';
 $html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		} 


		elseif($tab->type==2) {

		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type = $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();

	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  $html.='<div>';
			$html.='<img src="'.$image_url[0].'" alt="dining" />';
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div></div>';


		$html.='</li>';
}
		 endwhile; 
			$html.='</ul>';
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		


		} 

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
}
  public function Congresshall_gallery($menu, $p_id, $subMenu) {
   $tabs = get_all_tabs($p_id, $menu->nid);  
	$html.='';
		 foreach($tabs as $tab) { 

		//$html.='<h3 class="nav-single">'.$tab->t_name.'</h3>';

$html.='<div class="gallery-container">';
      
		$html.='<h3 class="nav-single">'.$tab->t_name.'</h3>';

      $html.='<div class="gallery-images">';
         $html.='<ul>';

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }
    foreach( $galleries as $key => $galleryitem ){ 

 $gallery = $nggdb->get_gallery($galleryitem['id']);

if(count($gallery) > 0) {
	 $html.='<li>';


			$i=0;
			foreach($gallery as $gall) { 
$gall->slug = str_replace("-","",$gall->slug);
		if($i==0) { 
		$html.='<a class="pretty_photo" rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'"><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a>';

		  $html.='<div class="galler-text">';
            $html.='<h3>'.$gall->title.'</h3>';
            $html.='<span>'.$gall->galdesc.'</span>';
            $html.='<a rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'" class="view_gallery pretty_photo">view</a>';
         $html.='</div>';		
$html.='<div style="display:none"><div class="overall-popup" id="'.$gall->slug.'-gallery">';
$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';


$html.='<div class="gallerydet"><h3>'.$gall->title.'</h3>';
           $html.='<span>'.$gall->galdesc.'</span></div>';
		} $i++;} 
$html.='<div class="gallery_popup" id="gallery_popup'.$gall->slug.'">';
$html.='<ul class="bxslider bxslider'.$gall->slug.'">';
foreach($gallery as $gall) { 
  $html.='<li><a target="_bank" href="//pinterest.com/pin/create/button/?url='.get_permalink().'/gallery&media='.$gall->imageURL.'&description='.$gall->description.'" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" class="pint" /></a>

<img src="'.$gall->imageURL.'" title="'.$gall->description.'" alt="corner-img" /></li>';
 } 
$html.='</ul>';
$html.='</div>';
$html.='<ul class="bx-pager-images jcarousel jcarousel-skin-tango" id="bx-pager'.$gall->slug.'">';
$i=0; foreach($gallery as $gall) { 
  $html.='<li><a data-slide-index="'.$i.'" href=""><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a></li>';
 $i++; } 
 $html.='</ul>';
$html.='</div>';
$html.='</li>';
}
  }

 $html.='</ul>

      </div>
    </div>';	
	}
return $html;
	}

  public function Congresshall_seaspa($menu, $p_id, $subMenu) {

	$html ='';
	$tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']); 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}
		


		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;


} 

public function Congresshall_specials($menu, $p_id, $subMenu) {   

		$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv specials_nav" >';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li style="background: none;
border-bottom-color: transparent;
border-right: 0;"><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']); 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==2) {
 

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= $post_type;
			$args['posts_per_page']=-1;
	
			 $args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

	
$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();

	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}

if($reOut) {
		
		$html.='<li class="dining-content" id="'.get_field('special_offer_title').'">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ if($p_id==17){$html.='';}else{$html.='<img src="'.$image_url[0].'" alt="meeting" />';} }
        $html.='<div>';
 $html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 300;}

         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';
         $html.='<span class="v-gallery">';


			$html .=get_book_button(get_the_ID(), $p_id);


		$html.='</span>';
       $html.='</p>';
       $html.='</div></div>';



		$html.='</li>';
			}
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		} 

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
}

public function Congresshall_meetings($menu, $p_id, $subMenu) {   

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);

		$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="meeting">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 

}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==1) {



	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="meeting-content meeting_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
 	$html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';	

$html .='<span class="v-gallery">';

	if(get_field('capacitychartlabel')){ $capacitychartlabel =  get_field('capacitychartlabel');  }
	else {
		$capacitychartlabel='VIEW CAPACITY CHART';
	}
	if(trim(get_field('capacity_chart_image')) !=""){  
		$capacity_chart_image = get_field('capacity_chart_image');
		$html .='<a class="ajax capacity-chart" href="'.$capacity_chart_image.'" title="'.get_the_title().'">'.$capacitychartlabel.'</a>';
 }
if(trim(get_field('meeting_specialist')) !="") { 
	$html .='<a class="weddingspecialist" href="'.get_field('meeting_specialist').'" title="'.get_the_title().'">CONTACT A MEETINGS SPECIALIST</a>';
}
$html .='</span>';
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		}elseif($tab->type==2) {

		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type =  $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();
	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		


		} elseif($tab->type==3){
$html.='<div id="'.$tab->slug.'" class="events tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
			        $html.='<ul>';

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }

    foreach( $galleries as $key => $galleryitem ){ 


 $gallery = $nggdb->get_gallery($galleryitem['id']);


	if(count($gallery) > 0) {
	 $html.='<li>';
			$i=0;
			foreach($gallery as $gall) { 

		 $gall->slug = str_replace("-","",$gall->slug );
		if($i==0) {

 
		$html.='<a class="pretty_photo" rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'"><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a>';

		  $html.='<div class="galler-text">';
            $html.='<h3>'.$gall->title.'</h3>';
            $html.='<span>'.$gall->galdesc.'</span>';
            $html.='<a rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'" class="view_gallery pretty_photo">view</a>';
         $html.='</div>';		
$html.='<div style="display:none">

<div class="overall-popup" id="'.$gall->slug.'-gallery">';
$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';


$html.='<div class="gallerydet"><h3>'.$gall->title.'</h3>';
           $html.='<span>'.$gall->galdesc.'</span></div>';
		} 

$i++;

} 




$html.='<div class="gallery_popup" id="gallery_popup'.$gall->slug.'">';
$html.='<ul class="bxslider bxslider'.$gall->slug.'">';
foreach($gallery as $gall) { 
  $html.='<li><a target="_bank" href="//pinterest.com/pin/create/button/?url='.get_permalink().'/gallery&media='.$gall->imageURL.'&description='.$gall->description.'" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" class="pint" /></a>

<img src="'.$gall->imageURL.'" title="'.$gall->description.'" alt="corner-img" /></li>';
 } 

$html.='</ul>';
$html.='</div>';




$html.='<ul class="bx-pager-images jcarousel jcarousel-skin-tango" id="bx-pager'.$gall->slug.'">';
$i=0; 

foreach($gallery as $gall) { 
  $html.='<li><a data-slide-index="'.$i.'" href=""><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a></li>';
 $i++; } 
 $html.='</ul>';



$html.='</div>';
$html.='</li>';

  }

}
$html.='</div>';
$html.='</div>';
 $html.='</ul>';
$html.='</div>';
		}
		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;

}

public function Congresshall_weddingevents($menu, $p_id, $subMenu) {   

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}


		elseif($tab->type==1) {
		if($tab->slug=="faq"){
				$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div class="faq-ques">';
 	$html.='Q: <a href="'.get_permalink().'">'.get_the_title().'</a>';
         $newcontent= get_the_content();
         $html.='<div class="faq-ans" style="display:none;">'. $newcontent .'</div>';
         
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		}else{

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
 	$html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';	
	if(get_field('wedding_specialist')){ $wedding_specialist =  get_field('wedding_specialist'); 
$html .='<span class="v-gallery">';
$html .='<a class="weddingspecialist" href="#contact" title="'.get_the_title().'">CONTACT A WEDDING SPECIALIST</a>';
$html .='</span>';
}
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		
		}

		} 





		elseif($tab->type==2) {

		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type =  $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();
	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		


		} elseif($tab->type==3){
$html.='<div id="'.$tab->slug.'" class="events tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
			        $html.='<ul>';

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }
    foreach( $galleries as $key => $galleryitem ){ 


 $gallery = $nggdb->get_gallery($galleryitem['id']);


	if(count($gallery) > 0)  {
	 $html.='<li>';


			$i=0;
			foreach($gallery as $gall) { 
 $gall->slug = str_replace("-","",$gall->slug );
		
		if($i==0) { 
		$html.='<a class="pretty_photo" rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'"><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a>';

		  $html.='<div class="galler-text">';
            $html.='<h3>'.$gall->title.'</h3>';
            $html.='<span>'.$gall->galdesc.'</span>';
            $html.='<a rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'" class="view_gallery pretty_photo">view</a>';
         $html.='</div>';		
$html.='<div style="display:none"><div class="overall-popup" id="'.$gall->slug.'-gallery">';
$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';


$html.='<div class="gallerydet"><h3>'.$gall->title.'</h3>';
           $html.='<span>'.$gall->galdesc.'</span></div>';
		} $i++;} 
$html.='<div class="gallery_popup" id="gallery_popup'.$gall->slug.'">';
$html.='<ul class="bxslider bxslider'.$gall->slug.'">';
foreach($gallery as $gall) { 
  $html.='<li><a target="_bank" href="//pinterest.com/pin/create/button/?url='.get_permalink().'/gallery&media='.$gall->imageURL.'&description='.$gall->description.'" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" class="pint" /></a>

<img src="'.$gall->imageURL.'" title="'.$gall->description.'" alt="corner-img" /></li>';
 } 
$html.='</ul>';
$html.='</div>';
$html.='<ul class="bx-pager-images jcarousel jcarousel-skin-tango" id="bx-pager'.$gall->slug.'">';
$i=0; foreach($gallery as $gall) { 
  $html.='<li><a data-slide-index="'.$i.'" href=""><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a></li>';
 $i++; } 
 $html.='</ul>';
$html.='</div>';
$html.='</li>';

	}
  }

$html.='</div>';
$html.='</div>';
 $html.='</ul>';
$html.='</div>';
		} elseif($tab->type==4){
$html.='<div id="'.$tab->slug.'" class="events ja tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';

if($p_id==4) {

$html.= woothemes_testimonials( array( 'echo' => false,'category' =>'beach-shack-wedding-events', 'size' => 274 ) );//do_action( 'woothemes_testimonials' );
}
if($p_id==1){
$html.= woothemes_testimonials( array( 'echo' => false,'category' =>'congress-hall-wedding-events', 'size' => 274 ) );//do_action( 'woothemes_testimonials' );
}
if($p_id==2){
$html.= woothemes_testimonials( array( 'echo' => false,'category' =>'virginia-wedding-events', 'size' => 274 ) );//do_action( 'woothemes_testimonials' );
}

$html.='</div>';
$html.='</div>';
$html.='</div>';
}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}
    public function Congresshall_beachhome($menu, $p_id, $subMenu) {
	$html ='';
	$tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div beachshack">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']); 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos '.$tab->promo_lists.'" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==1) {



	$html.='<div id="'.$tab->slug.'" class="events tabs_div beachshack" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
 $html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos '.$tab->promo_lists.'" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		}elseif($tab->type==2) {


		$html.='<div id="'.$tab->slug.'" class="eventss tabs_div beachshack">';

		$html.='<p class="all-events"><a href="'.site_url().'/concierge/seasonal-events/?property='.$p_id.'">all events</a><a href="'.site_url().'/concierge/concierge-calender/?property='.$p_id.'">daily calendar</a></p>';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type = $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();
	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos '.$tab->promo_lists.'" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		

		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
    }
    public function Congresshall_beachspecials($menu, $p_id, $subMenu) {

		$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv specials_nav" >';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li style="background: none;
border-bottom-color: transparent;
border-right: 0;"><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']); 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==2) { 
	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;
	
			 $args[ 'meta_key'] = 'property';
	
$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		

$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}

if($reOut) {

		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  $html.='<div>';
			$html.='<img src="'.$image_url[0].'" alt="dining" />';
        $html.='<div>';
 $html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';
         $html.='<span class="v-gallery">';

	$html .=get_book_button(get_the_ID(), $p_id);
		

		$html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		}
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		} 

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
	}
    public function Congresshall_reservations($menu, $p_id, $subMenu) {
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
	 $html .= get_nav_open1();
    $html.='<ul>';
	 $html.=get_tabs_htmlNav($tabs);
    $html.='</ul>';

    $html.='<div class="all-tabs">';
	$html .= get_tabs_htmlContent($tabs, $p_id, $subMenu);
    $html.='</div>'; /* All Tabs */

	$html .= get_nav_close1();
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;
	}
    public function Congresshall_beachaccommodations($menu, $p_id, $subMenu) {
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}


		elseif($tab->type==1) {
		if($tab->slug=="faq"){
				$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div class="faq-ques">';
 	$html.='Q: <a href="'.get_permalink().'">'.get_the_title().'</a>';
         $newcontent= get_the_content();
         $html.='<div class="faq-ans" style="display:none;">'. $newcontent .'</div>';
         
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		}else{

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
 	$html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';	
	if(get_field('wedding_specialist')){ $wedding_specialist =  get_field('wedding_specialist'); 
$html .='<span class="v-gallery">';
$html .='<a class="weddingspecialist" href="'.$wedding_specialist.'" title="'.get_the_title().'">CONTACT A WEDDING SPECIALIST</a>';
$html .='</span>';
}
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		
		}

		} 





		elseif($tab->type==2) {

		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type =  $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();
	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		


		} elseif($tab->type==3){
$html.='<div id="'.$tab->slug.'" class="events tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
			        $html.='<ul>';

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }
    foreach( $galleries as $key => $galleryitem ){ 


 $gallery = $nggdb->get_gallery($galleryitem['id']);

	if(count($gallery) > 0)  {

	 $html.='<li>';

			$i=0;
			foreach($gallery as $gall) { 
 $gall->slug = str_replace("-","",$gall->slug );
		
		if($i==0) { 
		$html.='<a class="pretty_photo" rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'"><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a>';

		  $html.='<div class="galler-text">';
            $html.='<h3>'.$gall->title.'</h3>';
            $html.='<span>'.$gall->galdesc.'</span>';
            $html.='<a rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'" class="view_gallery pretty_photo">view</a>';
         $html.='</div>';		
$html.='<div style="display:none"><div class="overall-popup" id="'.$gall->slug.'-gallery">';
$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';


$html.='<div class="gallerydet"><h3>'.$gall->title.'</h3>';
           $html.='<span>'.$gall->galdesc.'</span></div>';
		} $i++;} 
$html.='<div class="gallery_popup" id="gallery_popup'.$gall->slug.'">';
$html.='<ul class="bxslider bxslider'.$gall->slug.'">';
foreach($gallery as $gall) { 
  $html.='<li><a target="_bank" href="//pinterest.com/pin/create/button/?url='.get_permalink().'/gallery&media='.$gall->imageURL.'&description='.$gall->description.'" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" class="pint" /></a>

<img src="'.$gall->imageURL.'" title="'.$gall->description.'" alt="corner-img" /></li>';
 } 
$html.='</ul>';
$html.='</div>';
$html.='<ul class="bx-pager-images jcarousel jcarousel-skin-tango" id="bx-pager'.$gall->slug.'">';
$i=0; foreach($gallery as $gall) { 
  $html.='<li><a data-slide-index="'.$i.'" href=""><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a></li>';
 $i++; } 
 $html.='</ul>';
$html.='</div>';
$html.='</li>';
}
  }
$html.='</div>';
$html.='</div>';
 $html.='</ul>';
$html.='</div>';
		} elseif($tab->type==4){
$html.='<div id="'.$tab->slug.'" class="events ja tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
$html.= woothemes_testimonials( array( 'echo' => false,'category' =>'beach-shack-wedding-events', 'size' => 274 ) );//do_action( 'woothemes_testimonials' );
$html.='</div>';
$html.='</div>';
$html.='</div>';
}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;
	}
    public function Congresshall_beachresortlife($menu, $p_id, $subMenu) {
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}


		elseif($tab->type==1) {
		if($tab->slug=="faq"){
				$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div class="faq-ques">';
 	$html.='Q: <a href="'.get_permalink().'">'.get_the_title().'</a>';
         $newcontent= get_the_content();
         $html.='<div class="faq-ans" style="display:none;">'. $newcontent .'</div>';
         
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		}else{

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
 	$html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';	
	if(get_field('wedding_specialist')){ $wedding_specialist =  get_field('wedding_specialist'); 
$html .='<span class="v-gallery">';
$html .='<a class="weddingspecialist" href="'.$wedding_specialist.'" title="'.get_the_title().'">CONTACT A WEDDING SPECIALIST</a>';
$html .='</span>';
}
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		
		}

		} 





		elseif($tab->type==2) {

		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
		$post_type =  $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();
	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		


		} elseif($tab->type==3){
$html.='<div id="'.$tab->slug.'" class="events tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
			        $html.='<ul>';

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }
    foreach( $galleries as $key => $galleryitem ){

 $gallery = $nggdb->get_gallery($galleryitem['id']);
if(count($gallery) > 0)  {
 
	 $html.='<li>';

			$i=0;
			foreach($gallery as $gall) { 
 $gall->slug = str_replace("-","",$gall->slug );
		
		if($i==0) { 
		$html.='<a class="pretty_photo" rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'"><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a>';

		  $html.='<div class="galler-text">';
            $html.='<h3>'.$gall->title.'</h3>';
            $html.='<span>'.$gall->galdesc.'</span>';
            $html.='<a rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'" class="view_gallery pretty_photo">view</a>';
         $html.='</div>';		
$html.='<div style="display:none"><div class="overall-popup" id="'.$gall->slug.'-gallery">';
$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';


$html.='<div class="gallerydet"><h3>'.$gall->title.'</h3>';
           $html.='<span>'.$gall->galdesc.'</span></div>';
		} $i++;} 
$html.='<div class="gallery_popup" id="gallery_popup'.$gall->slug.'">';
$html.='<ul class="bxslider bxslider'.$gall->slug.'">';
foreach($gallery as $gall) { 
  $html.='<li><a target="_bank" href="//pinterest.com/pin/create/button/?url='.get_permalink().'/gallery&media='.$gall->imageURL.'&description='.$gall->description.'" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" class="pint" /></a>

<img src="'.$gall->imageURL.'" title="'.$gall->description.'" alt="corner-img" /></li>';
 } 
$html.='</ul>';
$html.='</div>';
$html.='<ul class="bx-pager-images jcarousel jcarousel-skin-tango" id="bx-pager'.$gall->slug.'">';
$i=0; foreach($gallery as $gall) { 
  $html.='<li><a data-slide-index="'.$i.'" href=""><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a></li>';
 $i++; } 
 $html.='</ul>';
$html.='</div>';
$html.='</li>';
}
  }
$html.='</div>';
$html.='</div>';
 $html.='</ul>';
$html.='</div>';
		} elseif($tab->type==4){
$html.='<div id="'.$tab->slug.'" class="events ja tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
$html.= woothemes_testimonials( array( 'echo' => false,'category' =>'beach-shack-wedding-events', 'size' => 274 ) );//do_action( 'woothemes_testimonials' );
$html.='</div>';
$html.='</div>';
$html.='</div>';
}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;
	}
  
public function Congresshall_beachgallery($menu, $p_id, $subMenu) {
   $tabs = get_all_tabs($p_id, $menu->nid);  
	$html.='';
		 foreach($tabs as $tab) { 

		//$html.='<h3 class="nav-single">'.$tab->t_name.'</h3>';

$html.='<div class="gallery-container">';
      
		$html.='<h3 class="nav-single">'.$tab->t_name.'</h3>';

      $html.='<div class="gallery-images">';
         $html.='<ul>';

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }
    foreach( $galleries as $key => $galleryitem ){ 

 $gallery = $nggdb->get_gallery($galleryitem['id']);

if(count($gallery) > 0) {
	 $html.='<li>';


			$i=0;
			foreach($gallery as $gall) { 

		 $gall->slug = str_replace("-","",$gall->slug );
		if($i==0) { 
		$html.='<a class="pretty_photo" rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'"><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a>';

		  $html.='<div class="galler-text">';
            $html.='<h3>'.$gall->title.'</h3>';
            $html.='<span>'.$gall->galdesc.'</span>';
            $html.='<a rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'" class="view_gallery pretty_photo">view</a>';
         $html.='</div>';		
$html.='<div style="display:none"><div class="overall-popup" id="'.$gall->slug.'-gallery">';
$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';


$html.='<div class="gallerydet"><h3>'.$gall->title.'</h3>';
           $html.='<span>'.$gall->galdesc.'</span></div>';
		} $i++;} 
$html.='<div class="gallery_popup" id="gallery_popup'.$gall->slug.'">';
$html.='<ul class="bxslider bxslider'.$gall->slug.'">';
foreach($gallery as $gall) { 
  $html.='<li><a target="_bank" href="//pinterest.com/pin/create/button/?url='.get_permalink().'/gallery&media='.$gall->imageURL.'&description='.$gall->description.'" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" class="pint" /></a>

<img src="'.$gall->imageURL.'" title="'.$gall->description.'" alt="corner-img" /></li>';
 } 
$html.='</ul>';
$html.='</div>';
$html.='<ul class="bx-pager-images jcarousel jcarousel-skin-tango" id="bx-pager'.$gall->slug.'">';
$i=0; foreach($gallery as $gall) { 
  $html.='<li><a data-slide-index="'.$i.'" href=""><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a></li>';
 $i++; } 
 $html.='</ul>';
$html.='</div>';
$html.='</li>';
}
  }

 $html.='</ul>

      </div>
    </div></div>';	
	}
return $html;
	}

    public function Congresshall_rustynail($menu, $p_id, $subMenu) {
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}


		elseif($tab->type==1) {
		if($tab->slug=="faq"){
				$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div class="faq-ques">';
 	$html.='Q: <a href="'.get_permalink().'">'.get_the_title().'</a>';
         $newcontent= get_the_content();
         $html.='<div class="faq-ans" style="display:none;">'. $newcontent .'</div>';
         
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		}else{

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
 	$html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';	
	if(get_field('wedding_specialist')){ $wedding_specialist =  get_field('wedding_specialist'); 
$html .='<span class="v-gallery">';
$html .='<a class="weddingspecialist" href="'.$wedding_specialist.'" title="'.get_the_title().'">CONTACT A WEDDING SPECIALIST</a>';
$html .='</span>';
}
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		
		}

		} 





		elseif($tab->type==2) {

		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type =  $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();
	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		


		} elseif($tab->type==3){
$html.='<div id="'.$tab->slug.'" class="events tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
			        $html.='<ul>';

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }
    foreach( $galleries as $key => $galleryitem ){ 

 $gallery = $nggdb->get_gallery($galleryitem['id']);

	if(count($gallery) > 0) {
	 $html.='<li>';


			$i=0;
			foreach($gallery as $gall) { 
 $gall->slug = str_replace("-","",$gall->slug );
		
		if($i==0) { 
		$html.='<a class="pretty_photo" rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'"><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a>';

		  $html.='<div class="galler-text">';
            $html.='<h3>'.$gall->title.'</h3>';
            $html.='<span>'.$gall->galdesc.'</span>';
            $html.='<a rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'" class="view_gallery pretty_photo">view</a>';
         $html.='</div>';		
$html.='<div style="display:none"><div class="overall-popup" id="'.$gall->slug.'-gallery">';
$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';


$html.='<div class="gallerydet"><h3>'.$gall->title.'</h3>';
           $html.='<span>'.$gall->galdesc.'</span></div>';
		} $i++;} 
$html.='<div class="gallery_popup" id="gallery_popup'.$gall->slug.'">';
$html.='<ul class="bxslider bxslider'.$gall->slug.'">';
foreach($gallery as $gall) { 
  $html.='<li><a target="_bank" href="//pinterest.com/pin/create/button/?url='.get_permalink().'/gallery&media='.$gall->imageURL.'&description='.$gall->description.'" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" class="pint" /></a>

<img src="'.$gall->imageURL.'" title="'.$gall->description.'" alt="corner-img" /></li>';
 } 
$html.='</ul>';
$html.='</div>';
$html.='<ul class="bx-pager-images jcarousel jcarousel-skin-tango" id="bx-pager'.$gall->slug.'">';
$i=0; foreach($gallery as $gall) { 
  $html.='<li><a data-slide-index="'.$i.'" href=""><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a></li>';
 $i++; } 
 $html.='</ul>';
$html.='</div>';
$html.='</li>';
}
  }
$html.='</div>';
$html.='</div>';
 $html.='</ul>';
$html.='</div>';
		} elseif($tab->type==4){
$html.='<div id="'.$tab->slug.'" class="events ja tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
$html.= woothemes_testimonials( array( 'echo' => false,'category' =>'beach-shack-wedding-events', 'size' => 274 ) );//do_action( 'woothemes_testimonials' );
$html.='</div>';
$html.='</div>';
$html.='</div>';
}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;
	}
    public function Congresshall_beachweddingevents($menu, $p_id, $subMenu) {
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
						if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}


		elseif($tab->type==1) {
		if($tab->slug=="faq"){
				$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div class="faq-ques">';
 	$html.='Q: <a href="'.get_permalink().'">'.get_the_title().'</a>';
         $newcontent= get_the_content();
         $html.='<div class="faq-ans" style="display:none;">'. $newcontent .'</div>';
         
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		}else{

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
 	$html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';	
	if(get_field('wedding_specialist')){ $wedding_specialist =  get_field('wedding_specialist'); 
$html .='<span class="v-gallery">';
$html .='<a class="weddingspecialist" href="'.$wedding_specialist.'" title="'.get_the_title().'">CONTACT A WEDDING SPECIALIST</a>';
$html .='</span>';
}
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		
		}

		} 





		elseif($tab->type==2) {

		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type =  $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();
	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}


      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		


		} elseif($tab->type==3){
$html.='<div id="'.$tab->slug.'" class="events tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
			        $html.='<ul>';

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }
    foreach( $galleries as $key => $galleryitem ){ 

 $gallery = $nggdb->get_gallery($galleryitem['id']);
if(count($gallery) > 0) {
	 $html.='<li>';


			$i=0;
			foreach($gallery as $gall) { 

		 $gall->slug = str_replace("-","",$gall->slug );
		if($i==0) { 
		$html.='<a class="pretty_photo" rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'"><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a>';

		  $html.='<div class="galler-text">';
            $html.='<h3>'.$gall->title.'</h3>';
            $html.='<span>'.$gall->galdesc.'</span>';
            $html.='<a rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'" class="view_gallery pretty_photo">view</a>';
         $html.='</div>';		
$html.='<div style="display:none"><div class="overall-popup" id="'.$gall->slug.'-gallery">';
$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';


$html.='<div class="gallerydet"><h3>'.$gall->title.'</h3>';
           $html.='<span>'.$gall->galdesc.'</span></div>';
		} $i++;} 
$html.='<div class="gallery_popup" id="gallery_popup'.$gall->slug.'">';
$html.='<ul class="bxslider bxslider'.$gall->slug.'">';
foreach($gallery as $gall) { 
  $html.='<li><a target="_bank" href="//pinterest.com/pin/create/button/?url='.get_permalink().'/gallery&media='.$gall->imageURL.'&description='.$gall->description.'" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" class="pint" /></a>

<img src="'.$gall->imageURL.'" title="'.$gall->description.'" alt="corner-img" /></li>';
 } 
$html.='</ul>';
$html.='</div>';
$html.='<ul class="bx-pager-images jcarousel jcarousel-skin-tango" id="bx-pager'.$gall->slug.'">';
$i=0; foreach($gallery as $gall) { 
  $html.='<li><a data-slide-index="'.$i.'" href=""><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a></li>';
 $i++; } 
 $html.='</ul>';
$html.='</div>';
$html.='</li>';

  }
}
$html.='</div>';
$html.='</div>';
 $html.='</ul>';
$html.='</div>';
		} elseif($tab->type==4){
$html.='<div id="'.$tab->slug.'" class="events ja tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
$html.= woothemes_testimonials( array( 'echo' => false,'category' =>'beach-shack-wedding-events', 'size' => 274 ) );//do_action( 'woothemes_testimonials' );
$html.='</div>';
$html.='</div>';
$html.='</div>';
}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;
	}
    public function Congresshall_theebbittroom($menu, $p_id, $subMenu) {
	$html ='';
	$tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv home ">';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']); 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}

			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}		elseif($tab->type==1) {
		if($tab->slug=="faq"){
				$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div class="faq-ques">';
 	$html.='Q: <a href="'.get_permalink().'">'.get_the_title().'</a>';
         $newcontent= get_the_content();
         $html.='<div class="faq-ans" style="display:none;">'. $newcontent .'</div>';
         
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		}else{

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
 	$html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';	
	if(get_field('wedding_specialist')){ $wedding_specialist =  get_field('wedding_specialist'); 
$html .='<span class="v-gallery">';
$html .='<a class="weddingspecialist" href="'.$wedding_specialist.'" title="'.get_the_title().'">CONTACT A WEDDING SPECIALIST</a>';
$html .='</span>';
}
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		
		}

		}elseif($tab->type==2) {


		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

		$html.='<p class="all-events"><a href="'.site_url().'/concierge/seasonal-events/?property='.$p_id.'">all events</a><a href="'.site_url().'/concierge/concierge-calender/?property='.$p_id.'">daily calendar</a></p>';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type = $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();

	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		

		} 

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
    }

public function Congresshall_spamenu($menu, $p_id, $subMenu) {   

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}


		elseif($tab->type==1) {
		if($tab->slug=="faq"){
				$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div class="faq-ques">';
 	$html.='Q: <a href="'.get_permalink().'">'.get_the_title().'</a>';
         $newcontent= get_the_content();
         $html.='<div class="faq-ans" style="display:none;">'. $newcontent .'</div>';
         
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		}else{

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
 	$html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';	
	if(get_field('wedding_specialist')){ $wedding_specialist =  get_field('wedding_specialist'); 
$html .='<span class="v-gallery">';
$html .='<a class="weddingspecialist" href="#contact" title="'.get_the_title().'">CONTACT A WEDDING SPECIALIST</a>';
$html .='</span>';
}
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		
		}

		} 


		elseif($tab->type==2) {

		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type =  $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();
	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		


		} elseif($tab->type==3){
$html.='<div id="'.$tab->slug.'" class="events tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
			        $html.='<ul>';

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }
    foreach( $galleries as $key => $galleryitem ){

 $gallery = $nggdb->get_gallery($galleryitem['id']);
 if(count($gallery) > 0) {
	 $html.='<li>';


			$i=0;
			foreach($gallery as $gall) { 
	 $gall->slug = str_replace("-","",$gall->slug );	
		
		if($i==0) { 
		$html.='<a class="pretty_photo" rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'"><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a>';

		  $html.='<div class="galler-text">';
            $html.='<h3>'.$gall->title.'</h3>';
            $html.='<span>'.$gall->galdesc.'</span>';
            $html.='<a rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'" class="view_gallery pretty_photo">view</a>';
         $html.='</div>';		
$html.='<div style="display:none"><div class="overall-popup" id="'.$gall->slug.'-gallery">';
$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';


$html.='<div class="gallerydet"><h3>'.$gall->title.'</h3>';
           $html.='<span>'.$gall->galdesc.'</span></div>';
		} $i++;} 
$html.='<div class="gallery_popup" id="gallery_popup'.$gall->slug.'">';
$html.='<ul class="bxslider bxslider'.$gall->slug.'">';
foreach($gallery as $gall) { 
  $html.='<li><a target="_bank" href="//pinterest.com/pin/create/button/?url='.get_permalink().'/gallery&media='.$gall->imageURL.'&description='.$gall->description.'" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" class="pint" /></a>

<img src="'.$gall->imageURL.'" title="'.$gall->description.'" alt="corner-img" /></li>';
 } 
$html.='</ul>';
$html.='</div>';
$html.='<ul class="bx-pager-images jcarousel jcarousel-skin-tango" id="bx-pager'.$gall->slug.'">';
$i=0; foreach($gallery as $gall) { 
  $html.='<li><a data-slide-index="'.$i.'" href=""><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a></li>';
 $i++; } 
 $html.='</ul>';
$html.='</div>';
$html.='</li>';
}
  }
$html.='</div>';
$html.='</div>';
 $html.='</ul>';
$html.='</div>';
		} elseif($tab->type==4){
$html.='<div id="'.$tab->slug.'" class="events ja tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
$html.= woothemes_testimonials( array( 'echo' => false,'category' =>'congress-hall-wedding-events', 'size' => 274 ) );//do_action( 'woothemes_testimonials' );
$html.='</div>';
$html.='</div>';
$html.='</div>';
}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}

public function Congresshall_spaspecials($menu, $p_id, $subMenu) {   

		$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv specials_nav" >';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li style="background: none;
border-bottom-color: transparent;
border-right: 0;"><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==2) {
 

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= $post_type;
			$args['posts_per_page']=-1;
	
			 $args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

	
$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();

	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}

if($reOut) {
		
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ if($p_id==17){$html.='';}else{$html.='<img src="'.$image_url[0].'" alt="meeting" />';} }
        $html.='<div>';
 $html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';
         $html.='<span class="v-gallery">';


			$html .=get_book_button(get_the_ID(), $p_id);


		$html.='</span>';
       $html.='</p>';
       $html.='</div></div>';



		$html.='</li>';
			}
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		} 

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
}

public function Congresshall_bookatreatment($menu, $p_id, $subMenu) {   

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==1) {
		if($tab->slug=="faq"){
				$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div class="faq-ques">';
 	$html.='Q: <a href="'.get_permalink().'">'.get_the_title().'</a>';
         $newcontent= get_the_content();
         $html.='<div class="faq-ans" style="display:none;">'. $newcontent .'</div>';
         
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		}else{

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
 	$html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';	
	if(get_field('wedding_specialist')){ $wedding_specialist =  get_field('wedding_specialist'); 
$html .='<span class="v-gallery">';
$html .='<a class="weddingspecialist" href="#contact" title="'.get_the_title().'">CONTACT A WEDDING SPECIALIST</a>';
$html .='</span>';
}
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		
		}

		} 


		elseif($tab->type==2) {

		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type =  $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();

	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {

   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		


		} elseif($tab->type==3){
$html.='<div id="'.$tab->slug.'" class="events tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
			        $html.='<ul>';

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }
    foreach( $galleries as $key => $galleryitem ){ 
 $gallery = $nggdb->get_gallery($galleryitem['id']);
if(count($gallery) > 0) {

	 $html.='<li>';


			$i=0;
			foreach($gallery as $gall) { 

	$gall->slug = str_replace("-","",$gall->slug);	
		if($i==0) { 
		$html.='<a class="pretty_photo" rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'"><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a>';

		  $html.='<div class="galler-text">';
            $html.='<h3>'.$gall->title.'</h3>';
            $html.='<span>'.$gall->galdesc.'</span>';
            $html.='<a rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'" class="view_gallery pretty_photo">view</a>';
         $html.='</div>';		
$html.='<div style="display:none"><div class="overall-popup" id="'.$gall->slug.'-gallery">';
$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';


$html.='<div class="gallerydet"><h3>'.$gall->title.'</h3>';
           $html.='<span>'.$gall->galdesc.'</span></div>';
		} $i++;} 
$html.='<div class="gallery_popup" id="gallery_popup'.$gall->slug.'">';
$html.='<ul class="bxslider bxslider'.$gall->slug.'">';
foreach($gallery as $gall) { 
  $html.='<li><a target="_bank" href="//pinterest.com/pin/create/button/?url='.get_permalink().'/gallery&media='.$gall->imageURL.'&description='.$gall->description.'" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" class="pint" /></a>

<img src="'.$gall->imageURL.'" title="'.$gall->description.'" alt="corner-img" /></li>';
 } 
$html.='</ul>';
$html.='</div>';
$html.='<ul class="bx-pager-images jcarousel jcarousel-skin-tango" id="bx-pager'.$gall->slug.'">';
$i=0; foreach($gallery as $gall) { 
  $html.='<li><a data-slide-index="'.$i.'" href=""><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a></li>';
 $i++; } 
 $html.='</ul>';
$html.='</div>';
$html.='</li>';

  }
}
$html.='</div>';
$html.='</div>';
 $html.='</ul>';
$html.='</div>';
		} elseif($tab->type==4){
$html.='<div id="'.$tab->slug.'" class="events ja tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
$html.= woothemes_testimonials( array( 'echo' => false,'category' =>'congress-hall-wedding-events', 'size' => 274 ) );//do_action( 'woothemes_testimonials' );
$html.='</div>';
$html.='</div>';
$html.='</div>';
}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}

public function Congresshall_congresshall($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==1) {
		if($tab->slug=="faq"){
				$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div class="faq-ques">';
 	$html.='Q: <a href="'.get_permalink().'">'.get_the_title().'</a>';
         $newcontent= get_the_content();
         $html.='<div class="faq-ans" style="display:none;">'. $newcontent .'</div>';
         
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		}else{

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
 	$html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';	
	if(get_field('wedding_specialist')){ $wedding_specialist =  get_field('wedding_specialist'); 
$html .='<span class="v-gallery">';
$html .='<a class="weddingspecialist" href="#contact" title="'.get_the_title().'">CONTACT A WEDDING SPECIALIST</a>';
$html .='</span>';
}
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		
		}

		} 





		elseif($tab->type==2) {

		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type = $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();
	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		


		} elseif($tab->type==3){
$html.='<div id="'.$tab->slug.'" class="events tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
			        $html.='<ul>';

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }
    foreach( $galleries as $key => $galleryitem ){ 

 $gallery = $nggdb->get_gallery($galleryitem['id']);

if(count($gallery) > 0) {

	 $html.='<li>';

			$i=0;
			foreach($gallery as $gall) { 
$gall->slug = str_replace("-","",$gall->slug);
		
		if($i==0) { 
		$html.='<a class="pretty_photo" rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'"><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a>';

		  $html.='<div class="galler-text">';
            $html.='<h3>'.$gall->title.'</h3>';
            $html.='<span>'.$gall->galdesc.'</span>';
            $html.='<a rel="prettyPhoto" href="#'.$gall->slug.'-gallery" data-rel="'.$gall->slug.'" class="view_gallery pretty_photo">view</a>';
         $html.='</div>';		
$html.='<div style="display:none"><div class="overall-popup" id="'.$gall->slug.'-gallery">';
$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';


$html.='<div class="gallerydet"><h3>'.$gall->title.'</h3>';
           $html.='<span>'.$gall->galdesc.'</span></div>';
		} $i++;} 
$html.='<div class="gallery_popup" id="gallery_popup'.$gall->slug.'">';
$html.='<ul class="bxslider bxslider'.$gall->slug.'">';
foreach($gallery as $gall) { 
  $html.='<li><a target="_bank" href="//pinterest.com/pin/create/button/?url='.get_permalink().'/gallery&media='.$gall->imageURL.'&description='.$gall->description.'" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" class="pint" /></a>

<img src="'.$gall->imageURL.'" title="'.$gall->description.'" alt="corner-img" /></li>';
 } 
$html.='</ul>';
$html.='</div>';
$html.='<ul class="bx-pager-images jcarousel jcarousel-skin-tango" id="bx-pager'.$gall->slug.'">';
$i=0; foreach($gallery as $gall) { 
  $html.='<li><a data-slide-index="'.$i.'" href=""><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a></li>';
 $i++; } 
 $html.='</ul>';
$html.='</div>';
$html.='</li>';
}
  }
$html.='</div>';
$html.='</div>';
 $html.='</ul>';
$html.='</div>';
		} elseif($tab->type==4){
$html.='<div id="'.$tab->slug.'" class="events ja tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
$html.= woothemes_testimonials( array( 'echo' => false,'category' =>'congress-hall-wedding-events', 'size' => 274 ) );//do_action( 'woothemes_testimonials' );
$html.='</div>';
$html.='</div>';
$html.='</div>';
}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}

public function Congresshall_tommysfollygeneralstore($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}

public function Congresshall_tommysfollycoffeeshop($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}
public function Congresshall_tommysfollyhome($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}

public function Congresshall_tommysfollyboutique($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}
public function Congresshall_vendorlisting($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==1) {



	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;
			$args['order']='ASC';
			$args['orderby']='title';


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  $html.='<div>';
			$html.='<img src="'.$image_url[0].'" alt="dining" />';
        $html.='<div>';
 $html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}

public function Congresshall_ownerssignin($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
		if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==1) {



	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  $html.='<div>';			
        $html.='<div>';
         $html.='<p class="view-links">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}
public function Congresshall_amenities($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 

			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==1) {



	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="amenities" />'; }
        $html.='<div>';
 $html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}

public function Congresshall_whatsinseason($menu, $p_id, $subMenu) {  

$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
	 $html .= get_nav_open1();
    $html.='<ul>';
	 $html.=get_tabs_htmlNav($tabs);
    $html.='</ul>';

    $html.='<div class="all-tabs">';
	$html .= get_tabs_htmlContent($tabs, $p_id, $subMenu);
    $html.='</div>'; /* All Tabs */

	$html .= get_nav_close1();
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}
public function Congresshall_farmmap($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab):
$args = array(); 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}
public function Congresshall_farmtours($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}
public function Congresshall_farmdinners($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}

public function Congresshall_thebeachshack($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}
   public function Congresshall_entertainment($menu, $p_id, $subMenu) {
		$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):

$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==1) {

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  $html.='<div>';
			$html.='<img src="'.$image_url[0].'" alt="dining" />';
        $html.='<div>';
 $html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		}elseif($tab->type==2) {

		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type = $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();

	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  $html.='<div>';
			$html.='<img src="'.$image_url[0].'" alt="dining" />';
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div></div>';


		$html.='</li>';
}
		 endwhile; 
			$html.='</ul>';
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		


		} 

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
}
   public function Congresshall_menus($menu, $p_id, $subMenu) {
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):

$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
	if($tab->file_path!=''){$html.='<div class="dwmenu"><a href="'.$tab->file_path.'" target="_blank">Download Menu</a></div>'; }
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']); 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==1) {
	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >';
	if($tab->file_path!=''){$html.='<div class="dwmenu"><a href="'.$tab->file_path.'" target="_blank">Download Menu</a></div>'; }
			$html.='<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="dining-content eachmenu">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  $html.='<div>';
        $html.='<div>';
 $html.='<h4>'.get_the_title().'</h4>';
         $html.='<span class="rate">'.get_field('rate').'</span>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		} 

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
}

public function Congresshall_beachrules($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):

$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}

public function Congresshall_reserveacabana($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):

$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}
public function Congresshall_accolades($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):

$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==1) {



	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;
			$args['order']='ASC';
			$args['orderby']='menu_order';

$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
if(get_the_title()){ $html.='<span>- '.get_the_title().'</span>'; }
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;

}
public function Congresshall_thevirginia($menu, $p_id, $subMenu) {  

	//$html.='<div style="MIn-height:200px;"><h1>Under Construction</h1></div>';
	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul class="wedding">';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			if($tab->slug=='contact' || $tab->slug=='rfp'){
			$html.='<li><a href="#'.$tab->slug.'" id="weddingcontact" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
			}else{
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';}
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):
$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar wed-rt" style="display:none;"></div>';

	return $html;

}
   public function Congresshall_winelist($menu, $p_id, $subMenu) {
		$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv">';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):

$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$html.='<div class="pagecont">';
			$page = get_post($tab->page_id);
			$html.='<h3>'.$page->post_title.'</h3>';
			$html .='<h4>'.get_field('sub_header',$tab->page_id).'</h4>';
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			$html .= '</div>';
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==1) {



	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
 $html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			 if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		} 

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
}
public function Congresshall_thecottages($menu, $p_id, $subMenu) {  


	$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
	 $html .= get_nav_open1();
    $html.='<ul class="wedding">';
	 $html.=get_tabs_htmlNav($tabs);
    $html.='</ul>';

    $html.='<div class="all-tabs">';
	$html .= get_tabs_htmlContent($tabs, $p_id, $subMenu);
    $html.='</div>'; /* All Tabs */

	$html .= get_nav_close1();
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;

}
    public function Congresshall_openingspring($menu, $p_id, $subMenu) {
	$html ='';
	$tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv home ">';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):

$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']); 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}
		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
    }
public function Congresshall_diningspecials($menu, $p_id, $subMenu) {   

		$html ='';
	 $tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv specials_nav" >';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li style="background: none;
border-bottom-color: transparent;
border-right: 0;"><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):

$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']); 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}elseif($tab->type==2) {
 

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;
	
			 $args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

	
$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
			$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
		$html.='<li class="dining-content">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ if($p_id==17){$html.='';}else{$html.='<img src="'.$image_url[0].'" alt="meeting" />';} }
        $html.='<div>';
 $html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';
 
        $html.='<span class="v-gallery">';
			$html .=get_book_button(get_the_ID(), $p_id);
		 $html.='</span>';

       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
}
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';

		} 

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
}
    public function Congresshall_seasonalexperiences($menu, $p_id, $subMenu) {
	$html ='';
	$tabs = get_all_tabs($p_id, $menu->nid);
    $html.='<div class="tabsDiv home ">';
    $html.='<div id="tabs">';
    $html.='<ul>';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
    $html.='</ul>';
    $html.='<div class="all-tabs">';
		foreach($tabs as $tab):

$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);;
			$html.=wpautop($post['post_content']); 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
if($gotowebsite[0]!="Seasonal Experiences"){
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}
		elseif($tab->type==2) {


		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

		$html.='<p class="all-events"><a href="'.site_url().'/concierge/seasonal-events/?property='.$p_id.'">all events</a><a href="'.site_url().'/concierge/concierge-calender/?property='.$p_id.'">daily calendar</a></p>';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type = $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();
	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
		if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
			if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		

		} 

		endforeach;
    $html.='</div>';
	$html.='</div>';
    $html.='</div>';
    $html.='<div class="rt-bar" style="display:none;"></div>';

	return $html;
    }
public function Congresshall_homepage($menu, $p_id, $subMenu) {
	$html ='';
	$html .='<div class="cong-content"><ul class="home_specials01">';
	$tabs = get_all_tabs($p_id, $menu->nid);
		foreach($tabs as $tab):
$args = array();
		if($tab->type==1) {
		$post_type = $tab->post_type;
		$args['post_type']= array( $post_type );
		$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();		
if($tab->t_name=="Our Hotels" || $tab->slug=="Our Hotels" || $tab->t_name=="Our hotels" || $tab->t_name=="our hotels"){
$html.='<div class="ourhotels"><p style="float: left;margin-top: 7px;">'.$tab->t_name.'</p>
<div class="wedding_home" style="float: left;width: 870px;margin-left: 10px;margin-bottom: 35px;">';
         $newcontent= get_the_content();
	if(strlen($newcontent)>250){
         $html.='<div class="initial-content smrdmore">'.cr_trim_chars( $newcontent, 250).'<a href="#" class="readmore" data-id="'.get_the_ID().'"> ... read more</a></div>';
	 $html.='<div class="final-content smrdless" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
$html.='</div></div>';

}else{
$html .='<li>';
if($tab->slug=="splsavings"){
$html .='<p style="padding: 0 0 0 20px;">'.$tab->t_name.'</p>';
$html .='<div class="wedding_home middle">'; }else{
$html .='<p>'.$tab->t_name.'</p>';
$html .='<div class="wedding_home">';}
$html .='<p class="winter">'.get_the_title().'</p>';
         $newcontent= get_the_content();
         $html.='<p>'. $newcontent .'</p>';
$html.='<strong><a href="'.get_field('learn_more').'">Learn More.</a></strong></div></li>';
}	
		 endwhile; 
		}
		endforeach;
$html.='</ul>
</div>
<p class="star08"><img src="'.site_url().'/wp-content/themes/twentytwelve-child/img/con-images/star.png" style="width:14px;height:14px;" alt="Capre resorts star"><img src="'.site_url().'/wp-content/themes/twentytwelve-child/img/con-images/star.png" style="width:14px;height:14px;" alt="Capre resorts star"><img src="'.site_url().'/wp-content/themes/twentytwelve-child/img/con-images/star.png" style="width:14px;height:14px;" alt="Capre resorts star"></p>';

	return $html;
    }
}
