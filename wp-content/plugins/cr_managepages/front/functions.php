<?php 


$cong = new Congresshall();
global $cong;


function getMenuBySlug($p_id, $slug) {
	global $wpdb;
	$nav=$wpdb->get_row("SELECT * FROM $wpdb->hotelsnav WHERE nav_slug='$slug' AND hotel_id=$p_id");
	return $nav;
}
function get_cong_template($menu, $p_id, $subMenu) {
	global $cong;

	$func ='Congresshall_'.$subMenu;

	$content = $cong->$func($menu, $p_id, $subMenu);

	return $content;
}

function get_hotel_menus($p_id="", $current) {
		global $wpdb;
		
		$hotels=$wpdb->get_results("SELECT t1.*, t2.* FROM $wpdb->hotels AS t1 INNER JOIN $wpdb->hotelsnav AS t2 ON t1.id = t2.hotel_id WHERE t1.id=$p_id order by t2.menu_order");
		
		$html = get_menu_temp($hotels, $current, $p_id);
		return $html;
}

function get_menu_temp($values, $current,  $p_id) { 


	$html='<ul class="banner-nav-li">';

	foreach ($values as $value):
		$onclick="";
		$class = $current == $value->nav_slug ? 'selected' : '';
		$url = 'home' == $value->nav_slug ? get_permalink() : get_permalink().$value->nav_slug; 
		if($value->nav_slug=='bookatreatment'){$special_menu = $value->nav_slug;$new_tab ='';
		}elseif($value->nav_slug=='ownerssignin'){
			$url = get_post_meta(715, 'button_1', true);$special_menu = 'bookatreatment';$new_tab ='target="_blank"';
		}elseif($value->nav_slug=='reserveacabana'){$special_menu = 'bookatreatment';$new_tab ='';}
		elseif($value->nav_slug=='farmmap'){$attachment_id = get_field('cottage_photo_image', 697);
$size = "full";$image = wp_get_attachment_image_src( $attachment_id, $size );$url = $image[0];
$special_menu = 'bookatreatment capacity-chart';$new_tab ='';}
		elseif($value->nav_slug=='seasonalexperiences'){
$gotoweb =  get_post_meta(1121, 'go_to_website', true);
$gotowebsite = explode(",", $gotoweb);
if($gotowebsite[1]){$url = $gotowebsite[1];}else{$url = $gotowebsite[0];}
		$special_menu = 'bookatreatment';$new_tab ='';
		}elseif($value->nav_slug=='beachreservations' || ($value->nav_slug=='reservations' && $p_id !=3) ){
		$phone = get_globalvalues($value->id, 'logo_phone'); 
		if($phone->reservation_link){$url = $phone->reservation_link;$special_menu = 'bookatreatment';$new_tab =''; 

			$onclick= 'onclick="_gaq.push(['.'_link'.', '.$url.']); return false;"';


}else{$special_menu = '';$new_tab ='';}
		}else{$special_menu = '';$new_tab ='';}
		$data_url = '/'.$value->nav_slug;

		$html.='<li><a class="'.$class.' '. $special_menu .'" '.$new_tab.' href="'.$url.'" '.$onclick.' data-prop="'.$value->id.'" data-url="'.$data_url.'" data-menu="'.$value->nav_slug.'" data-menu_id="'.$value->nid.'">'.$value->nav_name.'</a></li>';

	endforeach;

	$html.='</ul>';
	return $html;
}
function get_property_social($p_id="") {

	$links= get_globalvalues($p_id, 'social_links');

    $html='<ul class="social-icons">';
if($links->trip_advisor!=''){	$html.='<li class="vec-icon"><a href="'.$links->trip_advisor.'" target="_blank"></a></li>';}
if($links->youtube!=''){	$html.='<li class="you-icon"><a href="'.$links->youtube.'" target="_blank"></a></li>';}
if($links->facebook!=''){	$html.='<li class="fac-icon"><a href="'.$links->facebook.'" target="_blank"></a></li>';}
if($links->twitter!=''){	$html.='<li class="twi-icon"><a href="'.$links->twitter.'" target="_blank"></a></li>';}
if($links->pinterest!=''){	$html.='<li class="pin-icon"><a href="'.$links->pinterest.'" target="_blank"></a></li>';}  
$html.='</ul>';
	echo  $html;

}
function get_prop_newsletter($p_id) {
		global $wpdb;
	
		$newsletter=$wpdb->get_row("SELECT t1.id, t2.id as fid FROM ". $wpdb->prefix."wpr_newsletters AS t1 INNER JOIN " .$wpdb->prefix."wpr_subscription_form AS t2 ON t1.id = t2.nid WHERE t1.hotel_id=$p_id limit 1");
		return $newsletter;
	
}
function get_prop_slides($p_id, $menu_slug ) {

		global $wpdb;
		$gid=$wpdb->get_row("SELECT gid FROM $wpdb->hotelsnav WHERE hotel_id=$p_id AND nav_slug='$menu_slug'");
		$slides = get_menu_slides($gid->gid);
		return $slides;
}
function get_menu_slides($gid) {

		global $wpdb;

		$slides=$wpdb->get_results("SELECT t1.path, t2.filename, t2.alttext, t2.description FROM $wpdb->prefix".ngg_gallery." AS t1 INNER JOIN $wpdb->prefix".ngg_pictures." AS t2 ON t1.gid = t2.galleryid WHERE t2.galleryid=$gid order by t2.sortorder");
		return $slides;

}
function get_all_tabs($h_id, $menu_id) {

		global $wpdb; 
   		$tabs = $wpdb->get_results("SELECT * FROM $wpdb->property_tab WHERE menu_id=$menu_id AND prop_id=$h_id and active=0 ORDER BY menu_order" , OBJECT_K );

		return $tabs;
	}
function get_contact_temp($p_id, $field) {

		$contacts = get_globalvalues($p_id, $field);
?>		
		    <!--<h3>contact us</h3> -->

    <div class="directionDiv">

 <h3>contact us</h3>
		    <div class="direct-content">
		        <div class="gen-information">
		         	<?php if($contacts->contact[0]->position){ ?><h3><?php echo $contacts->contact[0]->position; ?>:</h3><?php } ?>

					<?php if($contacts->contact[0]->name){ ?><h5><?php echo $contacts->contact[0]->name; ?> <?php echo $contacts->contact[0]->contact_number; ?></h5> <?php } ?>


					<?php if($contacts->contact[0]->email_id){ ?><h5><a href="mailto:<?php echo $contacts->contact[0]->email_id; ?>"><?php echo $contacts->contact[0]->email_id; ?></a></h5> <?php } ?>
						
				<?php if($contacts->contact[1]->position){?><h3><?php echo $contacts->contact[1]->position; ?>:</h3><?php } ?>
			<?php if($contacts->contact[1]->name || $contacts->contact[1]->contact_number ){?><h5><?php echo $contacts->contact[1]->name; ?>  <?php echo $contacts->contact[1]->contact_number; ?></h5> <?php } ?>
			
				<?php if($contacts->contact[1]->email_id){ ?><h5><a href="mailto:<?php echo $contacts->contact[1]->email_id; ?>"><?php echo $contacts->contact[1]->email_id; ?></a></h5> <?php } ?>
						
				<?php if($contacts->contact[2]->position){ ?><h3><?php echo $contacts->contact[2]->position; ?>:</h3><?php } ?>
<?php if($contacts->contact[2]->name || $contacts->contact[2]->contact_number){ ?> <h5><?php echo $contacts->contact[2]->name; ?> <?php echo $contacts->contact[2]->contact_number; ?></h5> <?php } ?>
					<?php if($contacts->contact[2]->email_id){ ?>	<h5><a href="mailto:<?php $contacts->contact[2]->email_id; ?>"><?php echo $contacts->contact[2]->email_id; ?></a></h5> <?php } ?>
						
				<?php if($contacts->inquiry[0]->position && trim($contacts->inquiry[0]->position) !=""){ ?><h3><?php echo $contacts->inquiry[0]->position; ?>:</h3><?php } ?>
						<h5><?php echo $contacts->inquiry[0]->name; ?><?php echo $contacts->inquiry[0]->contact_number; ?></h5>




				<?php if($contacts->inquiry[0]->email_id){ ?><h5><a href="mailto:<?php echo $contacts->inquiry[0]->email_id; ?>"><?php echo $contacts->inquiry[0]->email_id; ?></a></h5> <?php } ?>


				<?php if($contacts->inquiry[0]->inquiry_link){ 

					if($contacts->inquiry[0]->inquiry_text)
						$linktext  =$contacts->inquiry[0]->inquiry_text;
					else
						$linktext  ="click here to fill out an inquiry form";
				?>

					<h5><a href="<?php echo $contacts->inquiry[0]->inquiry_link; ?>"><?php echo $linktext; ?></a></h5>

					<?php } ?>

		          </div>
		          <div class="gen-information">
		         	<?php if($contacts->contact[3]->position){ ?><h3><?php echo $contacts->contact[3]->position; ?>:</h3><?php } ?>
						<h5><?php echo $contacts->contact[3]->name; ?> <?php echo $contacts->contact[3]->contact_number; ?></h5>
						<h5><a href="mailto:<?php echo $contacts->contact[3]->email_id; ?>"><?php echo $contacts->contact[3]->email_id; ?></a></h5>
						
				<?php if($contacts->contact[4]->position){ ?><h3><?php echo $contacts->contact[4]->position; ?>:</h3><?php } ?>
<h5><?php echo $contacts->contact[4]->name; ?> <?php echo $contacts->contact[4]->contact_number; ?></h5>
						<h5><a href="mailto:<?php echo $contacts->contact[4]->email_id; ?>"><?php echo $contacts->contact[4]->email_id; ?></a></h5>
						
				<?php if($contacts->contact[5]->position){ ?><h3><?php echo $contacts->contact[5]->position; ?>:</h3><?php } ?>
<h5><?php echo $contacts->contact[5]->name; ?> <?php echo $contacts->contact[5]->contact_number; ?></h5>
						<h5><a href="mailto:<?php echo $contacts->contact[5]->email_id; ?>"><?php echo $contacts->contact[5]->email_id; ?></a></h5>
						
				<?php if($contacts->inquiry[1]->position && trim($contacts->inquiry[1]->position) !=""){ ?>
						<h3><?php echo $contacts->inquiry[1]->position; ?>:</h3><?php } ?>
						<h5><?php echo $contacts->inquiry[1]->name; ?><?php echo $contacts->inquiry[1]->contact_number; ?></h5>
<?php if($contacts->inquiry[1]->email_id){ ?><h5><a href="mailto:<?php echo $contacts->inquiry[1]->email_id; ?>"><?php echo $contacts->inquiry[1]->email_id; ?></a></h5> <?php } ?>
				<?php if($contacts->inquiry[1]->inquiry_link){ 
	
					if($contacts->inquiry[1]->inquiry_text)
						$linktext  =$contacts->inquiry[1]->inquiry_text;
					else
						$linktext  ="click here to fill out an inquiry form";

				?>
					<h5><a href="<?php echo $contacts->inquiry[1]->inquiry_link; ?>"><?php echo $linktext; ?></a></h5>
		
				<?php } ?>



		          </div>
		        
		        
		        
				<div class="gen-information phone">
		         	<h4>reach us by phone:</h4>
		         	<h4><?php echo $contacts->phone1; if($contacts->phone2){ echo ' or '.$contacts->phone2; } ?></h4>
		          </div>		        
		        
		       </div>
		        
		    </div>
<?php } 

function get_address_temp($p_id, $field)  {

			$directions = get_globalvalues($p_id, $field);
?>
    <!-- <h3>Directions</h3> -->
   <div class="directionDiv">
		    <h3><?php if($p_id==9){ ?>Cape Resorts Beaches<?php }else{ ?>Directions<?php } ?></h3>
		    <div class="direct-content">
		        <div class="dir-left">
		          <h2><?php echo $directions->addressline1; ?></h2>
		          <h2><?php echo $directions->addressline2; ?></h2>
		          <h2><?php echo $directions->city; ?>, <?php echo $directions->state; ?> <?php echo $directions->zipcode; ?></h2>
		          <h2><?php echo $directions->phone; ?></h2>
		          <div class="find-us">
		            <h2><?php if($directions->find_us_text){echo $directions->find_us_text;}else{?> it's easy to find us!<?php } ?> </h2>
		            	<?php echo $directions->find_us; ?>
		          </div>
		        </div>
		        <div class="google-map">
		          <h2>from google maps:</h2>
		          <span><a href="<?php echo $directions->google_map; ?>" target="_blank">View Google Map &amp; Enter Your Starting Point</a></span>
		          <div class="specific">
		            <h2>from specific points</h2>

					<?php foreach($directions->points as $point) { ?>

						<a href="javascript:void(0);" class="points">From <?php echo $point->from; ?></a>
						<div class="hide point_details">
							  <span>approx. <?php echo $point->distance; ?></span>
								<p><?php echo stripslashes($point->route); ?> </p>
							
						</div>

					<?php } ?>
		            
		          </div>
		        </div>
		    </div>
    </div>
<?php } 
function get_promo_container($promos) {

		$promos = explode(",",$promos);
$html='';
		$posts = query_posts(array('post_type' => 'ps_promotion', 'post__in' => $promos,  'orderby' => 'post__in' ));

		if(count($posts) > 0) {  

		foreach($posts as $post): ?>

		<?php $links = get_post_meta($post->ID, '_promo_slider_disable_links', TRUE); ?>
		<?php $buttoncontent = get_post_meta($post->ID, '_promo_slider_linkText', TRUE);
			$gaqurl = get_post_meta($post->ID, '_promo_slider_url', TRUE);
			$button_content = explode(",", $buttoncontent);
			$button_text = $button_content[0];
			if($button_content[1]){
			$onclick = "onclick=\"_gaq.push(['_link','".$gaqurl."']); return false;\"";
			}else{$onclick = "";}	

		?>
		<?php if($links):  ?>
		<?php 
		if($button_text=='reserve online'){$splclass='reserveonline';}else{$splclass='';}
		$html .='<div class="resort-video '.$splclass.'">';
		if(preg_match("/youtube/i", $gaqurl)){ $youtube = 'class="promovideo"';}else{$youtube = '';}
        $html .='<a '.$youtube.' href="'.$gaqurl.'" '.$onclick.' target="'.get_post_meta($post->ID, '_promo_slider_target', TRUE).'">'.$button_text.'</a>';
     	 $html .='</div>';

		else:
		if($post->post_title=='spring'){$splclass='springbrunch';}elseif($post->post_title=="Baron's Cove"){$splclass='baroncove';}elseif($post->post_title=="press contact:"){$splclass='presscontact';}else{$splclass='';}
 		 $html .='<div class="spring '.$splclass.'">';
		 $html .='<h2>'.$post->post_title.'</h2>';
		if($post->post_title=="Baron's Cove" || $post->post_title=="press contact:"){
		 $html .= $post->post_content;
		}else{
		 $html .='<h3 class="normal">'.$post->post_content.'</h3>';
		}
		if($gaqurl){
		 $html .='<p><a href="'.$gaqurl.'" '.$onclick.' target="'.get_post_meta($post->ID, '_promo_slider_target', TRUE).'">'.$button_text.'</a></p>';
		}
     	$html .='</div>';

		 endif; 		 

 	endforeach; } 
	return $html; 
}
function get_book_button($id, $p_id) {
		$html ='';
	$hotelIds = array(1, 2, 3, 4, 5, 6, 7 );
		if(get_field('button_1', $id)):

			$text =  get_field('button_1', $id);
			$pieces = explode(",", $text);

			if(in_array($p_id, $hotelIds))	{	
				$html =get_booking_hotel($pieces, $p_id);
			if($html != '')
				return $html;
			}
			else {
				if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
				$html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
			}
					
		 endif; 



		 if(get_field('button_2', $id)): 

				$text =  get_field('button_2', $id);
				$pieces = explode(",", $text);
				if(in_array($p_id, $hotelIds))	{	
				$html =get_booking_hotel($pieces, $p_id);
				if($html != '')
				return $html;
				}
				else {
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
				$html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
				}

		
		 endif; 
		 if(get_field('button_3', $id)): 

			$text =  get_field('button_3');
			$pieces = explode(",", $text);
			if(in_array($p_id, $hotelIds))	{	
			$html =get_booking_hotel($pieces, $p_id);
			if($html != '')
				return $html;
			}
			else {
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
				$html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
			}


		 endif; 
		 if(get_field('button_4', $id)): 

			$text =  get_field('button_4');
			$pieces = explode(",", $text);
				if(in_array($p_id, $hotelIds))	{	
				$html =get_booking_hotel($pieces, $p_id);
				if($html != '')
				return $html;
				}
				else {
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
				$html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
				}

		endif;
	return $html;
}
function get_booking_hotel($pieces, $p_id) {

	$hotelMenus = array('Congress', 'Virginia', 'Cottages', 'Beach', 'Star', 'Sandpiper', 'Barons cove' );

	$hotel = $hotelMenus[$p_id-1];
	$pos = stripos($pieces[0], $hotel); 
	if ($pos !== false) 
		$html.='<a href="'.$pieces[1].'" target="_bank">'.$pieces[0].'</a>';
	else
	$html ='';
	return $html;

}
function get_nav_open1() {
	$html.='<div class="tabsDiv">';
	$html.='<div id="tabs">';
	return $html;
}
function get_nav_close1() {
	$html.='</div>';
	$html.='</div>';
	return $html;
}
function get_tabs_htmlNav($tabs) {
$html='';
		 foreach($tabs as $tab): 
			$dataClass= $tab->temp_type;
			$html.='<li><a href="#'.$tab->slug.'" data-class="'.$dataClass.'" data-promo="'.$tab->promos.'">'.$tab->t_name.'</a></li>';
		endforeach;
	return $html;
}
function get_tabs_htmlContent($tabs) {
$html='';

	foreach($tabs as $tab):
		$args = array();
	if($tab->type==0) { 
		$html.='<div id="'.$tab->slug.'" class="tabs_div">';
			 if($tab->header):
			$html.='<h3>'.$tab->header;
				if($tab->sub_header){
				$html.='<br>'.$tab->sub_header;
				 }
			$html.='</h3>';
			endif;
			$tab->page_id;			
			$post = get_post($tab->page_id, ARRAY_A);
			$content = apply_filters('the_content', $post['post_content']);

			$html.=$content; 
			if(get_field('go_to_website',$tab->page_id)){
$gotoweb =  get_field('go_to_website',$tab->page_id);
$gotowebsite = explode(",", $gotoweb);
	$html.='<p><a href="'.$gotowebsite[1].'" >'.$gotowebsite[0].'</a></p>'; 
}
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		
		$html.='</div>';
		}


		elseif($tab->type==1) {
		if($tab->slug=="faq"){
				$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div class="faq-ques">';
 	$html.='Q: <a href="'.get_permalink().'">'.get_the_title().'</a>';
         $newcontent= get_the_content();
         $html.='<div class="faq-ans" style="display:none;">'. $newcontent .'</div>';
         
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		}else{

	$html.='<div id="'.$tab->slug.'" class="events tabs_div" >
			<ul>';
				$post_type = $tab->post_type;

			$args['post_type']= array( $post_type );
			$args['posts_per_page']=-1;


$props = new WP_Query( $args );

while ( $props->have_posts() ) : $props->the_post();
		
		$html.='<li class="wedding-content wedding_promo">';
		$image_id = get_post_thumbnail_id();
 			 $image_url = wp_get_attachment_image_src($image_id,'med_274_122', true); 
  	$html.='<div class="';
	if(!$image_id){ $html.='noimg'; }	
	$html.='">';
	if($image_id){ $html.='<img src="'.$image_url[0].'" alt="meeting" />'; }
        $html.='<div>';
 	$html.='<h4>'.get_the_title().'</h4>';
         $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 150;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}
         $html.='<p class="view-links">	<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';	
	if(get_field('wedding_specialist')){ $wedding_specialist =  get_field('wedding_specialist'); 
$html .='<span class="v-gallery">';
$html .='<a class="weddingspecialist" href="'.$wedding_specialist.'" title="'.get_the_title().'">CONTACT A WEDDING SPECIALIST</a>';
$html .='</span>';
}
         $html.='<span class="v-gallery">';
			if(get_field('button_1')):

		 $text =  get_field('button_1');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_2')): 

		 $text =  get_field('button_2');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			$html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif; 
		 if(get_field('button_3')): 

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			 $html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		
		 endif;  $html.='</span>';
       $html.='</p>';
      $html.='</div></div>';



		$html.='</li>';
		 endwhile; 
			$html.='</ul>';


	if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}		

		$html.='</div>';
		
		}

		} 

		elseif($tab->type==2) {

		$html.='<div id="'.$tab->slug.'" class="events tabs_div">';

			if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
				$post_type =  $tab->post_type;
		$args['post_type']=$post_type;
		$args['posts_per_page']=-1;

		$args[ 'meta_key'] = 'property';
		//$args[ 'meta_value'] = $p_id;

		$activities = new WP_Query( $args );
while ( $activities->have_posts() ) : $activities->the_post();
	$propId = get_field('property');

if(	is_array($propId)) {
	$reOut = in_array($p_id, $propId);
} else {
	$reOut = $p_id == $propId? true : false;
}if($reOut) {
   $html.='<div>';
      $html.='<h4>'.get_the_title().'</h4>';
      $html.='<h5>'.get_field('subtitle1').'</h5>';
         $html.='<h6>'.get_field('subtitle_2').'</h6>';
	if(get_field('subtitle_2')){$newcont_count = 200;}else{$newcont_count = 250;}
         $newcontent= get_the_content();
	if(strlen($newcontent)>$newcont_count){
         $html.='<div class="initial-content">'.cr_trim_chars( $newcontent, $newcont_count).' <a href="'.get_permalink().'" class="readmore" data-id="'.get_the_ID().'">... READ MORE</a></div>';
	 $html.='<div class="final-content" style="display:none;">'. $newcontent .'</div>';
	}else{
         $html.='<div class="initial-content">'. $newcontent .'</div>';
	}

      $html.='<p class="view-links">';

		$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';

        $html.='<span class="v-gallery">';

		if(get_field('button_1')):

		$text =  get_field('button_1');

		$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
		$html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		 endif;
		 if(get_field('button_2')):

		$text =  get_field('button_2');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
		$html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';

		endif;
		if(get_field('button_3')):

		$text =  get_field('button_3');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			$html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
 endif; 
		 if(get_field('button_4')): 

		 $text =  get_field('button_4');

			$pieces = explode(",", $text);
if($pieces[2]){ $onclick = "onclick=\"_gaq.push(['_link','".$pieces[1]."']); return false;\"";}else{$onclick = "";}
			$html.='<a href="'.$pieces[1].'" '.$onclick.' target="_bank">'.$pieces[0].'</a>';
		 endif;

        $html.='</span> </p>  </div>';
}
	 endwhile;
			if($tab->promos==1) {

				$html.='<div class="hide hided_promos" >';
				
				$html.=get_promo_container($tab->promo_lists);
				$html .='</div>';
		}
		$html.='</div>';		


		} elseif($tab->type==3){
$html.='<div id="'.$tab->slug.'" class="events tabs_div">';
		if($tab->header):
			$html.='<h3>'.$tab->header;
			if($tab->sub_header){
			$html.='<br>'.$tab->sub_header;
			 } 
			$html.='</h3>';
			endif; 
$html.='<div class="gallery-container">';
$html.='<div class="gallery-images">';
			        $html.='<ul>';

global $nggdb;
    $galleries = array();

    $album = $nggdb->find_album($tab->album_id);

    foreach( $album->gallery_ids as $gallery => $gid ) { 
       $galleries[]['id'] = $gid;
    }
    foreach( $galleries as $key => $galleryitem ){ 


 $gallery = $nggdb->get_gallery($galleryitem['id']);

	if(count($gallery) > 0)  {

	 $html.='<li>';

			$i=0;
			foreach($gallery as $gall) { 
 $gall->slug = str_replace("-","",$gall->slug );
		
		if($i==0) { 
		$html.='<a class="pretty_photo" rel="prettyPhoto" href="#'.$gall->slug.'" data-rel="'.$gall->slug.'"><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a>';

		  $html.='<div class="galler-text">';
            $html.='<h3>'.$gall->title.'</h3>';
            $html.='<span>'.$gall->galdesc.'</span>';
            $html.='<a rel="prettyPhoto" href="#'.$gall->slug.'" data-rel="'.$gall->slug.'" class="view_gallery pretty_photo">view</a>';
         $html.='</div>';		
$html.='<div style="display:none"><div class="overall-popup" id="'.$gall->slug.'">';
$html.='<a href="javascript:void(0);" class="share"><span class="st_sharethis" displayText="SHARE" st_title="'.get_the_title().'" st_url="'.get_permalink().'"></span></a>';


$html.='<div class="gallerydet"><h3>'.$gall->title.'</h3>';
           $html.='<span>'.$gall->galdesc.'</span></div>';
		} $i++;} 
$html.='<div class="gallery_popup" id="gallery_popup'.$gall->slug.'">';
$html.='<ul class="bxslider bxslider'.$gall->slug.'">';
foreach($gallery as $gall) { 
  $html.='<li><a target="_bank" href="//pinterest.com/pin/create/button/?url='.get_permalink().'/gallery&media='.$gall->imageURL.'&description='.$gall->description.'" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" class="pint" /></a>

<img src="'.$gall->imageURL.'" title="test'.$gall->description.'" alt="corner-img" /></li>';
 } 
$html.='</ul>';
$html.='</div>';
$html.='<ul class="bx-pager-images jcarousel jcarousel-skin-tango" id="bx-pager'.$gall->slug.'">';
$i=0; foreach($gallery as $gall) { 
  $html.='<li><a data-slide-index="'.$i.'" href=""><img src="'.$gall->thumbURL.'" title="'.$gall->description.'" alt="corner-img" /></a></li>';
 $i++; } 
 $html.='</ul>';
$html.='</div>';
$html.='</li>';
}
  }
$html.='</div>';
$html.='</div>';
 $html.='</ul>';
$html.='</div>';
		} 

		endforeach;
	return $html;
}
?>
