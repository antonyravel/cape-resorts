<?php
/*
	* Plugin Name: cr manage pages
	* Description: Plugin to manage all sub pages of CR
	*Author: Shriram IT Solutions
	*Author URI: http://shriramits.com
	*Version: 0.1
 */
global $wpdb;

if(!isset( $wpdb->{$table_prefix.'conciergepages'} ) ){
  $wpdb->conciergepages = $table_prefix.'conciergepages';
  
}
if(!isset( $wpdb->{$table_prefix.'hotels'} ) ){
    $wpdb->hotels = $table_prefix.'hotels';
}
if(!isset( $wpdb->{$table_prefix.'hotelsnav'} ) ){
	$wpdb->hotelsnav = $table_prefix.'hotelsnav';
}
if(!isset( $wpdb->{$table_prefix.'hotels_globaldata'} ) ){
	$wpdb->hotels_globaldata = $table_prefix.'hotels_globaldata';
}
if(!isset( $wpdb->{$table_prefix.'property_tab'} ) ){
	$wpdb->property_tab = $table_prefix.'property_tab';
}

include('front/cong-temp.php');
include_once('front/functions.php');
include_once('functions.php');

add_action('admin_enqueue_scripts', 'wptuts_options_enqueue_scripts');
function wptuts_options_enqueue_scripts() {  
    
    wp_register_script( 'cr_upload_pdf', plugins_url('',__FILE__).'/js/cr_upload_pdf.js', array('jquery','media-upload','thickbox') );  
 
}  

/* Create pages for Admin */
add_action('admin_menu', 'cr_create_admin_page');


add_action('admin_menu', 'cr_hide_admin_posts');

//add_action('admin_init', 'editor_admin_init');

add_action('admin_head', 'editor_admin_head');
add_action( 'init', 'create_posttypes');

function editor_admin_head() {
		
  wp_tiny_mce();

}

function cr_hide_admin_posts() {

		global $wpdb;
		
		$results = array();
		
		$results = getProperty_postTypes();

		foreach ( $results as $result ) {

	$post_type = get_current_post_type();

	
		if ( $post_type != $result->post_type ) { 
			remove_menu_page( 'edit.php?post_type='.$result->post_type );
		} 

		}
			remove_menu_page( 'edit.php?post_type=menutab' );
} 
function getProperty_postTypes() {

		global $wpdb;

			$sql = "SELECT
				post_type, slug
			FROM 
				$wpdb->property_tab 
			WHERE type=1
			";
		
		$results = $wpdb->get_results($sql); 

		return $results;

}

function create_posttypes()  {

		global $wpdb;

		$results = array();

		$results = getProperty_postTypes();

		foreach ( $results as $result ) {

			if($result->post_type !='poolbeach_5_101') {
    $argss = array(
           'label' => __($result->slug),
           'singular_label' => __($result->slug),
           'public' => true,
           'show_ui' => true,
           'capability_type' => 'page',
           'hierarchical' => true,
           'rewrite' => true,
           'supports' => array('title', 'thumbnail', 'editor' , 'custom-fields', 'page-attributes'),
           'exclude_from_search' => true
		
         );   
         register_post_type($result->post_type, $argss);

		} 
}

  $argss = array(
           'label' => __('poolbeach'),
           'singular_label' => __('poolbeach'),
           'public' => true,
           'show_ui' => true,
           'capability_type' => 'page',
           'hierarchical' => true,
           'rewrite' => true,
           'supports' => array('title', 'thumbnail', 'editor' , 'custom-fields', 'page-attributes'),
           'exclude_from_search' => true,
			'menu_position' => 3
         );   
         register_post_type('poolbeach_5_101', $argss); 



}


function cr_create_admin_page()
{
    $page_title = "Manage Pages";
    $menu_title = "Manage Pages";
    $capability = 'manage_options';
    $menu_slug = "Manage Pages";
   
    
     add_menu_page($page_title, $menu_title, '', $menu_slug,'cr_culture_page');
     add_submenu_page($menu_slug, 'Concierge Pages', 'Concierge Pages', $capability, 'conciergepages', 'cr_conciergemainpage');
	add_submenu_page($menu_slug, 'Concierge Home', 'Concierge Home', $capability, 'conciergehome', 'cr_conciergehome');
     add_submenu_page($menu_slug, 'Manage Hotels', 'Manage Hotels', $capability, 'managehotels', 'cr_managehotels');
     add_submenu_page($menu_slug, 'Experiences Pages', 'Experiences Pages', $capability, 'experiencespages', 'cr_experiencespages');
     add_submenu_page($menu_slug, 'Restaurants Pages', 'Restaurants Pages', $capability, 'restaurantspages', 'cr_restaurantspages');
   	 add_submenu_page($menu_slug, 'Page Gallery Mapping', 'Page Gallery Mapping', $capability, 'PageGalleryMapping', 'cr_pagegallerymapping');
   	 add_submenu_page($menu_slug, 'Album Gallery Mapping', 'Album Gallery Mapping', $capability, 'AlbumGalleryMapping', 'cr_albumgallerymapping');
   	 add_submenu_page($menu_slug, 'Home Page', 'Home Page', $capability, 'homepage', 'cr_homepage');
   	 add_submenu_page($menu_slug, 'Capemay Overview', 'Capemay Overview', $capability, 'capemayoverview', 'cr_capemayoverviewpage');

     //add_submenu_page($menu_slug, 'Manage Hotels (pdf & availability)', 'Manage Hotels (pdf & availability)', $capability, 'hotelfactsheets', 'ic_factsheets_page');
}
function cr_culture_page(){
	add_requiredscripts();
}
function cr_conciergemainpage()
{
	add_requiredscripts();
    include_once('conciergemainpage.php');
   
}
function cr_managehotels(){
	add_requiredscripts();
    include_once('hotelmainpage.php');
}
function cr_capemayoverviewpage(){
	add_requiredscripts();
    include_once('capemayoverviewpage.php');
}
function cr_homepage(){
	add_requiredscripts();
    include_once('homemainpage.php');
}
function cr_experiencespages(){
	add_requiredscripts();
    include_once('experiencesmainpage.php');
}
function cr_restaurantspages(){
	add_requiredscripts();
    include_once('restaurantsmainpage.php');
}
function cr_pagegallerymapping() {
    include_once('pagegallerymapping.php');
}
function cr_albumgallerymapping() {
    include_once('albumgallerymapping.php');
}
function cr_conciergehome() {
add_requiredscripts();
    include_once('concierge-home.php');
}
function add_requiredscripts() {

include_once('admin/functions.php');
		wp_register_style( 'cr_manage_pages_css', plugins_url('style.css',__FILE__) ); 
		wp_enqueue_script('jquery');  
		wp_enqueue_script('thickbox');  
		wp_enqueue_script('media-upload');  
		wp_enqueue_script('cr_upload_pdf'); 
		wp_enqueue_script('word-count');
		wp_enqueue_script('post');
		wp_enqueue_script('page');
		wp_enqueue_script('editor');
		wp_enqueue_script( 'common' );
		wp_enqueue_script('utils');
		wp_enqueue_style('thickbox');  
		wp_enqueue_style('cr_manage_pages_css');

		wp_enqueue_script( 'jquery-ui-tabs' );
wp_enqueue_script('draganddrop', site_url().'/jquery.tablednd.js', array('jquery'));
		wp_enqueue_style( 'pw-admin-ui-tabs', plugins_url( '/css/ui.tabs.css', __FILE__ ) );

}

add_action('admin_init','cr_update_data');
function cr_update_data(){

	if(isset($_GET['magazine_url']))
	{
		$new_value = $_GET['magazine_url'];

		if ( get_option('magazine_url') ) {
			update_option( 'magazine_url', $new_value );
		} else {
			$deprecated = ' ';
			$autoload = 'no';
			add_option( 'magazine_url', $new_value, $deprecated, $autoload );
		}
			wp_redirect( site_url().'/wp-admin/admin.php?page=conciergepages'); exit; 
	}
}


/* Admin Ajax Area */

add_action('wp_ajax_address', 'hotel_address');
add_action('wp_ajax_nopriv_address', 'hotel_address');

/* Save Gallery */
add_action('wp_ajax_menugallery', 'hotel_menugallery');
add_action('wp_ajax_nopriv_menugallery', 'hotel_menugallery');

add_action('wp_ajax_conciergehomepost', 'conciergehomepost');
add_action('wp_ajax_nopriv_conciergehomepost', 'conciergehomepost');


function hotel_address(){

global $wpdb;
$field = $wpdb->get_row("select hotel_id FROM $wpdb->hotels_globaldata WHERE hotel_id=".$_POST['hotel_id']." AND field='".$_POST['field_value']."'", ARRAY_A);

$post_array = $_POST;
$post_array['addressline1'] = stripslashes(htmlspecialchars($post_array['addressline1'], ENT_QUOTES));
$post_array['addressline2'] = stripslashes(htmlspecialchars($post_array['addressline2'], ENT_QUOTES));
$post_array['city'] = stripslashes(htmlspecialchars($post_array['city'], ENT_QUOTES));
$post_array['state'] = stripslashes(htmlspecialchars($post_array['state'], ENT_QUOTES));
$post_array['find_us_text'] = stripslashes(htmlspecialchars($post_array['find_us_text'], ENT_QUOTES));
$post_array['find_us'] = mysql_real_escape_string($post_array['find_us']);

foreach ($post_array['points'] as $key => $value) {
	$post_array['points'][$key]['route'] =mysql_real_escape_string($post_array['points'][$key]['route']);
	
}

//$main = mysql_real_escape_string($_POST['main']);
if(count($field) > 0) {

$wpdb->query("UPDATE $wpdb->hotels_globaldata SET value ='". json_encode($post_array)."' WHERE hotel_id =". $_POST['hotel_id']." AND field ='". $_POST['field_value']."'");


}else {

$result['id'] = $wpdb->insert($wpdb->hotels_globaldata, array('hotel_id' => $_POST['hotel_id'], 'field' => $_POST['field_value'],'value'=> json_encode($post_array)));  

}

$result['type'] = "success";

$result = json_encode($result);
echo $result; 

die();
}
function hotel_menugallery() {

global $wpdb;


if(isset($_POST['tab_action']) && $_POST['tab_action']=='tab') {

		if(isset($_POST['tab_id'])) {

	if($_POST['promos']) {
		$temp_type = 'yespromo';
	
	} else {

		$temp_type = 'diningDiv';
	}

	//echo $temp_type;			
			if($_POST['data_action']=='delete') {

				if($_POST['type']=='0') { 

					 wp_delete_post( $_POST['page_id'], true); 

				} elseif($_POST['type']=='1') {
				echo $query;
				$query = "DELETE FROM $wpdb->posts WHERE post_type = '".$_POST['post_type']."' LIMIT 0, 3000";

				$query1 = "DELETE FROM $wpdb->options WHERE option_value LIKE '%".$_POST['post_type']."%'";

				$wpdb->query($query1);

				}

			
				$wpdb->query("DELETE FROM $wpdb->property_tab WHERE id =". $_POST['tab_id']." AND prop_id=".$_POST['prop']." AND menu_id=".$_POST['menu'] );


			}elseif($_POST['data_action']=='edit'){ 

	$new = str_replace('array[]=','',$_POST['promos_list']);
	$new = str_replace('&',',',$new);


$wpdb->update( $wpdb->property_tab, array('t_name' => stripslashes(htmlspecialchars($_POST['t_name'], ENT_QUOTES)), 'header' => stripslashes(htmlspecialchars($_POST['header'], ENT_QUOTES)), 'promo_lists'=>$new, 'sub_header'=> stripslashes(htmlspecialchars($_POST['subheader'], ENT_QUOTES)), 'slug'=>$_POST['slug'],'promos'=>$_POST['promos'], 'temp_type' => $temp_type, 'file_path'=>$_POST['logo_url']), array("id" => $_POST['tab_id']), array("%s","%s","%s","%s","%s","%d","%s", "%s"), array("%d") );
			} else {

				$tab_status = $wpdb->get_row("select active from $wpdb->property_tab WHERE id =". $_POST['tab_id']." AND prop_id=".$_POST['prop']." AND menu_id=".$_POST['menu'] );
				if($tab_status->active==0) 
					$active = 1;
				else
					$active = 0;
				$wpdb->query("UPDATE $wpdb->property_tab SET active =$active WHERE id =". $_POST['tab_id']." AND prop_id=".$_POST['prop']." AND menu_id=".$_POST['menu'] );


			}




		} else {

	$slug = sanitize_title($_POST['t_name']);
	$postName = $slug.'_'.$_POST['hotel_id'].'_'.$_POST['menu_id'];
	
	$post_id=0;
	if($_POST['type'] == '0') {
		
		$post = array(
  			'post_status'    => 'publish',
  			'post_type'      => 'menutab',
		);  
		$post_id = wp_insert_post( $post);

	} 

		elseif($_POST['type'] == '2') {

			$postName = $_POST['concierge_post'];

		}


	elseif($_POST['type'] == '1') {

		 $options =  array('label'=>$_POST['t_name'], 'labels'=>$_POST['t_name'].'s', 'post_type'=>$postName, 'property'=>$_POST['hotel_id'], 'menu'=>$_POST['menu_id'] );
		update_option( 'prop_posttype', $options); 

	}

		if($_POST['promos']) {
		$temp_type = 'yespromo';
	
	} else {

		$temp_type = 'diningDiv';
	}


	  $wpdb->insert($wpdb->property_tab, array('prop_id' => $_POST['hotel_id'], 'menu_id' => $_POST['menu_id'],'t_name' => stripslashes(htmlspecialchars($_POST['t_name'], ENT_QUOTES)), 'slug' => stripslashes(htmlspecialchars($slug, ENT_QUOTES)), 'type' => $_POST['type'], 'page_id' => $post_id, 'post_type' => stripslashes(htmlspecialchars($postName, ENT_QUOTES)), 'header' => stripslashes(htmlspecialchars($_POST['header'], ENT_QUOTES)), 'sub_header'=> stripslashes(htmlspecialchars($_POST['subheader'], ENT_QUOTES)), 'file_path'=>$_POST['logo_url'], 'promos' => $_POST['promos'], 'temp_type'=>$temp_type));  

		}

}elseif($_POST['tab_action']=='promos') {
	$new = str_replace('array[]=','',$_POST['promos']);
	$new = str_replace('&',',',$new);
$wpdb->query("UPDATE $wpdb->hotelsnav SET promos ='". $new."' WHERE hotel_id =". $_POST['hotel_id']." AND nid ='". $_POST['menu_id']."'");

}

else {

	$wpdb->query("UPDATE $wpdb->hotelsnav SET gid ='". $_POST['gallery']."' , seotitle ='". $_POST['seotitle']."' , meta_description ='". $_POST['meta_description']."' , focus_keywords ='". $_POST['focus_keywords']."' WHERE hotel_id =". $_POST['hotel_id']." AND nid ='". $_POST['menu_id']."'");

}
	$result['type'] = "success";

	$result = json_encode($result);
	echo $result; 
die();

}



 function get_globalvalues($hotel_id, $field) {

	global $wpdb;

	$field = $wpdb->get_row("select value FROM $wpdb->hotels_globaldata WHERE hotel_id=".$hotel_id." AND field='".$field."'", ARRAY_A);

	//return json_decode($field['value']);
	return json_decode($field['value']);
}
function conciergehomepost() {
			
			$new = str_replace('item[]=','',$_POST['item']);
			$new = str_replace('&',',',$new);
//if ( get_option( 'concierege_posts' ) ) { echo $new;
    update_option( 'concierege_posts', $new );
//} else {
 //   $deprecated = ' ';
 //   $autoload = 'no'; echo $new;
 //   add_option( 'concierege_posts', $new, $deprecated, $autoload );
//}
echo "Successfully updated";
die();
}

