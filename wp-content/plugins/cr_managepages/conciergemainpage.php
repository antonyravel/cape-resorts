<?php

/*
 * Concierge Main Page template
  */
$errorMsgNeighborHood =  '';
global $wpdb;

$crconcierges = $wpdb->get_results("select id, pagename, slug from $wpdb->conciergepages order by id asc",ARRAY_A);

//$rsPages = $wpdb->get_results("select id, pagename from $wpdb->neighborhoodpages  order by id asc",ARRAY_A);

//$rsMainpage = $wpdb->get_results("select * from $wpdb->neighborhoodspagesdata  where  hotelid = '".$_GET['hid']."' && pageid='".$_GET['pgid']."'",ARRAY_A);
?>
<script type="text/javascript">
var $ = jQuery;
	 function change_hotel()
	{  
		var pageSelect = document.getElementById('pages_select'),
		location =pageSelect.value, url;
		if( location != '' ) {
			if(location=="prearrival") {
				url ="<?php echo site_url(); ?>/wp-admin/post.php?post=9&action=edit";
			} 
		else if(location=="conciergemagazine") {
			$('#magazine_link').show();
		}
		else { 
				url ="<?php echo site_url(); ?>/wp-admin/edit.php?post_type="+location;
			} 
		if(url) 
			window.location.href= url;
		}
	}

</script>

<div id="poststuff">
	 <div id="post-body" class="metabox-holder columns-2">
		<div id="post-body-content">
			<div id="hotelcustomefields" class="postbox ">

				<div class="handlediv" title="Click to toggle"><br></div>
				<h3 class="hndle"><span>Manage Concierge Pages</span></h3>


				<div class="inside">
					
					<div style="align:left;"> 
					<?php 
						//echo apply_filters('ic_neighborhood_msg',$_REQUEST);
					?>
					</div>
					<div style="clear:both"></div>

					<table width="100%" cellpadding="8">
						<tr>
							<td align="left">Select Pages</td>
							<td align="left">
							<select id="pages_select" name="hotels">
							<option value="">Select Page</option>
							<?php 

							foreach($crconcierges as $crconcierge){
							if($crconcierge['slug'] != 'daily-calender' ) :
							?>
							<option value="<?php echo $crconcierge['slug']; ?>"><?php echo $crconcierge['pagename']; ?></option>
							<?php
							endif;
							}
							?>
							</select>
							</td>
							<td align="left"><button id="addconcierge" onclick="change_hotel()">Add/Edit</button></td>
						</tr>

						<tr valign="top" id="magazine_link">
							<form action="" method="get">
								<td scope="row"><label for="magazine_url">Enter magazine url</label></td>
								<td><input type="text" id="magazine_url" name="magazine_url" size="36" value="<?php echo get_option('magazine_url'); ?>" /><input id="upload-magazine" type="button" value="Upload PDF" /></td>

								<td align="left"><button id="savemagazine" type="submit">Save</button></td>
							</form>
						</tr>


					</table>
				</div><!-- .inside   -->

			</div> <!-- #hotelcustomefields  -->
		</div> <!-- #post-body-content  -->
	</div> <!-- #post-body Ends  -->
</div>	<!-- #poststuff Ends  -->
