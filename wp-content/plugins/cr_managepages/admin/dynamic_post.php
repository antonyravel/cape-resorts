<?php

/* Get hotels Dynamically */
global $wpdb;

function my_acf_load_field( $field )
{
	$field['property'] = array();

	$hotels = $wpdb->get_results("select id, name, slug from $wpdb->hotels order by id asc",ARRAY_A);
 	file_put_contents('foo.txt', serialize($hotels));
	if( is_array($hotels) )
	{
		foreach( $hotels as $hotel )
		{
			$field['property'][ $hotel['id'] ] = $hotel['name'];
		}
	}
 
    return $field;
}
 
add_filter('acf/load_field/name=color', 'my_acf_load_field');


?>
