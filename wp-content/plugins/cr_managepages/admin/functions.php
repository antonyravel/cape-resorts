<?php
global $functions;
$functions = new manageFunctions();
class manageFunctions {

		var $albums = false;
		var $currentId = false;
		var $galleries= false;
		var $all_galleries=false;
		var $currentGallery=false;

    public function manageFunctions() {
		

    }

	public function get_gallery_by_hotel($g_id) {

		global $wpdb;	global $nggdb;
		$this->currentGallery= $g_id;
		$this->currentId=$_GET['hotel_id']; 
		$this->galleries  = $this->get_galleries();
		
		$this->all_galleries  = $nggdb->find_all_galleries();
		return $this->get_container();
	}

	public function get_galleries()  {
		global $wpdb;
   		$album = $wpdb->get_results("SELECT * FROM $wpdb->nggalbum WHERE hotel_id=$this->currentId Limit 1" , OBJECT );
		$galleries = empty ($album[0]->sortorder) ? array() : (array) unserialize($album[0]->sortorder);
		return $galleries;
}
public function get_gallery($id)  {
		global $wpdb; 
   		$gallery = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $wpdb->nggallery WHERE gid = %d", $id ) );
		return $gallery;
}
public function get_seotitle($h_id, $menu_id)  {
		global $wpdb; 
   		$seotitle = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $wpdb->hotelsnav WHERE hotel_id=$h_id and nid=$menu_id" ) );
		return $seotitle;
}
	function get_container() {
		global $wpdb; ?>
		<select name="gallery">	
			<option value="">Select Gallery</option>
<?php	
			foreach($this->galleries as $id) {

			$gallery = $this->get_gallery($id); 
			$selected = $this->currentGallery== $gallery->gid ? 'selected' : ''; 
?>
			<option <?php echo $selected; ?> value="<?php echo $gallery->gid; ?>"><?php echo $gallery->title; ?></option>
<?php
			}
?> 					
		</select>
<?php
	}
function get_all_tabs($h_id, $menu_id) {

		global $wpdb; 
   		$tabs = $wpdb->get_results("SELECT * FROM $wpdb->property_tab WHERE menu_id=$menu_id AND prop_id=$h_id order by menu_order" , OBJECT_K );

		return $tabs;
	}
function get_tab_details($h_id, $menu_id,$id) {
		global $wpdb; 
   		$tabs = $wpdb->get_results("SELECT * FROM $wpdb->property_tab WHERE menu_id=$menu_id AND prop_id=$h_id and id=$id" , OBJECT_K );
		return $tabs;
	}
function get_nav_details($h_id, $menu_id, $key) {

		global $wpdb; 
   		$res = $wpdb->get_results("SELECT {$key} FROM $wpdb->hotelsnav WHERE nid=$menu_id AND hotel_id=$h_id order by menu_order");

		return $res;

}

}
