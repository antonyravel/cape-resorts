<?php

/*
 * Concierge Main Page template
*/


global $wpdb;
$title = "Manage Home Page";
$sub_title = "Global options";

add_action( 'admin_footer', 'admin_scripts_footer' );

function admin_scripts_footer() {
?>
<script type="text/javascript">
  //<![CDATA[
var $ = jQuery;
  jQuery(document).ready(function($) {
var appendEl, points;
	$('.jquery_tabs').tabs({ fx: { opacity: 'toggle' }});
	$('#add_points').bind('click', function(e) {
	e.preventDefault();
	if($('.route').length > 0) {
		appendEl = $('.route');
		points = appendEl.length+1;
	}
	else {
		appendEl = $('#specific_points');
		points=1;
	}
	appendEl.last().after('<tr class="route" id="route_'+points+'"><td align="left" colspan="2"><table style="width:95%;" data-count="'+points+'"><tr><td align="left" colspan="2"><h4>Route</h4></td></tr><tr><td align="left"><label for="from">From</label></td><td><input size="40" type="text" name="points['+points+'][from]"></td></tr><tr><td align="left"><label for="distance">Time</label></td><td><input size="40" type="text" name="points['+points+'][distance]" ></td></tr><tr><td align="left"><label for="distance">Route</label></td><td><textarea rows="6" cols="60" name="points['+points+'][route]"></textarea></td></tr><tr><td colspan="2"><span><button class="removeroute" data-id="'+points+'">Remove</button></span></td></tr></table></td></tr>');
	})

	$('.removeroute').live('click', function() {
		var rEl=$(this).attr('data-id');
		$('#route_'+rEl).remove();
	});
//$("#table-1").tableDnD();
$("#table-1").tableDnD({
onDrop: function(table, row) {
  $.tableDnD.serialize();

$.ajax({
 type: "POST",
 url: "draganddrop.php",
 data: $.tableDnD.serialize(),
 success: function(html){
 }
});
	    }
	});
 	});

	 function change_hotel(el)
	{  
	//	var pageSelect = document.getElementById('pages_select'),
		var location =el.value;
		if( location != '' ) {
				window.location.href= location;
			} 
	}

  //]]>
</script>
<?php
}
if($_GET['hotel_id'] || $_GET['submenu_id']) {
	$h_id = $_GET['hotel_id'];

if($_GET['submenu_id']) {

	$h_sub_id= $_GET['submenu_id'];
	$hotel_tabs=$wpdb->get_results("SELECT * FROM $wpdb->hotelsnav WHERE nid=$h_sub_id", ARRAY_A);
	$sub_title = $hotel_tabs[0]['nav_name'];
	$gallery_id = $hotel_tabs[0]['gid'];  
	$sub_title;
	}

	$hotel_navs=$wpdb->get_results("SELECT t1.*, t2.* FROM $wpdb->hotels AS t1 INNER JOIN $wpdb->hotelsnav AS t2 ON t1.id = t2.hotel_id WHERE t1.id=$h_id", ARRAY_A);
	$title ="Manage " .$hotel_navs[0]['name']; 
	
}
?>
<div id="poststuff" class="manage_hotelpage">
	 <div id="post-body" class="metabox-holder columns-2">
		<div id="post-body-content">
			<div id="hotelcustomefields" class="postbox ">

				<div class="handlediv" title="Click to toggle"><br></div>
				<h3 class="hndle"><span><?php echo $title; ?></span></h3>

				<div class="inside">
					
					<div style="clear:both"></div>

<?php 
if($h_id) {
?>
<div id="hotel_global">
		
		<?php echo get_hotelNav($hotel_navs); ?>

	<h2 class="sub_title"><?php echo $sub_title; ?></h2>
<?php if($h_sub_id) { 
		include_once('home_sub_nav.php');   
		menuManagement::loadingPage($h_id, $h_sub_id, $gallery_id);	

	?>
<?php } else { ?>
<div id="contact_tabs" class="jquery_tabs">
	<ul>
		<li><a href="#logo_phone"><span>Logo and Reservations</span></a></li>
	</ul>
		<div id="logo_phone">
				<?php $logo = get_globalvalues($h_id, 'logo_phone');  ?>
			<h2>Address</h2>
			<form id="logophone_form">
			<input type="hidden" name="hotel_id" value="<?php echo $h_id; ?>">
			<input type="hidden" name="action" value="address">
			<input type="hidden" name="field_value" value="logo_phone">
			<table width="100%" cellpadding="8">
				<tr>
					<td scope="row" valign="top"><label for="logo_url">Enter Hotel Logo</label></td>
					<td><input type="text" id="logo_url" name="logo_url" size="36" value="<?php echo $logo->logo_url; ?>" />
						<input class="upload-button" type="button" value="Upload Logo" />
						<div><img src="<?php echo $logo->logo_url; ?>" alt="Logo" /></div>

					</td>
				</tr>
				<tr>
					<td><label for="reservation_number">Reservation Phone number</label></td>
					<td align="left"><input size="40" type="text" name="reservation_number" value="<?php echo $logo->reservation_number; ?>" /></td>
				</tr>
				<tr>
					<td><label for="reservation_link">Reservation Link</label></td>
					<td align="left"><input size="40" type="text" name="reservation_link" value="<?php echo $logo->reservation_link; ?>" /></td>
				</tr>

				<tr><td align="left" colspan="2"><button class="submit_details">Save</button><span class="loader">Processing..</span></td></tr>
			</table>
		</form>
		</div> <!-- #social_links Ends -->





		</div> <!-- #contact_tabs Ends -->
		<?php } ?>

</div> <!-- #hotel_global Ends -->

<?php }
else {
$hotels = $wpdb->get_results("select id, name, slug from $wpdb->hotels where type='home' order by id asc",ARRAY_A);
?>
					<table width="100%" cellpadding="8">
						<tr>
							<td align="left">Select Page</td>
							<td align="left">
							<select id="pages_select" name="hotels" onchange="change_hotel(this)">
							<option value="">Select Page</option>
							<?php 

							foreach($hotels as $hotel){
							?>
							<option value="<?php echo site_url().'/wp-admin/admin.php?page=homepage&hotel_id='. $hotel['id']; ?>"><?php echo $hotel['name']; ?></option>
							<?php
							}
							?>
							</select>
							</td>
							<!-- <td align="left"><button id="addconcierge" onclick="change_hotel()">Add/Edit</button></td> -->
						</tr>

					</table>
<?php } ?>

				</div><!-- .inside   -->
			</div> <!-- #hotelcustomefields  -->
		</div> <!-- #post-body-content  -->
	</div> <!-- #post-body Ends  -->
</div>	<!-- #poststuff Ends  -->



<?php /* Functions  */   

function get_hotelNav($navs) { 
    global $wpdb; 
    $hotels = $wpdb->get_results("select id, name, slug from $wpdb->hotels where type='home' order by id asc",ARRAY_A);
    ?>
    
					<table width="100%" cellpadding="8">
				   	<tr>
							<td align="left">Select Page</td>
							<td align="left">
							<select id="pages_select" name="hotels" onchange="change_hotel(this)">
							<option value="">Select Page</option>
							<?php 
							foreach($hotels as $hotel){
							if($hotel['id']  == $_GET['hotel_id']){ $hsl = 'selected="selected"'; }else{$hsl ='';}
							?>
							<option <?php echo $hsl;?>  value="<?php echo site_url().'/wp-admin/admin.php?page=homepage&hotel_id='. $hotel['id']; ?>"><?php echo $hotel['name']; ?></option>
							<?php
							}
							?>
							</select>
							</td>
							<!-- <td align="left"><button id="addconcierge" onclick="change_hotel()">Add/Edit</button></td> -->
						</tr>
						<tr>
							<td align="left">Select Submenus</td>
							<td align="left">
							<select id="pages_select" name="hotels" onchange="change_hotel(this)">
							<option value="">Select sub menu</option>
							<?php 
							$current_nav=$_GET['submenu_id']; 
							foreach($navs as $nav){
								$selected = $current_nav == $nav['nid'] ? 'selected' : ''; 
								
							?>
							<option <?php echo $selected; ?> value="<?php echo site_url().'/wp-admin/admin.php?page=homepage&hotel_id='. $nav[id].'&submenu_id='.$nav[nid]; ?>"><?php echo $nav['nav_name']; ?></option>
							<?php
							}
							?>
							</select>
							</td>
							<!-- <td align="left"><button id="addconcierge" onclick="change_hotel()">Add/Edit</button></td> -->
						</tr>

					</table>
<?php }

?>
