var wp_cal_dates = new Array();
var wp_cal_posts = new Array();
var wp_cal_link = '';

function WP_Cal_open_link(lnk) {
    document.location.href = lnk;
}

function WP_Cal_convertMonth(month) {
    var m = new Array(7);
    m["January"] = 1;
    m["February"] = 2;
    m["March"] = 3;
    m["April"] = 4;
    m["May"] = 5;
    m["June"] = 6;
    m["July"] = 7;
    m["August"] = 8;
    m["September"] = 9;
    m["October"] = 10;
    m["November"] = 11;
    m["December"] = 12;
    return m[month];
}

jQuery(document).ready(function(){
    jQuery('#wp-calendar').datepicker({

dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
onSelect: function(date, el) {

 if($("td.WP-Cal-popup:contains('"+el.selectedDay+"')").length > 0) { 

	var n=date.split("/");
	var dayname = $.datepicker.formatDate('DD', $(this).datepicker('getDate'));
	page.calendernav = true;page.loaded=true; page.tempType='calender';

		page.calender={
				action: "concierge_page",						
				tempType: 'calender',
				slug: 'concierge-calender',
				month:n[0],
				day:n[1],
				year:n[2],
				dn:dayname,
				clicked: true
		}
		
	$.address.value('dailycalender?dn='+dayname+'&mo='+n[0]+'&d='+n[1]+'&y='+n[2]);

	}
  }

});
    
   function WP_Cal_get_posts_by_month( month, year){ 
       var check_date = month+'-'+year;
       var found_at = jQuery.inArray( check_date , wp_cal_dates);
		

       if( found_at == -1 ){
           wp_cal_dates.push(check_date);
           jQuery.ajax({
                type    :   'POST',
                url     :   wpCalendarObj.ajaxurl,
                data    :   {
                    action          :   'wp_calendar_get_posts',
                    ajax            :   'true',
                    post            :   wpCalendarObj.wpCalPostname,
                    future          :   wpCalendarObj.future,
                    author          :   wpCalendarObj.author,
                    comment_count   :   wpCalendarObj.comment_count,
                    taxonomy        :   wpCalendarObj.taxonomy,
                    term            :   wpCalendarObj.term,
                    month           :   month,
                    year            :   year
                },
                success :   function( data ){
                    data = jQuery.parseJSON( data );
                    var posts = data.posts;
                    var this_dates = data.classes; 
                    var curr_date = new Array();
                    var d = '';
                    jQuery.each( this_dates, function (key , value){
                        curr_date = key.split('-');
                        d = curr_date[2];
                        var element = jQuery('#wp-calendar a.ui-state-default:contains("'+d+'")');
                        if( element.length > 1 ){
                            jQuery(element).each( function(){
                                if( jQuery( this ).text() == d ){
                                    jQuery( this ).parent('td').addClass('WP-Cal-popup');
                                }
                            });
                            
                        }
                        else{
                            jQuery( element ).parent('td').addClass('WP-Cal-popup');
                        }
                    });
                    /* jQuery('#wp-calendar .WP-Cal-popup').each( function (){
                        var year = jQuery(this).attr('data-year');
                        var month = jQuery(this).attr('data-month');
                        month = parseInt( month ) + 1;
                        var day = jQuery(this).find('a.ui-state-default').text();
                        var this_date = year + '-' + month + '-' + day;
                        var innerContent = posts[this_date];
                        jQuery(this).append( '<div class="wp-cal-tooltip">'+innerContent+'</div>' );
                    }); */
                    var new_data_element = {month : check_date, classes : this_dates, posts : posts};
                    wp_cal_posts.push( new_data_element );
                }
            });
       }
       else{
            var this_dates = {};
            var this_posts = {};
            
            jQuery( wp_cal_posts ).each( function (){
                if( this.month == check_date ){
                    this_dates = this.classes;
                    this_posts = this.posts;
                    var curr_date = new Array();
                    var d = '';
                    jQuery.each( this_dates, function (key , value){
                        curr_date = key.split('-');
                        d = curr_date[2];
                        var element = jQuery('#wp-calendar a.ui-state-default:contains("'+d+'")');
                        if( element.length > 1 ){
                            jQuery(element).each( function(){
                                if( jQuery( this ).text() == d ){
                                    jQuery( this ).parent('td').addClass('WP-Cal-popup');
                                }
                            });

                        }
                        else{
                            jQuery( element ).parent('td').addClass('WP-Cal-popup');
                        }
                    });
                  /*  jQuery('#wp-calendar .WP-Cal-popup').each( function (){
                        var year = jQuery(this).attr('data-year');
                        var month = jQuery(this).attr('data-month');
                        month = parseInt( month ) + 1;
                        var day = jQuery(this).find('a.ui-state-default').text();
                        var this_date = year + '-' + month + '-' + day;
                        var innerContent = this_posts[this_date];
                        jQuery(this).append( '<div class="wp-cal-tooltip">'+innerContent+'</div>' );
                    }); */
                }
            });
       }
   }

   jQuery('.ui-datepicker-next, .ui-datepicker-prev, .wp-cal-prev, .wp-cal-next').live('click', function (){ 
       var Month = jQuery('#wp-calendar .ui-datepicker-month').text();
       var Year = jQuery('#wp-calendar .ui-datepicker-year').text();
       Month = WP_Cal_convertMonth(Month);
       WP_Cal_get_posts_by_month( Month, Year );
   });
   var c_date = new Date(), c_month = (parseInt(c_date.getMonth()) + 1), c_year = c_date.getFullYear();
   
   WP_Cal_get_posts_by_month(c_month, c_year);

   jQuery('.WP-Cal-popup').on({
      mouseenter: function() {
            jQuery(this).find('div').show();
      },
      mouseleave: function() {
            jQuery(this).find('div').hide();
      }
   });
});
