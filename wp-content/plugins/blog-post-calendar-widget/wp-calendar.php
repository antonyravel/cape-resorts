<?php
/*
 * Plugin Name: Blog Post Calendar Widget
 * Plugin URI: http://presshive.com
 * Author: <a href="http://presshive.com/">Presshive</a>
 * Version: 1.0
 * Description: The Blog Posts Calendar Widget allows you to display your archived or future posts in a widget. 
 * Tags: posts, calendar, widget, post types, future posts, events calendar
 * License: GPLv2 or later
 */

/*  Copyright 2013  

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
define('WP_CALANDER_PLUGIN_URL', WP_PLUGIN_URL . '/' . dirname(plugin_basename(__FILE__)));

if( !session_id() ){
    session_start();
}

/**
 * function register and enqueue scripts in front end
 */
function wp_calendar_enqueue_scripts() {
    wp_enqueue_script('jquery');
    wp_register_script('wp_calendar_datepicker', WP_CALANDER_PLUGIN_URL . '/js/jquery.ui.datepicker.js' );
    wp_register_script('wp_calendar_js', WP_CALANDER_PLUGIN_URL . '/js/wp_calendar.js', array('jquery') );
    wp_register_style('wp_calendar_css', WP_CALANDER_PLUGIN_URL . '/css/wp_calendar.css');

    wp_enqueue_script('wp_calendar_datepicker');
    wp_enqueue_style('wp_calendar_css');
}

add_action('wp_enqueue_scripts', 'wp_calendar_enqueue_scripts');

/**
 * function registers and enqueue scripts in admin end
 */
function wp_calendar_enqueue_admin_scripts(){
     wp_register_script('wp_calander_admin_js', WP_CALANDER_PLUGIN_URL . '/js/wp_calendar_admin.js' , array('jquery'));
     wp_enqueue_script('wp_calander_admin_js');
     wp_localize_script('wp_calander_admin_js', 'wpCalancerAdminObj', array('ajaxurl' => admin_url('admin-ajax.php')));
}

add_action('admin_enqueue_scripts', 'wp_calendar_enqueue_admin_scripts');

/**
 * WP_Calander_Widget class for WP CALENDAR widget
 */
class WP_Calander_Widget extends WP_Widget {
    function WP_Calander_Widget() {
            $widget_ops = array( 'classname' => 'wp_calander', 'description' => __('A calendar widget to show posts.') );
            $control_ops = array( 'width' => 300, 'height' => 350);
            $this->WP_Widget( '', __('Blog Post Calendar', 'calendar'), $widget_ops, $control_ops );
    }

    function widget( $args, $instance ) {
            global $post;
            extract( $args );

            $title = isset( $instance['title'] ) ? $instance['title']  : '';
            $title = apply_filters('widget_title', $title );
            $catname = isset( $instance['catname'] ) ? $instance['catname'] : '';
            $future = isset( $instance['future'] ) ? $instance['future'] : '';
            $selected_taxonomy = isset( $instance['taxonomy'] ) ? $instance['taxonomy'] : '';
            $selected_term = isset( $instance['term'] ) ? $instance['term'] : '';
            $show_author = isset( $instance['show_author'] ) ? $instance['show_author'] : '';
            $show_comment_count = isset( $instance['show_comment_count'] ) ? $instance['show_comment_count'] : '';

            wp_enqueue_script('wp_calendar_js');
            wp_localize_script('wp_calendar_js', 'wpCalendarObj', array(
                                                    'ajaxurl'       => admin_url('admin-ajax.php'),
                                                    'plugin_url'    => WP_CALANDER_PLUGIN_URL,
                                                    'wpCalPostname' => $catname,
                                                    'author'        => $show_author,
                                                    'comment_count' => $show_comment_count,
                                                    'future'        => $future,
                                                    'taxonomy'      => $selected_taxonomy,
                                                    'term'          => $selected_term
                                            ));
            $time = get_the_time("j n Y", $post->ID);
            $archive_time = intval(get_query_var('day')).' '.intval(get_query_var('monthnum')).' '.intval(get_query_var('year'));

            if ( $title ){
              echo $before_title . $title . $after_title;
            }
            echo $before_widget;
            ?>
            <div class="widget widget_calendar">
                <div class="widget_inner">
                    <div id="calendar_wrap">
                        <div id="wp-calendar"></div>
                        <div class="calendar-pagi">
                          <ul>
                            <li class="wp-cal-prev"><a onclick="jQuery.datepicker._adjustDate('#wp-calendar', -1, 'M');">< Prev Month</a></li>
                            <li class="wp-cal-next"><a onclick="jQuery.datepicker._adjustDate('#wp-calendar', +1, 'M');">Next Month ></a></li>
                          </ul>
                        </div>
                    </div>
                </div>
            </div>
        <?php echo $after_widget;
    }

    function update( $new_instance, $old_instance ) {
            $instance = $old_instance;
            $instance['title'] = strip_tags( $new_instance['title'] );
            $instance['catname'] = strip_tags( $new_instance['catname'] );
            $instance['future'] = strip_tags( $new_instance['future'] );
            $instance['taxonomy'] = strip_tags( $new_instance['taxonomy'] );
            $instance['term'] = strip_tags( $new_instance['term'] );
            $instance['show_author'] = strip_tags( $new_instance['show_author'] );
            $instance['show_comment_count'] = strip_tags( $new_instance['show_comment_count'] );

            return $instance;
    }

    function form( $instance ) {
            $defaults = array( 'title' => '', 'catname' => 'post' , 'future' => 'checked', 'taxonomy' =>'all', 'term' => 'all', 'show_author' => 'checked', 'show_comment_count'=>'checked');
            $instance = wp_parse_args( (array) $instance, $defaults );
            $taxonomies = array();
            $terms_str = '';
            $selected_taxonomy = isset( $instance['taxonomy'] ) ? $instance['taxonomy'] : '';
            $selected_term = isset( $instance['term'] ) ? $instance['term'] : '';
            ?>
            <p>
                    <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
                    <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php if( $instance['title'] ) echo $instance['title']; ?>" class="widefat" />
            </p>

            <p>
                    <label for="<?php echo $this->get_field_id( 'catname' ); ?>" >Select Post type:</label>
                    <select class="widefat" onchange="wp_calander_check_taxonomy('<?php echo $this->get_field_id( 'cal_p' ); ?>', '<?php echo $this->get_field_name('catname'); ?>')" id="<?php echo $this->get_field_id( 'catname' ); ?>" name="<?php echo $this->get_field_name('catname'); ?>">
                        <?php
                        $post_types = get_post_types();
                        foreach ($post_types as $post_type ) {
                            $selected = '';
                            if( $post_type == $instance['catname'] ){
                                $selected = 'selected="selected"';
                            }
                            echo '<option '.$selected.' value="'.$post_type.'">'. $post_type. '</option>';
                        }
                        ?>
                    </select>
            </p>
            <?php
            $taxo = get_object_taxonomies( $instance['catname'], 'objects');
            $style = 'style="Display:none"';
            if( $taxo ){
                $style = '';
            }
            ?>
            <div id="<?php echo $this->get_field_id( 'cal_p' ); ?>" <?php echo $style; ?>>
                <table>
                    <?php

                    echo '<tr>
                            <td>
                                <label for="'.$this->get_field_id( 'taxonomy' ).'" >Select a category</label>
                            </td>
                            <td>
                                <select onchange="wp_calander_check_terms(\''.$this->get_field_id( 'cal_p' ).'\', \''.$this->get_field_name( 'taxonomy' ).'\')"  id="'.$this->get_field_id( 'taxonomy' ).'" name="'.$this->get_field_name( 'taxonomy' ).'" rel="taxonomy">';
                    $selected = '';
                    if( $selected_taxonomy == 'all' ){
                        $selected = 'selected="selected"';
                    }
                    echo '<option '.$selected.' value="all" >All</option>';
                    if( !empty ($instance['catname']) ){

                        foreach( $taxo as $key => $val ){
                            $selected = '';
                            if( $selected_taxonomy == $val->name ){
                                $selected = 'selected="selected"';
                            }
                            echo '<option '.$selected.' value="'.$val->name.'" >'.$val->name.'</option>';
                        }
                    }
                    echo '      </select>
                                </td>
                            </tr>';

                    echo '<tr '.$style.'><td><label for="'.$this->get_field_id( 'term' ).'" >Select a term</label></td>
                            <td><select  id="'.$this->get_field_id( 'term' ).'" name="'.$this->get_field_name( 'term' ).'" rel="term">';
                    $selected = '';
                    if( $selected_term == 'all' ){
                        $selected = 'selected="selected"';
                    }
                    echo '<option '.$selected.' value="all" >All</option>';
                    if( !empty ($selected_term) ){
                        $terms = get_terms($selected_taxonomy, array('hide_empty'=>false));
                        foreach( $terms as $key => $val ){
                            $selected = '';
                            if( $selected_term == $val->term_id ){
                                $selected = 'selected="selected"';
                            }
                            echo '<option '.$selected.' value="'.$val->term_id.'" >'.$val->name.'</option>';
                        }
                    }
                    echo '</select></td></tr>';
                    ?>
                    </table>
            </div>


            <table>
                <tr>
                    <td><label for="<?php echo $this->get_field_id( 'show_author' ); ?>">Show Author:</label></td>
                    <td>
                        <?php
                    $checked = '';
                    if( $instance['show_author'] == 'checked' ){
                        $checked = 'checked="checked"';
                    }
                    ?>
                    <input type="checkbox" id="<?php echo $this->get_field_id( 'show_author' ); ?>" <?php echo $checked; ?> name="<?php echo $this->get_field_name( 'show_author' ); ?>" value="checked" class="widefat" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="<?php echo $this->get_field_id( 'show_comment_count' ); ?>">Show Comment Count:</label>
                    </td>
                    <td>
                        <?php
                        $checked = '';
                        if( $instance['show_comment_count'] == 'checked' ){
                            $checked = 'checked="checked"';
                        }
                        ?>
                        <input type="checkbox" id="<?php echo $this->get_field_id( 'show_comment_count' ); ?>" <?php echo $checked; ?> name="<?php echo $this->get_field_name( 'show_comment_count' ); ?>" value="checked" class="widefat" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="<?php echo $this->get_field_id( 'future' ); ?>">Show future posts:</label>
                    </td>
                    <td>
                        <?php
                        $checked = '';
                        if( $instance['future'] == 'checked' ){
                            $checked = 'checked="checked"';
                        }
                        ?>
                        <input type="checkbox" id="<?php echo $this->get_field_id( 'future' ); ?>" <?php echo $checked; ?> name="<?php echo $this->get_field_name( 'future' ); ?>" value="checked" class="widefat" />
                    </td>
                </tr>
            </table>
    <?php
    }
}

/**
 * Action to register widget
 */
add_action( 'widgets_init', create_function( '', 'register_widget( "WP_Calander_Widget" );' ) );

/**
 * function to return taxonomies included with post type (for ajax request)
 */
function wp_calender_get_taxonomy(){
    $post_type = $_REQUEST['post_type'];
    $taxonomies = array();
    if( $post_type ){
        $taxo = get_object_taxonomies( $post_type, 'objects');
        if( $taxo ){
            $taxonomies['all'] = 'ALL';
            foreach( $taxo as $key => $val ){
                $taxonomies[$val->name] = $val->name;
            }
            die(json_encode( $taxonomies ));
        }
        else{
            die(json_encode( 'false' ));
        }
    }
}

add_action('wp_ajax_wp_calender_get_taxonomy', 'wp_calender_get_taxonomy');

/**
 * function to return terms of a taxonomy(for ajax request)
 */
function wp_calender_get_terms (){
    $taxonomy = $_REQUEST['taxonomy'];
    $terms_r = array();
    if( $taxonomy ){
        $terms = get_terms($taxonomy, array('hide_empty'=>false));
        $terms_r['all'] = 'ALL';
        foreach( $terms as $key => $val ){
            $terms_r[$val->term_id] = $val->name;
        }
        die(json_encode( $terms_r ));
    }
    else{
        die('false');
    }
}

add_action('wp_ajax_wp_calender_get_terms', 'wp_calender_get_terms');

/**
 * function to return post data (in ajax request)
 */
function wp_calendar_get_posts(){

    $wp_cal_content = array();

$weekdays_name = array('Sunday','Monday','Tuesday','Wednesday', 'Thursday', 'Friday', 'Saturday');

$monthweek_name = array('1' => 'the first %s of the month','2' => 'the second %s of the month', '3' => 'the third %s of the month', '4' => 'the fourth %s of the month', '-1' => 'the last %s of the month');


global $wpdb;

$post_id = $_REQUEST['post'];


   	$ajax = $_POST['ajax'];
    $posttype = $_POST['post'];
    $future = $_POST['future'];
    $author = $_POST['author'];
    $show_comment_count = $_POST['comment_count'];
    $selected_month = $_POST['month'];
    $selected_year = $_POST['year'];
    $classes = array();
	if(strlen($selected_month)==1) {
		$selected_month = '0'.$selected_month;
	}
	$year_string = $selected_year.'-'.$selected_month;

$events = $wpdb->get_results("select * FROM cr_em_events where event_status=1 AND multiple=1 AND multiple_dates LIKE '%{$year_string}%'");

        if(count($events)){ 
             foreach($events as $event) {

		$multiple_dates = ($event->multiple_dates != '' ) ? explode ( ",", $event->multiple_dates ) : array(); 

			foreach($multiple_dates as $date) {
			

				if(preg_match('/'.$year_string.'/',$date)) {
    		//echo $date;
	 			$da = date("Y-n-j", strtotime($date));
				 $classes[$da]++;
				}
				}
		}
        }


$first = date('Y-m-d', mktime(0, 0, 0, $selected_month, 1,  $selected_year));
$last = date('Y-m-t', mktime(0, 0, 0, $selected_month, 1,  $selected_year));

$events = $wpdb->get_results("select * FROM cr_em_events where event_status=1 AND multiple=0 AND (STR_TO_DATE( `event_end_date`, '%Y-%m-%d%' ) >= STR_TO_DATE('$first', '%Y-%m-%d%')) ORDER BY event_start_date");
	
		$matching_days = array(); $post_ids=array();
		foreach ($events as $event) {

		$start_date = strtotime($event->event_start_date);

		$end_date = strtotime($event->event_end_date);
				
		$last_date = strtotime($last);
		$first_date = strtotime($first);
		//if($start_date < $first_date ) {
			//$start_date = $first_date;
		//}
		//echo date('Y-m-d', $start_date);
		if($end_date > $last_date) {
			$end_date = $last_date;
		}
 
		$aDay = 86400;  // a day in seconds
		$aWeek = $aDay * 7;	
	if(!$event->recurrence_interval || $event->recurrence_interval ==0) {

$event->recurrence_interval=1;
			}

	switch ( $event->recurrence_freq ){
		
		case 'monthly':

$current_arr = getdate($start_date);
	
			$end_arr = getdate($end_date);

				//$end_month_date = strtotime( date('Y-m-t', $start_date) ); //End date on last day of month
$end_month_date = strtotime( date('Y-m-t', $end_date) );
				$current_date = strtotime( date('Y-m-1', $end_date) ); //Start date on first day of month
				
			//go through the events and put them into a monthly array
			$day = $event->recurrence_byday;
				

				while( $current_date <= $end_month_date ){ 
					$last_day_of_month = date('t', $current_date);
					//Now find which day we're talking about
					$current_week_day = date('w',$current_date);
					$matching_month_days = array();
					//Loop through days of this years month and save matching days to temp array
					for($day = 1; $day <= $last_day_of_month; $day++){
						if((int) $current_week_day == $event->recurrence_byday){
							$matching_month_days[] = $day;
							
						}
						$current_week_day = ($current_week_day < 6) ? $current_week_day+1 : 0;							
					}
					//Now grab from the array the x day of the month
					$matching_day = ($event->recurrence_byweekno > 0) ? $matching_month_days[$event->recurrence_byweekno-1] : array_pop($matching_month_days);
					$matching_date = strtotime(date('Y-m',$current_date).'-'.$matching_day);
					if($matching_date >= $start_date && $matching_date <= $end_date){

$month = date("m",$matching_date); 
						
							if((int)$month == (int)$selected_month)
								$matching_days[] = $matching_date;
					
					}
					//add the number of days in this month to make start of next month
					$current_arr['mon'] += 1;
					if($current_arr['mon'] > 12){
						//FIXME this won't work if interval is more than 12
						$current_arr['mon'] = $current_arr['mon'] - 12;
						$current_arr['year']++;
					}
					$current_date = strtotime("{$current_arr['year']}-{$current_arr['mon']}-1"); 
				}
						
			break;
		case 'weekly':
//echo  $event->recurrence_days;
	$weekdays = explode("_", $event->recurrence_days);
if(is_null($weekdays[0])) { 
$weekdays[0] = $event->recurrence_days;
}
//print_r($weekdays);
$post_ids[]=$event->post_id;
//sort out week one, get starting days and then days that match time span of event (i.e. remove past events in week 1)
				$start_of_week = get_option('start_of_week'); //Start of week depends on WordPress
				//first, get the start of this week as timestamp

				$event_start_day = date('w', $start_date);
				//then get the timestamps of weekdays during this first week, regardless if within event range
				$start_weekday_dates = array(); //Days in week 1 where there would events, regardless of event date range


				for($i = 0; $i < 7; $i++){
					$weekday_date = $start_date+($aDay*$i); //the date of the weekday we're currently checking
					$weekday_day = date('w',$weekday_date); //the day of the week we're checking, taking into account wp start of week setting
					
					if( in_array( $weekday_day, $weekdays) ){
							$start_weekday_dates[] = $weekday_date; //it's in our starting week day, so add it

					}
				}					
				//for each day of eventful days in week 1, add 7 days * weekly intervals
				foreach ($start_weekday_dates as $weekday_date){

				//echo $event->recurrence_interval .'<br />';
				
					//Loop weeks by interval until we reach or surpass end date
					while($weekday_date <= $end_date){

						if( $weekday_date >= $start_date && $weekday_date <= $end_date ){
						$month = date("m",$weekday_date); //echo (int)$month; 
						//echo (int)$selected_month;
							if((int)$month == (int)$selected_month)
								$matching_days[] = $weekday_date;
						}
						$weekday_date = $weekday_date + ($aWeek *  $event->recurrence_interval);
					}
				}//done!
			

				
			break;

		

case 'daily':
				//If daily, it's simple. Get start date, add interval timestamps to that and create matching day for each interval until end date.
				$current_date = $start_date;
$event->recurrence_interval=1;
				while( $current_date <= $end_date ){
			
$month = date("m",$current_date); //echo $month; echo $selected_month;
							if((int)$month == (int)$selected_month) {

								$matching_days[] = $current_date;

}
						
					//$matching_days[] = $current_date; 
					$current_date = $current_date + ($aDay * $event->recurrence_interval);
				}
				break;


		}

}


foreach ($matching_days as $matching_day) {

$da = date("Y-n-j", $matching_day);
				 $classes[$da]++;


}

 $wp_cal_content['classes'] = $classes;

    die( json_encode($wp_cal_content) );
}

add_action('wp_ajax_wp_calendar_get_posts', 'wp_calendar_get_posts');
add_action('wp_ajax_nopriv_wp_calendar_get_posts', 'wp_calendar_get_posts');

/**
 * function returns month name by month number
 *
 * @return string
 */
function WP_Cal_convertMonth($month) {
    $m = array("January","February", "March","April","May","June","July","August","September","October","November" ,"December");

    foreach( $m as $key => $value ){
        if( ($key+1) == $month ){
            return $value;
        }
    }
}
?>
