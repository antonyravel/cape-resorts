<?php
/*
Plugin Name: Meta Box
Plugin URI: http://wp.tutsplus.com/
Description: Adds an example meta box to wordpress.
Version: None
Author: Christopher Davis
Author URI: http://wp.tutsplus.com/
License: Public Domain
*/

add_action( 'add_meta_boxes', 'cd_meta_box_add' );


function cd_meta_box_add()
{

	$post_types = array('activities', 'seasonalevents', 'entertainment', 'stories', 'specialoffers');

foreach( $post_types as $type ) {
		add_meta_box( 'my-meta-box-id', 'Recurrence', 'cd_meta_box_cb', $type, 'normal', 'high' );
}
	
}

function cd_meta_box_cb( $post )
{
	$values = get_post_custom( $post->ID );


$event_details = get_events_details();



 $days_names = meta_get_days_names();
$hours_format = meta_get_hour_format();

	//$text = isset( $values['my_meta_box_text'] ) ? esc_attr( $values['my_meta_box_text'][0] ) : '';
	//$selected = isset( $values['my_meta_box_select'] ) ? esc_attr( $values['my_meta_box_select'][0] ) : '';
	//$check = isset( $values['my_meta_box_check'] ) ? esc_attr( $values['my_meta_box_check'][0] ) : '';
	wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );
	?>
	<div id="em-form-recurrence" class="event-form-recurrence event-form-when">
<div id="recurring-event">
	<?php echo 'This event repeats'; ?> 
		<select id="recurrence-frequency" name="recurrence_freq">
			<?php
				$freq_options = array ("daily" => 'Daily', "weekly" =>'Weekly',"monthly" => 'Monthly' );
				meta_option_items ( $freq_options, $event_details->recurrence_freq); 
			?>
		</select>
<?php if($event_details->recurrence_interval ==1 || $event_details->recurrence_interval =="") { ?>
		<span class='interval-desc' id="interval-weekly-plural">
		<?php echo 'every'; ?>
		<input id="recurrence-interval" name='recurrence_interval' size='2' value="<?php echo ($event_details->recurrence_interval == '' || $event_details->recurrence_interval==0 ) ? 1 : $event_details->recurrence_interval; ?>" />



		<?php echo 'week on'; ?></span> <?php } else { ?>
		
		<span class='interval-desc' id="interval-weekly-plural">

		<?php echo 'every'; ?>
		<input id="recurrence-interval" name='recurrence_interval' size='2' value="<?php echo ($event_details->recurrence_interval == '' || $event_details->recurrence_interval==0 ) ? 1 : $event_details->recurrence_interval; ?>" />


<?php echo 'weeks on'; ?></span> <?php } ?>

	<p class="alternate-selector" id="weekly-selector">
		<?php


			$saved_bydays = ($event_details->recurrence_days != '' ) ? explode ( "_", $event_details->recurrence_days ) : array(); 
			meta_checkbox_items ( 'recurrence_bydays[]', $days_names, $saved_bydays ); 
		?>
	</p>


	<p class="alternate-selector" id="monthly-selector" style="display:inline;">
		<select id="monthly-modifier" name="recurrence_byweekno">
			<?php
				$weekno_options = array ("1" => 'first', '2' =>'second', '3' =>'third', '4' =>'fourth', '-1' => __ ( 'last', 'dbem' ) ); 
				meta_option_items ( $weekno_options, $event_details->recurrence_byweekno ); 
			?>
		</select>
		<select id="recurrence-weekday" name="recurrence_byday">
			<?php meta_option_items ( $days_names, $event_details->recurrence_byday ); ?>
		</select>
		<?php echo 'of each month'; ?>
		&nbsp;
	</p>
	<div class="event-form-recurrence-when">
		<p class="em-date-range">
			<?php echo 'Recurrences span from '; ?>					
			<input class="em-date-start em-date-input-loc" type="text" />
			<input class="em-date-input" type="hidden" name="event_start_date" value="<?php echo $event_details->event_start_date; ?>" />
			<?php echo 'to'; ?>
			<input class="em-date-end em-date-input-loc" type="text" />
			<input class="em-date-input" type="hidden" name="event_end_date" value="<?php echo $event_details->event_end_date; ?>" />
		<span style="color:red;"> *Required</span></p>
		<!-- <p class="em-time-range">
			<?php echo 'Events start from'; ?>
			<input id="start-time" class="em-time-input em-time-start" type="text" size="8" maxlength="8" name="event_start_time" value="<?php ?>" />
			<?php 'to'; ?>
			<input id="end-time" class="em-time-input em-time-end" type="text" size="8" maxlength="8" name="event_end_time" value="<?php //echo date( $hours_format, $EM_Event->end ); ?>" />
			<?php echo 'All day'; ?> <input type="checkbox" class="em-time-allday" name="event_all_day" id="em-time-all-day" value="1" <?php //if(!empty($EM_Event->event_all_day)) echo 'checked="checked"'; ?> />
		</p> -->
		<!-- <p class="em-duration-range">
			<?php //echo printf(__('Each event spans %s day(s)','dbem'), '<input id="end-days" type="text" size="8" maxlength="8" name="recurrence_days" value="'. $EM_Event->recurrence_days .'" />'); ?>
		</p> -->
	</div> 
</div>
		<p class="em-date"><?php echo 'Multiple Dates'; ?>&nbsp; &nbsp; &nbsp;<input type="checkbox" class="em-time-allday" name="event_date" id="event_dates" value="1" <?php if($event_details->multiple==1) echo 'checked="checked"'; ?>/>
		</p>

<div id="multiple_dates" style="display:none;">
<a href="noJs.html" class="date-pick">Click here to select Multiple Dates</a>
<input type="hidden" id="em-date-multiple" name="multiple_dates" value="<?php echo $event_details->multiple_dates; ?>"/>

</div>

</div>
	<?php	
}


function meta_option_items($array, $saved_value) {
	$output = "";
	foreach($array as $key => $item) {
		$selected ='';
		if ($key == $saved_value)
			$selected = "selected='selected'";
		$output .= "<option value='".esc_attr($key)."' $selected >".esc_html($item)."</option>\n";

	}
	echo $output;
}



function meta_checkbox_items($name, $array, $saved_values, $horizontal = true) {
	$output = "";
	foreach($array as $key => $item) {
		$checked = "";
		if (in_array($key, $saved_values))
			$checked = "checked='checked'";
		$output .=  "<input type='checkbox' name='".esc_attr($name)."' value='".esc_attr($key)."' $checked /> ".esc_html($item)."&nbsp; ";
		if(!$horizontal)
			$output .= "<br/>\n";
	}
	echo $output;

}

function meta_get_days_names(){
	return array (1 => __ ( 'Mon' ), 2 => __ ( 'Tue' ), 3 => __ ( 'Wed' ), 4 => __ ( 'Thu' ), 5 => __ ( 'Fri' ), 6 => __ ( 'Sat' ), 0 => __ ( 'Sun' ) );
}

function meta_get_hour_format(){
	return get_option('dbem_time_24h') ? "H:i":"h:i A";
}

function get_events_details() {
global $wpdb;

$post_id = $_REQUEST['post'];

$fields = $wpdb->get_row("select * FROM cr_em_events WHERE post_id=$post_id");

return $fields;

}


add_action( 'save_post', 'cd_meta_box_save' );
function cd_meta_box_save( $post_id )
{
	global $wpdb;
	// if our nonce isn't there, or we can't verify it, bail
	if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'my_meta_box_nonce' ) ) return;
	
	// if our current user can't edit this post, bail
	if( !current_user_can( 'edit_post' ) ) return;

$recur_freq= $_POST['recurrence_freq'];
$recurrence_interval= $_POST['recurrence_interval'];

$recurrence_bydays= json_encode($_POST['recurrence_bydays']);

$recurrence_bydays = str_replace('[','',$recurrence_bydays);
	$recurrence_bydays= str_replace(']','',$recurrence_bydays);
$recurrence_bydays= str_replace('"','',$recurrence_bydays);
$recurrence_bydays= str_replace(',','_',$recurrence_bydays);


$recurrence_byweekno= $_POST['recurrence_byweekno'];
$recurrence_byday= $_POST['recurrence_byday'];

$event_start_date= $_POST['event_start_date'];
$event_end_date= $_POST['event_end_date'];
$event_date= $_POST['event_date'];
$multiple_dates= $_POST['multiple_dates'];


$row = $wpdb->get_row("select * FROM cr_em_events WHERE post_id=$post_id", ARRAY_A);



$post = get_post($post_id); 
if($post->post_status = 'publish') {
	$event_status = 1;
}else{
	$event_status = 0;
}


if($row['event_id'] !="") {

 $sql = "UPDATE cr_em_events SET event_status = '$event_status'";

if($event_start_date) 

	 $sql .= ", event_start_date = '$event_start_date'";
if($event_end_date) 

	 $sql .= ", event_end_date = '$event_end_date'";

if($recurrence_interval) 

	 $sql .= ", recurrence_interval = '$recurrence_interval'";

if($recur_freq) 

	 $sql .= ", recurrence_freq = '$recur_freq'";

if($recurrence_byday || $recurrence_byday==0) 

	 $sql .= ",recurrence_byday = '$recurrence_byday'";

if($recurrence_byweekno) 

	 $sql .= ", recurrence_byweekno = '$recurrence_byweekno'";

if($recurrence_bydays || $recurrence_bydays=='0') 

	 $sql .= ",  recurrence_days = '$recurrence_bydays'";

if($event_date) 

	 $sql .= ",  multiple = '$event_date'";
else
	 $sql .= ",  multiple = '0'";

if($multiple_dates) 

	 $sql .= ",  multiple_dates = '$multiple_dates'";


$wpdb->query($sql. " WHERE post_id = $post_id");


		//$wpdb->update( 'cr_em_events', array( 'event_status' => $event_status, 'event_start_date' => $event_start_date,'event_end_date' => $event_end_date,'recurrence_interval' => $recurrence_interval,'recurrence_freq' => $recur_freq,'recurrence_byday' => $recurrence_byday,'recurrence_byweekno' => "$recurrence_byweekno",'recurrence_days' =>  $recurrence_bydays,'multiple' => $event_date,'multiple_dates' => $multiple_dates), array( "post_id"=>$post_id));



} else {


$wpdb->query("INSERT INTO cr_em_events ( post_id, event_status, event_start_date, event_end_date, recurrence_interval, recurrence_freq, recurrence_byday, recurrence_byweekno, recurrence_days, multiple, multiple_dates ) VALUES ( $post_id, $event_status, '$event_start_date', '$event_end_date', '$recurrence_interval', '$recur_freq', '$recurrence_byday', '$recurrence_byweekno', '$recurrence_bydays', '$event_date', '$multiple_dates')");
}

}

add_action( 'delete_post', 'my_delete_function' );
function my_delete_function($postId) { 
   global $wpdb;
   $wpdb->query("DELETE FROM cr_em_events WHERE post_id=".$postId);
}
?>
